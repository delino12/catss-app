<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use CATSS\Account;
use CATSS\User;

class Client extends Model
{
    /*
    |-----------------------------------------
    | Get brokers
    |-----------------------------------------
    */
    public function getMyClients($user_id, $firm_id){
    	// body
    	$clients = User::where([["brokerage_id", $firm_id], ['account_type', 'client']])->get();
    	if(count($clients) > 0){
    		$client_box = [];
    		foreach ($clients as $cl) {
    			$client_account = Account::where("user_id", $cl->id)->first();
    			$data = [
    				'id' 				=> $cl->id,
    				'client_id' 		=> $cl->id,
    				'client_name' 		=> $cl->name,
    				'client_email' 		=> $cl->email,
    				'client_account_id'	=> $cl->account_id,
    				'client_balance' 	=> number_format($client_account->account_balance, 2)
    			];

    			array_push($client_box, $data);
    		}
    	}else{
    		$client_box = [];
    	}

    	// return
    	return $client_box;
    }

    /*
    |-----------------------------------------
    | Hire a broker
    |-----------------------------------------
    */
    public function hireBroker($client_id, $broker_id){

    	$already_hired = Client::where([['client_id', $client_id], ['broker_id', $broker_id]])->first();
    	if($already_hired !== null){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> 'Agent already hired!'
    		];
    	}else{

    		// body
	    	$add_client 			= new Client();
	    	$add_client->broker_id 	= $broker_id;
	    	$add_client->client_id 	= $client_id;
	    	$add_client->status 	= "active";
	    	if($add_client->save()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> 'Hiring deal successful!'
	    		];
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Failed to complete process'
	    		];
	    	}
    	}

    	// return 
    	return $data;
    }
}
