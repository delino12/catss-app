<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Commission extends Model
{
    /*
    |-----------------------------------------
    | SAVE NEW COMMISSION
    |-----------------------------------------
    */
    public function saveBuyerCommission($user_id, $percentange_charge, $trade_qty, $commission_charge){
    	// body
        $commission = new Commission();
    	$commission->user_id = $user_id;
        $commission->percent = $percentange_charge;
        $commission->qty     = $trade_qty;
        $commission->type    = 'buy';
        $commission->amount  = $commission_charge;
        $commission->save();
    }

    /*
    |-----------------------------------------
    | SAVE NEW COMMISSION
    |-----------------------------------------
    */
    public function saveSellerCommission($user_id, $percentange_charge, $trade_qty, $commission_charge){
    	// body
        $commission = new Commission();
    	$commission->user_id = $user_id;
        $commission->percent = $percentange_charge;
        $commission->qty     = $trade_qty;
        $commission->type    = 'sell';
        $commission->amount  = $commission_charge;
        $commission->save();
    }
}
