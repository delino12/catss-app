<?php

namespace CATSS\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class PriceAlert implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    protected $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('set-price-alert');
    }

    public function broadcastWith()
    {
        return [
            'id'        => $this->data['id'],
            'user_id'   => $this->data['user_id'],
            'type'      => $this->data['type'],
            'security'  => $this->data['security'],
            'price'     => $this->data['price'],
            'date'      => $this->data['date']
        ];
    }
}
