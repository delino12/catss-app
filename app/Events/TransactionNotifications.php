<?php

namespace CATSS\Events;

use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class TransactionNotifications implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Create a new event instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        // get the transactions update
        $this->data = $data;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new Channel('transaction-updates');
    }

    public function broadcastWith()
    {
        return [
            'user_id'      => $this->data['user_id'],
            'stock_name'   => $this->data['stock_name'],
            'stock_unit'   => $this->data['stock_unit'],
            'stock_qty'    => $this->data['stock_qty'],
            'stock_trade'  => $this->data['stock_trade'],
            'stock_amount' => $this->data['stock_amount'],
            'stock_date'   => $this->data['stock_date']
        ];
    }
}
