<?php

namespace CATSS\Console\Commands;

use Illuminate\Console\Command;

class ComposerInstall extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'composer:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Composer install updates and packages remotely...';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        // execute command
        try {
            exec('composer dump-autoload');
            exec('composer install');
        } catch (Exception $e) {
            echo 'Message: ' .$e->getMessage();
        }
    }
}
