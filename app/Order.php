<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use CATSS\Events\PlaceOrder;
use CATSS\Events\NewOrderUpdate;
use CATSS\Security;
use Carbon\Carbon;
use CATSS\Nano;
use Auth;

class Order extends Model
{
    /*
    |-----------------------------------------------------------------
    | PLACE ONE-CLICK ORDER
    |-----------------------------------------------------------------
    */
    public function placeBidOrder($user_id, $sec_id, $qty, $price){
    	// body
    	// $qty        = str_replace(",", "", $qty);
        // $price      = str_replace(",", "", $price);
        
        // scan order ref_id
        $ref_id     = "CATSS-".rand(000,999).rand(000,999).rand(000,999);

    	$place_order 			= new Order();
        $place_order->broker_id = Auth::user()->id;
        $place_order->firm_id   = Auth::user()->brokerage_id;
    	$place_order->sec_id 	= $sec_id;
    	$place_order->user_id 	= $user_id;
        $place_order->ref_id    = $ref_id;
    	$place_order->price 	= $price;
    	$place_order->qty 		= $qty;
    	$place_order->type 		= 'buy';
    	$place_order->option 	= 'limit';
    	$place_order->save();

        // sec information
        $asset = Security::where('id', $sec_id)->first();
        // get last order details
        $last_order = Order::where('ref_id', $ref_id)->first();
        $user_order_data = array(
            'id'        => $last_order->id,
            'sec_id'    => $last_order->sec_id,
            'asset'     => $asset->security,
            'user_id'   => $user_id,
            'ref_id'    => $ref_id,
            'price'     => number_format($price, 2),
            'qty'       => number_format($qty),
            'type'      => $last_order->type,
            'option'    => $last_order->option,
            'date'      => $last_order->created_at->diffForHumans() 
        );

        $trade_order_data = array(
            'sec_id'    => $sec_id,
            'asset'     => $asset->security,
            'price'     => number_format($price, 2),
            'qty'       => number_format($qty),
            'type'      => $last_order->type,
            'date'      => $last_order->created_at->toTimeString()
        );

        // fire events
        \Event::fire(new NewOrderUpdate($trade_order_data));
        \Event::fire(new PlaceOrder($user_order_data));

    	$data = [
    		'status' 	=> 'success',
    		'message' 	=> 'Order has been placed successfully!'
    	];

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------------------------------
    | PLACE ONE-CLICK ORDER
    |-----------------------------------------------------------------
    */
    public function placeAskOrder($user_id, $sec_id, $qty, $price){
        // body
        // $qty        = str_replace(",", "", $qty);
        // $price      = str_replace(",", "", $price);
        
        // scan order ref_id
        $ref_id     = "CATSS-".rand(000,999).rand(000,999).rand(000,999);

        $place_order            = new Order();
        $place_order->sec_id    = $sec_id;
        $place_order->broker_id = Auth::user()->id;
        $place_order->firm_id   = Auth::user()->brokerage_id;
        $place_order->user_id   = $user_id;
        $place_order->ref_id    = $ref_id;
        $place_order->price     = $price;
        $place_order->qty       = $qty;
        $place_order->type      = 'sell';
        $place_order->option    = 'limit';
        $place_order->save();

        // sec information
        $asset = Security::where('id', $sec_id)->first();
        // get last order details
        $last_order = Order::where('ref_id', $ref_id)->first();
        $user_order_data = array(
            'id'        => $last_order->id,
            'sec_id'    => $last_order->sec_id,
            'asset'     => $asset->security,
            'user_id'   => $user_id,
            'ref_id'    => $ref_id,
            'price'     => number_format($price, 2),
            'qty'       => number_format($qty),
            'type'      => $last_order->type,
            'option'    => $last_order->option,
            'date'      => $last_order->created_at->diffForHumans() 
        );

        $trade_order_data = array(
            'sec_id'    => $sec_id,
            'asset'     => $asset->security,
            'price'     => number_format($price, 2),
            'qty'       => number_format($qty),
            'type'      => $last_order->type,
            'date'      => $last_order->created_at->toTimeString()
        );

        // fire events
        \Event::fire(new NewOrderUpdate($trade_order_data));
        \Event::fire(new PlaceOrder($user_order_data));

        $data = [
            'status'    => 'success',
            'message'   => 'Order has been placed successfully!'
        ];

        // return
        return $data;
    }

    /*
    |-----------------------------------------------------------------
    | LOAD GLOBAL ORDERS ALL EQUITIES
    |-----------------------------------------------------------------
    */
    public function getGlobalOrders(){
        // body
        $orders = Order::orderBy('id', 'DESC')->limit('10')->get();
        if(count($orders) > 0){
            $orders_box = [];
            foreach ($orders as $ol) {
                $security = Security::where("id", $ol->sec_id)->first();
                $data = [
                    'id'        => $ol->id,
                    'asset'     => $security->security,
                    'price'     => number_format($ol->price, 2),
                    'qty'       => number_format($ol->qty),
                    'option'    => $ol->option,
                    'type'      => $ol->type,
                    'date'      => $ol->created_at->toTimeString()
                ];

                array_push($orders_box, $data);
            }
        }else{
            $orders_box = [];
        }

        // return
        return $orders_box;
    }
}
