<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class Market extends Model
{
    public function fetchMarketPrices()
    {
    	// base url
    	$base_url = "https://comercio.timsmate.com/api/market-prices";

    	// fetch client
    	$client = new Client();
    	$fetch 	= $client->request('GET', $base_url);

    	// get response
    	$response = $fetch->getBody();

    	// return response
    	return json_decode($response);
    }
}
