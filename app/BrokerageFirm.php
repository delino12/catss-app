<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class BrokerageFirm extends Model
{
    /*
    |-----------------------------------------
    | ADD NEW BROKERAGE FIRM
    |-----------------------------------------
    */
    public function addNew($payload){
    	$already_exist = BrokerageFirm::where("broker_firm", $payload->broker_firm)->first();
    	if($already_exist !== null){
    		$data = [
    			'status' 	=> 'error',
    			'message' 	=> $payload->broker_firm.' already created!'
    		];
    	}else{
    		// body
	    	$brokerage 				= new BrokerageFirm();
	    	$brokerage->broker_firm = $payload->broker_firm;
	    	if($brokerage->save()){
	    		$data = [
	    			'status' 	=> 'success',
	    			'message' 	=> $payload->broker_firm.' has been created successfully!'
	    		];
	    	}else{
	    		$data = [
	    			'status' 	=> 'error',
	    			'message' 	=> 'Error, could not create '.$payload->broker_firm
	    		];
	    	}
	    }

    	// return 
    	return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL BROKERAGE FIRM
    |-----------------------------------------
    */
    public function getAllBrokerageFirm(){
    	// body
    	$all_add_firms = BrokerageFirm::all();
    	$all_box = [];
    	foreach ($all_add_firms as $firm) {

    		$total_broker = User::where([["account_type", "broker"], ["brokerage_id", $firm->id]])->count();
    		$total_client = User::where([["account_type", "client"], ["brokerage_id", $firm->id]])->count();

    		$data = [
    			"id" 			=> $firm->id,
    			"firm_name" 	=> $firm->broker_firm,
    			"total_broker" 	=> $total_broker,
    			"total_client" 	=> $total_client,
    			"created_at" 	=> $firm->created_at,
    			"updated_at" 	=> $firm->updated_at,
    		];

    		array_push($all_box, $data);
    	}

    	// return update
    	return $all_box;
    }

    /*
    |-----------------------------------------
    | GET LIST BROKERAGE FIRM
    |-----------------------------------------
    */
    public function listAddedFirms(){
        // body
        $all_add_firms = BrokerageFirm::all();
        $all_box = [];
        foreach ($all_add_firms as $firm) {
            $data = [
                "id"            => $firm->id,
                "firm_name"     => $firm->broker_firm
            ];

            array_push($all_box, $data);
        }

        // return update
        return $all_box;
    }
}
