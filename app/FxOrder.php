<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use DB;

class FxOrder extends Model
{
    /*
    |-----------------------------------------
    | place order
    |-----------------------------------------
    */
    public function placeOrder($request){
		
		// body
		$currency 	= $request->security;
		$bid_qty 	= $request->bidQty;
		$ask_qty 	= $request->askQty;
		$ask_price 	= $request->askPrice;
		$bid_price 	= $request->bidPrice;

		// order_ref
    	$order_ref 	= 'CATSS'.rand(000,555).rand(000,111).rand(000,999);
    	$order_date = Carbon::now();
		
		$this->order_ref	= $order_ref;    	
		$this->currency 	= $currency;
		$this->bid_qty 		= $bid_qty;
		$this->ask_qty 		= $ask_qty;
		$this->bid  		= $bid_price;
		$this->ask 			= $ask_price;
		$this->orderDate 	= $order_date;

		// save new order
		if($this->save() == true){
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Order place successfully !'
			];
		}else{
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'failed to place order !'
			];
		}

		// return response
		return $data;
    }

    /*
    |-----------------------------------------
    | load all orders
    |-----------------------------------------
    */
    public function loadOrders(){
    	// body
    	$all_orders = FxOrder::orderBy('id', 'desc')->limit('6')->get();
    	return $all_orders;
    }
}
