<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Transfer extends Model
{
    /*
    |-----------------------------------------
    | SAVE NEW TRANSFER
    |-----------------------------------------
    */
    public function saveNewTransfer($user_id, $amount){
    	// body
    	$this->user_id 	= $user_id;
    	$this->amount 	= $amount;
    	$this->status 	= false;
    	$this->save();
    }

    /*
    |-----------------------------------------
    | GET ALL WITHDRAWS
    |-----------------------------------------
    */
    public function fetchAll($user_id){
    	// body
    	return Transfer::where("user_id", $user_id)->get();
    }

    /*
    |-----------------------------------------
    | FETCH BANKS
    |-----------------------------------------
    */
    public function fetchBanks(){
    	// body
        $endpoint   = 'https://api.paystack.co/bank';
        $headers    = array('Content-Type: application/json', 'Authorization: Bearer '.env("PS_SK_KEY"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);

        // return response()->json($res); 
        $data = json_decode($res, true);

        // return
        return $data;

        curl_close($ch);
    }
}
