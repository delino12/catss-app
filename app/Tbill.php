<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use CATSS\Account;
use DB;

class Tbill extends Model
{
    /*
	|--------------------------------
	| handle Tbills when purchasing
	|--------------------------------
	|
	*/
	public function buyTbill($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option){

		
		$this->user_id 	= $user_id;
		$this->ref_id   = $refid;
		$this->discount	= $price;
		$this->qty 		= $qty;
		$this->security = $asset;
		$this->yield 	= $yield;
		$this->maturity = $maturity;
		$this->amount 	= $price * $qty;
		$this->TransactionTypeID = 1;
		$this->tradeDate = Carbon::now(); 
		
		// save stock purchase
		if($this->save()){

			// debit account
			$this->debitAccount($user_id, $this->amount);

			$data = [
				'status' => 'success',
				'message'=> '<b>'.$asset.'</b> Trade was successful !'
			];
		}else{
			$data = [
				'status' => 'error',
				'message'=> '<b>'.$asset.'</b> Trade was not successful !'
			];
		}
		

		// return callback
		return $data;
	}


	/*
	|--------------------------------
	| handle Tbills when selling
	|--------------------------------
	|
	*/
	public function sellTbill($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option){


		// validate bond balance
		// $qty_balance = $this->validateTbillBalanceSell($user_id, $maturity);
		$qty_balance = $this->fetchTbillsQtyBalance($user_id, $maturity);

		if($qty_balance >= $qty){

			$this->user_id 	= $user_id;
			$this->ref_id   = $refid;
			$this->discount	= $price;
			$this->qty 		= $qty;
			$this->security = $asset;
			$this->yield 	= $yield;
			$this->maturity = $maturity;
			$this->amount 	= $price * $qty;
			$this->TransactionTypeID = 2;
			$this->tradeDate = Carbon::now(); 
			
			// save stock transaction
			if($this->save()){

				// credit user account - debit tbills account
				$this->creditAccount($user_id, $this->amount);
				$data = [
					'status' 	=> 'success',
					'message'	=> '<b>'.$asset.'</b> Trade was successful !',
				];

			}else{
				$data = [
					'status' 	=> 'error',
					'message'	=> '<b>'.$asset.'</b> Trade was not successful !'
				];
			}
		}else{
			$data = [
				'status' 	=> 'error',
				'message'	=> '<b>'.$asset.'</b> Insufficient balance !',
				'balance' 	=> 'You have <i class="fa fa-database"></i> '.number_format($qty_balance)
			];
		}
		// return callback
		return $data;
	}

	/*
	|--------------------------------
	| handle load user Tbills
	|--------------------------------
	|
	*/
	public function loadUserTbill($user_id){
		$tbills = Tbill::where('user_id', $user_id)->orderBy('id', 'desc')->get();
		if(count($tbills) > 0){
			$tbill_box = [];
			foreach ($tbills as $bill) {
				# code...
				$data = [
					'id'				=> $bill->id,
					'user_id'			=> $bill->user_id,
					'security'			=> $bill->security,
					'TransactionTypeID'	=> $bill->TransactionTypeID,
					'price'				=> number_format($bill->discount, 2),
					'qty'				=> number_format($bill->qty),
					'yield'				=> $bill->yield,
					'amount'			=> $bill->amount,
					'maturity'			=> $bill->maturity,
					'tradeDate'			=> $bill->tradeDate
				];

				array_push($tbill_box, $data);
			}
		}else{
			$tbill_box = [];
		}

		return $tbill_box;
	}


	/*
	|--------------------------------
	| handle check on user TbillsTbill
	|--------------------------------
	|
	*/
	public function getTbillDetails($user_id, $ref_id){
		$tbills = Tbill::where([['user_id', $user_id], ['']])->orderBy('id', 'desc')->get();
		if(count($tbills) > 0){
			$tbill_box = [];
			foreach ($tbills as $bill) {
				# code...
				$data = [
					'id'				=> $bill->id,
					'user_id'			=> $bill->user_id,
					'security'			=> $bill->security,
					'TransactionTypeID'	=> $bill->TransactionTypeID,
					'price'				=> number_format($bill->price, 2),
					'qty'				=> number_format($bill->qty),
					'yield'				=> $bill->yield,
					'amount'			=> $bill->amount,
					'maturity'			=> $bill->maturity,
					'tradeDate'			=> $bill->tradeDate
				];

				array_push($tbill_box, $data);
			}
		}else{
			$tbill_box = [];
		}

		return $tbill_box;
	}


	/*
	|--------------------------------
	| handle load all Tbills
	|--------------------------------
	|
	*/
	public function loadAllTbill(){
		$tbills = Tbill::orderBy('id', 'desc')->get();
		if(count($tbills) > 0){
			$tbill_box = [];
			foreach ($tbills as $bill) {
				# code...
				$data = [
					'id'				=> $bill->id,
					'user_id'			=> $bill->user_id,
					'security'			=> $bill->security,
					'TransactionTypeID'	=> $bill->TransactionTypeID,
					'price'				=> number_format($bill->discount, 2),
					'qty'				=> $bill->qty,
					'yield'				=> $bill->yield,
					'amount'			=> $bill->amount,
					'maturity'			=> $bill->maturity,
					'tradeDate'			=> $bill->tradeDate
				];

				array_push($tbill_box, $data);
			}
		}else{
			$tbill_box = [];
		}

		return $tbill_box;
	}


	/*
    |--------------------------------------------------------------------------
    | SERVICES FOR HANDLING TRADES PURCHASE REQUEST
    |--------------------------------------------------------------------------
    |
    | Debit Cash account
    |
    */
    // debit account
    public function debitAccount($id, $amount){
        # code...
        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance - $amount;
        $account->update();

        return true;

    }

    /*
    |--------------------------------------------------------------------------
    | SERVICES FOR HANDLING TRADES SALES REQUEST
    |--------------------------------------------------------------------------
    |
    | Credit Cash account
    |
    */
    // credit user bank account
    public function creditAccount($id, $amount)
    {

        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance + $amount;
        $account->update();

        return true;
    }


    /*
    |--------------------------------
    | Debit Tbills Stock balance
    |--------------------------------
    |
    */
    public function debitTbillsAccount($user_id, $maturity, $qty){
    	$check_qty_balance = Tbill::where([
    		['user_id', $user_id], 
    		['maturity', $maturity], 
    		['TransactionTypeID', "1"]
    	])->get();

    	if(count($check_qty_balance) > 0){
    		// qty balance 
    		$qty_box = [];
    		$balance = 0;
    		foreach ($check_qty_balance as $tbill) {
    			// case for multiple qty
    			// check account with large qty

				// find qty to fit balance
				$scan_tbills = Tbill::find($tbill->id);
				
				if($scan_tbills->qty >= $qty){

					// update balance drawn
					$scan_tbills->qty = $scan_tbills->qty - $qty;
					$scan_tbills->update();

					// break out work done
					break;

				}elseif($scan_tbills->qty < $qty && $scan_tbills->qty !== 0){
					// get balance differences
					$scanned_bal = $qty - $scan_tbills->qty;

					$balance = $scanned_bal;

					// update balance drawn
					$scan_tbills->qty = $scan_tbills->qty - $scanned_bal;
					$scan_tbills->update();
				}
    		}
    	}
    }


    /*
    |--------------------------------
    | 	Handle bond balance validation
    |--------------------------------
    |
    */
    public function fetchTbillsQtyBalance($user_id, $maturity){

    	$check_qty_balance = DB::select(" EXEC procTBillsQtyBalance $user_id, '$maturity' ");
    	if(count($check_qty_balance) > 0){
			$qty_data = collect($check_qty_balance);
			$qty = $qty_data[0]->qty;
			return $qty;
    	}else{
    		$qty = 0;
    		return $qty;
    	}
    }


    /*
    |--------------------------------
    | Fetch user summary for Tbills
    |--------------------------------
    |
    */
    public function fetchTbillSummary($user_id){

    	$fetch_summary = DB::select(" EXEC procTbillsUserSummary $user_id ");
    	return $fetch_summary;
    }
}
