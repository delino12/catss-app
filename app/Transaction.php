<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /*
    |-----------------------------------------
    | save transactions
    |-----------------------------------------
    */
    public function saveBuyerTransaction($user_id, $pair_name, $buy_price, $trade_qty, $amount, $percentange_charge, $commission_charge){
    	// body
    	$transaction = new Transaction();
    	$transaction->user_id      = $user_id;
        $transaction->stock_name   = $pair_name;
        $transaction->stock_unit   = $buy_price;
        $transaction->stock_qty    = $trade_qty;
        $transaction->stock_trade  = 'buy';
        $transaction->stock_amount = $amount;
        $transaction->stock_timing = time();
        $transaction->com_percent  = $percentange_charge;
        $transaction->com_amount   = $commission_charge;
        $transaction->save();
    }

    /*
    |-----------------------------------------
    | save transactions
    |-----------------------------------------
    */
    public function saveSellerTransaction($user_id, $pair_name, $buy_price, $trade_qty, $amount, $percentange_charge, $commission_charge){
    	// body
    	$transaction = new Transaction();
    	$transaction->user_id      = $user_id;
        $transaction->stock_name   = $pair_name;
        $transaction->stock_unit   = $buy_price;
        $transaction->stock_qty    = $trade_qty;
        $transaction->stock_trade  = 'sell';
        $transaction->stock_amount = $amount;
        $transaction->stock_timing = time();
        $transaction->com_percent  = $percentange_charge;
        $transaction->com_amount   = $commission_charge;
        $transaction->save();
    }

    /*
    |-----------------------------------------
    | get all transactions
    |-----------------------------------------
    */
    public function getAllTransactions($user_id){
        // body
        $transactions = Transaction::where("user_id", $user_id)->get();
        return $transactions;
    }
}
