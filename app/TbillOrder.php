<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Auth;
use DB;

class TbillOrder extends Model
{
    /*
    |--------------------------------
    | load all orders
    |--------------------------------
    |
    */
    public function laodAllOrders(){

    	
    	// $tbills = TbillOrder::orderBy('id', 'desc')->get();
		$tbills = DB::select(' EXEC fetchTBillsOrders ');

		$tbills = collect($tbills);
		if(count($tbills) > 0){
			$order_box = [];
			foreach ($tbills as $order) {
				# code...
				$data = [
					'security' 	=> $order->security,
					'best_ask'	=> $order->ask_price,
					'best_bid'	=> $order->bid_price,
					'ask_qty' 	=> $order->ask_qty,
					'bid_qty' 	=> $order->bid_qty
				];

				array_push($order_box, $data);
			}
		}else{
			$order_box = [];
		}

		return array_reverse($order_box);
    }


    /*
    |-----------------------------------------
    | Place order
    |-----------------------------------------
    */
    public function placeOrder($request){
    	// body

    	$user_id 	= Auth::user()->id; 
    	$security 	= $request->security; 
    	$ask_price 	= $request->askPrice; 
    	$ask_qty 	= $request->askQty; 
    	$bid_price 	= $request->bidPrice; 
    	$bid_qty 	= $request->bidQty;
    	
    	// order_ref
    	$order_ref 	= 'CATSS'.rand(000,555).rand(000,111).rand(000,999);
    	$order_date = Carbon::now();

    	$new_order 				= new TbillOrder();
		$new_order->user_id 	= $user_id;
		$new_order->order_ref 	= $order_ref;
		$new_order->security 	= $security;
		$new_order->bid_qty 	= $bid_qty;
		$new_order->ask_qty 	= $ask_qty;
		$new_order->bid 		= $bid_price;
		$new_order->ask 		= $ask_price;
		$new_order->orderDate 	= $order_date;
		
		// save new order
		if($new_order->save() == true){
			$data = [
				'status' 	=> 'success',
				'message' 	=> 'Order place successfully !'
			];
		}else{
			$data = [
				'status' 	=> 'error',
				'message' 	=> 'failed to place order !'
			];
		}

		// return response
		return $data;
	}	
}
