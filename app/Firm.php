<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Firm extends Model
{
    /*
    |-----------------------------------------
    | GET ALL FIRM
    |-----------------------------------------
    */
    public function getAllFirm(){
    	// body
    	$all_firms = Firm::all();
    	if(count($all_firms) > 0){
    		$filtered_box = [];
    		foreach ($all_firms as $firm) {
    			$brokerage_firms = json_decode($firm->brokage_firm, true);
    			foreach ($brokerage_firms["feed"]["entry"] as $lists) {
    				$firm_name = $lists["content"]["properties"]["Firm_x0020_Name"]["__text"];
    				$firm_address = $lists["content"]["properties"]["Head_x0020_Office_x0020_Address"]["__text"];
    				
    				if(empty($lists["content"]["properties"]["Email"]["__text"])){
						$firm_email = "";
    				}else{
    					$firm_email = $lists["content"]["properties"]["Email"]["__text"];
    				}

					$data = [
						'firm_name' 	=> $firm_name,
						'firm_address' 	=> $firm_address,
						'firm_website' 	=> $firm_email
    				];

    				array_push($filtered_box, $data);
    			}
    		}
    	}else{
    		$filtered_box = [];
    	}

    	// return
    	return $filtered_box;
    }

    /*
    |-----------------------------------------
    | GET FIRMS NAMES
    |-----------------------------------------
    */
    public function getFirmNames(){
        // body
        $all_firms = Firm::all();
        if(count($all_firms) > 0){
            $filtered_box = [];
            foreach ($all_firms as $firm) {
                $brokerage_firms = json_decode($firm->brokage_firm, true);
                foreach ($brokerage_firms["feed"]["entry"] as $lists) {
                    $firm_name = $lists["content"]["properties"]["Firm_x0020_Name"]["__text"];
                    $firm_address = $lists["content"]["properties"]["Head_x0020_Office_x0020_Address"]["__text"];
                    
                    if(empty($lists["content"]["properties"]["Email"]["__text"])){
                        $firm_email = "";
                    }else{
                        $firm_email = $lists["content"]["properties"]["Email"]["__text"];
                    }

                    if(!empty($firm_name)){
                        $firm_email = "";
                        array_push($filtered_box, $firm_name);
                    }
                }
            }
        }else{
            $filtered_box = [];
        }

        // return filter
        return $filtered_box;
    }
}
