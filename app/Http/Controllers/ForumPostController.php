<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Events\NewForumPost;
use CATSS\Events\NewComment;
use CATSS\Comment;
use CATSS\User;
use CATSS\Basic;
use DB;

class ForumPostController extends Controller
{
    // create a new forum post
	public function createPost(Request $request)
	{
		$name  = $request->name;
		$email = $request->email;
		$post  = $request->post;

		// create new post
		$post = DB::table('forums')->insert([
			'name'       => $name,
			'email'      => $email,
			'title'      => $post,
			'vote'       => 0,
			'created_at' => NOW(),
			'updated_at' => NOW()
		]);
		if(!$post){
			$data = array(
				'status'  => 'error',
				'message' => 'Fail to send post !'
			);
			return response()->json($data);
		}else{
			// fetch last post
			$last_post = DB::table('forums')->orderBy('id', 'desc')->first();

			$data = array(
				'id'    => $last_post->id,
				'name'  => $last_post->name,
				'email' => $last_post->email,
				'title' => $last_post->title,
				'vote'  => $last_post->vote,
				'date'  =>  date('M D h:i a', strtotime($last_post->created_at))
			);

			\Event::fire(new NewForumPost($data));

			$data = array(
				'status'  => 'success',
				'message' => '1 new topic created !'
			);
			return response()->json($data);
		}
	}

	// comments 

	// load all forum topics
	public function loadPosts()
	{
		// load all 
		$all_posts = DB::table('forums')->orderBy('id', 'desc')->take('5')->get();

		// push json
		$post_box = [];
		foreach ($all_posts as $posts) {
			# code...
			// get no of comments
			$comments = Comment::where('post_id', $posts->id)->get();
			$no_of_com = count($comments);
			if($no_of_com > 0){
				$com_no = $no_of_com;
			}else{
				$com_no = "";
			}

			// get users id
			$users_info = User::where('email', $posts->email)->first();

			if($users_info == null){
				$avatar = null;
			}else{
				// get basic info and avatar
				$basic_info = Basic::where('user_id', $users_info->id)->first();
				$avatar = $basic_info->avatar;
			}

			// load forum post info
			$data = array(
				'id'         => $posts->id,
				'name'       => $posts->name,
				'email'      => $posts->email,
				'avatar'	 => $avatar,
				'title'      => $posts->title,
				'vote'       => $posts->vote,
				'no_comment' => $com_no,
				'date'       => date('M D h:i a', strtotime($posts->created_at))
			);

			array_push($post_box, $data);
		}

		// return post box response
		return response()->json($post_box);
	}

	// reply forum post
	public function comments($id)
	{
		# code...
		// load all 
		$all_posts = DB::table('forums')->where('id', $id)->first();
		$title = $all_posts->title;
		return view('external-pages.comments', compact('id', 'title'));
	}

	// load title posts
	public function loadCard($id)
	{
		// fetch title post
		$cards = DB::table('forums')->where('id', $id)->first();
		$data = array(
			'id'    => $cards->id,
			'name'  => $cards->name,
			'email' => $cards->email,
			'title' => $cards->title,
			'vote'  => $cards->vote,
			'date'  =>  date('M D h:i a', strtotime($cards->created_at))
		);

		return response()->json($data);
	}

	// post users comments 
	public function postComment(Request $request)
	{
		$id      = $request->id;
		$name    = $request->name;
		$email   = $request->email;
		$comment = $request->comment;


		// create new comment
		$com_new          = new Comment();
		$com_new->post_id = $id;
		$com_new->name    = $name;
		$com_new->email   = $email;
		$com_new->body    = $comment;
		$com_new->save();

		// fetch last post
		$last_comments = Comment::where([['name', $name], ['email', $email], ['body', $comment]])->first();
		$data = array(
			'id'    => $last_comments->id,
			'name'  => $last_comments->name,
			'email' => $last_comments->email,
			'body'  => $last_comments->body,
			'date'  =>  date('M D h:i a', strtotime($last_comments->created_at))
		);

		\Event::fire(new NewComment($data));

		$data = array(
			'status'  => 'success',
			'message' => 'comment sent !'
		);
		return response()->json($data);
	}


	// load comments for posts 
	public function loadComments($id)
	{
		// load all comments to posts
		$all_comments = Comment::where('post_id', $id)->get();

		$com_box = [];
		foreach ($all_comments as $comment) {
			# code...
			$data = array(
				'id'    => $comment->id,
				'name'  => $comment->name,
				'email' => $comment->email,
				'body'  => $comment->body,
				'date'  => $comment->created_at->diffForHumans()
			);

			array_push($com_box, $data);
		}

		return response()->json($com_box);
	}
}
