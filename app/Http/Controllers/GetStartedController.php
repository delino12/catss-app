<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\GetStarted;

class GetStartedController extends Controller
{
    /*
    |--------------------------------
    | Check for time access
    |--------------------------------
    |
    */
    public function checkFirstTimeAccess(Request $request){

    	// user request data
    	$user_id 	= $request->userid;
    	$view_page 	= $request->page;

    	$get_started = GetStarted::where([['user_id', $user_id], ['page', $view_page]])->first();
    	if($get_started == null){
    		// this is a first timer
    		$init_step 			= new GetStarted();
    		$init_step->page 	= $view_page;
    		$init_step->user_id = $user_id;
    		$init_step->status 	= 'unseen';
    		$init_step->save();

    		$last_updated = GetStarted::where([['user_id', $user_id], ['page', $view_page]])->latest()->first(); 

    		$data = [
    			'status' => $last_updated->status
    		];
    	
    	}else{

    		if($get_started->status == 'unseen'){

    			// update user information completed
	    		$updated_hints 			= GetStarted::find($get_started->id);
	    		$updated_hints->status 	= 'seen';
	    		$updated_hints->save();
    		}

    		$data = [
    			'status' 	=> 'success',
    			'message' 	=> 'Tutorials has been updated to seen !'
    		];
    	}

    	// return response 
    	return response()->json($data);
    }
}
