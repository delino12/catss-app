<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Events\PlaceOrder;
use CATSS\Events\TransactionNotifications;
use CATSS\Events\StockBalanceNotifications;
use CATSS\Events\AccountUpdate;
use CATSS\Events\NewOrderUpdate;
use CATSS\Mail\OrderNotify;
use CATSS\User;
use CATSS\Stock;
use CATSS\Order;
use CATSS\Security;
use CATSS\Account;
use CATSS\Transaction;
use CATSS\Statement;
use CATSS\TbillOrder;
use CATSS\Nano;
use Auth;


class OrderResponseController extends Controller
{
    /*
    |--------------------------------
    | LOAD ALL ORDERS
    |--------------------------------
    |
    */
    public function loadOrdersAll(Request $request)
    {
        $sec_id = $request->sec_id;
        # code...
        $orders = Order::where([['type', 'buy'], ['sec_id', $sec_id]])->orderBy('id', 'desc')->get();

        // check if users have any orders
        if(count($orders) > 0){
            // load users orders
            $order_box = [];
            foreach ($orders as $order) {

                // sec information
                $asset = Security::where('id', $sec_id)->first();

                # code...
                $data = array(
                    'id'        => $order->id,
                    'sec_id'    => $order->sec_id,
                    'asset'     => $asset->security,
                    'ref_id'    => $order->ref_id,
                    'price'     => number_format($order->price, 2),
                    'qty'       => number_format($order->qty),
                    'type'      => $order->type,
                    'option'    => $order->option,
                    'date'      => $order->created_at->diffForHumans()
                );

                array_push($order_box, $data);
            }
        }else{

            $order_box = [];
        }

        // return response data
        return response()->json($order_box);
    } 
    
    /*
    |--------------------------------
    | LOAD ORDERS SINGLE REQUEST
    |--------------------------------
    |
    */
    public function loadOrders($sec_id, $user_id)
    {
        # code...
        $orders = Order::where([['user_id', $user_id], ['sec_id', $sec_id]])->orderBy('id', 'desc')->get();

        // check if users have any orders
        if(count($orders) > 0){
            // load users orders
            $order_box = [];
            foreach ($orders as $order) {
                // sec information
                $asset = Security::where('id', $sec_id)->first();

                # code...
                $data = array(
                    'id'        => $order->id,
                    'sec_id'    => $order->sec_id,
                    'asset'     => $asset->security,
                    'user_id'   => $order->user_id,
                    'ref_id'    => $order->ref_id,
                    'price'     => number_format($order->price, 2),
                    'qty'       => number_format($order->qty),
                    'type'      => $order->type,
                    'option'    => $order->option,
                    'date'      => $order->created_at->diffForHumans()
                );

                array_push($order_box, $data);
            }

            // return response no orders
            $data = array(
                'status'    => 'success',
                'message'   => 'order has been loaded successfully!',
                'orders'    => $order_box
            );
        }else{

            // return response no orders
            $data = array(
                'status'  => 'success',
                'message' => 'You do not have any orders !',
                'orders'  => []
            );
        }

        // return response data
        return response()->json($data);
    }


    /*
    |--------------------------------
    | LOAD ASK ORDERS
    |--------------------------------
    |
    */
    public function askOrders($sec_id)
    {
        # code...
        $all_buy_orders = Order::where([['sec_id', $sec_id],['type', 'buy']])->get();
        if(count($all_buy_orders) > 0){
            $ask_box = [];
            foreach ($all_buy_orders as $order) {
    
                // sec information
                $asset = Security::where('id', $order->sec_id)->first();

                # code...
                $data = array(
                    'id'        => $order->id,
                    'sec_id'    => $order->sec_id,
                    'asset'     => $asset->security,
                    'user_id'   => $order->user_id,
                    'ref_id'    => $order->ref_id,
                    'price'     => number_format($order->price, 2),
                    'qty'       => number_format($order->qty),
                    'type'      => $order->type,
                    'option'    => $order->option,
                    'date'      => $order->created_at->diffForHumans()
                );

                array_push($ask_box, $data);
            }
        }else{
            $ask_box = [];
        }

        return response()->json($ask_box);
    }

    /*
    |--------------------------------
    | LOAD BID/OFFER ORDERS
    |--------------------------------
    |
    */
    public function offerOrders($sec_id)
    {
        # code...
        $all_sell_orders = Order::where([['sec_id', $sec_id],['type', 'sell']])->get();
        if(count($all_sell_orders) > 0){
            $offer_box = [];
            foreach ($all_sell_orders as $order) {

                // sec information
                $asset = Security::where('id', $order->sec_id)->first();

                # code...
                $data = array(
                    'id'        => $order->id,
                    'sec_id'    => $order->sec_id,
                    'asset'     => $asset->security,
                    'user_id'   => $order->user_id,
                    'ref_id'    => $order->ref_id,
                    'price'     => number_format($order->price, 2),
                    'qty'       => number_format($order->qty),
                    'type'      => $order->type,
                    'option'    => $order->option,
                    'date'      => $order->created_at->diffForHumans()
                );

                array_push($offer_box, $data);
            }
        }else{
            $offer_box = [];
        }

        return response()->json($offer_box);
    }



    /*
    |--------------------------------------------------------------------------
    | Trade ORDERS Section
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    |--------------------------------
    | PLACE ORDER TO BUY, SELL AND CANCEL
    |--------------------------------
    |
    */
    public function placeOrderBuy($sec_id, $user_id, Request $request)
    {
    	# code...
		// $id			= $request->id;
		$sec_id		= $request->sec_id;
		$user_id	= $request->user_id;
		$price		= $request->price;
		$qty		= $request->total;
		$type		= $request->type;    	
		$option		= $request->option;


        // filtered inputs
        $filtered   = $this->filterFields($price, $qty);
        if(!empty($filtered)){

            // return error response
            return response()->json($filtered);
        }

        $qty        = str_replace(",", "", $qty);
        $price      = str_replace(",", "", $price);
        
        // scan order ref_id
        $ref_id     = "CATSS-".rand(000,999).rand(000,999).rand(000,999);

    	$place_order 			= new Order();
    	$place_order->sec_id 	= $sec_id;
    	$place_order->user_id 	= $user_id;
        $place_order->ref_id    = $ref_id;
    	$place_order->price 	= $price;
    	$place_order->qty 		= $qty;
    	$place_order->type 		= $type;
    	$place_order->option 	= $option;
    	$place_order->save();


        // sec information
        $asset = Security::where('id', $sec_id)->first();

        // get last order details
        $last_order = Order::where('ref_id', $ref_id)->first();
        $last_order_data = array(
            'id'        => $last_order->id,
            'sec_id'    => $last_order->sec_id,
            'asset'     => $asset->security,
            'user_id'   => $last_order->user_id,
            'ref_id'    => $last_order->ref_id,
            'price'     => number_format($last_order->price, 2),
            'qty'       => number_format($last_order->qty),
            'type'      => $last_order->type,
            'option'    => $last_order->option,
            'date'      => $last_order->created_at->diffForHumans() 
        );

        // fire events
        \Event::fire(new PlaceOrder($last_order_data));

    	$data = array(
    		'status' 	=> 'success',
    		'message' 	=> 'order has been placed successfully !'
    	);

    	// return response
    	return response()->json($data);
    }

    // place orders SELL
    public function placeOrderSell($sec_id, $user_id, Request $request)
    {
        # code...
        // $id          = $request->id;
        $sec_id     = $request->sec_id;
        $user_id    = $request->user_id;
        $price      = $request->price;
        $qty        = $request->total;
        $type       = $request->type;       
        $option     = $request->option;

        // filtered inputs
        $filtered   = $this->filterFields($price, $qty);
        if(!empty($filtered)){

            // return error response
            return response()->json($filtered);
        }

        // scan order ref_id
        $ref_id     = "CATSS-".rand(000,999).rand(000,999).rand(000,999);

        // $price      = str_replace(",", "", $price);
        $qty        = str_replace(",", "", $qty);

        $place_order            = new Order();
        $place_order->sec_id    = $sec_id;
        $place_order->user_id   = $user_id;
        $place_order->ref_id    = $ref_id;
        $place_order->price     = $price;
        $place_order->qty       = $qty;
        $place_order->type      = $type;
        $place_order->option    = $option;
        $place_order->save();


        // sec information
        $asset = Security::where('id', $sec_id)->first();

        // get last order details
        $last_order = Order::where('ref_id', $ref_id)->first();
        $last_order_data = array(
            'id'        => $last_order->id,
            'sec_id'    => $last_order->sec_id,
            'asset'     => $asset->security,
            'user_id'   => $last_order->user_id,
            'ref_id'    => $last_order->ref_id,
            'price'     => number_format($last_order->price, 2),
            'qty'       => number_format($last_order->qty),
            'type'      => $last_order->type,
            'option'    => $last_order->option,
            'date'      => $last_order->created_at->diffForHumans() 
        );

        // fire events
        \Event::fire(new PlaceOrder($last_order_data));

        $data = array(
            'status'    => 'success',
            'message'   => 'order has been placed successfully !'
        );

        // return response
        return response()->json($data);
    }

    // cancel order
    public function cancelOrder(Request $request)
    {
        # code...
        $id = $request->id;

        $cancel_order = Order::find($id);
        $cancel_order->delete();

        $data = array(
            'status' => 'success',
            'message' => 'Order has been cancel successfully !'
        );

        // return response
        return response()->json($data);
    }



    /*
    |--------------------------------------------------------------------------
    | Asset Revaulation and Computation BUY SECTION
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    // value assets
    public function valueAsset(Request $request)
    {
        # code...
        $price      = $request->price;
        $user_id    = $request->user_id;

        $user_account = Account::where('user_id', $user_id)->first();
        if($user_account !== null){
            
            $balance = $user_account->account_balance;

            $expected_qty       = $balance / $price;
            $expected_amount    = $expected_qty * $price;

            $data = array(
                'status'    => 'success',
                'price'     => $price,
                'total'     => number_format($expected_qty),
                'amount'    => number_format($expected_amount, 2)
            );

            // return response
            return response()->json($data);
        }
    }

    // value assets by Percentage
    public function valueAssetPercent(Request $request)
    {
        // post data
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;
        $percent    = $request->percent;
        $price      = $request->price;
        $type       = $request->type;

        // compute asset worth on user balance
        $user_account = Account::where('user_id', $user_id)->first();
        $user_balance = $user_account->account_balance;

        // use security price
        $security = Security::where('id', $sec_id)->first();
        $price = $security->new_band;

        // get worth
        $worth = $this->computeAssetWorth($percent, $user_balance);

        $expected_qty       = $worth / $price;
        $expected_amount    = $expected_qty * $price;

        if($type == 'market'){
            $price = number_format($security->new_band, 2);
        }

        $data = array(
            'status'    => 'success',
            'price'     => $price,
            'total'     => number_format($expected_qty),
            'amount'    => number_format($expected_amount, 2)
        );

        // return response
        return response()->json($data);
    }

    // value assets by amount 
    public function valueAssetAmount(Request $request)
    {
        # code...
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;
        $amount     = $request->amount;
        $price      = $request->price;
        $type       = $request->type;

        // get security current mkt price
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){

            // trust in God, He will make a way
            if($type == 'market'){
                $price = number_format($security->new_band, 2);
            }elseif($price == ""){
                $price = number_format($security->new_band, 2);
            }else{
                $price = $request->price;
            }

            // return $price;

            $expected_qty       = $amount / $price;
            $expected_amount    = $expected_qty * $price;

            $data = array(
                'status'    => 'success',
                'price'     => $price,
                'total'     => number_format($expected_qty),
                'amount'    => number_format($expected_amount, 2)
            );

            // return response
            return response()->json($data);   
        }
    }

    // value assets by Quantity
    public function valueAssetQuantity(Request $request)
    {
        # code...
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;
        $unit       = $request->unit;
        $price      = $request->price;
        $type       = $request->type;
        

        // filtered unit
        $unit       = str_replace(",", "", $unit);

        // get security current mkt price
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){

            // check market price
            if($type == 'market'){
                $price = $security->new_band;
            }


            // trust in God, He will make a way
            // $price = $security->new_band;
            
            $expected_amount    = $unit * $price;
            $expected_qty       = $unit;

            $data = array(
                'status'    => 'success',
                'price'     => $price,
                'total'     => number_format($expected_qty),
                'amount'    => number_format($expected_amount, 2)
            );

            // return response
            return response()->json($data);   
        }
    }



    /*
    |--------------------------------------------------------------------------
    | Stock Revaulation and Computation SELL SECTION
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    // value assets
    public function valueAssetToSell(Request $request)
    {
        # code...
        $price      = $request->price;
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;

        // return $sec_id;
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){
            $stock = Stock::where('name', $security->security)->first();
            if($stock !== null){

                // evaluate stock with price
                $expected_amount    = $stock->qty * $price;
                $expected_qty       = $expected_amount / $price;

                $data = array(
                    'status'    => 'success',
                    'price'     => $price,
                    'total'     => number_format($expected_qty),
                    'amount'    => number_format($expected_amount, 2)
                );

            }else{
                $data = array(
                    'status'    => 'error',
                    'message'   => 'You do not have this stock !'
                );
            }

            // return response
            return response()->json($data);
        }
    }

    // value assets by Percentage
    public function valueAssetPercentToSell(Request $request)
    {
        // post data
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;
        $percent    = $request->percent;
        $price      = $request->price;

        // verify security
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){
            // get stock worth
            $stock = Stock::where('name', $security->security)->first();
            if($stock !== null){
                // get worth
                $worth = $this->computeAssetWorth($percent, $stock->qty);

                $expected_qty       = $worth;
                $expected_amount    = $expected_qty * $price;

                $data = array(
                    'status'    => 'success',
                    'price'     => $price,
                    'total'     => number_format($expected_qty),
                    'amount'    => number_format($expected_amount, 2)
                );
            }

            // return response
            return response()->json($data);
        }
    }

    // value assets by amount 
    public function valueAssetAmountToSell(Request $request)
    {
        # code...
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;
        $amount     = $request->amount;
        $type       = $request->type;

        // get security current mkt price
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){

            // get stock
            $stock = Stock::where('name', $security->security)->first();
            if($stock !== null){

                // evaluate stock with price
                $expected_amount    = $amount;
                $expected_qty       = $expected_amount / $security->new_band;

                $data = array(
                    'status'    => 'success',
                    'price'     => $security->new_band,
                    'total'     => number_format($expected_qty),
                    'amount'    => number_format($expected_amount, 2)
                );

            }else{
                $data = array(
                    'status'    => 'error',
                    'message'   => 'You do not have this stock !'
                ); 
            }

            // return response
            return response()->json($data);
        }
    }

    // value assets by Quantity
    public function valueAssetQuantityToSell(Request $request)
    {
        # code...
        $user_id    = $request->user_id;
        $sec_id     = $request->sec_id;
        $unit       = $request->unit;

        // get security current mkt price
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){

            // trust in God, He will make a way
            $price = $security->new_band;

            // check stock if user has the number 
            $stock = Stock::where('name', $security->security)->first();
            if($stock !== null){

                // check if quantity is less
                if($stock->qty >= $unit){

                    // 
                    $expected_amount    = $unit * $price;
                    $expected_qty       = $unit;

                    $data = array(
                        'status'    => 'success',
                        'price'     => $price,
                        'total'     => number_format($expected_qty),
                        'amount'    => number_format($expected_amount, 2)
                    );

                }else{

                    $data = array(
                        'status'    => 'error',
                        'message'   => 'you have a low stock balance !'
                    ); 
                }
                    
            }else{
                $data = array(
                    'status'    => 'error',
                    'message'   => 'You do not have this stock !'
                ); 
            }

            // return response
            return response()->json($data);   
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Compute and Load Assets information
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    // compute assets worth
    public function computeAssetWorth($percent, $user_balance)
    {
        if($percent == 25){
            # case 1 25% out of 100 Percent 
            $x = $percent / 100;
            $results = $user_balance * $x;

        }elseif($percent == 50){
            # case 2 50% out of 100 Percent 
            $x = $percent / 100;
            $results = $user_balance * $x;

        }elseif($percent == 75){
            # case 3 75% out of 100 Percent 
            $x = $percent / 100;
            $results = $user_balance * $x;

        }elseif($percent == 100){
            # case 4 100% out of 100 Percent 
            $x = $percent / 100;
            $results = $user_balance * $x;
        }

        return $results;
    }

    // load assets 
    public function loadAsset($sec_name)
    {
        # code...
        $asset = Security::where('security', $sec_name)->first();

        // random volume
        $volume = $asset->new_band * rand(000,999).rand(000, 888).rand(000, 999);
        $volume = $volume * 100;

        $data = array(
            'id'    => $asset->id,
            'high'  => number_format($asset->high_band, 2),
            'low'   => number_format($asset->low_band, 2),
            'new'   => number_format($asset->new_band, 2),
            'vol'   => number_format($volume),
            'date'  => $asset->updated_at->toTimeString()
        );

        // return response 
        return response()->json($data);
    }

    // load assets trade history
    public function loadAssetTradeHistory($sec_name)
    {
        # code...
        $assets = Transaction::where('stock_name', $sec_name)->orderBy('id', 'desc')->take('18')->get();
        if(count($assets) > 0){
            $assets_box = [];

            foreach ($assets as $asset) {
                # code...
                $data = array(

                    'id'    => $asset->id,
                    'name'  => $asset->stock_name,
                    'qty'   => number_format($asset->stock_qty),
                    'price' => number_format($asset->stock_unit, 2),
                    'type'  => $asset->stock_trade,
                    'date'  => $asset->created_at->toTimeString()
                );

                array_push($assets_box, $data);
            }
        }else{

            $assets_box = [];
        }

        // return response data
        return response()->json($assets_box);
    }

    


    /*
    |--------------------------------------------------------------------------
    | Trade At Market Price
    |--------------------------------------------------------------------------
    |
    | BUY: Debit Cash account and Credit Stock account
    | SELL: Credit Cash account and Debit Stock account
    |
    */
    // Trade at Market Price
    // Trade BUY
    public function handleBuyTrade($sec_id, $user_id, Request $request)
    {
        // return $request->all();
        $qty    = $request->total;
        $type   = "buy";

        // scan and convert qty
        $qty = str_replace(",", "", $qty);
        $qty = floatval($qty);

        # check security exits
        $security = Security::where("id", $sec_id)->first();
        if($security !== null){

            // get trade amount
            $price  = $security->new_band;
            $amount = $price * $qty;

            // validate user account balance
            $account = Account::where("user_id", $user_id)->first();
            if($account !== null){

                // validate user account balance
                if($account->account_balance >= $amount){

                    // process trade 
                    $this->creditStock($user_id, $sec_id, $qty, $price); // credit stock account
                    $this->debitAccount($account->id, $amount);  // debit user account
                    $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);  // log transaction

                    $data = array(
                        'status' => 'success',
                        'message' => 'Trade successful !'
                    );

                }else{

                    $data = array(
                        'status' => 'error',
                        'message' => 'insufficient balance !'
                    );
                }

                return response()->json($data);
            }
        }
    }

    // Trade SELL
    public function handleSellTrade($sec_id, $user_id, Request $request)
    {
        // return $request->all();
        $qty    = $request->total;
        $type   = "sell";

        // scan and convert qty
        $qty = str_replace(",", "", $qty);
        $qty = floatval($qty);

        # check security exits
        $security = Security::where("id", $sec_id)->first();
        if($security !== null){

            // get trade amount
            $price  = $security->new_band;
            $amount = $price * $qty;

            // validate user stock balance
            $stock = Stock::where("name", $security->security)->first();
            if($stock !== null){

                // validate user stock qty
                if($stock->qty >= $qty){

                    // get user account id 
                    $account = Account::where("user_id", $user_id)->first();

                    // process trade 
                    $this->debitStock($user_id, $sec_id, $qty, $price); // debit stock account
                    $this->creditAccount($account->id, $amount);  // credit user account
                    $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);  // log transaction

                    $data = array(
                        'status' => 'success',
                        'message' => 'Trade successful !'
                    );

                }else{

                    $data = array(
                        'status' => 'error',
                        'message' => 'insufficient balance !'
                    );
                }

                return response()->json($data);
            }
        }
    }



    /*
    |--------------------------------------------------------------------------
    | When purchasing 
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    // debit account
    public function debitAccount($id, $amount)
    {
        # code...
        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance - $amount;
        $account->update();
    }

    // credit user stocks
    public function creditStock($user_id, $sec_id, $qty, $price)
    {
        # code...
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){

            // first check if same stock already exits, if same stock exits update existing...
            $stocks = Stock::where([['name', $security->security], ['user_id', $user_id]])->first();

            // if pair is new
            if($stocks == null){
                // no stock exits so W/A is new
                $f1 = $qty * $price;
                $fresh_wa = $f1 / $qty;

                $amount =  $qty * $price;

                // else save new Stock
                $stocks            = new Stock();
                $stocks->user_id   = $user_id;
                $stocks->name      = $security->security;
                $stocks->qty       = $qty;
                $stocks->price     = $price;
                $stocks->amount    = $amount;
                $stocks->w_average = $fresh_wa;
                $stocks->save();
            }else{
                // now check if stock exist
                $existed_stock = Stock::find($stocks->id);

                // get the W/A Formular f1 + f2 / fQty
                // capture incoming stock qty*price 
                $f2 = $qty * $price;

                // get 
                $f1 = $existed_stock->qty * $existed_stock->w_average; 

                # w/average
                $f3 = $f1 + $f2; // compute net old qty * old price
                $f4 = $existed_stock->qty + $qty; // compute net qty
                $f5 = $f3/$f4;

                $existed_stock->price       = $price;
                $existed_stock->w_average   = $f5;
                $existed_stock->qty         = $f4;
                $existed_stock->amount      = $f3;
                $existed_stock->update();
            }
        }
    }



    /*
    |--------------------------------------------------------------------------
    | When selling
    |--------------------------------------------------------------------------
    |
    | Debit Stock account and Credit Cash account
    |
    */
    // credit user bank account
    public function creditAccount($id, $amount)
    {
        # code...
        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance + $amount;
        $account->update();

        // return account balance updates
        $account_balance = Account::where('user_id', $id)->first();
        $act_bal         = number_format($account_balance->account_balance, 2);

        // account data 
        $acct_data = array(
            'user_id' => $id,
            'bal'     => $act_bal
        );

        // Fire Account Balance
        \Event::fire(new AccountUpdate($acct_data));
    }

    // debit user stocks account
    public function debitStock($user_id, $sec_id, $qty, $price)
    {
        # code...
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){
            // first check if same stock already exits, if same stock exits update existing...
            $stock = Stock::where([['name', $security->security], ['user_id', $user_id]])->first();

            if($stock !== null){
                // now update if stock exist
                $stocks         = Stock::find($stock->id);
                $stocks->qty    = $stocks->qty - $qty; // keypiece when users sell-> qty update
                $stocks->update();

                // a little store cleaning..
                $stocks = Stock::where([['user_id', $user_id], ['qty', 0]])->first();
                if($stocks !== null){
                    $refresh_stocks = Stock::find($stocks->id);
                    $refresh_stocks->delete();
                }

                # code...
                $w_a = $stock->w_average; // w/a

                // remove negative return...
                if($w_a > $price){
                    
                    $gap = $w_a - $price; // trade gap on w/a
                
                }elseif($w_a < $price){
                    
                    $gap = $price - $w_a; // trade gap on w/a

                }elseif($w_a == $price) {
                    
                    $gap = $w_a - $price; // trade gap on w/a
                }


                $f1 = $w_a * $qty; // on weighted average (holding value)
                $f2 = $price * $qty; // on demand (market value)


                if($f1 > $f2){
                    
                    $comments   = " trade Successful with loss gap of ".$gap;
                    $status     = "loss";

                }elseif($f2 > $f1){
                    
                    $comments   = " trade Successful with profit gap of ".$gap;
                    $status     = "profit";

                }elseif($f2 == $f1){

                    $comments   = " trade Successful with no profit/loss with return gap of ".$gap;
                    $status     = "break-even";
                }

                $top_bal    = $gap * $qty;
                $amount     = $price * $qty;
        
                // get id and update state
                $account_statements                     = new Statement();
                $account_statements->equity             = $security->security;
                $account_statements->w_average          = $w_a;
                $account_statements->user_id            = $user_id;
                $account_statements->last_sold_price    = $price;
                $account_statements->last_sold_qty      = $qty;
                $account_statements->amount             = $amount;
                $account_statements->gap                = $gap;
                $account_statements->balance            = $top_bal;       
                $account_statements->status             = $status;
                $account_statements->comments           = $comments;
                $account_statements->save();
            }
        }
    }


    /*
    |--------------------------------------------------------------------------
    | Log transactions
    |--------------------------------------------------------------------------
    |
    | Log transactions details 
    |
    */
    // log transactions
    public function logTransaction($user_id, $amount, $qty, $price, $type, $sec_id)
    {
        $security = Security::where('id', $sec_id)->first();

        // update transactions logs
        $transaction               = new Transaction();
        $transaction->user_id      = $user_id;
        $transaction->stock_name   = $security->security;
        $transaction->stock_unit   = $price;
        $transaction->stock_qty    = $qty;
        $transaction->stock_trade  = $type;
        $transaction->stock_amount = $amount;
        $transaction->stock_timing = time();
        $transaction->com_percent  = 0;
        $transaction->com_amount   = 0;
        $transaction->save();

        // Get transactions notify user
        $transaction = Transaction::where('user_id', $user_id)->orderBy('id', 'desc')->take('1')->first();
        if($transaction !== null){
            $trans_box = array(
                'user_id'      => $user_id,
                'stock_name'   => $transaction->stock_name,
                'stock_unit'   => number_format($transaction->stock_unit, 2),
                'stock_qty'    => number_format($transaction->stock_qty),
                'stock_trade'  => $transaction->stock_trade,
                'stock_amount' => number_format($transaction->stock_amount, 2),
                'stock_date'   => $transaction->created_at->diffForHumans()
            );

            // Fire Transactions info
            \Event::fire(new TransactionNotifications($trans_box));
        }
    }


    
    /*
    |--------------------------------------------------------------------------
    | Compute and Load Assets information
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    public function filterFields($price, $qty)
    {
        $qty        = str_replace(",", "", $qty);
        $price      = str_replace(",", "", $price);
        # code...
        if(!is_numeric($price)){
            $data = array(
                'status'    => 'error',
                'message'   => $price.' is invalid !'
            );
        }elseif(!is_numeric($qty)){
            $data = array(
                'status'    => 'error',
                'message'   => $qty.' is invalid !'
            );
        }else{
            $data = "";
        }

        // return response
        return $data;
    }


    /*
    |-------------------------------------------------------
    | LOAD TBILLS USER-TO-USERS ORDERS
    |-------------------------------------------------------
    |
    */
    public function loadTbillOrders(){
        $all_tbills_orders = new TbillOrder();
        $data = $all_tbills_orders->laodAllOrders();

        // return response
        return response()->json($data);
    }

    /*
    |-------------------------------------------------------
    | PLACE ORDER DIRECT FROM ONE-CLICK TRADING
    |-------------------------------------------------------
    */
    public function placeOneclickOrder(Request $request){
        $bid_price  = $request->bid_price;
        $ask_price  = $request->ask_price;
        $bid_volume = $request->bid_volume;
        $ask_volume = $request->ask_volume;
        $sec_id     = $request->sec_id;
        $user_id    = $request->trader_id;

        if($user_id == Auth::user()->id || empty($user_id)){
            $data = [
                'status'    => 'error',
                'message'   => 'You are not allow to trade as a broker!'
            ];

            // return response.
            return response()->json($data);
        }

        if(!empty($bid_price) && !empty($bid_volume) && empty($ask_price) && empty($ask_volume)){
            // filtered inputs
            $filtered   = $this->filterFields($bid_price, $bid_volume);
            if(!empty($filtered)){

                // return error response
                return response()->json($filtered);
            }

            $order  = new Order();
            $data   = $order->placeBidOrder($user_id, $sec_id, $bid_volume, $bid_price);
        }elseif(empty($bid_price) && empty($bid_volume) && !empty($ask_price) && !empty($ask_volume)){
            // filtered inputs
            $filtered   = $this->filterFields($bid_price, $bid_volume);
            if(!empty($filtered)){

                // return error response
                return response()->json($filtered);
            }

            $order  = new Order();
            $data   = $order->placeAskOrder($user_id, $sec_id, $ask_volume, $ask_price);
        }elseif(empty($bid_price) && empty($bid_volume) && empty($ask_price) && empty($ask_volume)){
            
            $data = [
                'status'    => 'error',
                'message'   => 'Kindly enter a one way quote or two way quote!'
            ];
        }elseif(!empty($bid_price) && !empty($bid_volume) && !empty($ask_price) && !empty($ask_volume)){
            // process two legs
            $order  = new Order();
            $data   = $order->placeBidOrder($user_id, $sec_id, $bid_volume, $bid_price);
            $data   = $order->placeAskOrder($user_id, $sec_id, $ask_volume, $ask_price);
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Failed to place order, try again!'
            ];
        }
        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | GET GLOBAL ORDER FOR ALL EQUITIES
    |-----------------------------------------
    */
    public function loadGlobalOrders(){
        // body
        $orders = new Order();
        $data   = $orders->getGlobalOrders();

        // return response.
        return response()->json($data);
    }
}
