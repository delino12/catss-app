<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Pair;
use CATSS\Contact;
use CATSS\Mail\ContactUs;
use CATSS\Security;
use CATSS\Account;
use CATSS\Stock;
use CATSS\User;
use CATSS\Rank;

class ExternalPagesController extends Controller
{

    /*
    |-----------------------------------------
    | entry point
    |-----------------------------------------
    */
    public function __construct(){
        // body
        $this->middleware('auth')->only('priceList');
    }
    // Ranking users with W/A profit
    public function rankListing()
    {
        # code...
        // sum all stock from Evaluation Process
        return "welcome";
    }

    // load forum 
    public function showForum()
    {
        return view('external-pages.forum');
    }

    // load price listing 
    public function priceList()
    {
        # code...
        return view('trade-room.price-list');
    }

    // load winners
    public function winner()
    {
        $ranks = Rank::orderBy('id', 'asc')->limit('3')->get();

        $rank_box = [];
        foreach ($ranks as $rank) {
            # code...
            $user_info = User::where('id', $rank->user_id)->first();
            $data = array(
                'name'  => $user_info->name,
                'total' => $rank->net
            );

            array_push($rank_box, $data);
        }

        // return response
        return response()->json($rank_box);
    }

    // stock news json
    public function stockNews()
    {
    	// load pairs
        $pairs = Security::orderBy('id', 'desc')->get();

        $pair_box = [];
        foreach ($pairs as $pair) {
            # code...
            $data = array(
                "id" => $pair->id,
                "pairs" => $pair->security,
                "open" => number_format($pair->close_price, 2),
                "close" => number_format($pair->previous_close, 2)
            );
            array_push($pair_box, $data);
        }

        // return json response in array box
        return response()->json($pair_box);
    }

    // send Contact us email
    public function sendUsMail(Request $request)
    {
        $names   = $request->names;
        $subject = $request->subject;
        $message = $request->message;
        $email   = $request->email;
        $phone   = $request->phone;

        # code...
        \Mail::to($email)->send(new ContactUs($names));

        $feeds          = new Contact();
        $feeds->names   = $names;
        $feeds->email   = $email;
        $feeds->subject = $subject;
        $feeds->body    = $message;
        $feeds->phone   = $phone;
        $feeds->status  = 'unread';
        $feeds->save();

        $msg = "Thanks for contacting us CATSS.";
        $data = array(
            'status'  => 'success',
            'message' => 'Message sent !, '.$msg
        );

        return response()->json($data);
    }


    public function terms()
    {
        # code...
        return view('external-pages.terms_and_conditions');
    }

    public function policy()
    {
        # code...
        return view('external-pages.privacy_policy');
    }
}

