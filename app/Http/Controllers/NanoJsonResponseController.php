<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Nano;

class NanoJsonResponseController extends Controller
{
    /*
    |-----------------------------------------
    | RUN NANO PROCESS
    |-----------------------------------------
    */
    public function run(){
    	// body 
    	// run all matching process
    	$nano_bit = new Nano();
    	$nano_bit->run();

    	// return
    	return true;
    }
}
