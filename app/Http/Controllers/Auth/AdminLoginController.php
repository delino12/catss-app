<?php

namespace CATSS\Http\Controllers\Auth;

use CATSS\Admin;
use CATSS\Demotrade;
use CATSS\Http\Controllers\Controller;
use Auth;
use Illuminate\Http\Request;

class AdminLoginController extends Controller {
	//
	public $redirectTo = '/admin/dashboard';

	public function __construct() {
		$this->middleware('guest')->except('logoutAdmin');
	}

	// show admin form
	public function showLoginForm(Admin $admin) {

		// before show login form
		// create default admin
		$admin_name = "Bernard Cavidel";
		$admin_email = "admin@catss.com";
		$admin_password = "password12345";
		$admin_level = "omega";

		// first check to avoid duplicates
		$already_exits = Admin::where('email', $admin_email)->first();

		if ($already_exits !== null) {
			// just return the view
			return view('admin-pages.admin-login');
		} else {
			// create a demo trade
			$stocks = array(
				'CAPOIL', 'DANGFLOUR', 'DIAMONDBNK', 'FCMB', 'FIDELITYBK',
				'FLOURMILL', 'GOLDBREW', 'GUARANTY', 'GUINNESS', 'JAPAULOIL',
				'JBERGER', 'LASACO', 'NNFM', 'NESTLE', 'OANDO',
				'SKYEBANK', 'STANBIC', 'STERLNBANK', 'TOTAL', 'UNILEVER',
				'TRANSEXPR', 'ZENITHBANK',
			);

			$total = count($stocks);
			for ($i = 0; $i < $total; $i++) {
				# code...
				// save demo_trade
				$demo_trade = new Demotrade();
				$demo_trade->equity = $stocks[$i];
				$demo_trade->start_price = 1.20 + rand(1, 9);
				$demo_trade->close_price = 2.10 + rand(0, 9);
				$demo_trade->status = 'open';
				$demo_trade->stock_qty = 80000 + rand(000, 999);
				$demo_trade->traffic = 'high';
				$demo_trade->start_time = '12:00:AM';
				$demo_trade->close_time = '10:59:PM';
				$demo_trade->timing = time();
				$demo_trade->save();
			}

			// create the admin and still return the view
			// store admin
			$admin = new Admin;
			$admin->name = $admin_name;
			$admin->email = $admin_email;
			$admin->password = bcrypt($admin_password);
			$admin->level = $admin_level;
			$admin->save();

			// still return the view
			return view('admin-pages.admin-login');
		}
	}



	// login admin
	public function loginAdmin(Request $request) {
		// filter and re-assign request
		$username = $request->email;
		$password = $request->password;
		$rememberToken = $request->remember;

		// do admin login and take admin back to dashboard

		// Attemp to logged the user in
		if (Auth::guard('admin')->attempt(['email' => $username, 'password' => $password], $rememberToken)) {
			//return "true";
			return redirect()->intended('/admin/dashboard');
		} else {
			//return "false";
			return redirect()
				->back()
				->withInput($request->only('email', 'remember'))
				->with('login-status', 'Fail to login admin, please check your login credentials, CaSE-seNsiTive  ');
		}
	}


	public function logoutAdmin()
	{
		Auth::guard('admin')->logout();
		return redirect('/admin/login');
	}
}
