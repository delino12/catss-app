<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use CATSS\Events\NewAssets;
use CATSS\Pair;
use CATSS\Group;
use CATSS\User;
use CATSS\Account;
use CATSS\Transaction;
use CATSS\Stock;
use CATSS\News;
use CATSS\Statement;
use CATSS\Basic;
use CATSS\Asset;
use CATSS\Security;
use Storage;

// use carbon
use Carbon\Carbon;

class HomeController extends Controller 
{

    public function __construct(){
        $this->middleware('auth')->except('index', 'equityLive');
    }

    // welcome
    public function index(){
        return view('welcome-new');
    }

    // load account view
    public function account(){
        # code...
        return view('internal-pages.wallets');
    }

    // watch list view
    public function watchList(){
        # code...
        return view('internal-pages.watch-list');
    }

    // price alert view
    public function priceAlert(){
        # code...
        return view('internal-pages.price-alert');
    }

    // show brokers clients
    public function myClients(){
        # code...
        return view('internal-pages.my-clients');
    }
    
    // hire a broker
    public function hireBroker(){
        # code...
        return view('internal-pages.hire-broker');
    }
    
    // load view history
    public function transDetails($id){
        # code...
        return view('internal-pages.transaction-details', compact('id'));
    }

    // show dashboard
    public function dashboard(){
        // show users transactions
        $user_id = User::find(Auth::user()->id);
        $id = $user_id->id;

        // fetch all recent news
        $news = News::orderBy('id','desc')->take('5')->get();

        // find transactions 
        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('5')->get();
        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();
        // fetch group trade
        $groups = Group::where('user_id', $id)->get();

        return view('internal-pages.catss-dashboard', compact('all_transactions', 'account_info', 'news', 'groups'));
    }

    // show trade dashboard
    public function nTrade(){
        $user_id = User::find(Auth::user()->id);
        $id = $user_id->id;

        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // load pairs
        $pairs = Pair::orderBy('equity', 'desc')->get();

        return view('catss-live', compact('pairs', 'account_info'));
    }

    // load trading pad
    public function openEquity()
    {
        // check account market status
        $current_hour = Carbon::now()->hour;
        // $current_hour = 9;
        // check if greaten close time 24/clock or less than open time 9'O clock
        // if($current_hour >= 17 || $current_hour <= 8 ){
        //     return view('equities-closed');
        // }else{
            return view('equities-live');
            // return view('catss-live-random');
        // }
    }

    /*
    |--------------------------------
    | Bonds view
    |--------------------------------
    |
    */
    public function openBonds(){
        return view('trade-room.bonds-live');
    }


    /*
    |--------------------------------
    | Tbills view
    |--------------------------------
    |
    */
    public function openTbills(){
        return view('trade-room.tbills-live');
    }

    /*
    |--------------------------------
    | U2U Trading screen
    |--------------------------------
    |
    */
    public function openTbillsU2U(){
        return view('trade-room.u2u-tbills-live');
        // return "hello";
    }

    /*
    |--------------------------------
    | U2U Trading screen
    |--------------------------------
    |
    */
    public function openFxU2U(){
        return view('trade-room.u2u-fx-live');
        // return "hello";
    }

    // load fast trading
    public function fastTrade(){
        # code...
        return view('catss-live-random');
    }

    // load single equity history
    public function history($name, $id){
        # code...
        return view('security-history', compact('name', 'id'));
    }

    // return view and with Equity Live
    public function equityLive(){
        return view('equity-live');
    }

    // show trade transactions
    public function transactions(){
        // show users transactions
        $user_id = User::find(Auth::user()->id);
        $id = $user_id->id;

        // find transactions 
        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('30')->get();

            // fetch account information 
        $account_info = Account::where('user_id', $id)->get();
        // fetch group trade
        $groups = Group::where('user_id', $id)->get();


        return view('internal-pages.transactions', compact('all_transactions', 'account_info', 'groups'));   
    }

    // show stock balance 
    public function stocksBalance(){

        // get user stock balance 
        $id = Auth::user()->id;

        // fetch all stocks
        $stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();
        $stock_basket = [];
        $profit_basket = []; 
        $loss_basket = [];
        foreach ($stocks as $stock) {
            # code...
            // get last price from pairs
            $equity_market = Security::where('security', $stock->name)->get();

            foreach ($equity_market as $market) {

                // check on formular
                // $wa = (float)$stock->qty * $stock->w_average;

                if($stock->w_average > $market->close_price){
                    $wa = $stock->w_average - $market->close_price;
                }elseif ($stock->w_average < $market->close_price) {
                    $wa = $market->close_price - $stock->w_average;
                }elseif($stock->w_average = $market->close_price){
                    $wa = $market->close_price - $stock->w_average;
                }

                $swa =  $wa * $stock->qty;

                // market price + charges
                $two_percent = 2.00;
                $total_percent = 100;
                $diff = $total_percent - $two_percent;
                
                $charge_percent = $diff / $total_percent;
                $map_price = $charge_percent * $market->close_price;
                $map_diff = $market->close_price - $map_price;

                $real_price = $map_diff + $market->close_price;

                // market price vs w/a price
                $market_amount = $stock->qty * $market->close_price; // at market price
                $holding_amount = $stock->qty * $stock->w_average; // at closing price

                if($market_amount > $holding_amount){
                    // market price is up
                    $profit = $market_amount - $holding_amount;
                    // Calculate Profit or loss from ongoin market
                    $profit_data = array(
                        'profit' => $profit
                    );

                    // push to array
                    array_push($profit_basket, $profit_data);

                }elseif($market_amount < $holding_amount){
                    // holding is up
                    $loss = $holding_amount - $market_amount;

                    // calculate loss
                    $loss_data = array(
                        'loss' => $loss
                    );

                    // push to array
                    array_push($loss_basket, $loss_data);
                }
                

                # code...
                $data = array(
                    "equity"    => $stock->name,
                    "price"     => $real_price,
                    "stock"     => $stock->qty,
                    "w_average" => $stock->w_average,
                    "amount"    => $stock->qty * $market->close_price,
                    "swa"       => $swa,
                    "date"      => $market->updated_at
                );
                array_push($stock_basket, $data);
            }
        }

        // all to collections
        $loss_collection = collect($loss_basket);
        $profit_collection = collect($profit_basket);

        // total loss & profit
        $total_loss = $loss_collection->sum('loss');
        $total_profit = $profit_collection->sum('profit');

        // readable numeric
        $total_loss   = $total_loss;
        $total_profit = $total_profit;

        // cast to collection 
        $collect_stock = collect($stock_basket);
        // return $collect_stock->sum('amount');
        // scanned stock basket for profit or loss


        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // fetch group trade
        $groups = Group::where('user_id', $id)->get();

        // show users stocks bal.
        return view('internal-pages.stock-balance', compact('stocks', 'account_info', 'groups', 'stock_basket', 'total_profit', 'total_loss'));
    }

    // show stock history
    public function tradeHistory($equity){
        $pair = $equity;
        $transaction_history = Transaction::where('stock_name', $equity)->orderBy('id', 'desc')->get();
        return view('internal-pages.history', compact('transaction_history', 'pair'));
    }

    // return response 
    public function loadPairDetails($pair){
        $equity_log = Transaction::where('stock_name', $pair)->first();
        return response()->json($equity_log);
    }

    // show balance sheet extract
    public function balanceSheet(){

        // get user stock balance 
        $user_id = User::find(Auth::user()->id);
        $id = $user_id->id;

        // fetch all stocks
        $stocks = Stock::where('user_id', $id)->get();

        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // show users stocks bal.
        return view('internal-pages.balance-sheet', compact('stocks', 'account_info'));
    }

    // account statement 
    public function accountStatments(){
        // get user stock balance 
        $id = Auth::user()->id;

        // find transactions 
        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('5')->get();

        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // accounts
        $trading_statements = Statement::where('user_id', $id)->get();
        
        return view("internal-pages.accounts", compact('all_transactions', 'account_info', 'trading_statements'));
    }

    // trade ranking
    public function rankLists(){
        // return ranking view
        $user_id = User::find(Auth::user()->id);
        $id = $user_id->id;

        // fetch account information
        $account_info = Account::where('user_id', $id)->get();

        // ranking by profits 
        $rankings = Account::orderBy('account_balance', 'desc')->get();

        // rank table
        $rank_table = [];

        foreach ($rankings as $users_ranking) {

            // rank only those who made profit
            if($users_ranking->account_balance > 10000000){
                // fetch informations
                $users = User::where('id', $users_ranking->user_id)->get();

                foreach ($users as $user) {
                    $profit = $users_ranking->account_balance - 10000000;

                    if($profit < 200000){
                        $badge = "BOYSCOUT";
                    }elseif($profit > 300000){
                        $badge = "SERGENT";
                    }elseif($profit > 400000){
                        $badge = "SERGENT";
                    }elseif($profit > 500000){
                        $badge = "LIEUTENANT";
                    }elseif($profit > 700000){
                        $badge = "CAPTAIN";
                    }elseif($profit > 800000){
                        $badge = "MAJOR";
                    }elseif($profit > 900000){
                        $badge = "COLONEL";
                    }else{
                        $badge = "CADEC";
                    }

                    # code...
                    $data = array(
                        "name" => $user->name,
                        "profit" => $profit,
                        "badge" => $badge,
                        "date" => $users_ranking->updated_at
                    );

                    array_push($rank_table, $data);
                }
            }
        }

        // load pairs
        $pairs = Pair::orderBy('equity', 'desc')->get();

        return view('internal-pages.rank-lists-live', compact('pairs', 'account_info', 'rank_table'));
    }

    // users settings 
    public function setting(){
        // get user stock balance 
        $id = Auth::user()->id;

        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // fetch profile 
        $basic_information = Basic::where('user_id', $id)->first();

        return view("internal-pages.setting", compact('account_info', 'basic_information'));
    }

    // update
    public function updateProfile(Request $request){
        $firstname = $request->first_name;
        $lastname = $request->last_name;

        // form request
        $name = $firstname.' '.$lastname;
        $gender = $request->gender;
        $address = $request->address;
        $state = $request->state;
        $zipcode = $request->zipcode;
        $phone = $request->phone;

        // get user stock balance 
        $id = Auth::user()->id;

        // get basic id rep
        $basic_id = Basic::where('user_id', $id)->first();
        $b_id = $basic_id->id;

        // find id and update
        $basic = Basic::find($b_id);
        $basic->name = $name;
        $basic->gender = $gender;
        $basic->address = $address;
        $basic->state = $state;
        $basic->zip_code = $zipcode;
        $basic->phone = $phone;
        $basic->update();

        $msg = " Profile details updated successfully ";
        return redirect()->back()->with('success', $msg);
    }

    // upload profile pictures
    public function uploadAvatar(Request $request){
        // get user stock balance 
        $id = Auth::user()->id;

        //upload and stored images
        $target_dir = "../public/uploads/";
        $ext = substr($_FILES['file']['name'], strpos($_FILES['file']['name'], '.'));

        // accepted extension
        $accepted_ext = array(".jpg", ".jpeg", ".png");
        if(in_array($ext, $accepted_ext)){
            $new_name = time() . rand(000, 999) . $ext;
            $target_file = $target_dir . $new_name;
            move_uploaded_file($_FILES['file']['tmp_name'], $target_file);
            $msg = " Profile details updated successfully ";

            // save profile avatar
            $basic = Basic::find($id);
            $basic->avatar = $new_name;
            $basic->update();

            return redirect()->back()->with('success', $msg);
        }else{
            // return with error in extenstion
            $msg = "Invalid image type, please uploada a valid image";
            return redirect()->back()->with('warning', $msg);
        }
    }

    // change users password
    public function changePassword(Request $request){
        // form request
        $password = $request->password;

        // encrypt password 
        $password = bcrypt($password);

        // get user stock balance 
        $id = Auth::user()->id;

        // change password
        $user = User::find($id);
        $user->password = $password;
        $user->update();

        $msg = "Profile details updated successfully ";
        return redirect()->back()->with('password_updates', $msg);
    }

    // load my stock
    public function myStock(){
        // get user stock balance 
        $id = Auth::user()->id;

        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // fetch profile 
        $basic_information = Basic::where('user_id', $id)->first();

        return view("internal-pages.my-stocks", compact('account_info', 'basic_information'));
    }

    // save personal stock
    public function savePersonalStock(Request $request){
        // user id 
        $id = Auth::user()->id;

        // request user personal Assets
        $name = $request->stockName;
        $buy  = $request->stockBuyPrice;
        $sell = $request->stockSellPrice;
        $Qty  = $request->stockQty;

        // check for user personal assets
        $check_assets = Asset::where([['name', $name], ['user_id', $id]])->first();

        if($check_assets == null){
            // this is a new assets
            $assets = new Asset();
            $assets->user_id = $id;
            $assets->name =  $name;
            $assets->buy  =  $buy;
            $assets->sell =  $sell;
            $assets->Qty  =  $Qty;
            $assets->save();

            // return msg
            $msg = $name." added to your realtime stock record!";
        }else{
            // update the existing assets with assets id
            // temp save asset id if found
            $assets_id = $check_assets->id;
            $assets = Asset::find($assets_id);
            $assets->user_id = $id;
            $assets->name =  $name;
            $assets->buy  =  $buy;
            $assets->sell =  $sell;
            $assets->Qty  =  $assets->qty + $Qty;
            $assets->update();

            $msg = $name." added to your realtime stock record!";
        }

        // fetch last updated and feed events
        $stocks = Asset::where([['name', $name], ['user_id', $id]])->first();

        // to array
        $data = array(
            'id'   => $stocks->id,
            'name' => $stocks->name,
            'buy'  => number_format($stocks->buy, 2),
            'sell' => number_format($stocks->sell, 2),
            'qty'  => number_format($stocks->qty),
            'date' => $stocks->updated_at->diffForHumans(),
        );

        // call events
        \Event::fire(new NewAssets($data));

        // res msg
        echo $msg;
    }

    // load personal stock
    public function loadPersonalStock(){
        // logged user
        $id = Auth::user()->id;

        // check if that Security Exist
        $securites = Security::all();

        foreach ($securites as $security) {
            # code...

            // fetch stock
            $all_realtime_assets = Asset::where('user_id', $id)->orderBy('id', 'desc')->get();
            $realtime_stocks = [];
            foreach ($all_realtime_assets as $stocks) {
                
                $amount = $stocks->sell * $stocks->qty;
                
                // get security market price
                $mkt_price = $security->close_price;
                $bal = $stocks->qty * $mkt_price;



                if($bal == $amount){
                    $status = "break-even";
                }elseif($bal > $amount){
                    $status = "profit";
                }elseif($bal < $amount){
                    $status = "loss";
                }

                # code...
                $data = array(
                    'id'        => $stocks->id,
                    'name'      => $stocks->name,
                    'buy'       => number_format($stocks->buy, 2),
                    'sell'      => number_format($stocks->sell, 2),
                    'bal'       => number_format($bal, 2),
                    'amount'    => number_format($amount, 2),
                    'status'    => $status,
                    'qty'       => number_format($stocks->qty),
                    'date'      => $stocks->updated_at->diffForHumans()
                );

                array_push($realtime_stocks, $data);            
            }           

            return response()->json($realtime_stocks);
        }   
    }
}