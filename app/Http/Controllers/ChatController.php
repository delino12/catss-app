<?php

namespace CATSS\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use CATSS\Events\NewChatMsg;
use CATSS\Chat;
use CATSS\User;

class ChatController extends Controller
{
    // load chats
    public function loadChat()
    {
        // users
        $user_id = Auth::user()->id;
        $from = Auth::user()->email;

        // check and single chat msg 
        $all_chats = Chat::where('user_id', $user_id)->orderBy('created_at')->take('3')->get();

        // $all_chats = array_reverse($all_chats);

        // check fetch chat message
        $msg_box = [];
        foreach ($all_chats as $chat) {
            # code...
            if($chat->from == Auth::user()->email){
                $from = "Me";
            }else{
                $from = "Agent";
            }
            $data = array(
                'from' => $from,
                'to' => $chat->to,
                'body' => $chat->body,
                'status' => $chat->status,
                'date' => $chat->created_at->diffForHumans()
            );

            // $data = array_reverse($data);
            array_push($msg_box, $data);
        }

        return response()->json($msg_box);

    }

    // load chats
    public function loadChatCatss()
    {
        // users
        $user_id = Auth::user()->id;
        $from = Auth::user()->email;

        // check and single chat msg 
        $all_chats = Chat::where('user_id', $user_id)
        ->orderBy('id', 'ASC')
        ->get();

        // $all_chats = array_reverse($all_chats);

        // check fetch chat message
        $msg_box = [];
        foreach ($all_chats as $chat) {
            # code...
            if($chat->from == Auth::user()->email){
                $from = "Me";
            }else{
                $from = "Agent";
            }
            $data = array(
                'from' => $from,
                'to' => $chat->to,
                'body' => $chat->body,
                'status' => $chat->status,
                'date' => $chat->created_at->diffForHumans()
            );

            // $data = array_reverse($data);
            array_push($msg_box, $data);
        }

        return response()->json($msg_box);

    }

    // load reply chat
    public function loadReply($id)
    {
        // find the init chat
        $chats = Chat::where('id', $id)->first();

        // get all chats from init
        $reply_chat = Chat::where([['from', $chats->from], ['to', $chats->to]])
                            ->orWhere([['to', $chats->from], ['from', $chats->to]])
                            ->orderBy('id', 'ASC')
                            ->get();
        $reply_box = [];
        foreach ($reply_chat as $replies) {
            # code...
            $data = array(
                'id'    => $replies->id,
                'to'    => $replies->to,
                'from'  =>  $replies->from,
                'body'  =>  $replies->body,
                'status' => $replies->status,
                'date'  =>  $replies->created_at->diffForHumans()
            );
            array_push($reply_box, $data);
        }
        return response()->json($reply_box);
    }

    public function sendChat(Request $request)
    {
    	$user_id = Auth::user()->id;
    	$from = Auth::user()->email;

    	$msg = $request->msg;
    	$to = "admin";
    	$status = "unread";

        // first count and check if chat is up to 10
        $scan_chat = Chat::where([['from', $from], ['to', $to]])->get();
        $counted = count($scan_chat);
        foreach ($scan_chat as $all_chat) {
            # code...
            if($counted > 6){
                $chat = Chat::find($all_chat->id);
                $chat->delete();
            }
        }

    	$chats = new Chat();
    	$chats->user_id = $user_id;
    	$chats->from = $from;
    	$chats->to   = $to;
    	$chats->body = $msg;
    	$chats->status = $status;
    	$chats->save();

        $last_chats = Chat::where([['user_id', $user_id], ['body', $msg]])->orderBy('id', 'desc')->take('1')->first();
        # code...
        if($last_chats->from == Auth::user()->email){
            $from = "Me";
        }else{
            $from = "Agent";
        }

        $data = array(
            'from' => $from,
            'to' => $last_chats->to,
            'body' => $last_chats->body,
            'status' => $last_chats->status,
            'date' => $last_chats->created_at->diffForHumans()
        );

        // call events
        \Event::fire(new NewChatMsg($data));

    	$chat_response = array(
            'status'  => 'success',
            'message' => 'chat sent !'
        );

        return response()->json($chat_response);
    }
}
