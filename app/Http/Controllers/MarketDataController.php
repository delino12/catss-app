<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Bond;
use CATSS\Tbill;
use CATSS\Market;
use CATSS\Account;
use CATSS\TbillOrder;
use CATSS\FxOrder;
use CATSS\Security;
use DB;
use Auth;

class MarketDataController extends Controller
{

	/*
	|--------------------------------
	| load market data from API
	|--------------------------------
	|
	*/
	public function loadMarketData($type){

		// initialize market data API
		$market = new Market();
    	$data = $market->fetchMarketPrices();

    	$bonds_box = [];
    	$tbills_box = [];

    	// convert to collection
    	$all_market_data = collect($data);
    	if(count($all_market_data) > 0){
    		foreach ($all_market_data as $market_data) {
    			// data-set
    			$data = [
    				'ClosingDate' 		=> $market_data->ClosingDate,
					'ClosingMktPrice' 	=> number_format($market_data->ClosingMktPrice, 2),
					'ClosingPrice' 		=> number_format($market_data->ClosingPrice, 2),
					'ClosingPriceRef' 	=> $market_data->ClosingPriceRef,
					'Comment' 			=> $market_data->Comment,
					'ConfigRef' 		=> $market_data->ConfigRef,
					'CostOfFunds' 		=> $market_data->CostOfFunds,
					'Description' 		=> $market_data->Description,
					'EOMDate' 			=> $market_data->EOMDate,
					'EOMMktPrice' 		=> $market_data->EOMMktPrice,
					'EOYDate' 			=> $market_data->EOYDate,
					'EOYMktPrice' 		=> $market_data->EOYMktPrice,
					'InputDate' 		=> $market_data->InputDate,
					'InputDatetime' 	=> $market_data->InputDatetime,
					'Inputter' 			=> $market_data->Inputter,
					'InputterID' 		=> $market_data->InputterID,
					'Maturity' 			=> $market_data->Maturity,
					'ModifiedDatetime' 	=> $market_data->ModifiedDatetime,
					'ModifierID' 		=> $market_data->ModifierID,
					'MonthEndDate' 		=> $market_data->MonthEndDate,
					'MonthStartDate' 	=> $market_data->MonthStartDate,
					'NextCloseDate' 	=> $market_data->NextCloseDate,
					'NextClosing' 		=> $market_data->NextClosing,
					'NextClosingMktPrice' => $market_data->NextClosingMktPrice,
					'PrevClosing' 		=> $market_data->PrevClosing,
					'PrevClosingMktPrice' => $market_data->PrevClosingMktPrice,
					'PrevTradeDate' 	=> $market_data->PrevTradeDate,
					'ProductID' 		=> $market_data->ProductID,
					'TradeDate' 		=> $market_data->TradeDate,
					'YearEndDate' 		=> $market_data->YearEndDate,
					'YearStartDate' 	=> $market_data->YearStartDate,
					'Yield' 			=> number_format($market_data->Yield, 2),
    			];

    			if((int)$market_data->ProductID == 1){
    				// case bonds 
    				// push data to bonds
    				array_push($bonds_box, $data);

    			}elseif ((int)$market_data->ProductID == 2) {
    				// case tbills
    				// push data to tbills
    				array_push($tbills_box, $data);
    			}
    		}
    	}else{
    		$bonds_box = [];
    		$tbills_box = [];
    	}

    	// initialize return type
    	if($type == 'bond'){
    		return $bonds_box;
    	}elseif ($type == 'tbill') {
    		# code...
    		return $tbills_box;
    	}
	}


    /*
    |--------------------------------
    | fetch bonds data
    |--------------------------------
    |
    */
    public function loadBonds(){

    	// init mkt data for bonds
    	$type = 'bond';
    	$data = $this->loadMarketData($type);

    	// return response
    	return response()->json($data); 
    }

    /*
    |--------------------------------
    | fetch tbills data
    |--------------------------------
    |
    */
    public function loadTbills(){

        // init mkt data for bonds
        $type = 'tbill';
        $data = $this->loadMarketData($type);

        // return response
        return response()->json($data); 
    }

    /*
    |-----------------------------------------
    | load single tbills data
    |-----------------------------------------
    |
    */
    public function loadOneTbills(Request $request){

        // init mkt data for bonds
        $type   = 'tbill';
        $stack  = $request->asset;
        $data   = $this->loadMarketData($type);

        // filtered data
        $filtered = collect($data);

        // scan and filter
        $results = $filtered->where('Description', $stack)->first();

        // return results
        return response()->json($results);
    }


    /*
    |-----------------------------------------
    | placeOrderTbill
    |-----------------------------------------
    */
    public function placeOrderTbill(Request $request){
        // body
        $new_order  = new TbillOrder();
        $data       = $new_order->placeOrder($request);

        // return response
        return response()->json($data); 
    }

    /*
    |-----------------------------------------
    | placeOrderBond
    |-----------------------------------------
    */
    public function placeOrderBond(Request $request){
        // body
        $new_order  = new TbillOrder();
        $data       = $new_order->placeOrder($request);

        // return response
        return response()->json($data); 
    }


    /*
    |-----------------------------------------
    | place order fx
    |-----------------------------------------
    */
    public function placeOrderFx(Request $request){
        // body
        $fx_trade   = new FxOrder();
        $data       = $fx_trade->placeOrder($request);

        // return response
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load all orders
    |-----------------------------------------
    */
    public function loadOrderFx(){
        // body
        $fx_trade   = new FxOrder();
        $data       = $fx_trade->loadOrders();

        // return response
        return response()->json($data);
    }



    /*
    |--------------------------------
    | Trade Bonds
    |--------------------------------
    |
    */
    public function tradeBonds(Request $request){
        $user_id    = Auth::user()->id;
        $price      = $request->price;
        $qty        = $request->qty;
        $refid      = $request->refid;
        $asset      = $request->security;
        $maturity   = $request->maturity;
        $option     = $request->option;
        $yield      = $request->secYield;

        $qty = $this->filteredFormatted($qty);

        $amount = $price * $qty;

        // validate trade type 
        if($option == 'buy'){

            if($this->verifyAccountBalance($user_id, $amount)){
                // when purchasing bond
                $bonds = new Bond();
                $data = $bonds->buyBond($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option);
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Insufficient balance !, To deposit click <a href="/account">here</a>'
                ];
            }

        }elseif($option == 'sell') {

            // when selling bond
            $bonds = new Bond();
            $data = $bonds->sellBond($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option);

        }else{

            // return response when no option found !
            $data = [
                'status'    => 'error',
                'message'   => 'No trade option executed'
            ];
        }

        // return response
        return response()->json($data);
    }


    /*
    |--------------------------------
    | Trade T-Bills
    |--------------------------------
    |
    */
    public function tradeTbills(Request $request){
        $user_id    = Auth::user()->id;
        $price      = $request->discount;
        $qty        = $request->qty;
        $refid      = $request->refid;
        $asset      = $request->security;
        $maturity   = $request->maturity;
        $option     = $request->option;
        $yield      = $request->secYield;

        $qty = $this->filteredFormatted($qty);

        $amount = $price * $qty;

        // validate trade type 
        if($option == 'buy'){

            if($this->verifyAccountBalance($user_id, $amount)){
                // when purchasing bond
                $tbills = new Tbill();
                $data = $tbills->buyTbill($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option);
            }else{
                $data = [
                    'status'  => 'error',
                    'message' => 'Insufficient balance !, To deposit click <a class="dino-link" href="/account">here</a>'
                ];
            }

        }elseif($option == 'sell') {

            // when selling bond
            $tbills = new Tbill();
            $data = $tbills->sellTbill($user_id, $price, $qty, $refid, $asset, $maturity, $yield, $option);

        }else{

            // return response when no option found !
            $data = [
                'status'    => 'error',
                'message'   => 'No trade option executed'
            ];
        }

        // return response
        return response()->json($data);
    }


    /*
    |--------------------------------
    | Load user stock for bonds
    |--------------------------------
    |
    */
    public function loadUserBonds(){
        $user_id = Auth::user()->id;
        
        // fetch bonds
        $bonds  = new Bond();
        $data   = $bonds->loadUserBond($user_id);

        // return response
        return response()->json($data);
    }


    /*
    |--------------------------------
    | Load user stock for Tbills
    |--------------------------------
    |
    */
    public function loadUserTbills(){
        $user_id = Auth::user()->id;
        
        // fetch tbill
        $tbills  = new Tbill();
        $data    = $tbills->loadUserTbill($user_id);

        // return response
        return response()->json($data);
    }

    
    /*
    |--------------------------------
    | Load all loadAllBonds
    |--------------------------------
    |
    */
    public function loadAllBonds(){
        // fetch tbill
        $tbills  = new Bond();
        $data    = $tbills->loadAllBond();

        // return response
        return response()->json($data);
    }

    
    /*
    |--------------------------------
    | Load all loadAllTbills
    |--------------------------------
    |
    */
    public function loadAllTbills(){
        // fetch tbill
        $tbills  = new Tbill();
        $data    = $tbills->loadAllTbill();

        // return response
        return response()->json($data);
    }    


    /*
    |--------------------------------
    | Load all bonds
    |--------------------------------
    |
    */
    public function loadAllPurchasedBonds(){
        $user_id = Auth::user()->id;

        // fetch bonds
        $bonds  = new Bond();
        $data   = $bonds->loadAllBond();

        // return response
        return response()->json($data);
    }


    /*
    |--------------------------------
    | Load all Tbills
    |--------------------------------
    |
    */
    public function loadAllPurchasedTbills(){
        $user_id = Auth::user()->id;

        // fetch tbills
        $tbills  = new Bond();
        $data   = $tbills->loadAllTbill();

        // return response
        return response()->json($data);
    }



    /*
    |--------------------------------
    | check & verify transaction balances
    |--------------------------------
    |
    */
    public function verifyAccountBalance($user_id, $amount){
        // check account 
        $account = Account::where('user_id', $user_id)->first();
        if($account->account_balance >= $amount){
            return true;
        }else{
            return false;
        }
    }

    /*
    |--------------------------------
    | check & verify transaction balances
    |--------------------------------
    |
    */
    public function verifyStockBalance($user_id, $ref_id, $req_qty, $stock_type){
        // check if user has stock
        if($stock_type == 'bond'){
            // check bond
            $bond = new Bond();
        }elseif($stock_type == 'tbill') {
            // check Tbills
        }
    }

    

    /*
    |--------------------------------
    | Fetch Bond summary
    |--------------------------------
    |
    */
    public function loadSummaryBonds(){

        $user_id = Auth::user()->id;

        $bond_summary   = new Bond();
        $total_summary  = $bond_summary->fetchBondSummary($user_id);
        $total_summary  = collect($total_summary);

        return response()->json($total_summary);
    }

    /*
    |--------------------------------
    | Fetch Tbills summary
    |--------------------------------
    |
    */
    public function loadSummaryTbills(){

        $user_id = Auth::user()->id;

        $tills_summary   = new Tbill();
        $total_summary  = $tills_summary->fetchTbillSummary($user_id);
        $total_summary  = collect($total_summary);

        return response()->json($total_summary);
    }


    /*
    |--------------------------------
    | Load Bonds balance from PROC
    |--------------------------------
    |
    */
    public function loadBondsBalance(){
        $user_id = Auth::user()->id;

        $fetch_balance = DB::select('
            EXEC procBondsQtyBalance
        ');

        // collect data 
        // $tbills_balance = collect($fetch_balance);
        // if(count($tbills_balance) > 0){
        //     $tbill_box = [];
        //     foreach ($tbills_balance as $bill) {
        //         # code...
        //         $data = [

        //         ];
        //     }
        // }
        return response()->json($fetch_balance);
    }


    /*
    |--------------------------------
    | clean formatted numbers
    |--------------------------------
    |
    */
    public function filteredFormatted($qty){
        $data = str_replace(",", "", $qty);
        return $data;
    }

    /*
    |-----------------------------------------
    | Load equities prices
    |-----------------------------------------
    */
    public function loadEquities(Request $request){
        $security_type = $request->security_type;

        // body
        $security   = new Security();
        $data       = $security->getNsePrices($security_type); 

        // return response.
        return response()->json($data);  
    }
}
