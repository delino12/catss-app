<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Mail\WelcomeMail;
use CATSS\Mail\NewUser;
use CATSS\User;
use CATSS\Account;
use CATSS\Basic;
use CATSS\Activation;
use CATSS\Firm;
use Auth;
use Session;

class RegisterController extends Controller
{
    //
    public function __construct(){
        $this->middleware('guest');
    }

    public function showSignup()
    {
        $stock_firms    = new Firm();
        $brokers_list   = collect($stock_firms->getAllFirm());

        // return
    	return view('internal-pages.signup', compact('brokers_list'));
    }


    public function createAccount(Request $request, User $user, Account $account)
    {
        // validate users requests data
    	$request->validate([
    		'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6',
    	]);

        // re-assgin requested data
        $name       = $request->name;
        $email      = $request->email;
        $password   = $request->password;
        $firm       = $request->brokers_firm;
        $account_type = $request->account_type;

        if($account_type == 'on'){
            $account_type = "broker";
        }else{
            $account_type = "client";
        }

        // encrypt password 
        $password = bcrypt($password);

        // generate unique account id
        $account_id     = "NTD".rand(000,999).rand(111,999);
        $account_bal    = 0; // set default account balance
        $account_status = "open"; // account status set to open
        $account_timing = time(); // account opening date

        // multible
        $user             = new User();
        $user->account_id = $account_id;
        $user->brokerage_firm = $firm;
        $user->account_type = $account_type;
        $user->name       = $name;
        $user->email      = $email;
        $user->password   = $password;
        $user->save();


        // After saving the user fetch and get user id 
        $users = User::where('email', $email)->get();

        // check for id and create relationship
        foreach ($users as $user) {
            // init account model
            $account                  = new Account();
            $account->user_id         = $user->id;
            $account->account_id      = $account_id;
            $account->account_balance = $account_bal;
            $account->account_status  = $account_status;
            $account->timing          = time();
            $account->save();

            // init a new basic informations
            $basic_info          = new Basic();
            $basic_info->user_id = $user->id;
            $basic_info->name    = $user->name;
            $basic_info->save();

            // set account for activations
            $activate           = new Activation();
            $activate->email    = $user->email;
            $activate->token    = rand(000, 999).'-'.rand(111, 555);
            $activate->status   = 'inactive';
            $activate->save();

            // get the users activations code details
            $activation_code = Activation::where('email', $email)->first();

            // build res data
            $data = array(
                'id' => $activation_code->id,
                'name' => $name,
                'code' => $activation_code->token
            );
        }

        // send User an Email
        \Mail::to($email)->send(new NewUser($data));
        // \Mail::to($email)->send(new WelcomeMail($data));

        if(Auth::attempt(['email' => $email, 'password' => $request->password])){
            $msg = 'Welcome to CATSS';
            return redirect('/dashboard')->with('success_msg', $msg);
        
        }else{
            
            return redirect('/login-account')
                        ->with('status', 'Account has been successfully created, confirmation link has been sent, check email to verify account !');
        }
    }
}
