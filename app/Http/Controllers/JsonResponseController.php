<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use CATSS\Events\NewAssets;
use CATSS\Events\PriceAlert;
use CATSS\Events\AccountUpdate;
use CATSS\Events\TransactionNotifications;
use CATSS\Events\StockBalanceNotifications;
use CATSS\Commission;
use CATSS\Order;
use CATSS\Wallet;
use CATSS\Pair;
use CATSS\Group;
use CATSS\User;
use CATSS\Account;
use CATSS\Transaction;
use CATSS\Stock;
use CATSS\News;
use CATSS\Statement;
use CATSS\Basic;
use CATSS\Asset;
use CATSS\Security;
use CATSS\Activation;
use CATSS\Invitation;
use CATSS\Graph;
use CATSS\Alert;
use CATSS\WatchList;
use CATSS\Client;
use CATSS\Nano;
use Storage;
use DB;

class JsonResponseController extends Controller
{
    /*
    |-----------------------------------------
    | authencticat users
    |-----------------------------------------
    */
    public function __construct()
    {
        $this->middleware('auth')->except('index', 'loadRanking', 'pairsIndexUpdates', 'loadEquityMarket');
    }

    /*
    |-----------------------------------------
    | load pairs for trade JSON
    |-----------------------------------------
    */
    public function equityPairs()
    {
        // load pairs
        $pairs = Security::all();

        $pair_box = [];
        foreach ($pairs as $equity) {

            $two_percent = 2.00;
            $total_percent = 100;
            $diff = $total_percent - $two_percent;
            
            $charge_percent = $diff / $total_percent;
            $map_price = $charge_percent * $equity->close_price;
            $map_diff = $equity->close_price - $map_price;

            $real_price = $map_diff + $equity->close_price; 

            $gap = $real_price - $equity->previous_close;

            $data = array(
                'id'                => $equity->id,
                'board'             => $equity->board,
                'security'          => $equity->security,
                'ref_price'         => number_format($equity->ref_price, 2),
                'open_price'        => number_format($equity->open_price, 2),
                'high_price'        => number_format($equity->high_price, 2),
                'low_price'         => number_format($equity->low_price, 2),
                'close_price'       => number_format($equity->close_price, 2),
                'change_price'      => number_format($gap, 2),
                'daily_volume'      => number_format($equity->daily_volume, 2),
                'daily_value'       => number_format($equity->daily_value, 2),
                'dvmv_today'        => $equity->dvmv_today,
                'mvtn_trade'        => $equity->mvtn_trade,
                'previous_close'    => number_format($equity->previous_close, 2),
                'no_of_trade'       => number_format($equity->no_of_trade),
                'real_price'        => number_format($real_price, 2),
                'status'            => $equity->status,
                'date'              => $equity->created_at->toFormattedDateString()
            );
            array_push($pair_box, $data);
        }

        // return json response in array box
        return response()->json($pair_box);
    }

    /*
    |-----------------------------------------
    | load a random trading screen
    |-----------------------------------------
    */
    public function equityPairsRandom()
    {
        // load pairs from random
        $pairs = Security::all();
        if(count($pairs) > 0){
            $pair_box = [];
            foreach ($pairs as $equity) {
                // best buy price
                $buy_order = Order::select(['orders.id', 'orders.user_id', 'orders.sec_id', 'orders.qty', 'orders.type', 'orders.created_at', DB::raw('max(price) as price')])
                ->where([['sec_id', $equity->id], ['type', 'buy']])
                ->groupBy(['orders.id', 'orders.user_id', 'orders.sec_id', 'orders.qty', 'orders.type', 'orders.created_at'])
                ->orderBy('price', 'DESC')->limit('1')->get();

                // best sell price
                $sell_order = Order::select(['orders.id', 'orders.user_id', 'orders.sec_id', 'orders.qty', 'orders.type', 'orders.created_at', DB::raw('min(price) as price')])
                ->where([['sec_id', $equity->id], ['type', 'sell']])
                ->groupBy(['orders.id', 'orders.user_id', 'orders.sec_id', 'orders.qty', 'orders.type', 'orders.created_at'])
                ->orderBy('price', 'DESC')->limit('1')->get();

                if(count($buy_order) > 0){
                    $buy_id     = $buy_order[0]->id;
                    $buy_user   = $buy_order[0]->user_id;
                    $buy_price  = $buy_order[0]->price;
                    $buy_qty    = $buy_order[0]->qty;
                }else{
                    $buy_id     = 0;
                    $buy_user   = 0;
                    $buy_price  = 0;
                    $buy_qty    = 0;
                }

                if(count($sell_order) > 0){
                    $sell_id     = $sell_order[0]->id;
                    $sell_user   = $sell_order[0]->user_id;
                    $sell_price  = $sell_order[0]->price;
                    $sell_qty    = $sell_order[0]->qty;
                }else{
                    $sell_id     = 0;
                    $sell_user   = 0;
                    $sell_price  = 0;
                    $sell_qty    = 0;
                }

                $data = array(
                    'id'        => $equity->id,
                    'board'     => $equity->board,
                    'security'  => $equity->security,
                    'buy_id'    => $buy_id,
                    'buy_user'  => $buy_user,
                    'buy_price' => number_format($buy_price, 2),
                    'buy_qty'   => number_format($buy_qty),
                    'sell_id'   => $sell_id,
                    'sell_user' => $sell_user,
                    'sell_price'=> number_format($sell_price, 2),
                    'sell_qty'  => number_format($sell_qty),
                    'date'      => $equity->updated_at->toFormattedDateString()
                );

                array_push($pair_box, $data); 
            }
        }else{
            $pair_box = [];
        }

        // return json response in array box
        return response()->json($pair_box);
    }

    /*
    |-----------------------------------------
    | load stock balance 
    |-----------------------------------------
    */
    public function loadStockBal()
    {
        // get user stock balance 
        $id = Auth::user()->id;

        // fetch all stocks
        $stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();
        $stock_basket = [];

        // Realize profit
        $realize_profit = []; 

        foreach ($stocks as $stock) {
            # code...
            // get last price from pairs
            $equity_market = Security::where('security', $stock->name)->get();

            foreach ($equity_market as $market) {
                // get all the W/A
                if($stock->w_average > $market->close_price){
                    $wa = $stock->w_average - $market->close_price;
                }elseif ($stock->w_average < $market->close_price) {
                    $wa = $market->close_price - $stock->w_average;
                }elseif($stock->w_average = $market->close_price){
                    $wa = $market->close_price - $stock->w_average;
                }

                // stock weighted average * stock
                $swa =  $wa * $stock->qty;

                # code...
                $data = array(
                    "equity"    => $stock->name,
                    "price"     => (float)$market->close_price,
                    "stock"     => (float)$stock->qty,
                    "w_average" => number_format($stock->w_average, 2),
                    "amount"    => $stock->qty * $market->close_price,
                    "swa"       => $swa,
                    "date"      => $market->updated_at->diffForHumans()
                );
                array_push($stock_basket, $data);
            }
        }

        return response()->json($stock_basket);
    }

    /*
    |-----------------------------------------
    | load orders
    |-----------------------------------------
    */
    public function loadOrders()
    {
        # code...
        $user_id = Auth::user()->id;

        $orders = Order::where('user_id', $user_id)->get();
        $orders_box = [];
        foreach ($orders as $order) {
            # code...
            $asset = Security::where('id', $order->sec_id)->first();
            $data = array(
                'id'        => $order->id,
                'sec_id'    => $order->sec_id,
                'asset'     => $asset->security,
                'user_id'   => $order->user_id,
                'ref_id'    => $order->ref_id,
                'price'     => number_format($order->price, 2),
                'qty'       => number_format($order->qty),
                'type'      => $order->type,
                'option'    => $order->option,
                'date'      => $order->created_at->diffForHumans()
            );

            array_push($orders_box, $data);
        }

        // return response
        return response()->json($orders_box);
    }

    /*
    |-----------------------------------------
    | load total stock qty
    |-----------------------------------------
    */
    public function countTotalStock()
    {
        // get user stock balance 
        $id = Auth::user()->id;

        // clean stock balance
        // fetch all stocks
        $stocks = Stock::where('user_id', $id)->sum('qty');
        
        // data
        $data = array(
            'total_stock' => number_format($stocks)
        );

        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | account balance JSON
    |-----------------------------------------
    */
    public function accountBalance()
    {
        // return user account balance
        $id = Auth::user()->id;
        $balance = Account::where('user_id', $id)->first();

        // get user profile image
        $basic = Basic::where('user_id', $id)->first();

        $data = array(
            "account_balance" => number_format($balance->account_balance, 2),
            "profile_image"   => $basic->avatar
        );

        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load news JSON
    |-----------------------------------------
    */
    public function newsUpdates()
    {
        // fetch all recent news
        $latest_news = News::orderBy('id','desc')->take('10')->get();

        $news_box = [];
        foreach ($latest_news as $news) {
            # code...
            $data = array(
                'news_title' => $news->title,
                'news_body' => $news->body,
                'news_date' => $news->created_at->diffForHumans() 
            );

            array_push($news_box, $data);
        }

        // Return json url response for news
        return response()->json($news_box);
    }

    /*
    |-----------------------------------------
    | load single transaction details
    |-----------------------------------------
    */
    public function viewTransaction($id){

        $transactions = Transaction::where('id', $id)->first();

        if($transactions->stock_trade == 'sell'){
            $total = $transactions->stock_amount - $transactions->com_amount;
        }elseif($transactions->stock_trade == 'buy'){
            $total = $transactions->stock_amount + $transactions->com_amount;
        }

        $data = array(
            'user_id'      => $transactions->user_id,
            'stock_name'   => $transactions->stock_name,
            'stock_unit'   => number_format($transactions->stock_unit, 2),
            'stock_qty'    => number_format($transactions->stock_qty),
            'stock_trade'  => $transactions->stock_trade,
            'stock_amount' => number_format($transactions->stock_amount, 2),
            'stock_timing' => $transactions->stock_timing,
            'com_percent'  => number_format($transactions->com_percent, 4),
            'com_amount'   => number_format($transactions->com_amount, 2),
            'net_total'    => number_format($total, 2),
            'date'         => $transactions->created_at->diffForHumans()
        );

        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load transaction
    |-----------------------------------------
    */
    public function loadAllTransaction(){
        # code...
        $id = Auth::user()->id;
        // find transactions 
        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->get();

        if(count($all_transactions) > 0){
            $transaction_box = [];
            foreach($all_transactions as $transaction){
                $data = array(
                    'stock_name'    => $transaction->stock_name,
                    'stock_unit'    => number_format($transaction->stock_unit, 2),
                    'stock_qty'     => number_format($transaction->stock_qty),
                    'stock_trade'   => $transaction->stock_trade,
                    'stock_amount'  => number_format($transaction->stock_amount, 2),
                    'stock_date'    => $transaction->created_at->diffForHumans()
                );

                array_push($transaction_box, $data);
            }
        }else{
            $transaction_box = [];
        }

        // Json Url Response for transactions notifications
        return response()->json($transaction_box);
    }

    /*
    |-----------------------------------------
    | load transactions JSON
    |-----------------------------------------
    */
    public function transactionsUpdates()
    {
        $id = Auth::user()->id;
        // find transactions 
        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('1')->get();

        if(count($all_transactions) > 0){
            $transaction_box = [];
            foreach($all_transactions as $transaction){
                $data = array(
                    'stock_name' => $transaction->stock_name,
                    'stock_unit' => number_format($transaction->stock_unit, 2),
                    'stock_qty' => number_format($transaction->stock_qty),
                    'stock_trade' => $transaction->stock_trade,
                    'stock_amount' => number_format($transaction->stock_amount, 2),
                    'stock_date' => $transaction->created_at->diffForHumans()
                );

                array_push($transaction_box, $data);
            }
        }else{
            $transaction_box = [];
        }

        // Json Url Response for transactions notifications
        return response()->json($transaction_box);
    }

    /*
    |-----------------------------------------
    | load stocks JSON
    |-----------------------------------------
    */
    public function stocksUpdates()
    {
        $id = Auth::user()->id;
        // find transactions 
        $all_stocks = Stock::where('user_id', $id)->orderBy('id', 'desc')->get();

        // count stock balance only
        $count_stock = Stock::where([['user_id', $id], ['qty','>','0']])->get();

        $stock_box = [];
        $total_stock = count($count_stock);
        foreach ($all_stocks as $stocks) {
            if($stocks->qty > 0){
                # code...
                $data = array(
                    'name'      => $stocks->name,
                    'qty'       => number_format($stocks->qty),
                    'price'     => number_format($stocks->price, 2),
                    'total_qty' => $total_stock
                );
                array_push($stock_box, $data);
            }   
        }

        // Json Url Response for transactions notifications
        return response()->json($stock_box);
    }

    /*
    |-----------------------------------------
    | load pairs JSON
    |-----------------------------------------
    */
    public function pairsIndexUpdates()
    {
        // load pairs
        $pairs = Security::all();

        $pair_box = [];
        foreach ($pairs as $pair) {
            # code...
            $data = array(
                "id" => $pair->id,
                "pairs" => $pair->security,
                "open" => number_format($pair->open_price, 2),
                "close" => number_format($pair->close_price, 2)
            );
            array_push($pair_box, $data);
        }

        // return json response in array box
        return response()->json($pair_box);
    }

    /*
    |-----------------------------------------
    | load Equity Market in Json
    |-----------------------------------------
    */
    public function loadEquityMarket()
    {
    	// load securities
    	$all_equities = Security::all();

    	$equities_box = [];
    	foreach ($all_equities as $equity) {
    		$data = array(
    			'id' 				=> $equity->id,
				'board' 			=> $equity->board,
				'security' 			=> $equity->security,
				'ref_price' 		=> number_format($equity->ref_price, 2),
				'open_price' 		=> number_format($equity->open_price, 2),
				'high_price' 		=> number_format($equity->high_price, 2),
				'low_price' 		=> number_format($equity->low_price, 2),
				'close_price' 		=> number_format($equity->close_price, 2),
				'change_price' 		=> number_format($equity->change_price, 2),
				'daily_volume' 		=> number_format($equity->daily_volume, 2),
				'daily_value' 		=> number_format($equity->daily_value, 2),
				'dvmv_today' 		=> $equity->dvmv_today,
				'mvtn_trade' 		=> $equity->mvtn_trade,
				'previous_close' 	=> number_format($equity->previous_close, 2),
				'no_of_trade' 		=> number_format($equity->no_of_trade),
				'status' 			=> $equity->status,
				'date'				=> $equity->updated_at->toFormattedDateString()
    		);

    		array_push($equities_box, $data);
    	}

    	return response()->json($equities_box);
    }

    /*
    |-----------------------------------------
    | process trade in Json
    |-----------------------------------------
    */
    public function trade(Request $request, Transaction $transaction)
    {
        // get the current logged in user
        $user_id    = $request->trader_id;

        if($user_id == Auth::user()->id || empty($user_id)){
            $data = [
                'status'    => 'error',
                'message'   => 'You are not allow to trade as a broker!'
            ];

            // return response.
            return response()->json($data);
        }
        $guest_user = User::where("id", $user_id)->first();
        $email      = $guest_user->email;

        // request sec id
        $pid        = $request->pid;
        $pair_name  = $request->pn;

        // get trade qty
        $trade_qty  = $request->qty;
        $trade_type = $request->trade_type; 

        // buy section
        $buy_price  = $request->buy_price;
        $buy_id     = $request->buy_id;

        // sell section
        $sell_price = $request->sell_price;
        $sell_id    = $request->sell_id;

        // scan trade price 
        $buy_price  = str_replace(',', '', $buy_price);
        $sell_price = str_replace(',', '', $sell_price);
        if(empty($trade_qty)){
            // Send user a message !
            $msg = 'Kindly enter quantity!';
            $data = [
                'status'  => 'error',
                'message' => $msg
            ];
            
            // return response.
            return response()->json($data);
        }else{
            // proceed to buying stock
            if($trade_type == "buy"){

                $order = Order::where('id', $buy_id)->first();
                if($order !== null){
                    if($order->user_id == $user_id){
                        $data = [
                            'status'    => 'error',
                            'message'   => 'You can trade on your own order!'
                        ];

                        // return response.
                        return response()->json($data);
                    }elseif($trade_qty > $order->qty){

                        $data = [
                            'status'    => 'error',
                            'message'   => 'You can only purchase '.number_format($order->qty).' at the moment!'
                        ];

                        // return response.
                        return response()->json($data);
                    }else{
                        
                        $nano = new Nano();
                        $data = $nano->settleBuyTrade($request);

                        // return response.
                        return response()->json($data);
                    }
                }else{
                    $data = [
                        'status'    => 'error',
                        'message'   => 'This order is not available...'
                    ];

                    // return response.
                    return response()->json($data);
                }
            }elseif($trade_type == "sell"){
                // calculate consideration
                $sell_amount = $trade_qty * $sell_price;

                // charge percentange on consideration
                $percentange_charge = 0.02025;
                $commission_charge  = ($percentange_charge * $sell_amount) + 4;
                $expected_total     = $sell_amount - $commission_charge;

                // check if user have the stock..
                $stocks = Stock::where([['name', $pair_name], ['user_id', $user_id]])->get();

                // check if stock is less than request qty
                if(count($stocks) > 0){
                    
                    // get the temp stock id
                    foreach ($stocks as $tem_stock) {
                        # catch the current stock id
                        $tem_id = $tem_stock->id;
                        $tem_qty = $tem_stock->qty;

                        // check if old qty is less than requested trade qty
                        if($tem_qty < $trade_qty){

                            $msg = $pair_name.' Trade Unsuccessful due to low stock balance';
                            $data = [
                                'status'  => 'error',
                                'message' => $msg
                            ];
                            
                            // return response
                            return response()->json($data);

                        }else{

                            // save commission charges
                            $commission          = new Commission();
                            $commission->user_id = $user_id;
                            $commission->percent = $percentange_charge;
                            $commission->qty     = $trade_qty;
                            $commission->type    = $trade_type;
                            $commission->amount  = $commission_charge;
                            $commission->save();

                            // update transactions logs
                            $transaction               = new Transaction();
                            $transaction->user_id      = $user_id;
                            $transaction->stock_name   = $pair_name;
                            $transaction->stock_unit   = $sell_price;
                            $transaction->stock_qty    = $trade_qty;
                            $transaction->stock_trade  = $trade_type;
                            $transaction->stock_amount = $sell_amount;
                            $transaction->stock_timing = time();
                            $transaction->com_percent  = $percentange_charge;
                            $transaction->com_amount   = $commission_charge;
                            $transaction->save();

                            // get account id
                            $user_account = Account::where('user_id', $user_id)->first();
                            // update user account balance
                            $update_account = Account::find($user_account->id);
                            $update_account->account_balance = $update_account->account_balance + $expected_total;
                            $update_account->update();

                            // return account balance updates
                            $account_balance = Account::where('user_id', $user_id)->first();
                            $last_balance    = number_format($account_balance->account_balance, 2);
                            // account data 
                            $acct_data = array(
                                'user_id' => $user_id,
                                'bal'     => $last_balance
                            );
                            

                            // Get transactions 
                            $transaction = Transaction::where('user_id', $user_id)->orderBy('id', 'desc')->take('1')->first();
                            $trans_box = array(
                                'user_id'      => $user_id,
                                'stock_name'   => $transaction->stock_name,
                                'stock_unit'   => number_format($transaction->stock_unit, 2),
                                'stock_qty'    => number_format($transaction->stock_qty),
                                'stock_trade'  => $transaction->stock_trade,
                                'stock_amount' => number_format($transaction->stock_amount, 2),
                                'stock_date'   => $transaction->created_at->diffForHumans()
                            );

                            // now update if stock exist
                            $stocks = Stock::find($tem_id);
                            $stocks->qty = $stocks->qty - $trade_qty; // keypiece when users sell-> qty update
                            $stocks->update();

                            // find stock balance 
                            $all_stocks  = Stock::where('user_id', $user_id)->orderBy('id', 'desc')->get();
                            // count stock balance only
                            $count_stock = Stock::where([['user_id', $user_id], ['qty','>','0']])->get();
                            $total_stock = count($count_stock);

                            $stock_box = [];
                            foreach ($all_stocks as $stocks) {
                                if($stocks->qty > 0){
                                    # code...
                                    $data = array(
                                        'user_id'   => $user_id,
                                        'id'        => $stocks->id,
                                        'name'      => $stocks->name,
                                        'qty'       => number_format($stocks->qty),
                                        'price'     => number_format($stocks->price, 2),
                                        'total_qty' => $total_stock
                                    );
                                    array_push($stock_box, $data);
                                }   
                            }

                            // Fire Stock balance info
                            \Event::fire(new StockBalanceNotifications($stock_box));
                            // Fire Account Balance
                            \Event::fire(new AccountUpdate($acct_data));
                            // Fire Transactions info
                            \Event::fire(new TransactionNotifications($trans_box));

                            // get stock w/a on statements 
                            $stocks = Stock::where([['name', $pair_name], ['user_id', $user_id]])->get();
                            foreach ($stocks as $stock) {
                                # code...
                                $w_a = $stock->w_average; // w/a

                                // remove negative return...
                                if($w_a > $sell_price){
                                    $gap = $w_a - $sell_price; // trade gap on w/a
                                }elseif($w_a < $sell_price){
                                    $gap = $sell_price - $w_a; // trade gap on w/a
                                }elseif($w_a == $sell_price) {
                                    $gap = $w_a - $sell_price; // trade gap on w/a
                                }

                                $f1 = $w_a * $trade_qty; // on weighted average (holding value)
                                $f2 = $sell_price * $trade_qty; // on demand (market value)


                                if($f1 > $f2){
                                    $comments = " trade Successful with loss gap of ".$gap;
                                    $status = "loss";
                                }elseif($f2 > $f1){
                                    $comments = " trade Successful with profit gap of ".$gap;
                                    $status = "profit";
                                }elseif($f2 == $f1){
                                    $comments = " trade Successful with no profit/loss with return gap of ".$gap;
                                    $status = "break-even";
                                }


                                $top_bal = $gap * $trade_qty;
                        
                                // get id and update state
                                $account_statements                     = new Statement();
                                $account_statements->equity             = $pair_name;
                                $account_statements->w_average          = $w_a;
                                $account_statements->user_id            = $user_id;
                                $account_statements->last_sold_price    = $sell_price;
                                $account_statements->last_sold_qty      = $trade_qty;
                                $account_statements->amount             = $sell_amount;
                                $account_statements->gap                = $gap;
                                $account_statements->balance            = $top_bal;       
                                $account_statements->status             = $status;
                                $account_statements->comments           = $comments;
                                $account_statements->save();
                            }

                            // a little house cleaning..
                            $stocks = Stock::where([['user_id', $user_id], ['qty', 0]])->first();
                            if($stocks !== null){
                                $refresh_stocks = Stock::find($stocks->id);
                                $refresh_stocks->delete();
                            }
                            
                            // proceed to trade
                            $msg = ' Sold '.$pair_name.' at '.number_format($sell_price, 2).', Trade Successful ';
                            
                            // response
                            $res_msg = array(
                                'status'  => 'success',
                                'message' => $msg
                            );
                            
                            return $res_msg; 
                        } 
                    }
                     
                }else{

                    $msg = $pair_name.' Trade Unsuccessful, you do not have this stock for sale ';
                    $data = [
                        'status'  => 'error',
                        'message' => $msg
                    ];
                    
                    // return response
                    return response()->json($data); 
                }
            }else{
                // Send user a message !
                $msg = 'Failed, No trade type specified!';
                $data = [
                    'status'  => 'error',
                    'message' => $msg
                ];
                
                // return response.
                return response()->json($data, 401);
            }
        }
    }

    /*
    |-----------------------------------------
    | check account status
    |-----------------------------------------
    */
    public function activationStatus()
    {

        $email = Auth::user()->email;

        // check if users account is activated 
        $account_status = Activation::where([['email', $email], ['status', 'active']])->first();
        if($account_status ==  null){
            $data = array(
                'status'  => 'error',
                'message' => 'check '.$email.' and activate your account! | <a href="/resend/activation">resend</a>'
            );
            return response()->json($data);
        }
    }

    /*
    |-----------------------------------------
    | check invitations status 
    |-----------------------------------------
    */
    public function invitationsStatus()
    {

        // check if logged in users has been invited 
        $email = Auth::user()->email;

        // check if logged user has invitations 
        $invites = Invitation::where([['guest', $email], ['status', 'no']])->get();

        $invite_box = [];
        foreach ($invites as $invite) {
            # code...
            // get host name
            $host_info = User::where('email', $invite->host)->first();
            $msg = array(
                'id'   => $invite->id,
                'host' => $host_info->name,
                'date' => $invite->created_at->diffForHumans()
            );

            array_push($invite_box, $msg);
        }
        return response()->json($invite_box);  
    }

    /*
    |-----------------------------------------
    | load joined groups
    |-----------------------------------------
    */
    public function loadAcceptedGroups()
    {
        // logged user
        $email = Auth::user()->email;
        
        // grouped user
        $groups = Group::all();


        // return $groups;
        $group_box = [];
        // scan group
        foreach ($groups as $group) {
            # scan members inside the group
            $group_check = explode(',', $group->members);
            
            for ($i=0; $i < count($group_check); $i++) { 
                # code...
                $group_check[$i] = str_replace(' ', '', $group_check[$i]);
                if($group_check[$i] == $email){

                    $data = array(
                        'id'          => $group->id,
                        'group_name'  => $group->name
                    );

                    array_push($group_box, $data);
                }
            }
        } 

        return $group_box; 
    }

    /*
    |-----------------------------------------
    | load group informations
    |-----------------------------------------
    */
    public function loadMembersInfo($id)
    {
        # Get Groups and Memebers information
        $groups = Group::where('id', $id)->first();

        # Information...
        $all_emails = [];
        // search and find users
        $split_memebers = explode(',', $groups->members);

        // filter string
        for ($i=0; $i < count($split_memebers); $i++) { 
            # code...
            $split_memebers[$i] = str_replace(' ', '', $split_memebers[$i]);

            if($user_name = User::where('email', $split_memebers[$i])->first()){
                if($user_account = Account::where('user_id', $user_name->id)->first()){
                    $data = array(
                        'net_worth' => number_format($user_account->account_balance, 2),
                        'name'  => $user_name->name,
                        'email' => $split_memebers[$i]
                    );
                }
            }

            

            array_push($all_emails, $data);
        }
        return $all_emails; 
    }

    /*
    |-----------------------------------------
    | load users ranking 
    |-----------------------------------------
    */
    public function loadRanking()
    {
        // first get all users 
        $all_users = User::all();

        // realized & unrealized data
        $rank_box = [];
        $realized = []; 
        $unrealized = [];
        // check all users 
        foreach ($all_users as $user) {

            # code... for financial statements
            // get catss statements information (Profit Section)
            $profit_statements = Statement::where([['user_id', $user->id], ['status', 'profit']])->get();

            // load profit statements
            $stmt_profit_box = [];
            foreach ($profit_statements as $stats) {
                # code...
                $profit_data = array(
                    'amount' => $stats->balance
                );

                array_push($stmt_profit_box, $profit_data);
            }

            // return response in sum
            $stmt_sum_profit = collect($stmt_profit_box);
            $stmt_net_profit = $stmt_sum_profit->sum('amount');


            // get catss statements information (loss Section)
            $loss_statements = Statement::where([['user_id', $user->id], ['status', 'loss']])->get();

            // load profit statements
            $stmt_loss_box = [];
            foreach ($loss_statements as $stats) {
                # code...
                $profit_data = array(
                    'amount' => $stats->balance
                );

                array_push($stmt_loss_box, $profit_data);
            }

            // return response in sum
            $stmt_sum_loss = collect($stmt_loss_box);
            $stmt_net_loss = $stmt_sum_loss->sum('amount');

            // statement net
            $stmt_total = $stmt_net_profit - $stmt_net_loss;

            # code...
            $stocks = Stock::where('user_id', $user->id)->orderBy('id', 'desc')->get();
            $profit_basket = []; 
            $loss_basket   = [];

            // scanned stocks
            foreach ($stocks as $stock) {
                // get last price from pairs
                $equity_market = Security::where('security', $stock->name)->get();
                foreach ($equity_market as $market) {
                    // market price vs w/a price
                    $market_amount = $stock->qty * $market->close_price; // at market price
                    $holding_amount = $stock->qty * $stock->w_average; // at closing price

                    if($market_amount > $holding_amount){
                        // market price is up
                        $profit = $market_amount - $holding_amount;
                        // Calculate Profit or loss from ongoin market
                        $profit_data = array(
                            'profit' => $profit
                        );

                        // push to array
                        array_push($profit_basket, $profit_data);

                    }elseif($market_amount < $holding_amount){
                        // holding is up
                        $loss = $holding_amount - $market_amount;

                        // calculate loss
                        $loss_data = array(
                            'loss' => $loss
                        );

                        // push to array
                        array_push($loss_basket, $loss_data);
                    }
                }
            }

            // all to collections
            $loss_collection = collect($loss_basket);
            $profit_collection = collect($profit_basket);

            // total loss & profit
            $total_loss   = $loss_collection->sum('loss');
            $total_profit = $profit_collection->sum('profit');

            // get net profit 
            $net_total = $total_profit - $total_loss;

            // fin. statement report data
            $statement_data = array(
                'user'   => $user->name,
                'profit' => $stmt_net_profit,
                'loss'   => $stmt_net_loss,
                'net'    => $stmt_total
            );

            // revaluation data
            $revauled_data = array(
                'user'   => $user->name,
                'profit' => $total_profit,
                'loss'   => $total_loss,
                'net'    => $net_total,
                'date'   => date("D M d, Y ")
            );

            // calculate rank profit
            $rank_total = $net_total + $stmt_total;

            // ranking data 
            $rank_data = array(
                'id'     => $user->id,
                'user'   => $user->name,
                'profit' => $rank_total,
                'date'   => date("D M d, Y ")
            );

            // array sold & reavaluation box
            array_push($realized, $statement_data);
            array_push($unrealized, $revauled_data);

            if($rank_total !== 0){
                // ranking box
                array_push($rank_box, $rank_data);
            }
        }

        $final_summary = array(
            'ranking'    => $rank_box
        );

        return response()->json($final_summary);
        // return response()->json($rank_box);
    }

    /*
    |-----------------------------------------
    | load trading stats
    |-----------------------------------------
    */
    public function loadTradingStat()
    {
        $user_id = Auth::user()->id;
        // first get all users 
        $all_users = User::where('id', $user_id)->get();
        $rank_box = [];

        // check all users 
        foreach ($all_users as $user) {
            # code...
            $stocks = Stock::where('user_id', $user->id)->orderBy('id', 'desc')->get();
            $profit_basket = []; 
            $loss_basket = [];

            foreach ($stocks as $stock) {
                // get last price from pairs
                $equity_market = Security::where('security', $stock->name)->get();

                foreach ($equity_market as $market) {
                    // market price vs w/a price
                    $market_amount = $stock->qty * $market->close_price; // at market price
                    $holding_amount = $stock->qty * $stock->w_average; // at closing price

                    if($market_amount > $holding_amount){
                        // market price is up
                        $profit = $market_amount - $holding_amount;
                        // Calculate Profit or loss from ongoin market
                        $profit_data = array(
                            'profit' => $profit
                        );

                        // push to array
                        array_push($profit_basket, $profit_data);

                    }elseif($market_amount < $holding_amount){
                        // holding is up
                        $loss = $holding_amount - $market_amount;

                        // calculate loss
                        $loss_data = array(
                            'loss' => $loss
                        );

                        // push to array
                        array_push($loss_basket, $loss_data);
                    }
                }
            }

            // all to collections
            $loss_collection = collect($loss_basket);
            $profit_collection = collect($profit_basket);

            // total loss & profit
            $total_loss = $loss_collection->sum('loss');
            $total_profit = $profit_collection->sum('profit');

            // net profit / loss
            if($total_profit > $total_loss){
                // when profit is made
                $net_profit = $total_profit - $total_loss;
                $net_loss   = null;
            }elseif($total_profit < $total_loss){
                // when loss is made
                $net_loss   = $total_loss - $total_profit;
                $net_profit = null;
            }

            // readable numeric
            $total_loss   = number_format($total_loss, 2);
            $total_profit = number_format($total_profit, 2);
            // $net_profit   = number_format($net_profit, 2);
            // $net_loss     = number_format($net_loss, 2);

            // return response
            $data = array(
                'user'       => $user->name,
                'profit'     => $total_profit,
                'loss'       => $total_loss,
                'net_loss'   => $net_loss,
                'net_profit' => $net_profit,
                'date'       => date("D M 'Y ")
            );

            array_push($rank_box, $data);
        }
        return response()->json($rank_box);
    }

    /*
    |-----------------------------------------
    | compute Financial Statment
    |-----------------------------------------
    */
    public function profitRevaluation()
    {
        # code...
        $id = Auth::user()->id;

        // GET FIN. STATEMENT PROFIT SECTION
        $profit_statements = Statement::where([['user_id', $id], ['status', 'profit']])->get();
        // load profit statements
        $stmt_profit_box = [];
        foreach ($profit_statements as $stats) {
            # code...
            $profit_data = array(
                'amount' => $stats->balance
            );

            array_push($stmt_profit_box, $profit_data);
        }

        // return response in sum
        $stmt_sum_profit = collect($stmt_profit_box);
        $stmt_net_profit = $stmt_sum_profit->sum('amount');

        // GET FIN. STATEMENT LOSS SECTION
        $loss_statements = Statement::where([['user_id', $id], ['status', 'loss']])->get();

        // load profit statements
        $stmt_loss_box = [];
        foreach ($loss_statements as $stats) {
            # code...
            $profit_data = array(
                'amount' => $stats->balance
            );

            array_push($stmt_loss_box, $profit_data);
        }

        // return response in sum
        $stmt_sum_loss = collect($stmt_loss_box);
        $stmt_net_loss = $stmt_sum_loss->sum('amount');

        // statement net
        $stmt_total = $stmt_net_profit - $stmt_net_loss;

        # STOCK STATMENT BALANCE SECTION
        $stocks = Stock::where('user_id', $id)->get();
        $profit_basket = []; 
        $loss_basket   = [];

        // scanned stocks
        foreach ($stocks as $stock) {
            // get last price from pairs
            $equity_market = Security::where('security', $stock->name)->get();

            foreach ($equity_market as $market) {
                // market price vs w/a price
                $market_amount = $stock->qty * $market->close_price; // at market price
                $holding_amount = $stock->qty * $stock->w_average; // at closing price

                if($market_amount > $holding_amount){
                    // market price is up
                    $profit = $market_amount - $holding_amount;
                    // Calculate Profit or loss from ongoin market
                    $profit_data = array(
                        'profit' => $profit
                    );

                    // push to array
                    array_push($profit_basket, $profit_data);

                }elseif($market_amount < $holding_amount){
                    // holding is up
                    $loss = $holding_amount - $market_amount;

                    // calculate loss
                    $loss_data = array(
                        'loss' => $loss
                    );

                    // push to array
                    array_push($loss_basket, $loss_data);
                }
            }
        }

        // all to collections
        $loss_collection = collect($loss_basket);
        $profit_collection = collect($profit_basket);

        // total loss & profit
        $total_loss   = $loss_collection->sum('loss');
        $total_profit = $profit_collection->sum('profit');
        
        // $net_total = $total_profit - $total_loss;
        $all_profit = $stmt_net_profit + $total_profit;
        $all_loss = $stmt_net_loss + $total_loss;

        // get net profit 
        if($all_profit > $all_loss){
            $tag = 'profit';
        }else{
            $tag = 'loss';
        }

        // net balance 
        $net_balance = $all_profit - $all_loss;

        // COMPUTE ASSETS (EQUITIES) VALUE  WORTH 
        $final_summary = array(
            'profit' => $stmt_net_profit,
            'loss'   => $stmt_net_loss,
            'net'    => $stmt_net_profit - $stmt_net_loss,
            'bal'    => $net_balance,
            'tag'    => $tag,
        );

        return response()->json($final_summary);
    }

    /*
    |-----------------------------------------
    | load wallets balance
    |-----------------------------------------
    */
    public function loadWallets()
    {
        # code...
        $user_id = Auth::user()->id;

        // check wallets
        $wallet = Wallet::where('user_id', $user_id)->first();
        if($wallet !== null){
            // data to array
            $data = array(
                'balance' => number_format($wallet->balance, 2)
            );
        }else{
            // data to array
            $data = array(
                'balance' => number_format(0.000, 2)
            );
        }

        // return data 
        return response()->json($data);
    }
 
    /*
    |-----------------------------------------
    | set price alert 
    |-----------------------------------------
    */
    public function setPriceAlert(Request $request)
    {
        # code...
        $user_id    = Auth::user()->id;
        $security   = $request->security;
        $price      = $request->price;

        // check if user already set price alert
        $check_already_set = Alert::where([['user_id', $user_id], ['security', $security]])->first();
        if($check_already_set == null){

            $set_price_alert            = new Alert();
            $set_price_alert->user_id   = $user_id;
            $set_price_alert->security  = $security;
            $set_price_alert->price     = $price;
            $set_price_alert->type      = "price-alert";
            $set_price_alert->save();

            // get last updated 
            $last_alert_set = Alert::where('user_id', $user_id)->latest()->first();

            if($last_alert_set !== null){
                $data = [
                    'id'        => $last_alert_set->id,
                    'user_id'   => $last_alert_set->user_id,
                    'type'      => $last_alert_set->type,
                    'security'  => $last_alert_set->security,
                    'price'     => number_format($last_alert_set->price, 2),
                    'date'      => $last_alert_set->created_at->diffForHumans()
                ];

                // fire events
                \Event::fire(new PriceAlert($data));
            }

            $data = [
                'status'    => 'success',
                'message'   => 'Price alert has been set successfully !'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Price alert already set to '.number_format($check_already_set->price, 2).' !'
            ];
        }

        // return response 
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | set watch list
    |-----------------------------------------
    */
    public function setWatchList(Request $request)
    {
        # code...
        $security = $request->security;
        $user_id  = $request->userid;

        // get details for transactions on 
        $transactions = Transaction::where('stock_name', $security)->get();
        $trade_count  = count($transactions);

        // get last traded price
        $last_traded_price = Transaction::where('stock_name', $security)->latest()->first();
        if($last_traded_price == null){

            // get price from the market
            $market = Security::where('security', $security)->first();
            $price = $market->close_price;
        
        }else{

            $price = $last_traded_price->stock_unit;
        }

        // check if already exits
        $watch_list = WatchList::where([['security', $security], ['user_id', $user_id]])->first();
        if($watch_list == null){
            // add new security to watch
            $watch_sec              = new WatchList();
            $watch_sec->user_id     = $user_id;
            $watch_sec->trade       = $trade_count;
            $watch_sec->security    = $security;
            $watch_sec->price       = $price;
            $watch_sec->save();

            $data = [
                'status'    => 'success',
                'message'   => $security.' added to watch list !'
            ];

        }else{

            $data = [
                'status'    => 'error',
                'message'   => $security.' already added to watch list !'
            ];
        }

        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load watch list
    |-----------------------------------------
    */
    public function loadWatchList(Request $request)
    {
        # code...
        $user_id = Auth::user()->id;
        
        $all_watchlist = WatchList::where('user_id', $user_id)->orderBy('id', 'desc')->get();
        if(count($all_watchlist) > 0){
            $list_box = [];
            foreach ($all_watchlist as $list) {
                # code...
                // get details for transactions on 
                $transactions = Transaction::where('stock_name', $list->security)->get();
                $trade_count  = count($transactions);

                // get last traded price
                $last_traded_price = Transaction::where('stock_name', $list->security)->latest()->first();
                if($last_traded_price == null){

                    // get price from the market
                    $market = Security::where('security', $list->security)->first();
                    $price = $market->close_price;
                
                }else{

                    $price = $last_traded_price->stock_unit;
                }

                $gap = $list->trade - $trade_count;

                $data = [
                    'id'        => $list->id,
                    'trade'     => $trade_count,
                    'security'  => $list->security,
                    'price'     => number_format($price, 2),
                    'gap'       => $gap,
                    'date'      => $list->created_at->diffForHumans()
                ];

                array_push($list_box, $data);
            }
        }else{
            $list_box = [];
        }

        return response()->json($list_box);
    }

    /*
    |-----------------------------------------
    | cancel watchlist
    |-----------------------------------------
    */
    public function cancelWatchList(Request $request)
    {
        $user_id = Auth::user()->id;
        $item_id = $request->itemid;

        // check if item id exits 
        $watch_list = WatchList::where('id', $item_id)->first();
        if($watch_list !== null){

            // update and delete
            $update_watchlist = WatchList::find($item_id);
            $update_watchlist->delete();

            $data = [
                'status'    => 'success',
                'message'   => 'item remove successfully !'
            ];

        }else{

            $data = [
                'status'   => 'error',
                'message'  => 'failed !, item does not exits !'
            ];
        }

        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load all security
    |-----------------------------------------
    */
    public function allSecurity()
    {
        # code...
        $all_security = Security::all();
        if(count($all_security) > 0){
            
            $sec_box = [];
            foreach ($all_security as $security) {
                # code...
                $data = [
                    'id'        => $security->id,
                    'security'  => $security->security,
                    'price'     => number_format($security->new_band, 2)
                ];

                array_push($sec_box, $data);
            }

        }else{
            
            $sec_box = [];
        }

        // return response 
        return response()->json($sec_box);
    }

    /*
    |-----------------------------------------
    | load price alert
    |-----------------------------------------
    */
    public function loadPriceAlert()
    {
        # code...
        $user_id = Auth::user()->id;
        $all_price_alert = Alert::where([['user_id', $user_id], ['type', 'price-alert']])->get();
        if(count($all_price_alert) > 0){
            $alert_box = [];
            foreach ($all_price_alert as $p_alert) {
                # code...
                $data = [
                    'id'        => $p_alert->id,
                    'user_id'   => $p_alert->user_id,
                    'type'      => $p_alert->type,
                    'security'  => $p_alert->security,
                    'price'     => number_format($p_alert->price, 2),
                    'date'      => $p_alert->created_at->diffForHumans()
                ];

                // return price alert
                array_push($alert_box, $data);
            }
        }else{
            $alert_box = [];
        }

        return response()->json($alert_box);
    }

    /*
    |-----------------------------------------
    | cancel price alert
    |-----------------------------------------
    */
    public function cancelAlert(Request $request)
    {
        # code...
        $id = $request->id;

        # update alert
        $cancel_alert = Alert::find($id);
        if($cancel_alert !== null){
            // delete price alert
            $cancel_alert->delete();

            $data = [
                'status'    => 'success',
                'message'   => 'Price alert remove successfully !'
            ];

        }else{
            
            $data = [
                'status'    => 'error',
                'message'   => 'Can not delete price alert !'
            ];
        }
            
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | load stock brokers
    |-----------------------------------------
    */
    public function loadStockBrokers()
    {
        $users = User::where("account_type", "broker")->get();
        if(count($users) > 0){
            $brokers_box = [];
            foreach ($users as $ul) {
                $data = [
                    'id'            => $ul->id,
                    'name'          => ucfirst($ul->name),
                    'email'         => $ul->email,
                    'account_id'    => $ul->account_id,
                    'rating'        => 0,
                    'firm'          => $ul->brokerage_firm
                ];

                array_push($brokers_box, $data);
            }
        }else{
            $brokers_box = [];
        }

        // return response.
        return response()->json($brokers_box);
    }

    /*
    |-----------------------------------------
    | hire a broker
    |-----------------------------------------
    */
    public function hireStockBroker(Request $request)
    {
        $user_id    = Auth::user()->id;
        $broker_id  = $request->broker_id;

        $client     = new Client();
        $data       = $client->hireBroker($user_id, $broker_id);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | fetch brokes clients
    |-----------------------------------------
    */
    public function fetchBrokersClient()
    {
        $user_id    = Auth::user()->id;
        $firm_id    = Auth::user()->brokerage_id;
        $clients    = new Client();
        $data       = $clients->getMyClients($user_id, $firm_id);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | switch account
    |-----------------------------------------
    */
    public function switchAccount(Request $request)
    {
        $client_email   = $request->client_email;
        $user_account   = new User();
        $data           = $user_account->switchUserAccount($client_email);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | add new client
    |-----------------------------------------
    */
    public function addNewClient(Request $request)
    {
        // body
        $broker     = new User();
        $data       = $broker->saveNewClient($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | fetch dashboard news
    |-----------------------------------------
    */
    public function fetchDashboardNews()
    {
        $news = News::orderBy('id','desc')->take('5')->get();

        // return response.
        return response()->json($news);
    }

    /*
    |-----------------------------------------
    | fetch user transactions
    |-----------------------------------------
    */
    public function getUserTransactions()
    {
        // find transactions 
        $id = Auth::user()->id;
        $all_transactions = Transaction::where('user_id', $id)->orderBy('id', 'desc')->take('5')->get();

        // return response.
        return response()->json($all_transactions);
    }

    /*
    |-----------------------------------------
    | fetch user account information
    |-----------------------------------------
    */
    public function getUserAccountInfo()
    {
        // fetch account information 
        $account_info = Account::where('user_id', $id)->get();

        // return response.
        return response()->json($account_info);
    }

    /*
    |-----------------------------------------
    | fetch groups
    |-----------------------------------------
    */
    public function getUserGroups()
    {
        // fetch group trade
        $groups = Group::where('user_id', $id)->get();

        // return response.
        return response()->json($groups);
    }

    /*
    |-----------------------------------------
    | fetch all banks
    |-----------------------------------------
    */
    public function listAllBanks(){
        // body
        // charge endpoint
        $endpoint   = 'https://api.paystack.co/bank';
        $headers    = array('Content-Type: application/json', 'Authorization: Bearer '.env("PS_SK_KEY"));

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $endpoint);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
        curl_setopt($ch, CURLOPT_TIMEOUT, 200);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $res = curl_exec($ch);

        // return response()->json($res); 
        $data = json_decode($res, true);

        // return
        return $data;

        curl_close($ch);
    }

    /*
    |-----------------------------------------
    | update user bank account
    |-----------------------------------------
    */
    public function updateBank(Request $request){
        $id = Auth::user()->id;
        // body
        $account    = new Account();
        $data       = $account->updateBankAccount($id, $request);

        // return response.
        return response()->json($data);
    }
}
