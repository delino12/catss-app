<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Events\PlaceOrder;
use CATSS\Events\TransactionNotifications;
use CATSS\Events\StockBalanceNotifications;
use CATSS\Events\AccountUpdate;
use CATSS\Events\PriceAlert;
use CATSS\Mail\OrderNotify;
use CATSS\Mail\WelcomeMail;
use CATSS\Mail\ContactAutoReplyAndroid;
use CATSS\Mail\ContactUsAndroid;
use CATSS\Mail\NewUser;
use CATSS\Transaction;
use CATSS\AndroidNotification;
use CATSS\Alert;
use CATSS\WatchList;
use CATSS\News;
use CATSS\Security;
use CATSS\Statement;
use CATSS\Rank;
use CATSS\Stock;
use CATSS\User;
use CATSS\Account;
use CATSS\Basic;
use CATSS\Activation;
use CATSS\Transfer;
use CATSS\Payment;
use CATSS\Vault;
use CATSS\Order;
use Auth;
use Hash;

class DroidApiController extends Controller
{
    /*
    |---------------------------------------------
    | API AUTHENTICATION
    |---------------------------------------------
    |
    */  
    public function __construct()
    {
        # code...
        $this->middleware('android')->except('greet');
        $this->middleware('trade.android')->only('handleBuyTrade', 'handleSellTrade');
    }

    /*
    |--------------------------------
    | WELCOME USER ENTRY POINT
    |--------------------------------
    |
    */
    public function greet(){

        $data = [
            "status"    => "success",
            "message"   => "This is the beginning for a new Age",
            "data"      => [
                "error"     => 0,
            ]
        ];
        return response()->json($data);
    }

    /*
    |--------------------------------
    | PUSH  NOTIFICATION
    |--------------------------------
    |
    */
    public function pushNotification(Request $request){
        $title  = $request->title;
        $body   = $request->body;


        $push = new AndroidNotification();
        $push_response = $push->fire($title, $body);

        return response()->json($push_response);
    }


    /*
    |--------------------------------
    | VERIFY AUTHENTICATED
    |--------------------------------
    |
    */
    public function verifyAuthenticated(Request $request)
    {

        # code...
        $user_id = $request->userid;
        $password = $request->password;
        
        $user = User::where("id", $user_id)->first();
        if($user !== null){
            if(Hash::check($password, $user->password)){
                $data = [
                    "status"    => "success",
                    "message"   => "password match !"
                ];
            }else{
                $data = [
                    "status"    => "error",
                    "message"   => "password did not match !"
                ];
            }
        }else{
            $data = [
                "status"    => "error",
                "message"   => "User not found !"
            ];
        }
            

        return response()->json($data);
    }

    /*
    |------------------------------------------
    | CREATE NEW ACCOUNT
    |------------------------------------------
    */
    public function signup(Request $request)
    {
        # verify user inputs
        $this->verifySignupInput($request);
        
		# code...
    	$name 		= $request->name;
    	$email 		= $request->email;
    	$password 	= $request->password;
    	$phone 		= $request->phone;

    	// encrypt password 
        $password = bcrypt($password);

        // check if already registered
        if($this->alreadyExist($email)){
        	// generate unique account id
	        $account_id     = "NTD".rand(000,999).rand(111,999);
	        $account_bal    = 100000.00; // set default account balance
	        $account_status = "open"; // account status set to open
	        $account_timing = time(); // account opening date

	        // save new user
	        $user             = new User();
	        $user->account_id = $account_id;
	        $user->name       = $name;
	        $user->email      = $email;
	        $user->password   = $password;
	        $user->save();

	        // get user info
	        $user = User::where('email', $email)->first();

	        // init account model
            $account                  = new Account();
            $account->user_id         = $user->id;
            $account->account_id      = $account_id;
            $account->account_balance = $account_bal;
            $account->account_status  = $account_status;
            $account->timing          = time();
            $account->save();

            // init a new basic informations
            $basic_info          = new Basic();
            $basic_info->user_id = $user->id;
            $basic_info->name    = $user->name;
            $basic_info->save();

            // set account for activations
            $activate           = new Activation();
            $activate->email    = $user->email;
            $activate->token    = rand(000, 999).'-'.rand(111, 555);
            $activate->status   = 'inactive';
            $activate->save();

            // get the users activations code details
            $activation_code = Activation::where('email', $email)->first();

            // build res data
            $mail_data = array(
                'id' 	=> $activation_code->id,
                'name' 	=> $name,
                'code' 	=> $activation_code->token
            );

            // send User an Email
        	\Mail::to($email)->send(new NewUser($mail_data));


	    	$data = array(
	    		'status' 	=> 'success',
	    		'message' 	=> 'registration successful !',
	    		'error'		=> 100
	    	);

            return $this->apiJsonResponse($data, 200);
        }else{
        	$data = array(
	    		'status' 	=> 'error',
	    		'message' 	=> 'user already exist !',
	    		'error'		=> 101
	    	);

            return $this->apiJsonResponse($data, 401);
        }
    }


    /*
    |------------------------------------------
    | LOGIN USER ACCOUNT
    |------------------------------------------
    */
    public function login(Request $request)
    {
    	# code...
        $email 		= $request->email;
        $password 	= $request->password;
    
        // login using collect 
        if (Auth::attempt(['email' => $email, 'password' => $password])) {
        	// get user id
        	$user = User::where('email', $email)->first();

            // Authentication passed...
            $data = array(
            	'status' 	=> 'success',
	    		'message' 	=> 'login successful !',
	    		'userid'	=> $user->id,
	    		'error'		=> 200
            );

            return response($data, $status = 200, $headers = [
                'Content-Type' => 'application/json'
            ]);

        }else{
        	$data = array(
	    		'status' 	=> 'error',
	    		'message' 	=> 'wrong email/password',
	    		'error'		=> 404
	    	);

            return response($data, $status = 404, $headers = [
                'Content-Type' => 'application/json'
            ]);
        }
    }


    /*
    |------------------------------------------
    | GET USER TRANSACTIONS LOGS
    |------------------------------------------
    */
    public function transactionLogs(Request $request)
    {
        // find transactions
        // user id 
        $user_id = $request->userid;
        $stock   = $request->stock;

        if($stock !== ""){
            $stock = strtoupper($stock);
        }else{
            $stock = null;
        }

        // fetch transactions
        $data = $this->scanTransactionLogs($user_id, $stock);

        // return response
        return response()->json($data);
    }



    /*
    |------------------------------------------
    | GET USER TRANSACTIONS LOGS
    |------------------------------------------
    */
    public function stocksBalance(Request $request)
    {
        // find transactions
        // user id 
        $user_id = $request->userid;
        $stock   = $request->stock;

        if($stock !== ""){
            $stock = strtoupper($stock);
        }else{
            $stock = null;
        }

        $account = Account::where('user_id', $user_id)->first();

        // fetch transactions
        $stock_data = $this->scanStockBalance($user_id, $stock);
        $stock_revaulation = $this->scanForProfit($user_id);
        

        // return response
        $data = [
            'account_balance'   => $account->account_balance,
            'stock_balance'     => $stock_data,
            'stock_revaulation' => $stock_revaulation
        ];

        // return response
        return response()->json($data);
    }

    /*
    |--------------------------------
    | LOAD WATCH LIST
    |--------------------------------
    |
    */
    public function loadWatchList(Request $request){

        $user_id = $request->userid;

        // fetch all stock on watchlist
        $all_watchlist = WatchList::where('user_id', $user_id)->orderBy('id', 'desc')->get();
        if(count($all_watchlist) > 0){

            $list_box = [];
            foreach ($all_watchlist as $list) {
                # code...
                // get details for transactions on 
                $transactions = Transaction::where('stock_name', $list->security)->get();
                $trade_count  = count($transactions);

                    // get last traded price
                $last_traded_price = Transaction::where('stock_name', $list->security)->latest()->first();
                if($last_traded_price == null){

                    // get price from the market
                    $market = Security::where('security', $list->security)->first();
                    $price = $market->close_price;

                }else{

                    $price = $last_traded_price->stock_unit;
                }

                $gap = $list->trade - $trade_count;

                $data = [
                    'id'        => $list->id,
                    'trade'     => $trade_count,
                    'security'  => $list->security,
                    'price'     => $price,
                    'gap'       => $gap,
                    'date'      => $list->created_at->diffForHumans()
                ];
                array_push($list_box, $data);
            }
        }else{

            $list_box = [];
        }

        return response()->json($list_box);
    }


    /*
    |--------------------------------
    | SET WATCH LIST
    |--------------------------------
    |
    */
    public function setWatchList(Request $request){
        # code...
        // $security = strtoupper($request->security);
        $sec_id   = $request->secid;
        $user_id  = $request->userid;

        // get security by name
        $security = Security::where("id", $sec_id)->first();
        $security = $security->security;

        if($security == null){
            // when sec id not found !
            $data = [
                'status' => 'error',
                'message' => 'could not find security with id of: '.$sec_id
            ];
        }else{
            // get details for transactions on 
            $transactions = Transaction::where('stock_name', $security)->get();
            $trade_count  = count($transactions);

            // get last traded price
            $last_traded_price = Transaction::where('stock_name', $security)->latest()->first();
            if($last_traded_price == null){

                // get price from the market
                $market = Security::where('security', $security)->first();
                $price = $market->close_price;
            }else{

                $price = $last_traded_price->stock_unit;
            }

            // check if already exits
            $watch_list = WatchList::where([['security', $security], ['user_id', $user_id]])->first();
            if($watch_list == null){
                // add new security to watch
                $watch_sec              = new WatchList();
                $watch_sec->user_id     = $user_id;
                $watch_sec->trade       = $trade_count;
                $watch_sec->security    = $security;
                $watch_sec->price       = $price;
                $watch_sec->save();

                $data = [
                    'status'    => 'success',
                    'message'   => $security.' added to watch list !'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => $security.' already added to watch list !'
                ];
            }
        }

        return response()->json($data);
    }

    /*
    |--------------------------------
    | CANCEL WATCH LIST
    |--------------------------------
    |
    */
    public function cancelWatchList(Request $request){
        
        $user_id = $request->userid;
        $item_id = $request->itemid;

        // check if item id exits 
        $watch_list = WatchList::where('id', $item_id)->first();
        if($watch_list !== null){

            // update and delete
            $update_watchlist = WatchList::find($item_id);
            $update_watchlist->delete();

            $data = [
                'status'    => 'success',
                'message'   => 'item remove successfully !'
            ];

        }else{

            $data = [
                'status'   => 'error',
                'message'  => 'failed !, item does not exits !'
            ];
        }

        return response()->json($data);
    }


    /*
    |--------------------------------
    | SET PRICE ALERT
    |--------------------------------
    |
    */
    public function setPriceAlert(Request $request){

        $user_id    = $request->userid;
        $sec_id     = $request->secid;
        $price      = $request->price;

        // get security by name
        $security = Security::where("id", $sec_id)->first();
        $security = $security->security;
        if($security == null){
            // when sec id not found !
            $data = [
                'status' => 'error',
                'message' => 'could not find security with id of: '.$sec_id
            ];
        }else{

            // check if user already set price alert
            $check_already_set = Alert::where([['user_id', $user_id], ['security', $security], ['price', $price]])->first();
            if($check_already_set == null){

                $set_price_alert            = new Alert();
                $set_price_alert->user_id   = $user_id;
                $set_price_alert->security  = $security;
                $set_price_alert->price     = $price;
                $set_price_alert->type      = "price-alert";
                $set_price_alert->save();

                // get last updated 
                $last_alert_set = Alert::where('user_id', $user_id)->latest()->first();

                if($last_alert_set !== null){
                    $data = [
                        'id'        => $last_alert_set->id,
                        'user_id'   => $last_alert_set->user_id,
                        'type'      => $last_alert_set->type,
                        'security'  => $last_alert_set->security,
                        'price'     => number_format($last_alert_set->price, 2),
                        'date'      => $last_alert_set->created_at->diffForHumans()
                    ];

                    // fire events
                    // \Event::fire(new PriceAlert($data));
                }

                $data = [
                    'status'    => 'success',
                    'message'   => 'Price alert has been set successfully !'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Price alert already set to '.number_format($check_already_set->price, 2).' !'
                ];
            }
        }

        // return response 
        return response()->json($data);
    }


    /*
    |------------------------------------------
    | GET EQUITIES MARKET DATA
    |------------------------------------------
    */
    public function loadEquities(Request $request)
    {
        // find transactions
        // user id 
        $user_id = $request->userid;
        $stock   = $request->stock;

        if($stock !== ""){
            $stock = strtoupper($stock);
        }else{
            $stock = null;
        }

        // fetch transactions
        $data = $this->scanEquities($stock);

        // return response
        return response()->json($data);
    }


    /*
    |------------------------------------------
    | GET USER RANKING LIST
    |------------------------------------------
    */
    public function loadRankingList(Request $request)
    {

        // first get all users 
        $all_users = User::all();

        // realized & unrealized data
        $rank_box = [];
        $realized = []; 
        $unrealized = [];
        // check all users 
        foreach ($all_users as $user) {

            # code... for financial statements
            // get catss statements information (Profit Section)
            $profit_statements = Statement::where([['user_id', $user->id], ['status', 'profit']])->get();

            // load profit statements
            $stmt_profit_box = [];
            foreach ($profit_statements as $stats) {
                # code...
                $profit_data = array(
                    'amount' => $stats->balance
                );

                array_push($stmt_profit_box, $profit_data);
            }

            // return response in sum
            $stmt_sum_profit = collect($stmt_profit_box);
            $stmt_net_profit = $stmt_sum_profit->sum('amount');


            // get catss statements information (loss Section)
            $loss_statements = Statement::where([['user_id', $user->id], ['status', 'loss']])->get();

            // load profit statements
            $stmt_loss_box = [];
            foreach ($loss_statements as $stats) {
                # code...
                $profit_data = array(
                    'amount' => $stats->balance
                );

                array_push($stmt_loss_box, $profit_data);
            }

            // return response in sum
            $stmt_sum_loss = collect($stmt_loss_box);
            $stmt_net_loss = $stmt_sum_loss->sum('amount');

            // statement net
            $stmt_total = $stmt_net_profit - $stmt_net_loss;

            # code...
            $stocks = Stock::where('user_id', $user->id)->orderBy('id', 'desc')->get();
            $profit_basket = []; 
            $loss_basket   = [];

            // scanned stocks
            foreach ($stocks as $stock) {
                // get last price from pairs
                $equity_market = Security::where('security', $stock->name)->get();
                foreach ($equity_market as $market) {
                    // market price vs w/a price
                    $market_amount = $stock->qty * $market->close_price; // at market price
                    $holding_amount = $stock->qty * $stock->w_average; // at closing price

                    if($market_amount > $holding_amount){
                        // market price is up
                        $profit = $market_amount - $holding_amount;
                        // Calculate Profit or loss from ongoin market
                        $profit_data = array(
                            'profit' => $profit
                        );

                        // push to array
                        array_push($profit_basket, $profit_data);

                    }elseif($market_amount < $holding_amount){
                        // holding is up
                        $loss = $holding_amount - $market_amount;

                        // calculate loss
                        $loss_data = array(
                            'loss' => $loss
                        );

                        // push to array
                        array_push($loss_basket, $loss_data);
                    }
                }
            }

            // all to collections
            $loss_collection = collect($loss_basket);
            $profit_collection = collect($profit_basket);

            // total loss & profit
            $total_loss   = $loss_collection->sum('loss');
            $total_profit = $profit_collection->sum('profit');

            // get net profit 
            $net_total = $total_profit - $total_loss;

            // fin. statement report data
            $statement_data = array(
                'user'   => $user->name,
                'profit' => $stmt_net_profit,
                'loss'   => $stmt_net_loss,
                'net'    => $stmt_total
            );

            // revaluation data
            $revauled_data = array(
                'user'   => $user->name,
                'profit' => $total_profit,
                'loss'   => $total_loss,
                'net'    => $net_total,
                'date'   => date("D M d, Y ")
            );

            // calculate rank profit
            $rank_total = $net_total + $stmt_total;

            // ranking data 
            $rank_data = array(
                'id'     => $user->id,
                'user'   => $user->name,
                'profit' => $rank_total,
                'date'   => date("D M d, Y ")
            );

            // array sold & reavaluation box
            array_push($realized, $statement_data);
            array_push($unrealized, $revauled_data);

            if($rank_total !== 0){
                // ranking box
                array_push($rank_box, $rank_data);
            }

        }

        $final_summary = array(
            'ranking'    => $rank_box
            // 'realized'   => $realized,
            // 'unrealized' => $unrealized
        );

        return response()->json($final_summary);
    }


    /*
    |------------------------------------------
    | GET USER ACCOUNT INFORMATION
    |------------------------------------------
    */
    public function getAccountInformation(Request $request)
    {
        # user basic information
        $id = $request->userid;

        // convert to init
        $id = (int)$id;

        // user information
        $user_info  = User::where('id', $id)->first();
        if($user_info !== null){
            // account information
            $account    = Account::where('user_id', $id)->first();
            
            $data = array(
                'account_id'    => $user_info->account_id,
                'name'          => $user_info->name,
                'email'         => $user_info->email,
                // 'account_bal'   => $account->account_balance
                'account_bal'   => number_format($account->account_balance, 2)
            );
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'User information not found !'
            ];
        }

        return response()->json($data);
    }


    /*
    |--------------------------------------------------
    | GET USER BASIC PROFILE INFORMATION
    |--------------------------------------------------
    |
    */
    public function loadProfile(Request $request){

        $user_id = $request->userid;
        $basic_info = Basic::where('user_id', $user_id)->first();
        if($basic_info !== null){
            $account = Account::where('user_id', $user_id)->first();
            if($account !== null){
                $data = [
                    'user_id'   => $basic_info->user_id,
                    'name'      => $basic_info->name,
                    'address'   => $basic_info->address,
                    'zip_code'  => $basic_info->zip_code,
                    'state'     => $basic_info->state,
                    'phone'     => $basic_info->phone,
                    'avatar'    => 'http://www.equities.catss.ng/uploads/'.$basic_info->avatar,
                    'account_no'=> $account->account_number,
                    'bank_name' => $account->account_bank
                ];
            }
        }else{
            
            $data = [];
        }

        // return response
        return response()->json($data);
    }

    /*
    |--------------------------------------------------
    | UPDATE PROFILE
    |--------------------------------------------------
    */
    public function updateProfile(Request $request){
        $user_id        = $request->userid;
        $phone          = $request->phone;
        $address        = $request->address;
        $zipcode        = $request->zipcode;
        $account_number = $request->account_number;
        $account_bank   = $request->bank_name;

        $account    = Account::where("user_id", $user_id)->first();
        $basic_info = Basic::where("user_id", $user_id)->first();

        if($basic_info !== null){
            $update_basic           = Basic::find($basic_info->id);
            $update_basic->address  = $address;
            $update_basic->zip_code = $zipcode;
            $update_basic->phone    = $phone;
            $update_basic->update();

            if($account !== null){
                $update_account                 = Account::find($account->id);
                $update_account->account_number = $account_number;
                $update_account->account_bank   = $account_bank;
                $update_account->update();
            }

            $data = [
                'status'    => 'success',
                'message'   => 'Update successful!'
            ];
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'User profile not found!'
            ];
        }

        // return response.
        return response()->json($data);
    }

    /*
    |------------------------------------------
    | GET NEWS INFORMATION
    |------------------------------------------
    */
    public function loadNews(Request $request)
    {
       # code...
        $load_news      = new News();
        $bloomberg_news = $load_news->loadNewsBloomberg();
        $local_news     = $load_news->localNews();
        $financial_news = $load_news->loadNews();

        // filtered data
        // $data = json_decode($news_contents);

        $financial_news = json_decode($financial_news);
        $bloomberg_news = json_decode($bloomberg_news);

        $turn = [1, 2, 3];
        $total = count($turn);
        $shuffle = rand(0, $total - 1);
        if($shuffle == 0){
            return response()->json($local_news);
        }elseif($shuffle == 1){
            return response()->json($financial_news->articles);
        }elseif($shuffle == 2){
            return response()->json($bloomberg_news->articles);
        }else{
            return response()->json($local_news);
        }
    }

    /*
    |------------------------------------------
    | VERIFYING SIGNUP FIELD
    |------------------------------------------
    */
    public function verifySignupInput($request)
    {
        # filtered data
        $name       = $request->name;
        $email      = $request->email;
        $password   = $request->password;
        $phone      = $request->phone;

        # verify missing input
        if($name == ""){
            # check if input name is empty
            $data = array(
                'status'    => 'error',
                'message'   => 'name field required !'
            );

            return response()->json($data);

        }elseif($email == ""){

            $data = array(
                'status'    => 'error',
                'message'   => 'email field required !'
            );

            return response()->json($data);

        }elseif($password == "" && strlen($password) < 4){

            $data = array(
                'status'    => 'error',
                'message'   => 'password field required and min char > 5 !'
            );

            return response()->json($data);

        }elseif($phone == ""){

            if(!is_numeric($phone) && $phone < 11){

                $data = array(
                    'status'    => 'error',
                    'message'   => 'phone field required and number must be 11 char !'
                );

                return response()->json($data);
            }
        }
    }


    /*
    |------------------------------------------
    | VERIFY USER ALREADY EXIST
    |------------------------------------------
    */
    public function alreadyExist($email)
    {
        // check for already signed up user
        $user = User::where('email', $email)->first();
        if($user !== null){
            
            // if user exits
            return false;

        }else{

            // if user is new
            return true;
        }
    }


    /*
    |------------------------------------------
    | VALIDATE API SECRET AND TOKEN
    |------------------------------------------
    */
    public function authApi($token, $secret)
    {
    	// check for token
    	if($token == '6b971eac2f876685b4ff2d07ffeb545c41B756F2DCAC80BFD910D1BED0633974'){

    		// check for secret
    		if($secret == 'secret12345' ){

    			// return true
    			return true;

    		}else{
    			// secret did not match
    			return false;
    		}

    	}else{
			
			// token did not match 
    		return false;
    	}
    }


    /*
    |------------------------------------------
    | GET USER TRANSACTIONS ENGINE ROOM
    |------------------------------------------
    */
    public function scanTransactionLogs($user_id, $query)
    {
        // check if transaction is empty
        if($query == null){

            // get all transactions
            $all_transactions = Transaction::where('user_id', $user_id)->orderBy('id', 'desc')->get();

            // check if user has transactions
            if(count($all_transactions) > 0){
                $transaction_box = [];
                foreach($all_transactions as $transaction){
                    $data = array(
                        'stock_name'    => $transaction->stock_name,
                        'stock_unit'    => number_format($transaction->stock_unit, 2),
                        'stock_qty'     => number_format($transaction->stock_qty),
                        'stock_trade'   => $transaction->stock_trade,
                        'stock_amount'  => number_format($transaction->stock_amount, 2),
                        'stock_date'    => $this->scanDateForMobile($transaction->created_at->diffForHumans())
                    );

                    array_push($transaction_box, $data);
                }
            }else{
                $transaction_box = [];
            }
        }else{

            // get all transactions
            $all_transactions = Transaction::where([['user_id', $user_id], ['stock_name', $query]])->orderBy('id', 'desc')->get();

            // check if user has transactions
            if(count($all_transactions) > 0){
                $transaction_box = [];
                foreach($all_transactions as $transaction){
                    $data = array(
                        'stock_name'    => $transaction->stock_name,
                        'stock_unit'    => number_format($transaction->stock_unit, 2),
                        'stock_qty'     => number_format($transaction->stock_qty),
                        'stock_trade'   => $transaction->stock_trade,
                        'stock_amount'  => number_format($transaction->stock_amount, 2),
                        'stock_date'    => $this->scanDateForMobile($transaction->created_at->diffForHumans())
                    );

                    array_push($transaction_box, $data);
                }
            }else{
                $transaction_box = [];
            }
        }

        // return response
        return $transaction_box;
    }


    /*
    |------------------------------------------
    | GET USER STOCK ENGINE ROOM
    |------------------------------------------
    */
    public function scanStockBalance($user_id, $query)
    {
        // check if transaction is empty
        if($query == null){

            // find transactions 
            $all_stocks = Stock::where('user_id', $user_id)->orderBy('id', 'desc')->get();

            if(count($all_stocks) > 0){
                $stock_box = [];
                foreach ($all_stocks as $stocks) {
                    if($stocks->qty > 0){
                        # code...
                        $data = array(
                            'name'  => $stocks->name,
                            'qty'   => number_format($stocks->qty),
                            'price' => number_format($stocks->w_average, 2) 
                        );
                        array_push($stock_box, $data);
                    }   
                }
            }else{
                $stock_box = [];
            }

        }else{

            // Find Stock balance 
            $stocks = Stock::where([['user_id', $user_id], ['name', $query]])->first();
            $stock_box = [];
            if($stocks !== null){
                # code...
                $data = array(
                    'name'  => $stocks->name,
                    'qty'   => number_format($stocks->qty),
                    'price' => number_format($stocks->price, 2) 
                );
                array_push($stock_box, $data);

            }else{

                $stock_box = [];
            }
        }

        // return response
        return $stock_box;
    }


    /*
    |----------------------------------------------------
    | REVALUATION PROFIT ACCOUNT
    |----------------------------------------------------    |
    */
    public function scanForProfit($id){

        // GET FIN. STATEMENT PROFIT SECTION
        $profit_statements = Statement::where([['user_id', $id], ['status', 'profit']])->get();
        // load profit statements
        $stmt_profit_box = [];
        foreach ($profit_statements as $stats) {
            # code...
            $profit_data = array(
                'amount' => $stats->balance
            );

            array_push($stmt_profit_box, $profit_data);
        }

        // return response in sum
        $stmt_sum_profit = collect($stmt_profit_box);
        $stmt_net_profit = $stmt_sum_profit->sum('amount');

        // GET FIN. STATEMENT LOSS SECTION
        $loss_statements = Statement::where([['user_id', $id], ['status', 'loss']])->get();

        // load profit statements
        $stmt_loss_box = [];
        foreach ($loss_statements as $stats) {
            # code...
            $profit_data = array(
                'amount' => $stats->balance
            );

            array_push($stmt_loss_box, $profit_data);
        }

        // return response in sum
        $stmt_sum_loss = collect($stmt_loss_box);
        $stmt_net_loss = $stmt_sum_loss->sum('amount');

        // statement net
        $stmt_total = $stmt_net_profit - $stmt_net_loss;

        # STOCK STATMENT BALANCE SECTION
        $stocks = Stock::where('user_id', $id)->get();
        $profit_basket = []; 
        $loss_basket   = [];

        // scanned stocks
        foreach ($stocks as $stock) {
            // get last price from pairs
            $equity_market = Security::where('security', $stock->name)->get();

            foreach ($equity_market as $market) {
                // market price vs w/a price
                $market_amount = $stock->qty * $market->close_price; // at market price
                $holding_amount = $stock->qty * $stock->w_average; // at closing price

                if($market_amount > $holding_amount){
                    // market price is up
                    $profit = $market_amount - $holding_amount;
                    // Calculate Profit or loss from ongoin market
                    $profit_data = array(
                        'profit' => $profit
                    );

                    // push to array
                    array_push($profit_basket, $profit_data);

                }elseif($market_amount < $holding_amount){
                    // holding is up
                    $loss = $holding_amount - $market_amount;

                    // calculate loss
                    $loss_data = array(
                        'loss' => $loss
                    );

                    // push to array
                    array_push($loss_basket, $loss_data);
                }
            }
        }

        // all to collections
        $loss_collection = collect($loss_basket);
        $profit_collection = collect($profit_basket);

        // total loss & profit
        $total_loss   = $loss_collection->sum('loss');
        $total_profit = $profit_collection->sum('profit');
        
        // $net_total = $total_profit - $total_loss;
        $all_profit = $stmt_net_profit + $total_profit;
        $all_loss = $stmt_net_loss + $total_loss;

        // get net profit 
        if($all_profit > $all_loss){
            $tag = 'profit';
        }else{
            $tag = 'loss';
        }

        // net balance 
        $net_balance = $all_profit - $all_loss;

        // COMPUTE ASSETS (EQUITIES) VALUE  WORTH 
        $final_summary = array(
            'profit' => $stmt_net_profit,
            'loss'   => $stmt_net_loss,
            'net'    => $stmt_net_profit - $stmt_net_loss,
            'bal'    => $net_balance,
            'tag'    => $tag,
        );

        return $final_summary;
    }


    /*
    |------------------------------------------
    | GET LOAD MARKET DATA ENGINE ROOM
    |------------------------------------------
    */
    public function scanEquities($query)
    {
        // check if query is empty
        if($query == null){

            // load pairs
            $pairs = Security::all();

            if(count($pairs) >  0){
                $pair_box = [];
                foreach ($pairs as $equity) {

                    $two_percent = 2.00;
                    $total_percent = 100;
                    $diff = $total_percent - $two_percent;
                    
                    $charge_percent = $diff / $total_percent;
                    $map_price = $charge_percent * $equity->close_price;
                    $map_diff = $equity->close_price - $map_price;

                    $real_price = $map_diff + $equity->close_price; 

                    $gap = $real_price - $equity->previous_close;

                    $data = array(
                        'id'                => $equity->id,
                        'board'             => $equity->board,
                        'security'          => $equity->security,
                        'ref_price'         => $this->scanEveryDigits($equity->ref_price),
                        'open_price'        => $this->scanEveryDigits($equity->open_price),
                        'close_price'       => $this->scanEveryDigits($equity->close_price),
                        'change_price'      => $this->scanEveryDigits($gap),
                        'previous_close'    => $this->scanEveryDigits($equity->previous_close),
                        'real_price'        => $this->scanEveryDigits($real_price),
                        'new_price'         => $this->scanEveryDigits($equity->open_price),
                        'status'            => $equity->status,
                        'date'              => $equity->updated_at->toFormattedDateString()
                    );
                    array_push($pair_box, $data);
                }

            }else{
                $pair_box = [];
            }
        }else{

            // load single
            $equity = Security::where('security', $query)->first();
            $pair_box = [];
            if($equity !== null){

                $two_percent = 2.00;
                $total_percent = 100;
                $diff = $total_percent - $two_percent;
                
                $charge_percent = $diff / $total_percent;
                $map_price = $charge_percent * $equity->close_price;
                $map_diff = $equity->close_price - $map_price;

                $real_price = $map_diff + $equity->close_price; 

                $gap = $real_price - $equity->previous_close;

                $data = array(
                    'id'                => $equity->id,
                    'board'             => $equity->board,
                    'security'          => $equity->security,
                    'ref_price'         => $this->scanEveryDigits($equity->ref_price),
                    'open_price'        => $this->scanEveryDigits($equity->open_price),
                    'close_price'       => $this->scanEveryDigits($equity->close_price),
                    'change_price'      => $this->scanEveryDigits($gap),
                    'previous_close'    => $this->scanEveryDigits($equity->previous_close),
                    'real_price'        => $this->scanEveryDigits($real_price),
                    'new_price'         => $this->scanEveryDigits($equity->open_price),
                    'status'            => $equity->status,
                    'date'              => $equity->created_at->toFormattedDateString()
                );
                array_push($pair_box, $data);
            }else{
                $pair_box = [];
            }
        }

        // return response
        return $pair_box;
    }


    /*
    |--------------------------------
    | LOAD SINGLE TRADE ORDERS
    |--------------------------------
    |
    */
    public function loadSingleOrders(Request $request)
    {
        # code...
        $user_id    = $request->userid;
        $sec_id     = $request->secid;
        $type       = $request->type;

        if($request->has('type') && !empty($request->type)){
            $orders = Order::where([['user_id', $user_id], ['sec_id', $sec_id], ['type', $type]])->orderBy('id', 'desc')->get();

            // check if users have any orders
            if(count($orders) > 0){
                // load users orders
                $order_box = [];
                foreach ($orders as $order) {
                    // sec information
                    $asset = Security::where('id', $sec_id)->first();

                    # code...
                    $data = array(
                        'id'        => $order->id,
                        'sec_id'    => $order->sec_id,
                        'asset'     => $asset->security,
                        'user_id'   => $order->user_id,
                        'ref_id'    => $order->ref_id,
                        'price'     => number_format($order->price, 2),
                        'qty'       => number_format($order->qty),
                        'type'      => $order->type,
                        'option'    => $order->option,
                        'date'      => $order->created_at->diffForHumans()
                    );

                    array_push($order_box, $data);
                }

                // return response no orders
                $data = array(
                    'status'    => 'success',
                    'message'   => 'order has been loaded successfully!',
                    'orders'    => $order_box
                );

            }else{

                // return response no orders
                $data = array(
                    'status'  => 'success',
                    'message' => 'You do not have any orders !',
                    'orders'  => []
                );
            }

        }else{

            $orders = Order::where([['user_id', $user_id], ['sec_id', $sec_id]])->orderBy('id', 'desc')->get();
            // check if users have any orders
            if(count($orders) > 0){
                // load users orders
                $order_box = [];
                foreach ($orders as $order) {
                    // sec information
                    $asset = Security::where('id', $sec_id)->first();

                    # code...
                    $data = array(
                        'id'        => $order->id,
                        'sec_id'    => $order->sec_id,
                        'asset'     => $asset->security,
                        'user_id'   => $order->user_id,
                        'ref_id'    => $order->ref_id,
                        'price'     => number_format($order->price, 2),
                        'qty'       => number_format($order->qty),
                        'type'      => $order->type,
                        'option'    => $order->option,
                        'date'      => $order->created_at->diffForHumans()
                    );

                    array_push($order_box, $data);
                }

                // return response no orders
                $data = array(
                    'status'    => 'success',
                    'message'   => 'order has been loaded successfully!',
                    'orders'    => $order_box
                );

            }else{

                // return response no orders
                $data = array(
                    'status'  => 'success',
                    'message' => 'You do not have any orders !',
                    'orders'  => []
                );
            }
        }

            

        // return response data
        return response()->json($data);
    }


    /*
    |--------------------------------
    | LOAD ALL TRADE ORDERS
    |--------------------------------
    |
    */
    public function loadAllOrders(Request $request)
    {
        # code...
        // $user_id = $request->userid;
        $sec_id     = $request->secid;
        $user_id    = $request->userid;
        $type       = $request->type;

        if($request->has('type') && !empty($request->type)){

            $orders = Order::where([
                ['sec_id', $sec_id], 
                ['type', $type],
            ])->orderBy('id', 'desc')->get();

            // check if users have any orders
            if(count($orders) > 0){
                // load users orders
                $order_box = [];
                foreach ($orders as $order) {
                    // sec information
                    $asset = Security::where('id', $sec_id)->first();

                    # code...
                    $data = array(
                        'id'        => $order->id,
                        'sec_id'    => $order->sec_id,
                        'asset'     => $asset->security,
                        'user_id'   => $order->user_id,
                        'ref_id'    => $order->ref_id,
                        'price'     => number_format($order->price, 2),
                        'qty'       => number_format($order->qty),
                        'type'      => $order->type,
                        'option'    => $order->option,
                        'date'      => $order->created_at->diffForHumans(),
                        'raw_date'  => $order->created_at
                    );

                    array_push($order_box, $data);
                }

                // return response no orders
                $data = array(
                    'status'    => 'success',
                    'message'   => 'order has been loaded successfully!',
                    'orders'    => $order_box
                );
            }else{
                // return response no orders
                $data = array(
                    'status'  => 'success',
                    'message' => 'You do not have any order(s)!',
                    'orders'  => []
                );
            }

        }else{

            $orders = Order::where('user_id', $user_id)->orderBy('id', 'desc')->get();

            // check if users have any orders
            if(count($orders) > 0){
                // load users orders
                $order_box = [];
                foreach ($orders as $order) {
                    // sec information
                    $asset = Security::where('id', $order->sec_id)->first();

                    # code...
                    $data = array(
                        'id'        => $order->id,
                        'sec_id'    => $order->sec_id,
                        'asset'     => $asset->security,
                        'user_id'   => $order->user_id,
                        'ref_id'    => $order->ref_id,
                        'price'     => number_format($order->price, 2),
                        'qty'       => number_format($order->qty),
                        'type'      => $order->type,
                        'option'    => $order->option,
                        'date'      => $order->created_at->diffForHumans(),
                        'raw_date'  => $order->created_at
                    );

                    array_push($order_box, $data);
                }

                // return response no orders
                $data = array(
                    'status'    => 'success',
                    'message'   => 'order has been loaded successfully!',
                    'orders'    => $order_box
                );
            }else{
                // return response no orders
                $data = array(
                    'status'  => 'success',
                    'message' => 'You do not have any orders !',
                    'orders'  => []
                );
            }
        }

        // return response data
        return response()->json($data);
    }




    /*
    |--------------------------------------------------------------------------
    | TRADE AT MARKET 
    |--------------------------------------------------------------------------
    |
    | BUY: Debit Cash account and Credit Stock account
    | SELL: Credit Cash account and Debit Stock account
    |
    */
    // Trade at Market Price
    // Trade BUY
    public function handleBuyTrade(Request $request)
    {
        // return $request->all();
        $user_id    = $request->userid;
        $sec_id     = $request->secid;
        $qty        = $request->total;
        $type       = "buy";

        // scan and convert qty
        // $qty = str_replace(",", "", $qty);
        // $qty = floatval($qty);

        $user_id = $this->scanEveryDigits($user_id);
        $sec_id  = $this->scanEveryDigits($sec_id);
        $qty     = $this->scanEveryDigits($qty);

        # check security exits
        $security = Security::where("id", $sec_id)->first();
        if($security !== null){

            // get trade amount
            $price  = $security->new_band;
            $amount = $price * $qty;

            // validate user account balance
            $account = Account::where("user_id", $user_id)->first();
            if($account !== null){

                // validate user account balance
                if($account->account_balance >= $amount){

                    // process trade 
                    $this->creditStock($user_id, $sec_id, $qty, $price); // credit stock account
                    $this->debitAccount($account->id, $amount);  // debit user account
                    $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);  // log transaction

                    $data = array(
                        'status' => 'success',
                        'message' => 'Trade successful!'
                    );

                }else{

                    $data = array(
                        'status'  => 'error',
                        'message' => 'insufficient balance!'
                    );
                }
            }
        }else{
            $data = [
                'status'  => 'error',
                'message' => 'Invalid trade asset id is not a valid security'
            ];
        }

        return response()->json($data);
    }

    // Trade SELL
    public function handleSellTrade(Request $request)
    {
        // return $request->all();
        $user_id    = $request->userid;
        $sec_id     = $request->secid;
        $qty        = $request->total;
        $type       = "sell";

        // scan and convert qty
        // $qty = str_replace(",", "", $qty);
        // $qty = floatval($qty);

        $user_id = $this->scanEveryDigits($user_id);
        $sec_id  = $this->scanEveryDigits($sec_id);
        $qty     = $this->scanEveryDigits($qty);

        # check security exits
        $security = Security::where("id", $sec_id)->first();
        if($security !== null){

            // get trade amount
            $price  = $security->new_band;
            $amount = $price * $qty;

            // validate user stock balance
            $stock = Stock::where([["name", $security->security], ['user_id', $user_id]])->first();
            if($stock !== null){

                // validate user stock qty
                if($stock->qty >= $qty){

                    // get user account id 
                    $account = Account::where("user_id", $user_id)->first();

                    // process trade 
                    $this->debitStock($user_id, $sec_id, $qty, $price); // debit stock account
                    $this->creditAccount($account->id, $amount);  // credit user account
                    $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);  // log transaction

                    $data = array(
                        'status' => 'success',
                        'message' => 'Trade successful !'
                    );

                }else{

                    $data = array(
                        'status' => 'error',
                        'message' => 'Insufficient balance !'
                    );
                }
            }else{
                
                $data = array(
                    'status' => 'error',
                    'message' => 'You do not have this stock for sale !'
                );

            }
        }else{

            $data = [
                'status'  => 'error',
                'message' => 'Invalid trade asset id is not a valid security '
            ];
        }


        return response()->json($data);
    }


    /*
    |--------------------------------------------------------------------------
    | TRADE USING ORDERS / JOBS (LIMIT)
    |--------------------------------------------------------------------------
    |
    | BUY: Debit Cash account and Credit Stock account
    | SELL: Credit Cash account and Debit Stock account
    |
    */
    // Trade at Market Price
    // Trade BUY
    public function handleBuyOrderTrade(Request $request)
    {
        // $id          = $request->id;
        $sec_id     = $request->secid;
        $user_id    = $request->userid;
        $price      = $request->price;
        $qty        = $request->total;
        $type       = $request->type;       
        $option     = $request->option;

        // get amount
        $amount = $qty * $price;

        // filtered inputs
        $filtered   = $this->filterFields($price, $qty);
        if(!empty($filtered)){

            // return error response
            return response()->json($filtered);
        }

        // check user account balance 
        $account = Account::where('user_id', $user_id)->first();
        if($account->account_balance <= $amount ){

            $data = [
                'status'    => 'error',
                'message'   => 'insufficient account balance !' 
            ];

        }else{

            // compare market price and order price
            $asset = Security::where('id', $sec_id)->first();
            if($asset->open_price == $price){
                // process trade 
                $this->creditStock($user_id, $sec_id, $qty, $price); // credit stock account
                $this->debitAccount($account->id, $amount);  // debit user account
                $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);  // log transaction

                $data = [
                    'status'    => 'success',
                    'message'   => 'Trade successful!'
                ];
            }else{
                // scan order ref_id
                $ref_id     = "CATSS-".rand(000,999).rand(000,999).rand(000,999);

                $place_order            = new Order();
                $place_order->sec_id    = $sec_id;
                $place_order->user_id   = $user_id;
                $place_order->ref_id    = $ref_id;
                $place_order->price     = $price;
                $place_order->qty       = $qty;
                $place_order->type      = $type;
                $place_order->option    = $option;
                $place_order->save();

                // get last order details
                $last_order = Order::where('ref_id', $ref_id)->first();

                $last_order_data = array(
                    'id'        => $last_order->id,
                    'sec_id'    => $last_order->sec_id,
                    'asset'     => $asset->security,
                    'user_id'   => $last_order->user_id,
                    'ref_id'    => $last_order->ref_id,
                    'price'     => number_format($last_order->price, 2),
                    'qty'       => number_format($last_order->qty),
                    'type'      => $last_order->type,
                    'option'    => $last_order->option,
                    'date'      => $last_order->created_at->diffForHumans() 
                );

                // fire events
                \Event::fire(new PlaceOrder($last_order_data));

                $data = [
                    'status'    => 'success',
                    'message'   => 'order has been placed successfully !'
                ];
            }
        }
            
        // return response
        return response()->json($data);
    }

    // Trade SELL
    public function handleSellOrderTrade(Request $request){
        $user_id    = $request->userid;
        $sec_id     = $request->secid;
        $price      = $request->price;
        $qty        = $request->total;
        $type       = "sell";
        $option     = $request->option;

        // scan and convert qty
        $user_id = $this->scanEveryDigits($user_id);
        $sec_id  = $this->scanEveryDigits($sec_id);
        $qty     = $this->scanEveryDigits($qty);

        # check security exits
        $security = Security::where("id", $sec_id)->first();
        if($security !== null){

            // get trade amount
            // $price  = $security->new_band;
            $amount = $price * $qty;

            // validate user stock balance
            $stock = Stock::where([["name", $security->security], ['user_id', $user_id]])->first();
            if($stock !== null){

                // validate user stock qty
                if($stock->qty >= $qty){

                    if($security->open_price == $price){
                        // get user account id 
                        $account = Account::where("user_id", $user_id)->first();

                        // process trade 
                        $this->debitStock($user_id, $sec_id, $qty, $price); // debit stock account
                        $this->creditAccount($account->id, $amount);  // credit user account
                        $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);  // log transaction

                        $data = array(
                            'status' => 'success',
                            'message' => 'Trade successful !'
                        );
                        
                    }else{
                        // scan order ref_id
                        $ref_id     = "CATSS-".rand(000,999).rand(000,999).rand(000,999);

                        $place_order            = new Order();
                        $place_order->sec_id    = $sec_id;
                        $place_order->user_id   = $user_id;
                        $place_order->ref_id    = $ref_id;
                        $place_order->price     = $price;
                        $place_order->qty       = $qty;
                        $place_order->type      = $type;
                        $place_order->option    = $option;
                        $place_order->save();


                        // sec information
                        $asset = Security::where('id', $sec_id)->first();
                        // get last order details
                        $last_order = Order::where('ref_id', $ref_id)->first();

                        $last_order_data = array(
                            'id'        => $last_order->id,
                            'sec_id'    => $last_order->sec_id,
                            'asset'     => $asset->security,
                            'user_id'   => $last_order->user_id,
                            'ref_id'    => $last_order->ref_id,
                            'price'     => number_format($last_order->price, 2),
                            'qty'       => number_format($last_order->qty),
                            'type'      => $last_order->type,
                            'option'    => $last_order->option,
                            'date'      => $last_order->created_at->diffForHumans() 
                        );

                        // fire events
                        \Event::fire(new PlaceOrder($last_order_data));

                        $data = array(
                            'status'    => 'success',
                            'message'   => 'Order has been placed successfully !'
                        );
                    }

                }else{

                    $data = array(
                        'status'    => 'error',
                        'message'   => 'Insufficient balance !'
                    );
                }
            }else{
                
                $data = array(
                    'status'    => 'error',
                    'message'   => 'You do not have this stock for sale !'
                );

            }
        }else{
            $data = [
                'status'  => 'error',
                'message' => 'Invalid trade, asset id is not a valid security '
            ];
        }
        
        // return response
        return response()->json($data);
    }

    // Cancel Orders
    public function cancelOrder(Request $request)
    {
        # code...
        $order_id = $request->orderid;

        // find id user information
        $check_exits = Order::where('id', $order_id)->first();
        if($check_exits !== null){

            $cancel_order = Order::find($order_id);
            $cancel_order->delete();

            $data = array(
                'status'    => 'success',
                'message'   => 'Order has been cancel successfully !'
            );

        }else{

            $data = [
                'status'    => 'error',
                'message'   => 'Not found ! or deleted !' 
            ];
        }

        // return response
        return response()->json($data);
    }


    /*
    |--------------------------------------------------------------------------
    | SERVICES FOR HANDLING TRADES PURCHASE REQUEST
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    // debit account
    public function debitAccount($id, $amount)
    {
        # code...
        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance - $amount;
        $account->update();

        return true;
    }

    // credit user stocks
    public function creditStock($user_id, $sec_id, $qty, $price)
    {
        # code...
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){

            // first check if same stock already exits, if same stock exits update existing...
            $stocks = Stock::where([['name', $security->security], ['user_id', $user_id]])->first();

            // if pair is new
            if($stocks == null){
                // no stock exits so W/A is new
                $f1 = $qty * $price;
                $fresh_wa = $f1 / $qty;

                $amount =  $qty * $price;

                // else save new Stock
                $stocks            = new Stock();
                $stocks->user_id   = $user_id;
                $stocks->name      = $security->security;
                $stocks->qty       = $qty;
                $stocks->price     = $price;
                $stocks->amount    = $amount;
                $stocks->w_average = $fresh_wa;
                $stocks->save();
            }else{
                // now check if stock exist
                $existed_stock = Stock::find($stocks->id);

                // get the W/A Formular f1 + f2 / fQty
                // capture incoming stock qty*price 
                $f2 = $qty * $price;

                // get 
                $f1 = $existed_stock->qty * $existed_stock->w_average; 

                # w/average
                $f3 = $f1 + $f2; // compute net old qty * old price
                $f4 = $existed_stock->qty + $qty; // compute net qty
                $f5 = $f3/$f4;

                $existed_stock->price       = $price;
                $existed_stock->w_average   = $f5;
                $existed_stock->qty         = $f4;
                $existed_stock->amount      = $f3;
                $existed_stock->update();
            }
        }

        return true;
    }


    /*
    |--------------------------------------------------------------------------
    | SERVICES FOR HANDLING TRADES SALES REQUEST
    |--------------------------------------------------------------------------
    |
    | Debit Stock account and Credit Cash account
    |
    */
    // credit user bank account
    public function creditAccount($id, $amount)
    {

        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance + $amount;
        $account->update();

        return true;
    }

    // debit user stocks account
    public function debitStock($user_id, $sec_id, $qty, $price)
    {
        # code...
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){
            
            // first check if same stock already exits, if same stock exits update existing...
            $stock = Stock::where([['name', $security->security], ['user_id', $user_id]])->first();

            if($stock !== null){
                // now update if stock exist
                $stocks         = Stock::find($stock->id);
                $stocks->qty    = $stocks->qty - $qty; // keypiece when users sell-> qty update
                $stocks->update();

                // a little store cleaning..
                $stocks = Stock::where([['user_id', $user_id], ['qty', 0]])->first();
                if($stocks !== null){
                    $refresh_stocks = Stock::find($stocks->id);
                    $refresh_stocks->delete();
                }

                # code...
                $w_a = $stock->w_average; // w/a

                // remove negative return...
                if($w_a > $price){
                    
                    $gap = $w_a - $price; // trade gap on w/a
                
                }elseif($w_a < $price){
                    
                    $gap = $price - $w_a; // trade gap on w/a

                }elseif($w_a == $price) {
                    
                    $gap = $w_a - $price; // trade gap on w/a
                }


                $f1 = $w_a * $qty; // on weighted average (holding value)
                $f2 = $price * $qty; // on demand (market value)


                if($f1 > $f2){
                    
                    $comments   = " trade Successful with loss gap of ".$gap;
                    $status     = "loss";

                }elseif($f2 > $f1){
                    
                    $comments   = " trade Successful with profit gap of ".$gap;
                    $status     = "profit";

                }elseif($f2 == $f1){

                    $comments   = " trade Successful with no profit/loss with return gap of ".$gap;
                    $status     = "break-even";
                }

                $top_bal    = $gap * $qty;
                $amount     = $price * $qty;
        
                // get id and update state
                $account_statements                     = new Statement();
                $account_statements->equity             = $security->security;
                $account_statements->w_average          = $w_a;
                $account_statements->user_id            = $user_id;
                $account_statements->last_sold_price    = $price;
                $account_statements->last_sold_qty      = $qty;
                $account_statements->amount             = $amount;
                $account_statements->gap                = $gap;
                $account_statements->balance            = $top_bal;       
                $account_statements->status             = $status;
                $account_statements->comments           = $comments;
                $account_statements->save();
            }
        }

        return true;
    }


    /*
    |--------------------------------------------------------------------------
    | Log transactions
    |--------------------------------------------------------------------------
    |
    | Log transactions details 
    |
    */
    // log transactions
    public function logTransaction($user_id, $amount, $qty, $price, $type, $sec_id)
    {
        $security = Security::where('id', $sec_id)->first();

        // update transactions logs
        $transaction               = new Transaction();
        $transaction->user_id      = $user_id;
        $transaction->stock_name   = $security->security;
        $transaction->stock_unit   = $price;
        $transaction->stock_qty    = $qty;
        $transaction->stock_trade  = $type;
        $transaction->stock_amount = $amount;
        $transaction->stock_timing = time();
        $transaction->com_percent  = 0;
        $transaction->com_amount   = 0;
        $transaction->save();

        // Get transactions notify user
        $transaction = Transaction::where('user_id', $user_id)->orderBy('id', 'desc')->take('1')->first();
        if($transaction !== null){
            $trans_box = array(
                'user_id'      => $user_id,
                'stock_name'   => $transaction->stock_name,
                'stock_unit'   => number_format($transaction->stock_unit, 2),
                'stock_qty'    => number_format($transaction->stock_qty),
                'stock_trade'  => $transaction->stock_trade,
                'stock_amount' => number_format($transaction->stock_amount, 2),
                'stock_date'   => $transaction->created_at->diffForHumans()
            );

            // Fire Transactions info
            // \Event::fire(new TransactionNotifications($trans_box));
        }

        return true;
    }


    /*
    |---------------------------------------------------------------------------
    | LOG DEPOSIT AND UPDATE USER ACCOUNT
    |--------------------------------------------------------------------------
    |
    */
    // start process 
    public function logPayment(Request $request)
    {
        // user id
        $id     = $request->userid;
        $ref_id = $request->refid;
        $amount = $request->amount;
        $status = 'paid';

        if($this->verifyPaystackRef($ref_id) == false){
            // response message
            $data = array(
                'status'  => 'error',
                'message' => 'Invalid ref id provided, transaction was unsuccessful !'
            );

            // return response
            return response()->json($data);
        }else{
            // update double payment
            $payment = Payment::where('user_id', $id)->first();

            if($payment == null){

                // record payment quota
                $log_payments           = new Payment();
                $log_payments->trans_id = $ref_id;
                $log_payments->user_id  = $id;
                $log_payments->amount   = $amount;
                $log_payments->balance  = $amount;
                $log_payments->quota    = 4;
                $log_payments->status   = $status;
                $log_payments->save();
            }else{

                $log_payments = Payment::find($payment->id);
                $log_payments->trans_id = $ref_id;
                $log_payments->amount   = $log_payments->amount + $amount;
                $log_payments->balance  = $log_payments->balance + $amount;
                $log_payments->quota    = $log_payments->quota + 4;
                $log_payments->update();
            }
            
            // vault type
            $type = 'deposit';

            // log vault
            $log_vault          = new Vault();
            $log_vault->user_id = $id;
            $log_vault->type    = $type;
            $log_vault->amount  = $amount;
            $log_vault->save();

            // reset account 
            $this->refreshTrade($id);

            // response message
            $data = array(
                'status'  => 'success',
                'message' => 'Payment successful !'
            );

            // $push = new AndroidNotification();
            // $push_response = $push->fire('Deposit was Successful !', 'You have deposited N20,000.00, You have N100,000,000.00 to start Trading on CATSS Equities Market');

            // return response
            return response()->json($data);
        }
    }

    // reset account information
    public function refreshTrade($id)
    {
        # reset account 
        $account = Account::where('user_id', $id)->first();

        // reset account balance
        $reset_account = Account::find($account->id);
        $reset_account->account_balance = 100000000;
        $reset_account->update();

        // delete all transaction
        $transactions = Transaction::where('user_id', $id)->get();
        if(count($transactions) > 0){
            foreach ($transactions as $trans) {
                # code...
                $delete_transaction = Transaction::find($trans->id);
                $delete_transaction->delete();
            }
        }
            

        // delete all stocks
        $stocks = Stock::where('user_id', $id)->get();
        if(count($stocks) > 0){
            foreach ($stocks as $stock) {
                # code...
                $delete_stock = Stock::find($stock->id);
                $delete_stock->delete();
            }
        }


        // delete all statement
        $statements = Statement::where('user_id', $id)->get();
        if(count($statements) > 0){
            foreach ($statements as $stat) {
                # code...
                $delete_statement = Statement::find($stat->id);
                $delete_statement->delete();
            }
        }
    }

    // verify transaction refid
    // public function verifyPaystackRef($ref_id)
    // {
    //     # Verify Trade information
    //     $paystack = new \Yabacon\Paystack('sk_live_afdd8317e6b65c282bbb001381f8b038a9e087d4');
    //     // $paystack = new \Yabacon\Paystack('sk_live_afdd8317e6b65c282bbb001381f8b038a9e087d4');
        
    //     # use try catch
    //     try{

    //         // verify using the library
    //         $tranx = $paystack->transaction->verify([
    //             'reference' => $ref_id, // unique to transactions
    //         ]);

    //         return true;

    //     } catch(\Yabacon\Paystack\Exception\ApiException $e){

    //         // print_r($e->getResponseObject());
    //         // die($e->getMessage());
    //         return false;

    //     }
    // }

    // verify transaction refid
    public function verifyPaystackRef($ref_id)
    {
        // added for testing purposes
        return true;
    }

    
    /*
    |--------------------------------------------------------------------------
    | Compute and Load Assets information
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */
    public function filterFields($price, $qty)
    {
        $qty        = str_replace(",", "", $qty);
        $price      = str_replace(",", "", $price);
        # code...
        if(!is_numeric($price)){
            $data = array(
                'status'    => 'error',
                'message'   => $price.' is invalid !'
            );
        }elseif(!is_numeric($qty)){
            $data = array(
                'status'    => 'error',
                'message'   => $qty.' is invalid !'
            );
        }else{
            $data = "";
        }

        // return response
        return $data;
    }


    /*
    |--------------------------------
    | SCAN EVERY DIGITS TO INT
    |--------------------------------
    |
    */
    public function scanEveryDigits($data){

        $data = str_replace(",", "", $data);
        $data = (float)$data;
        // $data = (int)$data;
        return $data;
    }


    /*
    |--------------------------------
    | SHORTEN REQUEST DATE
    |--------------------------------
    |
    */
    public function scanDateForMobile($data){
        
        $data = str_replace('minutes', 'min', $data);
        $data = str_replace('minute', 'mins', $data);
        
        $data = str_replace('second', 'sec', $data);
        $data = str_replace('seconds', 'secs', $data);

        return $data;
    }

    /*
    |--------------------------------
    | this is a comments
    |--------------------------------
    |
    */
    public function stockNews()
    {
        // load pairs
        $pairs = Security::orderBy('id', 'desc')->get();
        if(count($pairs) > 0){
            $pair_box = [];
            foreach ($pairs as $pair) {
                # code...
                $data = [
                    "id"    => $pair->id,
                    "pairs" => $pair->security,
                    "open"  => number_format($pair->close_price, 2),
                    "close" => number_format($pair->previous_close, 2)
                ];
                array_push($pair_box, $data);
            }
        }else{
            $pair_box = [];
        }

        // return json response in array box
        return response()->json($pair_box);
    }


    /*
    |--------------------------------
    | LOAD SETTINGS
    |--------------------------------
    |
    */
    public function loadSettings(Request $request){

        $user_id    = $request->userid;        
        $basic_info = Basic::where('user_id', $user_id)->first();

        if($basic_info !== null){
            $data = [
                'user_id'   => $basic_info->user_id,
                'gender'    => $basic_info->gender,
                'address'   => $basic_info->address,
                'zip_code'  => $basic_info->zip_code,
                'state'     => $basic_info->state,
                'phone'     => $basic_info->phone,
                'avatar'    => 'http://equities.catss.ng/uploads/'.$basic_info->avatar              
            ];
            $code = 200;

        }else{

            $data = [
                'status'    => 'error',
                'message'   => 'user not found !'
            ];
            $code = 401;
        }

        // return response
        return $this->apiJsonResponse($data, $code);
    }

    /*
    |--------------------------------
    | UPDATE SETTINGS
    |--------------------------------
    |
    */
    public function updateSetting(Request $request){
        $user_id    = $request->userid;
        $address    = $request->address;
        $phone      = $request->phone;
        $state      = $request->state;

        $basic_info = Basic::where('user_id', $user_id)->first();
        if($basic_info !== null){

            // find and update
            $update_info            = Basic::find($basic_info->id);
            $update_info->address   = $address;
            $update_info->phone     = $phone;
            $update_info->state     = $state;
            $update_info->save();

            $data = [
                'status'    => 'success',
                'message'   => 'update settings successfully !'
            ];
            $code = 200;

        }else{

            $data = [
                'status'    => 'error',
                'message'   => 'user not found !'
            ];

            $code = 402;
        }

        // find and update fields
        return $this->apiJsonResponse($data, $code);
    }

    /*
    |--------------------------------
    | UPDATE SECURITY
    |--------------------------------
    |
    */
    public function updateSecurity(Request $request){

        $user_id        = $request->userid;
        $old_password   = $request->oldpassword;
        $new_password   = $request->newpassword;

        // check user
        $user = User::where('id', $user_id)->first();
        if($user !== null){

            // compare password
            if (\Hash::check($old_password, $user->password)){
                // The passwords match...
                $update_user            = User::find($user_id);
                $update_user->password  = bcrypt($new_password);
                $update_user->update();

                $data = [
                    'status' => 'success',
                    'message'=> 'password updated successfully !'
                ];
                $code = 200;

            }else{

                $data = [
                    'status' => 'error',
                    'message'=> 'invalid password, try again !'
                ];
                $code = 401;
            }
        }else{

            $data = [
                'status' => 'error',
                'message'=> 'invalid user'
            ];
            $code = 401;
        }

        // return response 
        return $this->apiJsonResponse($data, $code);
    }


    /*
    |--------------------------------
    | SEND CONTACT US 
    |--------------------------------
    |
    */
    public function contactUs(Request $request){
        $user_id = $request->userid;
        $subject = $request->subject;
        $body    = $request->message;

        $user = User::where('id', $user_id)->first();
        $admin_mail = 'bernard.ekpoto@cavidel.com';

        $mail_data = [
            'name'      => $user->name,
            'subject'   => $subject,
            'body'      => $body
        ];

        // send mail to admin and auto reply user
        \Mail::to($admin_mail)->send(new ContactUsAndroid($mail_data));
        \Mail::to($user->email)->send(new ContactAutoReplyAndroid($mail_data));

        $data = [
            'status'    => 'success',
            'message'   => 'message has been sent !'
        ];
        $code = 200;

        // return response 
        return $this->apiJsonResponse($data, $code);
    }


    /*
    |--------------------------------
    | Return json with type
    |--------------------------------
    |
    */
    public function apiJsonResponse($data, $code){
        return response($data, $code, $headers = [
            'Content-Type' => 'application/json'
        ]);
    }


    /*
    |-----------------------------------------
    | WITHDRAWAL REQUEST
    |-----------------------------------------
    */
    public function requestWithdraw(Request $request){
        // body
        $amount     = $request->amount;
        $user_id    = $request->userid;

        // validate user account balance
        $account = Account::where("user_id", $user_id)->first();
        if($account !== null){

            // validate account details 
            if($account->account_number !== null && $account->account_name !== null && $account->account_bank !== null){
                // validate user account balance
                if($account->account_balance >= $amount){

                    // process trade 
                    $this->debitAccount($account->id, $amount);  // debit user account
                    $this->logTransfer($user_id, $amount);

                    $data = [
                        'status'    => 'success',
                        'message'   => 'Transaction successful!'
                    ];

                }else{
                    $data = [
                        'status'  => 'error',
                        'message' => 'insufficient balance !'
                    ];
                }
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'No bank details found!'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Invalid user account'
            ];
        }

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | GET WITHDRAWS
    |-----------------------------------------
    */
    public function fetchUserWithdraw(Request $request){
        // body
        $user_id    = $request->userid;

        $withdraw   = new Transfer();
        $data       = $withdraw->fetchAll($user_id);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | ADD BANK DETAILS
    |-----------------------------------------
    */
    public function addBankDetails(Request $request){
        // body
        $user_id    = $request->userid;

        $bank_info  = new Account();
        $data       = $bank_info->updateBankAccount($user_id, $request);

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | FETCH ALL BANKS
    |-----------------------------------------
    */
    public function fetchAllBanks(Request $request){
        $banks  = new Transfer();
        $data   = $banks->fetchBanks();

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | FETCH USER BANK DETAILS
    |-----------------------------------------
    */
    public function getUserBankInfo(Request $request){
        // body
        $user_id    = $request->userid;

        $account    = new Account();
        $data       = $account->getAccountDetails($user_id);

        // return response.
        return response()->json($data);
    }


    /*
    |-----------------------------------------
    | LOG TRANSFER
    |-----------------------------------------
    */
    public function logTransfer($user_id, $amount){
        // body
        $transfer = new Transfer();
        $transfer->saveNewTransfer($user_id, $amount);
    }

    /*
    |-----------------------------------------
    | ADD FINGER PRINTS
    |-----------------------------------------
    */
    public function addFingerPrint(Request $request){
        // body
        $user = new User();
        $data = $user->addFingerPrint($request);

        // return response.
        return response()->json($data);
    }

    /*
    |-----------------------------------------
    | VERIFY FINGER PRINTS
    |-----------------------------------------
    */
    public function verifyFingerPrintHash(Request $request){
        // body
        $user = new User();
        $data = $user->verifyFingerPrint($request);

        // return response.
        return response()->json($data);
    }
}
