<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Invitation;
use Auth;

class AcceptInvitationsController extends Controller
{
    // accept invites
    public function acceptFromLink($token)
    {
    	# code...
    }

    // accept invites
    public function acceptFromBtn(Request $request, $id)
    {
    	$accept = Invitation::find($id);
    	$accept->status = 'yes';
    	$accept->save();

    	return redirect()->back();
    }

    // accept invites
    public function reject($id)
    {
    	$accept = Invitation::find($id);
    	$accept->status = 'rejected';
    	$accept->save();

    	return redirect()->back();
    }
}
