<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use CATSS\Events\NewStockUpload;
use CATSS\Events\EquityUpdateChange;
use CATSS\Events\UpdateChart;
use CATSS\Events\NewChatMsg;
use CATSS\Mail\WeeklyWinner;
use CATSS\TempSecurity;
use Carbon\Carbon;
use CATSS\Order;
use CATSS\Rank;
use CATSS\Statement;
use CATSS\Transaction;
use CATSS\Stock;
use CATSS\Vault;
use CATSS\Payment;
use CATSS\Account;
use CATSS\Wallet;
use CATSS\News;
use CATSS\Pair;
use CATSS\User;
use CATSS\Chat;
use CATSS\Cats;
use CATSS\Security;
use CATSS\Cast;
use CATSS\Graph;
use DB;
use Excel;

class AdminController extends Controller {

    protected $redirectTo = "admin/login";

	// admin guard
	public function __construct() {
		$this->middleware('auth:admin')->except('logoutAdmin');
	}

	// take admin to home
	public function dashboard(User $users) {
		$all_users = User::with('accounts')->orderBy('id', 'DESC')->get();
		return view("admin-pages.admin-dashboard", compact('all_users'));
	}

    // rewarding winner view
    public function winnerBoard($id, $p, $n)
    {
        $user_info = User::where('id', $id)->first();
        # code...
        return view('admin-pages.winner-board', compact('id', 'user_info', 'p', 'n'));
    }

	// customer care units
	public function helpDesk()
	{
		return view('admin-pages.customer-care');
	}

	// load commission 
	public function commission()
	{
		# code...
		return view('admin-pages.admin-commission');
	}
    
    // view transactions 
    public function transactions()
    {
        # code...
        return view('admin-pages.transactions');
    }

    // view transactions 
    public function showUserTransactions($user_id)
    {
        # code...
        return view('admin-pages.user-transactions', compact('user_id'));
    }

    // view orders 
    public function orders()
    {
        # code...
        return view('admin-pages.orders');
    }

	// show notifications
	public function showNotifications()
	{
		$all_chats = Chat::where([['to', 'admin'], ['status', 'unread']])->orderBy('id', 'desc')->take('10')->get();
		return view('admin-pages.notifications', compact('all_chats'));
	}

	// create admin 
	public function createBrokerageFirm()
	{
		# code...
		return view('admin-pages.create-brokerage-firm');
	}

    // create admin 
    public function createBroker()
    {
        # code...
        return view('admin-pages.create-broker');
    }


    // create admin 
    public function newClient()
    {
        # code...
        return view('admin-pages.create-clients');
    }
	
	// send rewards 
	public function rewardUser()
	{
		# code...
		return view('admin-pages.admin-reward');
	}

	// show chart screen 
	public function chartScreen()
	{
		return view('admin-pages.trade-charts');
	}


	// show trade ranking 
	public function tradeRanking()
	{
		# code...
		return view('admin-pages.trade-ranking');
	}

    public function viewBrokerAgent($id)
    {
       # code...
        $broker = User::where('id', $id)->first();
        return view('admin-pages.view-brokers', compact('broker')); 
    }
	
	// show users screen 
	public function allUsers()
	{
		$all_users = User::with('accounts')->orderBy('id', 'desc')->get();
		return view('admin-pages.all-users', compact('all_users'));
	}

	// public function upload Equities
	public function dropzoneStore(Request $request)
    {
    	// read files 
        $excel = $request->file('file');
        $path = $request->file('file')->getRealPath();

        // run excel import
        $data = Excel::load($path, function ($reader){})->get();

        // check and count total data
        if(!empty($data) && $data->count()){

        	// data prepared, cast securites as old data
        	$old_securities = TempSecurity::all();
            if(count($old_securities) > 0){
                foreach ($old_securities as $casting) {
                    # code...
                    $casts_data                 = new Cast();
                    $casts_data->board          = $casting->board;
                    $casts_data->security       = $casting->security;
                    $casts_data->ref_price      = $casting->ref_price;
                    $casts_data->open_price     = $casting->open_price;
                    $casts_data->high_price     = $casting->high_price;
                    $casts_data->low_price      = $casting->low_price;
                    $casts_data->close_price    = $casting->close_price;
                    $casts_data->change_price   = $casting->change_price;
                    $casts_data->daily_volume   = $casting->daily_volume;
                    $casts_data->daily_value    = $casting->daily_value;
                    $casts_data->dvmv_today     = $casting->off_mkt_volume_today;
                    $casts_data->mvtn_trade     = $casting->off_mkt_value_today;
                    $casts_data->no_of_trade    = $casting->number_of_trades;
                    $casts_data->previous_close = $casting->previous_close;
                    $casts_data->status         = 'casted';
                    $casts_data->save();
                }
            }

			// empty securities for newest updates
			TempSecurity::truncate();
        	foreach ($data->toArray() as $key => $value) {
    			$security 					= new TempSecurity();
				$security->board	 		= $value['board'];
				$security->security 		= $value['security'];
				$security->ref_price 		= $value['ref_price'];
				$security->open_price 		= $value['open_price'];
				$security->high_price 		= $value['high_price'];
				$security->low_price 		= $value['low_price'];
				$security->close_price 		= $value['close_price'];
				$security->change_price 	= $value['change_price'];
				$security->daily_volume 	= $value['daily_volume'];
				$security->daily_value 		= $value['daily_value'];
				$security->dvmv_today 		= 0000;
				$security->mvtn_trade 		= 0000;
				$security->previous_close 	= $value['previous_close'];
				$security->no_of_trade 		= 0000;
				$security->status 			= 'open';
				$security->save();
        	}

            // update market from Temp Security
            $temp_security_updates = TempSecurity::all();
            if(count($temp_security_updates) > 0){

                // update records only
                foreach ($temp_security_updates as $temp_security) {
                    # code...
                    # find and update security
                    $security = Security::where("security", $temp_security->security)->first();
                    if($security !== null){
                        // security exits, update prices
                        $update_security                 = Security::find($security->id);
                        $update_security->board          = $temp_security->board;
                        $update_security->security       = $temp_security->security;
                        $update_security->ref_price      = $temp_security->ref_price;
                        $update_security->open_price     = $temp_security->open_price;
                        $update_security->high_price     = $temp_security->high_price;
                        $update_security->low_price      = $temp_security->low_price;
                        $update_security->close_price    = $temp_security->close_price;
                        $update_security->change_price   = $temp_security->change_price;
                        $update_security->daily_volume   = $temp_security->daily_volume;
                        $update_security->daily_value    = $temp_security->daily_value;
                        $update_security->dvmv_today     = $temp_security->off_mkt_volume_today;
                        $update_security->mvtn_trade     = $temp_security->off_mkt_value_today;
                        $update_security->no_of_trade    = $temp_security->number_of_trades;
                        $update_security->previous_close = $temp_security->previous_close;
                        $update_security->status         = 'open';
                        $update_security->update();
                    }else{
                        // this is a new security add to security table
                        $add_new_security                 = new Security();
                        $add_new_security->board          = $temp_security->board;
                        $add_new_security->security       = $temp_security->security;
                        $add_new_security->ref_price      = $temp_security->ref_price;
                        $add_new_security->open_price     = $temp_security->open_price;
                        $add_new_security->high_price     = $temp_security->high_price;
                        $add_new_security->low_price      = $temp_security->low_price;
                        $add_new_security->close_price    = $temp_security->close_price;
                        $add_new_security->change_price   = $temp_security->change_price;
                        $add_new_security->daily_volume   = $temp_security->daily_volume;
                        $add_new_security->daily_value    = $temp_security->daily_value;
                        $add_new_security->dvmv_today     = $temp_security->off_mkt_volume_today;
                        $add_new_security->mvtn_trade     = $temp_security->off_mkt_value_today;
                        $add_new_security->no_of_trade    = $temp_security->number_of_trades;
                        $add_new_security->previous_close = $temp_security->previous_close;
                        $add_new_security->status         = 'open';
                        $add_new_security->save();
                    }
                }
            }
        }

        // rename file
        // $excelName = time().$excel->getClientOriginalName();
        $excel_extension = $excel->getClientOriginalExtension();
        $excel_newname   = 'CATSS-EQUITIES'.date("M-D-Y-h-i").'.'.$excel_extension;

        $excel->move(public_path('equities'),$excel_newname);

        $current = Carbon::now();

        $catss = DB::table('catss')->insert([
        	'name'         => $excel_newname,
        	'created_at'   => $current,
        	'updated_at'   => $current
        ]);

        // get last updated data
        $catss = DB::table('catss')->where('name', $excel_newname)->first();

        $data = [
        	'id'   => $catss->id,
        	'name' => $catss->name,
        	'date' => $catss->created_at
        ];

        // send Event
        \Event::fire(new NewStockUpload($data));

        // res data
        $res_data = [
            'status'    => 'success',
            'message'   => 'file uploaded successfully !',
            'data'      => $excel_newname
        ];

        // return jsonImage Name
        return response()->json($res_data);
    }

    // load all equities list
    public function loadEquityList()
    {
    	$catss_nse = DB::table('catss')->orderBy('created_at', 'desc')->limit('20')->get();

    	$data_box = [];
    	foreach ($catss_nse as $download) {
    		# code...
    		$data = array(
    			'id'     => $download->id,
    			'name'   => $download->name,
    			'date'   => $download->created_at
    		);

    		array_push($data_box, $data);
    	}

    	return response()->json($data_box);
    }

	// show add form interface
	public function toggleStock() {
		$securities = Security::orderBy('id', 'asc')->get();
		return view('admin-pages.admin-create-stock', compact('securities'));
	}

	// update sec
	public function updateSec(Request $request, $sid)
	{
		$new_price =  $request->touch_price;

		# code...
		$security = Security::find($sid);
		$security->close_price = $new_price; 
		$msg = $security->security." price updated successfully to ".$new_price;
		$security->update();

		return redirect()->back()->with('t_status', $msg);
	}

	// update sec
	public function updatePrice()
	{
		// load pairs from random
        $pairs = Security::all();

        // random float functio for decim 
        function rand_float($st_num = 0,  $end_num = 1, $mul = 1000000){
            if ($st_num > $end_num) {
                return false;
            }
            return mt_rand($st_num * $mul, $end_num * $mul) / $mul;
        }

        // pair box 
        $pair_box = [];
        foreach ($pairs as $equity) {
            // between high & low band value 
            $take_off = rand_float($equity->low_band, $equity->high_band);

            // close price 
            $close_price = $take_off;

            // round close price to 4 decimal place
            $close_price = round($close_price, 4);

            // find & updates securities
            $security = Security::find($equity->id);
            $security->new_band = $close_price;
            $security->update();
        }

        // return response
        $data = array(
            'status'  => 'success',
            'message' => 'Price updates return successful !'
        );

        // return json response in array box
        \Event::fire(new EquityUpdateChange());
        // \Event::fire(new UpdateChart());
        return response()->json($data);
	}

	// show all stocks
	public function allStock() 
	{
		$pairs = Pair::orderBy('equity', 'desc')->get();
		return view('admin-pages.admin-update-stock', compact('pairs'));
	}

	// load admin chats 
    public function loadChatAdmin()
    {

        // check and single chat msg 
        $all_chats = Chat::where('to', 'admin')->orderBy('id', 'desc')->get();

        // check fetch chat message
        $msg_box = [];
        foreach ($all_chats as $chat) {
            $data = array(
            	'id' => $chat->id,
            	'from' => $chat->from,
                'to' => $chat->to,
                'body' => $chat->body,
                'status' => $chat->status,
                'date' => $chat->created_at->diffForHumans()
            );
            array_push($msg_box, $data);
        }

        return response()->json($msg_box);
    }

    // reply chat admin
    public function replyChat(Request $request)
    {
    	// assign request
    	$id = $request->id;
    	$msg = $request->msg;

    	//check who sent msg
    	$replying_msg = Chat::where('id', $id)->first();

    	$from = $replying_msg->from;
    	$from_id = $replying_msg->user_id;

    	// store new data
    	$reply = new Chat();
    	$reply->to = $from;
    	$reply->from = 'admin';
    	$reply->body = $msg;
    	$reply->status = 'unread';
    	$reply->user_id = $from_id;
    	$reply->save();


    	$last_chats = Chat::where([['user_id', $from_id], ['body', $msg]])->orderBy('id', 'desc')->take('1')->first();

        $data = array(
            'from' => 'Agent',
            'to' => $last_chats->to,
            'body' => $last_chats->body,
            'status' => $last_chats->status,
            'date' => $last_chats->created_at->diffForHumans()
        );

        // call events
        \Event::fire(new NewChatMsg($data));

    	$msg = "reply sent! ";
    	echo $msg;
    }

	// delete chat msg
	public function deleteChatMsg($id)
	{
		// $id = $request->id;

		$chatMsg = Chat::find($id);
		$chatMsg->status = 'recycle';
		$chatMsg->update();

		$msg = " Message deleted !";
		return redirect()->back()->with('msg_delete_status', $msg);
	}

	// delete user msg
	public function removeUser($id)
	{
		// $id = $request->id;

		$user = User::find($id);
		$user->delete();

		$msg = " User has been deleted !";
		return redirect()->back()->with('remove_status', $msg);
	}

	// delete user msg
	public function blockUser($id)
	{
		// $id = $request->id;
		$accounts = Acccount::where('user_id', $id)->first();
		$update_account = Acccount::find($accounts->id);
		$update_account->account_status = 'blocked';
		$update_account->update();

		$msg = " User has been blocked !";
		return redirect()->back()->with('remove_status', $msg);
	}

	// show news form
	public function newsForm() {
		// fetch all recent news
		$news = News::orderBy('id', 'desc')->get();

		return view("admin-pages.news-form", compact('news'));
	}

	// Publish a news 
	public function postNews(Request $request, News $news) {
		// get request from users
		$news_title = $request->title;
		$news_body  = $request->body;

		// filtered news body
		$news_body = trim(strip_tags($news_body));

		// save news
		$news        = new News();
		$news->title = $news_title;
		$news->body  = $news_body;
		$news->save();

		// pass message
		$msg = $news_title . " has been published successfully ";
		// redirect back to news
		return redirect()->back()->with('news_status', $msg);
	}

	// reset account 
	public function resetWeek($id)
    {
        # reset account 
        $account = Account::where('user_id', $id)->first();

        // reset account balance
        $reset_account = Account::find($account->id);
        $reset_account->account_balance = 0;
        $reset_account->update();

        // delete all transaction
        $transactions = Transaction::where('user_id', $id)->get();
        foreach ($transactions as $trans) {
            # code...
            $delete_transaction = Transaction::find($trans->id);
            $delete_transaction->delete();
        }

        // delete all stocks
        $stocks = Stock::where('user_id', $id)->get();
        foreach ($stocks as $stock) {
            # code...
            $delete_stock = Stock::find($stock->id);
            $delete_stock->delete();
        }

        // delete all statement
        $statements = Statement::where('user_id', $id)->get();
        foreach ($statements as $stat) {
            # code...
            $delete_statement = Statement::find($stat->id);
            $delete_statement->delete();
        }

        $msg = $account->account_id.' Account reset successfully !';
        
        // redirect response
        return redirect()->back()->with('reset_msg', $msg);
    }

    // reset account 
    public function resetWeekPaidUsers($id)
    {
        # reset account 
        $account = Account::where('user_id', $id)->first();
        if($account !== null){
            // reset account balance
            $reset_account = Account::find($account->id);
            $reset_account->account_balance = 10000000;
            $reset_account->update();
        }

        // update payments quota
        $payments_quota = Payment::where('user_id', $id)->first();
        
        // update quota
        $updated_payment        = Payment::find($payments_quota->id);
        $updated_payment->quota = $updated_payment->quota - 1;
        $updated_payment->update(); 

        // delete all transaction
        $transactions = Transaction::where('user_id', $id)->get();
        foreach ($transactions as $trans) {
            # code...
            $delete_transaction = Transaction::find($trans->id);
            $delete_transaction->delete();
        }

        // delete all stocks
        $stocks = Stock::where('user_id', $id)->get();
        foreach ($stocks as $stock) {
            # code...
            $delete_stock = Stock::find($stock->id);
            $delete_stock->delete();
        }

        // delete all statement
        $statements = Statement::where('user_id', $id)->get();
        foreach ($statements as $stat) {
            # code...
            $delete_statement = Statement::find($stat->id);
            $delete_statement->delete();
        }

        $msg = $account->account_id.' Account reset and rollover successfully !';
        
        // redirect response
        return redirect()->back()->with('reset_msg', $msg);
    }

	// update news
	public function updateNews(Request $request){
		$id    = $request->id;
		$title = $request->title;
		$body  = $request->body;

		$update_news        = News::find($id);
		$update_news->title = $title;
		$update_news->body  = $body;
		$update_news->update();

		$data = array(
			'status' => 'success',
			'message' => 'new updated successfully !'
		);
		// return response
		return response()->json($data);
	}

	// update news
	public function loadNews($id){

		$news = News::where('id', $id)->first();

		$body = str_replace('  ', '</p><p>', $news->body);
		$data = array(
			'id'    => $news->id,
			'title' => $news->title,
			'body'  => $body,
			'date'  => $news->created_at->diffForHumans(),
		);
		// return response
		return response()->json($data);
	}

	// update news
	public function loadAllNews(){

		$all_news = News::orderBy('id', 'desc')->get();

		$news_box = [];
		foreach ($all_news as $news) {
			# code...
			$body = str_replace('  ', '</p><p>', $news->body);
			$data = array(
				'id'    => $news->id,
				'title' => $news->title,
				'body'  => $body,
				'date'  => $news->created_at->diffForHumans()
			);
			array_push($news_box, $data);
		}

		// return response
		return response()->json($news_box);
	}

	// edit news 
	public function editNews($id){
		$news = News::where('id', $id)->first();
		return view("admin-pages.news-edit", compact('news'));
	}

	// load users ranking 
    public function loadRanking()
    {
        // first get all users 
        $all_users = User::all();
        $rank_box = [];
        foreach ($all_users as $user) {
            // scanned users stock by user_id 
            $amount = DB::table('stocks')->where('user_id', $user->id)->sum('amount');

            $data = array(
                'name'   => $user->name,
                'amount' => number_format($amount, 2),
                'date'   => date("d' D M Y")
            );

            array_push($rank_box, $data);
        }

        return $rank_box;
    }

    // load vault balance
    public function loadVault()
    {
    	# code...
    	$vaults = Vault::all();

    	$vault_box = [];
    	foreach ($vaults as $vault) {
    		$user_info = User::where('id', $vault->user_id)->first();
    		$account_info = Account::where('user_id', $vault->user_id)->first();
    		# code...
    		$data = array(
    			'id'     => $vault->id,
    			'name'   => $user_info->name,
    			'email'  => $account_info->account_balance,
    			'type'   => $vault->type,
    			'amount' => number_format($vault->amount, 2),
    			'date'   => $vault->created_at->diffForHumans()
    		);

    		array_push($vault_box, $data);
    	}

    	// return data
    	return response()->json($vault_box);
    }

    // log payment
    public function paymentLog()
    {
    	# code...
    	$payments = Payment::all();

    	$payment_box = [];
    	foreach ($payments as $payment) {
    		$user_info = User::where('id', $payment->user_id)->first();
    		# code...
    		$data = array(
    			'id'       => $payment->id,
    			'trans_id' => $payment->trans_id,
                'user_id'  => $payment->user_id,
    			'name'     => $user_info->name,
    			'amount'   => number_format($payment->amount, 2),
    			'balance'  => number_format($payment->balance, 2),
    			'quota'    => $payment->quota,
    			'status'   => $payment->status,
    			'date'     => $payment->created_at->diffForHumans()
    		);

    		array_push($payment_box, $data);
    	}

    	// return data
    	return response()->json($payment_box);
    }

    // pay winners payouts
    public function rewardPrice(Request $request)
    {

        $userId   = $request->userId;
        $position = $request->position;
        $amount   = $request->amount;

        // set_postion
        if($position == 1){
            # code...
            $position = '1st Place';
            $percent = 0.25;

        }elseif ($position == 2) {
            # code...
            $position = '2nd Place';
            $percent = 0.15;

        }elseif ($position == 3) {
            # code...
            $position = '3rd Place';
            $percent = 0.10;
        }

        // user_information
        $user = User::where('id', $userId)->first();

        $data = array(
            'name'    => $user->name,
            'amount'  => $amount,
            'position' => $position
        );

        // wallet calculation
        $payments = Payment::all();
        $total = $payments->sum('amount') / 4;

        // calculate prize
        $balance = $percent * $total;
        $status = 'active';

        // depost wallet amount 
        $wallets          = new Wallet();
        $wallets->user_id = $userId;
        $wallets->balance = $balance;
        $wallets->status  = $status;
        $wallets->save();

        // create records
        $log_ranking          = new Rank();
        $log_ranking->user_id = $userId;        
        $log_ranking->net     = $amount;
        $log_ranking->save();

        // send User an Email
        \Mail::to($user->email)->send(new WeeklyWinner($data));

        # code...
        $data = array(
            'status'  => 'success',
            'message' => 'Payout successfully sent !'
        );

        return response()->json($data);
    }

	// delete news updates
	public function deleteNews($id) {
		$news = News::find($id);
		$news->delete();
		return redirect()->back();
	}

	// signout admin
	public function logoutAdmin() {
		Auth::guard('admin')->logout();
		return redirect('/admin/login');
	}
}
