<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Activitylog\Models\Activity;
use CATSS\User;

class ActivityLogController extends Controller
{

    /*
    |--------------------------------
    | LOAD ACTIVITY LOGS 
    |--------------------------------
    |
    */
    public function loadLogs(){

    	$all_logs = Activity::orderBy('created_at', 'desc')->get();

    	if(count($all_logs) >0){

    		$logs_box = [];
			foreach ($all_logs as $log) {

				$user_info = User::where('id', $log->causer_id)->first();
				if($user_info == null){
					$by = 'Guest';
				}else{
					$by = $user_info->name;
				}


				# code...
				$data = [
					'id' 		=> $log->id,
					'by' 		=> $by,
					'activity'	=> $log->description,
					'created' 	=> $log->created_at->diffForHumans()
				];

				array_push($logs_box, $data);
			}

    	}else{
    		
    		$logs_box = [];
    	}

    	return response()->json($logs_box);
    }
}
