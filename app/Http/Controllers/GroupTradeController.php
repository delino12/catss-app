<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use CATSS\Mail\TradeInvitations;
use CATSS\Account;
use CATSS\Demotrade;
use CATSS\User;
use CATSS\Group;
use CATSS\Invitation;
use Session;

class GroupTradeController extends Controller
{
	public function __construct() {
		$this->middleware('auth');
	}

    //
    public function loadTrade($group_name, $group_id)
    {
    	$msg = $group_name.' with id of '.$group_id;
    	return "welcome to ".$msg;
    }

	public function startDemo() {
		$user_id = User::find(Auth::user()->id);
		$id = $user_id->id;

		// fetch account information
		$account_info = Account::where('user_id', $id)->get();

		// load pairs
		$pairs = Demotrade::orderBy('equity', 'desc')->get();

		return view('internal-pages.demo-setup-live', compact('pairs', 'account_info'));
	}

	public function demo() {

		$user_id = User::find(Auth::user()->id);
		$id = $user_id->id;

		// fetch account information
		$account_info = Account::where('user_id', $id)->get();

		// load pairs
		$pairs = Demotrade::orderBy('equity', 'desc')->get();

		$msg = "You are trading on CATSS ";
		Session::flash('flash_msg', $msg);

		return view('internal-pages.demo-ntrade-live', compact('pairs', 'account_info'));
	}

	public function createTradeGroup(Request $request, Group $groups) {
		// auth data
		$user_id = Auth::user()->id;
		// form data
		$name = $request->group_name;
		$level = 'stage-1';
		$members = $request->group_memebers;

		// generated data
		$status = 'open';
		$link = 'http://equities.catss.ng/accept-invites/?token='.$request->_token;

		// check if group is already create 
		$already_exits = Group::where('name', $name)->first();
		if($already_exits !== null){
			// if group already created
			$msg = $name." already_exits !";	
			return redirect()->back()->with("error_status", $msg);
		}else{
			// this is a new group
			// save and create trade group
			$groups          = new Group();
			$groups->user_id = $user_id;
			$groups->name    = $name;
			$groups->level   = $level;
			$groups->members = $members;
			$groups->status  = $status;
			$groups->links   = $link;
			$groups->save();

			// check if email is single
			$comma_mail = strpos($members, ',');
			if($comma_mail === false){
				$data = array(
					'host'  => Auth::user()->name,
					'guest' => $members,
					'link'  => $link
				);

				# send mail to single users
				\Mail::to($members)->send(new TradeInvitations($data));

				// Create invitations boards
				$invites = new Invitation();
				$invites->host   = Auth::user()->email;
				$invites->guest  = $members;
				$invites->status = 'no';
				$invites->save();

			}else{
				// send to multiple users
				// fetch group id from group also create new trading board
				$split_memebers = explode(',', $members);
				$mail_box = array();
				for ($i=0; $i < count($split_memebers); $i++) { 
					# code...
					$split_memebers[$i] = str_replace(' ', '', $split_memebers[$i]);
					array_push($mail_box, $split_memebers[$i]);

					$data = array(
						'host'  => Auth::user()->name,
						'guest' => $split_memebers[$i],
						'link'  => $link
					);

					# send mail to mulitple members 
					\Mail::to($split_memebers[$i])->send(new TradeInvitations($data));
				}

				// create invites list
				for ($i=0; $i < count($mail_box); $i++) { 
					# create boards for multiple invites
					$invites = new Invitation();
					$invites->host   = Auth::user()->email;
					$invites->guest  = $mail_box[$i];
					$invites->status = 'no';
					$invites->save();
				}
			}	

			// All has been created now tell users is clear
			$msg = $name." successfully created !, invitations link has been sent !";	
			return redirect()->back()->with("update_status", $msg);
		}
	}

	// load Group trading...
	public function loadGroupTrade($id)
	{
		$group_info = Group::where('id', $id)->get();

		return view('catss-live-group', compact('group_info'));
	}
}
