<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Events\ClearOrder;
use CATSS\Events\NewAssets;
use CATSS\Events\AccountUpdate;
use CATSS\Events\TransactionNotifications;
use CATSS\Events\StockBalanceNotifications;
use CATSS\Mail\OrderNotify;
use CATSS\Account;
use CATSS\Transaction;
use CATSS\Stock;
use CATSS\Statement;
use CATSS\User;
use CATSS\Order;
use CATSS\Security;

class ProcessOrdersController extends Controller
{
	// init trade command
	protected $trade;


	// init contructor
	public function __construct()
	{
		# init trade action...
		$this->trade = false;
	}

    // scan orders ready for trade
    public function scanningBuyOrders()
    {
    	// scanning orders 
    	$all_orders = Order::where('type', 'buy')->get();
    	if(count($all_orders) > 0){
    		// order results
    		$order_box = [];
	    	foreach ($all_orders as $order) {
	    		// check security for scan results
	    		$security = Security::where([['id', $order->sec_id]])->get();
    			$sec_box = [];
    			foreach ($security as $asset) {
    				// check if order pass market price check
    				if($order->price >= $asset->new_band){
    					$this->trade = true;
    				}else{
    					$this->trade = false;
    				}

    				// execute trade
    				if($this->trade == true){

    					// jarvis trade process trade
    					$this->sweepOrders($order->user_id, $order->price, $order->qty, $order->sec_id, $order->type, $order->ref_id);

    					# code...
	    				$data = array(
	    					'order_id' 		=> $order->id,
	    					'user_id'		=> $order->user_id,
	    					'ask_price'		=> $order->price,
	    					'mkt_price'		=> $asset->new_band,
	    					'trade'			=> $this->trade
	    				);
	    				array_push($sec_box, $data);

    				}else{
    					// keep goin jarvis
    					# code...
	    				$data = array(
	    					'order_id' 		=> $order->id,
	    					'user_id'		=> $order->user_id,
	    					'ask_price'		=> $order->price,
	    					'mkt_price'		=> $asset->new_band,
	    					'trade'			=> $this->trade
	    				);
	    				array_push($sec_box, $data);
    				}
    			}

    			array_push($order_box, $sec_box);
	    	}

	    	$data = array(
	            'status'  => 'success',
	            'message' => 'found orders',
	            'orders'  => $order_box
	        );
	    }else{
	    	$data = array(
	            'status'  => 'success',
	            'message' => 'no order has been job yet !'
	        );
	    }

        // response data
        return response()->json($data);
    }

    // scan orders ready for trade
    public function scanningSellOrders()
    {
        // scanning orders 
        $all_orders = Order::where('type', 'sell')->get();
        if(count($all_orders) > 0){
            // order results
            $order_box = [];
            foreach ($all_orders as $order) {
                // check security for scan results
                $security = Security::where([['id', $order->sec_id]])->get();
                $sec_box = [];
                foreach ($security as $asset) {
                    // check if order pass market price check
                    if($order->price <= $asset->new_band){
                        $this->trade = true;
                    }else{
                        $this->trade = false;
                    }

                    // execute trade
                    if($this->trade == true){

                        // jarvis trade process trade
                        $this->sweepOrders($order->user_id, $order->price, $order->qty, $order->sec_id, $order->type, $order->ref_id);

                        # code...
                        $data = array(
                            'order_id'      => $order->id,
                            'user_id'       => $order->user_id,
                            'ask_price'     => $order->price,
                            'mkt_price'     => $asset->new_band,
                            'trade'         => $this->trade
                        );
                        array_push($sec_box, $data);

                    }else{
                        // keep goin jarvis
                        # code...
                        $data = array(
                            'order_id'      => $order->id,
                            'user_id'       => $order->user_id,
                            'ask_price'     => $order->price,
                            'mkt_price'     => $asset->new_band,
                            'trade'         => $this->trade
                        );
                        array_push($sec_box, $data);
                    }
                }

                array_push($order_box, $sec_box);
            }

            $data = array(
                'status'  => 'success',
                'message' => 'found orders',
                'orders'  => $order_box
            );
        }else{
            $data = array(
                'status'  => 'success',
                'message' => 'no order has been job yet !'
            );
        }

        // response data
        return response()->json($data);
    }

    // sweep orders with due state
    public function sweepOrders($user_id, $price, $qty, $sec_id, $type, $ref_id)
    {

    	// jarvis sweep orders
        $qty   = $qty;
        $type  = $type; 
        $price = $price;

        $user = User::where("id", $user_id)->first();

        // jarvis is buying 
        if($type == 'buy'){

        	// check account 
        	$user_account = Account::where('user_id', $user_id)->first();
        	if($user_account !== null){
        		
        		// get account balance 
        		$user_balance = $user_account->account_balance;

        		// check latest market price
        		$security = Security::where('id', $sec_id)->first();
        		if($security !== null){

        			// get current market price
        			$mkt_price = $security->new_band;

        			// use propose quantity
        			$amount = $mkt_price * $qty;

        			// check if user have enough balance
        			if($user_balance > $amount ){

        				// debit account 
        				$this->debitAccount($user_account->id, $amount);

        				// credit stock
        				$this->creditStock($user_id, $sec_id, $qty, $price);

        				// log transaction
        				$this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);

        				// log fail transactions (order)
        				$this->trashOrder($ref_id);

        				$data = array(
        					'user_id'	=> $user_id,
        					'status'	=> 'success',
        					'message' 	=> $security->security.' order has been cleared'
        				);

        				// fire events 
        				\Event::fire(new ClearOrder($data));


                        // user information
                        $user = User::where('id', $user_account->user_id)->first();
                        // mail data and mail fire
                        $mail_data = array(
                            'name'      => $user->name,
                            'message'   => $security->security.' equity purchased was successfully !'
                        );
                        \Mail::to($user->email)->send(new OrderNotify($mail_data));

        			}else{
        				// mail data and mail fire
                        $mail_data = array(
                            'name'      => $user->name,
                            'message'   => $security->security.' purchase failed. You do not have sufficient balance you can deposit '
                        );
                        \Mail::to($user->email)->send(new OrderNotify($mail_data));
        			}
        		}
        	}
        }

        // jarvis is selling 
        if($type == 'sell'){
            // get security
            $security = Security::where('id', $sec_id)->first();
            if($security !== null){

                // check account 
                $user_account = Account::where('user_id', $user_id)->first();
                if($user_account !== null){

                    // check if user has stock he's selling
                    $stock = Stock::where([['user_id', $user_id], ['name', $security->security]])->first();
                    if($stock !== null){

                        // get current market price
                        $mkt_price = $security->new_band;

                        $amount = $mkt_price * $qty;

                        // check if user has enough stock balance
                        if($stock->qty >= $qty){
                            // debit account 
                            $this->creditAccount($user_account->id, $amount);

                            // credit stock
                            $this->debitStock($user_id, $sec_id, $qty, $price);

                            // log transaction
                            $this->logTransaction($user_id, $amount, $qty, $price, $type, $sec_id);

                            // log fail transactions (order)
                            $this->trashOrder($ref_id);

                            $data = array(
                                'user_id'   => $user_id,
                                'status'    => 'success',
                                'message'   => $security->security.' order has been cleared'
                            );

                            // fire events 
                            \Event::fire(new ClearOrder($data));

                            // user information
                            $user = User::where('id', $user_account->user_id)->first();
                            // mail data and mail fire
                            $mail_data = array(
                                'name'      => $user->name,
                                'message'   => $security->security.' equity was sold successfully !'
                            );
                            \Mail::to($user->email)->send(new OrderNotify($mail_data));
                        }else{
                            // mail data and mail fire
                            $mail_data = array(
                                'name'      => $user->name,
                                'message'   => $asset->security.' trade fail. You do not have sufficient stock balance, you can always buy more units. '
                            );
                            \Mail::to($user->email)->send(new OrderNotify($mail_data));
                        }
                    }
                }
            }
        }
    }


    /*
    * Buy Section
    * Version 1.2.0
    * CATSS Engine
    * Trade request
    */

    /*
    |--------------------------------------------------------------------------
    | When purchasing 
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */

    // debit account
    public function debitAccount($id, $amount)
    {
    	# code...
    	// process trading and debit account balance 
		$account 					= Account::find($id);
		$account->account_balance 	= $account->account_balance - $amount;
		$account->update();
    }

    // credit user stocks
    public function creditStock($user_id, $sec_id, $qty, $price)
    {
    	# code...
    	$security = Security::where('id', $sec_id)->first();
    	if($security !== null){

	    	// first check if same stock already exits, if same stock exits update existing...
	        $stocks = Stock::where([['name', $security->security], ['user_id', $user_id]])->first();

	        // if pair is new
	        if($stocks == null){
	            // no stock exits so W/A is new
	            $f1 = $qty * $price;
	            $fresh_wa = $f1 / $qty;

	            $amount =  $qty * $price;

	            // else save new Stock
	            $stocks            = new Stock();
	            $stocks->user_id   = $user_id;
	            $stocks->name      = $security->security;
	            $stocks->qty       = $qty;
	            $stocks->price     = $price;
	            $stocks->amount    = $amount;
	            $stocks->w_average = $fresh_wa;
	            $stocks->save();
	        }else{
	            // now check if stock exist
	            $existed_stock = Stock::find($stocks->id);

	            // get the W/A Formular f1 + f2 / fQty
	            // capture incoming stock qty*price 
	            $f2 = $qty * $price;

	            // get 
	            $f1 = $existed_stock->qty * $existed_stock->w_average; 

	            # w/average
	            $f3 = $f1 + $f2; // compute net old qty * old price
	            $f4 = $existed_stock->qty + $qty; // compute net qty
	            $f5 = $f3/$f4;

	            $existed_stock->price       = $price;
	            $existed_stock->w_average   = $f5;
	            $existed_stock->qty         = $f4;
	            $existed_stock->amount      = $f3;
	            $existed_stock->update();
	        }
	    }
	}


    /*
    |--------------------------------------------------------------------------
    | When selling
    |--------------------------------------------------------------------------
    |
    | Debit Stock account and Credit Cash account
    |
    */

    // credit user bank account
    public function creditAccount($id, $amount)
    {
        # code...
        // process trading and debit account balance 
        $account                    = Account::find($id);
        $account->account_balance   = $account->account_balance + $amount;
        $account->update();

        // return account balance updates
        $account_balance = Account::where('user_id', $id)->first();
        $act_bal         = number_format($account_balance->account_balance, 2);

        // account data 
        $acct_data = array(
            'user_id' => $id,
            'bal'     => $act_bal
        );

        // Fire Account Balance
        \Event::fire(new AccountUpdate($acct_data));
    }

    // debit user stocks account
    public function debitStock($user_id, $sec_id, $qty, $price)
    {
        # code...
        $security = Security::where('id', $sec_id)->first();
        if($security !== null){
            // first check if same stock already exits, if same stock exits update existing...
            $stock = Stock::where([['name', $security->security], ['user_id', $user_id]])->first();

            if($stock !== null){
                // now update if stock exist
                $stocks         = Stock::find($stock->id);
                $stocks->qty    = $stocks->qty - $qty; // keypiece when users sell-> qty update
                $stocks->update();

                // a little store cleaning..
                $stocks = Stock::where([['user_id', $user_id], ['qty', 0]])->first();
                if($stocks !== null){
                    $refresh_stocks = Stock::find($stocks->id);
                    $refresh_stocks->delete();
                }

                # code...
                $w_a = $stock->w_average; // w/a

                // remove negative return...
                if($w_a > $price){
                    
                    $gap = $w_a - $price; // trade gap on w/a
                
                }elseif($w_a < $price){
                    
                    $gap = $price - $w_a; // trade gap on w/a

                }elseif($w_a == $price) {
                    
                    $gap = $w_a - $price; // trade gap on w/a
                }


                $f1 = $w_a * $qty; // on weighted average (holding value)
                $f2 = $price * $qty; // on demand (market value)


                if($f1 > $f2){
                    
                    $comments   = " trade Successful with loss gap of ".$gap;
                    $status     = "loss";

                }elseif($f2 > $f1){
                    
                    $comments   = " trade Successful with profit gap of ".$gap;
                    $status     = "profit";

                }elseif($f2 == $f1){

                    $comments   = " trade Successful with no profit/loss with return gap of ".$gap;
                    $status     = "break-even";
                }

                $top_bal    = $gap * $qty;
                $amount     = $price * $qty;
        
                // get id and update state
                $account_statements                     = new Statement();
                $account_statements->equity             = $security->security;
                $account_statements->w_average          = $w_a;
                $account_statements->user_id            = $user_id;
                $account_statements->last_sold_price    = $price;
                $account_statements->last_sold_qty      = $qty;
                $account_statements->amount             = $amount;
                $account_statements->gap                = $gap;
                $account_statements->balance            = $top_bal;       
                $account_statements->status             = $status;
                $account_statements->comments           = $comments;
                $account_statements->save();
            }
        }
    }

    /*
    |--------------------------------------------------------------------------
    | Log transactions and create statement
    |--------------------------------------------------------------------------
    |
    | Here is where you can register web routes for your application. These
    |
    */

    // log financial statement for revenue
    public function financialStatment($user_id, $sec_id, $qty, $price)
    {
        # code...
        
    }

	// log transactions
	public function logTransaction($user_id, $amount, $qty, $price, $type, $sec_id)
	{
		$security = Security::where('id', $sec_id)->first();

		// update transactions logs
        $transaction               = new Transaction();
        $transaction->user_id      = $user_id;
        $transaction->stock_name   = $security->security;
        $transaction->stock_unit   = $price;
        $transaction->stock_qty    = $qty;
        $transaction->stock_trade  = $type;
        $transaction->stock_amount = $amount;
        $transaction->stock_timing = time();
        $transaction->com_percent  = 0;
        $transaction->com_amount   = 0;
        $transaction->save();

        // Get transactions notify user
        $transaction = Transaction::where('user_id', $user_id)->orderBy('id', 'desc')->take('1')->first();
        if($transaction !== null){
            $trans_box = array(
                'user_id'      => $user_id,
                'stock_name'   => $transaction->stock_name,
                'stock_unit'   => number_format($transaction->stock_unit, 2),
                'stock_qty'    => number_format($transaction->stock_qty),
                'stock_trade'  => $transaction->stock_trade,
                'stock_amount' => number_format($transaction->stock_amount, 2),
                'stock_date'   => $transaction->created_at->diffForHumans()
            );

            // Fire Transactions info
            \Event::fire(new TransactionNotifications($trans_box));
        }
	}


    /*
    |--------------------------------------------------------------------------
    | Delete executed order
    |--------------------------------------------------------------------------
    |
    | Clear client other
    |
    */

	// trash order 
	public function trashOrder($ref_id)
	{
		# code...
		$order = Order::where('ref_id', $ref_id)->first();
		if($order !== null){
			$trash_order = Order::find($order->id);
			$trash_order->delete();
		}
	}
}
