<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\AndroidNotification;

class AndroidTestingController extends Controller
{
    //
    /*
    |--------------------------------
    | Test PusherJs
    |--------------------------------
    |
    */
    public function pusher(Request $request){

        $title = $request->title;
        $body  = $request->body;

        // $title  = "Congratulations";
        // $body   = "You just recieved N10million CATSS credit, Start trading now !, win up to N10,000 cash prizes every week. ";

        // $data = [
        //     'status' => 'success',
        //     'activity' => 'landing',
        //     'data' => [
        //         'user_id' => $user_id,
        //         'section' => $section
        //     ];
        // ];


    	$push = new AndroidNotification();
    	$push_response = $push->fire($title, $body);


    	return response()->json($push_response);
    }
}
