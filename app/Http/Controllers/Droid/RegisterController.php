<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\Http\Controllers\Droid\Functions\BasicFunc;
use CATSS\Mail\NewUser;
use CATSS\User;
use CATSS\Account;
use CATSS\Basic;
use CATSS\Activation;

class RegisterController extends Controller
{
     private $func;
    
    public function __construct() {
        $this->middleware('guest');
        $this->func = new BasicFunc();
    }

    public function create(Request $request) {

       if ($request->header("User-Agent") == APP_TOKEN) {

            $response = array();
            $data = json_decode($request->getContent(), true);
            $username = $data['email'];
            $password = $this->func->decodePassword($data['password']);
            $name = $data['fname'] . " " . $data['lname'];

            $user = User::where('email', $username)->first();

            if ($user !== null) {
                $response["status"] = "96";
                $response["message"] = 'The email has already been taken';
            } else {
                // encrypt password 
                $password = bcrypt($password);

                // generate unique account id
                $account_id = "NTD" . rand(000, 999) . rand(111, 999);
                $account_bal = 100000.00; // set default account balance
                $account_status = "open"; // account status set to open
                $account_timing = time(); // account opening date
                // multible
                $user = new User();
                $user->account_id = $account_id;
                $user->name = $name;
                $user->email = $username;
                $user->password = $password;
                $user->save();


                // After saving the user fetch and get user id 
                $users = User::where('email', $username)->get();

                // check for id and create relationship
                foreach ($users as $user) {
                    // init account model
                    $account = new Account();
                    $account->user_id = $user->id;
                    $account->account_id = $account_id;
                    $account->account_balance = $account_bal;
                    $account->account_status = $account_status;
                    $account->timing = time();
                    $account->save();

                    // init a new basic informations
                    $basic_info = new Basic();
                    $basic_info->user_id = $user->id;
                    $basic_info->name = $user->name;
                    $basic_info->save();

                    // set account for activations
                    $activate = new Activation();
                    $activate->email = $user->email;
                    $activate->token = rand(000, 999) . '-' . rand(111, 555);
                    $activate->status = 'inactive';
                    $activate->save();

                    // get the users activations code details
                    $activation_code = Activation::where('email', $username)->first();

                    // build res data
                    $data = array(
                        'id' => $activation_code->id,
                        'name' => $name,
                        'code' => $activation_code->token
                    );
                }

                // send User an Email
                //   \Mail::to($email)->send(new NewUser($data));
                
                $response["status"] = "00";
                $response["message"] = 'Account has been successfully created, confirmation link has been sent, check email to verify account !';
            }
            return json_encode($response); 
        }
   }
    
}
