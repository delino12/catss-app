<?php

namespace CATSS\Http\Controllers\Droid;

use Illuminate\Http\Request;
use CATSS\Http\Controllers\Controller;
use CATSS\User;

class RecoverAccountController extends Controller
{
    

    // search account information
    public function reset(Request $request){
    	 
         if ($request->header("User-Agent") == APP_TOKEN) {
            $response = array();
            $data = json_decode($request->getContent(), true);
            $email = $data['email'];

    	$user = User::where('email', $email)->get();

    	$total = count($user);
    	if($total > 0){
            $response["status"] = "00";
            $response["message"] = 'Password reset link has been sent to '.$email;
    	}else{
            $response["status"] = "00";
            $response["message"] = $email.' does not exit, please check the email and try again ';
    	}

    	return json_encode($response);
    }
    
}#
}
