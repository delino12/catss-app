<?php

namespace CATSS\Http\Controllers;

use Illuminate\Http\Request;
use CATSS\Graph;

class GraphChartController extends Controller
{
    // plot graphs
    public function fetchData(Request $request)
    {
    	// load chat data
    	$name = $request->name;
    	// query
    	$security = Graph::where('security', $name)->orderBy('id', 'desc')->take('30')->get();
    	$graph_box = [];
    	foreach ($security as $equity) {
    		# code...
    		$data = array(
    			'price' => $equity->price,
    			'date'  => $equity->created_at->toDateTimeString() 
    		);

    		array_push($graph_box, $data);
    	}

    	// seal date to collection
    	// seal price to collection
    	return response()->json($graph_box);
    }
}
