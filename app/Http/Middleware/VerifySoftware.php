<?php

namespace CATSS\Http\Middleware;

use Closure;
use Applock;

class VerifySoftware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        return Applock::verifySoftware($request, $next);
    }
}
