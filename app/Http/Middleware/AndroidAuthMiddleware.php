<?php

namespace CATSS\Http\Middleware;

use Closure;

class AndroidAuthMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($this->verifyHeader($request) == false){
            
            // fail access
            $data = [
                'status'    => 'error',
                'message'   => 'Error, Invalid Authorization Access Token',
                'error'     => 419

            ];

            // return response
            return response()->json($data);
        }
        return $next($request);
    }

    /*
    |------------------------------------------
    | VERIFY ANDROID HEADER REQUEST
    |------------------------------------------
    |
    */
    public function verifyHeader($request)
    {
        # verify access token for android
        if($request->header('x-access-token') == 'yc3ROYW1lIjoiQnJldHQiLCJsYXN0TmFtZSI6Ikxhd3NvbiIsInBob25lTnVtYmVyIjoiNTIxMzM4MTQwOCIsInVybCI6InRlam'){
            
            # access granted 
            return true;

        }else{

            # access denied
            return false;
        }
    }
}
