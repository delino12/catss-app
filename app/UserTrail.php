<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class UserTrail extends Model
{
    /*
    |-----------------------------------------
    | login user
    |-----------------------------------------
    */
    public function loginBy($host_id, $guest_id){

    	$check_already_login = UserTrail::where([['host_id', $host_id], ['guest_id', $guest_id]])->first();
    	if($check_already_login !== null){
    		$update_trail 			= UserTrail::find($check_already_login->id);
    		$update_trail->status 	= 'active';
    		$update_trail->login_count = $update_trail->login_count + 1;
    		$update_trail->update();
    	}else{
    		// body
	    	$new_trail 				= new UserTrail();
	    	$new_trail->host_id 	= $host_id;
	    	$new_trail->guest_id 	= $guest_id;
	    	$new_trail->login_count = 1;
	    	$new_trail->status 		= 'active';
	    	$new_trail->save();
    	}
    }
}
