<?php

namespace CATSS\Mail;
use CATSS\User;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class TradeInvitations extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */

    protected $data;

    public function __construct($data)
    {
        //
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $invites_info = $this->data;
        $subject = 'CATSS Trading Invitations From '.$this->data['host'];
        return $this->subject($subject)->view('emails.trade-invites', compact('invites_info'));
    }
}
