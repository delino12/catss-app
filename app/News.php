<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use GuzzleHttp\Client;

class News extends Model
{
	protected $key;

	public function __construct()
	{
		# code...
		$this->key = '7d8d32b47d9e4631824cfa3cb64702a7';
	}

    // get news finance
    public function loadNews()
    {

    	$base_url = "https://newsapi.org/v2/top-headlines?sources=financial-post&apiKey=".$this->key;
    	$fetch_news = new Client();
    	$news_res 	= $fetch_news->request('GET', $base_url);

    	// filter response
        $res_code = $news_res->getStatusCode();
        $res_body = $news_res->getBody()->getContents();

        // return body contents
        return $res_body;
    }

     // get news 
    public function loadNewsBloomberg()
    {
    	$base_url = "https://newsapi.org/v2/top-headlines?sources=bloomberg&apiKey=".$this->key;
    	$fetch_news = new Client();
    	$news_res 	= $fetch_news->request('GET', $base_url);

    	// filter response
        $res_code = $news_res->getStatusCode();
        $res_body = $news_res->getBody()->getContents();

        // return body contents
        return $res_body;
    }

    /*
    |--------------------------------
    | fetch local news
    |--------------------------------
    |
    */
    public function localNews(){
        // fetch news
        $local_news = News::orderBy('id', 'DESC')->get();
        if(count($local_news) > 0){
            $local_news_box = [];
            foreach ($local_news as $news) {
                # code...
                $data = [
                    "source" => [
                        "id"    => $news->id,
                        "name"  => 'cavidel'
                    ],
                    "author"        => "Yinka Sulieman",
                    "title"         => $news->title,
                    "description"   => $news->body,
                    "url"           => "http://www.equities.catss.ng",
                    "urlToImage"    => "http://www.equities.catss.ng/images/news/catss-news.jpg",
                    "publishedAt"   => $news->created_at->diffForHumans()
                ];

                array_push($local_news_box, $data);
            }
        }else{
            $local_news_box = [];
        }

        // return data
        return $local_news_box;
    }
}
