<?php

namespace CATSS;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;
use CATSS\Mail\ClientLoginDetails;
use CATSS\Mail\AgentLoginDetails;
use CATSS\Mail\WelcomeMail;
use CATSS\Mail\NewUser;
use CATSS\Activation;
use CATSS\Account;
use CATSS\Basic;
use CATSS\UserTrail;
use CATSS\BrokerageFirm;
use Auth;

class User extends Authenticatable
{
    use Notifiable;
    
    protected $guard = 'user';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    

    /*
    |-----------------------------------------
    | ACCOUNT RELATIONSHIP
    |-----------------------------------------
    */
    public function accounts()
    {
        // body
        return $this->hasOne(Account::class);
    }

    /*
    |-----------------------------------------
    | ADD NEW BROKER
    |-----------------------------------------
    */
    public function saveNewBroker($payload)
    {

        $brokerage_firm = BrokerageFirm::where("id", $payload->broker_firm)->first();
        if($brokerage_firm !== null){
            $payload->broker_id     = $brokerage_firm->id;
            $payload->broker_firm   = $brokerage_firm->broker_firm;
        }else{
            $payload->broker_id     = "";
            $payload->broker_firm   = "";
        }

        $payload->broker_password = str_random('7');

        // generate unique account id
        $account_id     = "NTD".rand(000,999).rand(111,999);
        $account_bal    = 0; // set default account balance
        $account_status = "open"; // account status set to open
        $account_timing = time(); // account opening date

        $user                     = new User();
        $user->account_type       = "broker";
        $user->account_id         = $account_id;
        $user->brokerage_id       = $payload->broker_id;
        $user->brokerage_firm     = $payload->broker_firm;
        $user->name               = $payload->broker_name;
        $user->email              = $payload->broker_email;
        $user->password           = bcrypt($payload->broker_password);

        if($user->save()){
            // init account model
            $account                  = new Account();
            $account->user_id         = $user->id;
            $account->account_id      = $account_id;
            $account->account_balance = $account_bal;
            $account->account_status  = $account_status;
            $account->timing          = time();
            $account->save();

            // init a new basic informations
            $basic_info          = new Basic();
            $basic_info->user_id = $user->id;
            $basic_info->name    = $user->name;
            $basic_info->save();

            // set account for activations
            $activate           = new Activation();
            $activate->email    = $user->email;
            $activate->token    = rand(000, 999).'-'.rand(111, 555);
            $activate->status   = 'inactive';
            $activate->save();

            // get the users activations code details
            $activation_code = Activation::where('email', $user->email)->first();

            // build res data
            $mail_data = array(
                'id'    => $activation_code->id,
                'name'  => $user->name,
                'code'  => $activation_code->token,
                'firm'  => $payload->broker_firm,
                'email' => $payload->broker_email,
                'password' => $payload->broker_password
            );

            $data = [
                'status'    => 'success',
                'message'   => 'broker has been added successfully!'
            ];

            // send User an Email
            \Mail::to($user->email)->send(new NewUser($mail_data));
            \Mail::to($user->email)->send(new AgentLoginDetails($mail_data));
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'failed to create new user'
            ];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | ADD NEW CLIENT
    |-----------------------------------------
    */
    public function saveNewClient($payload)
    {
        $brokerage_firm = BrokerageFirm::where("id", $payload->broker_firm)->first();
        if($brokerage_firm !== null){
            $payload->broker_id     = $brokerage_firm->id;
            $payload->broker_firm   = $brokerage_firm->broker_firm;
        }else{
            $payload->broker_id     = "";
            $payload->broker_firm   = "";
        }

        $payload->broker_password = str_random('7');

        // generate unique account id
        $account_id     = "NTD".rand(000,999).rand(111,999);
        $account_bal    = 0; // set default account balance
        $account_status = "open"; // account status set to open
        $account_timing = time(); // account opening date

        $user                     = new User();
        $user->account_type       = "client";
        $user->account_id         = $account_id;
        $user->brokerage_id       = $payload->broker_id;
        $user->brokerage_firm     = $payload->broker_firm;
        $user->name               = $payload->broker_name;
        $user->email              = $payload->broker_email;
        $user->created_by         = Auth::user()->id;
        $user->password           = bcrypt($payload->broker_password);

        if($user->save()){
            // init account model
            $account                  = new Account();
            $account->user_id         = $user->id;
            $account->account_id      = $account_id;
            $account->account_balance = $account_bal;
            $account->account_status  = $account_status;
            $account->timing          = time();
            $account->save();

            // init a new basic informations
            $basic_info          = new Basic();
            $basic_info->user_id = $user->id;
            $basic_info->name    = $user->name;
            $basic_info->save();

            // set account for activations
            $activate           = new Activation();
            $activate->email    = $user->email;
            $activate->token    = rand(000, 999).'-'.rand(111, 555);
            $activate->status   = 'inactive';
            $activate->save();

            // get the users activations code details
            $activation_code = Activation::where('email', $user->email)->first();

            // build res data
            $mail_data = array(
                'id'    => $activation_code->id,
                'name'  => $user->name,
                'code'  => $activation_code->token,
                'firm'  => $payload->broker_firm,
                'email' => $payload->broker_email,
                'password' => $payload->broker_password
            );

            $data = [
                'status'    => 'success',
                'message'   => 'broker has been added successfully!'
            ];

            // send User an Email
            \Mail::to($user->email)->send(new NewUser($mail_data));
            \Mail::to($user->email)->send(new ClientLoginDetails($mail_data));
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'failed to create new user'
            ];
        }

        // return
        return $data;
    }

    /*
    |-----------------------------------------
    | GET ALL BROKER
    |-----------------------------------------
    */
    public function getAllBrokers()
    {
        $users = User::where("account_type", "broker")->orderBy("id", "DESC")->get();
        if(count($users) > 0){
            $users_box = [];
            foreach ($users as $ul) {

                $account = Account::where("user_id", $ul->id)->first();
                $biodata = Basic::where("user_id", $ul->id)->first();

                $total_client = User::where([["brokerage_firm", $ul->brokerage_firm], ['account_type', 'client']])->count();
                
                $data = [
                    'id'        => $ul->id,
                    'name'      => $ul->name,
                    'email'     => $ul->email,
                    'firm_name' => $ul->brokerage_firm,
                    'account'   => $account,
                    'bio'       => $biodata,
                    'total_client' => $total_client,
                    'updated'   => $ul->created_at->diffForHumans(),
                    'date'      => $ul->created_at->toDateTimeString(),
                ];

                array_push($users_box, $data);
            }
        }else{
            $users_box = [];
        }

        // return 
        return $users_box;
    }

    /*
    |-----------------------------------------
    | GET ALL CLIENTS
    |-----------------------------------------
    */
    public function getAllClients()
    {
        $users = User::where("account_type", "client")->orderBy("id", "DESC")->get();
        if(count($users) > 0){
            $users_box = [];
            foreach ($users as $ul) {
                $account        = Account::where("user_id", $ul->id)->first();
                $biodata        = Basic::where("user_id", $ul->id)->first();
                $total_client   = Client::where("broker_id", $ul->id)->count();
                
                $data = [
                    'id'        => $ul->id,
                    'name'      => $ul->name,
                    'email'     => $ul->email,
                    'firm_name' => $ul->brokerage_firm,
                    'balance'   => $account,
                    'bio'       => $biodata,
                    'total_client' => $total_client,
                    'updated'   => $ul->created_at->diffForHumans(),
                    'date'      => $ul->created_at->toDateTimeString(),
                ];

                array_push($users_box, $data);
            }
        }else{
            $users_box = [];
        }

        // return 
        return $users_box;
    }

    /*
    |-----------------------------------------
    | SWITCH USER ACCOUNT
    |-----------------------------------------
    */
    public function switchUserAccount($client_email)
    {
        # code...
        $email  = $client_email;
        $user   = User::where("email", $email)->first();

        $host_id = Auth::user()->id;
        
        // check if user exits
        if($user !== null){
            // login using collect 
            // kill old session
            Auth::logout();

            if(Auth::loginUsingId($user->id)){

                $guest_id = Auth::user()->id;

                $new_trail = new UserTrail();
                $new_trail->loginBy($host_id, $guest_id);

                // on login successful !
                $data = array(
                    'status'    => 'success',
                    'message'   => 'Login successful !',
                    'state'     => true
                );

            }else{
                
                // on login failure
                $data = array(
                    'status'    => 'error',
                    'message'   => 'Login fail !',
                    'state'     => false
                );
            }
        }else{

            // user not found
            $data = array(
                'status'    => 'info',
                'message'   => 'Invalid email, Login fail !',
                'state'     => false
            );
        }

        // return response
        return $data;
    }

    /*
    |-----------------------------------------
    | SHOW ALL USERS
    |-----------------------------------------
    */
    public function getAllUsers()
    {
        $users = User::orderBy("id", "DESC")->get();
        if(count($users) > 0){
            $users_box = [];
            foreach ($users as $ul) {
                $account        = Account::where("user_id", $ul->id)->first();
                $biodata        = Basic::where("user_id", $ul->id)->first();
                $total_client   = Client::where("broker_id", $ul->id)->count();
                
                $data = [
                    'id'        => $ul->id,
                    'name'      => $ul->name,
                    'email'     => $ul->email,
                    'firm_name' => $ul->brokerage_firm,
                    'balance'   => number_format($account->account_balance, 2),
                    'status'    => $account->account_status,
                    'bio'       => $biodata,
                    'total_client' => $total_client,
                    'updated'   => $ul->created_at->diffForHumans(),
                    'date'      => $ul->created_at->toDateTimeString(),
                ];

                array_push($users_box, $data);
            }
        }else{
            $users_box = [];
        }

        // return 
        return $users_box;
    }

    /*
    |-----------------------------------------
    | ADD FIGNER PRINT
    |-----------------------------------------
    */
    public function addFingerPrint($payload)
    {
        $user_id    = $payload->userid;
        $print_hash = $payload->print_hash;

        $user = User::where("id", $user_id)->first();
        if($user !== null){
            $update_hash                = User::find($user->id);
            $update_hash->print_hash    = $print_hash;
            if($update_hash->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Finger print enrolment successful!',
                    'error'     => 200,
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Finger print enrolment failed!',
                    'error'     => 403,
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'This user does not exist!',
                'error'     => 401,
            ];
        }

        return $data;
    }

    /*
    |-----------------------------------------
    | VERIFY FINGER PRINT
    |-----------------------------------------
    */
    public function verifyFingerPrint($payload){
        // body
        $user_id    = $payload->userid;
        $print_hash = $payload->print_hash;

        $user = User::where([["id", $user_id], ['print_hash', $print_hash]])->first();
        $account = Account::where("user_id", $user_id)->first();

        if($user !== null){
            $data = [
                'status'    => 'success',
                'message'   => 'Finger verification successful!',
                'data'      => [
                    'details' => $user,
                    'account' => $account,
                ],
                'error'     => 200,
            ];

            $data["account"] = "";
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Finger verification failed, check user account and try again!',
                'error'     => 402,
            ];
        }

        return $data;
    }
}
