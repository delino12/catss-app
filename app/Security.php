<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use CATSS\Cast;
use Carbon\Carbon;
use DB;

class Security extends Model
{
    /*
    |-----------------------------------------
    | GET PRICES FROM NSE
    |-----------------------------------------
    */
    public function getNsePrices(){

    	if($this->verifyAlreadyUpdate() === false){

	    	$endpoint = 'http://www.nse.com.ng/REST/api/statistics/ticker?$filter=TickerType%20%20eq%20%27EQUITIES%27';
	    	
	    	$ch = curl_init();
	    	curl_setopt($ch, CURLOPT_URL, $endpoint);
	    	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 200);
	    	curl_setopt($ch, CURLOPT_TIMEOUT, 200);
	    	$result = curl_exec($ch);

	    	$data 		= json_decode($result, true);
	    	$callback 	= $this->updateEquitiesPrices($data);

	    	return [
	    		"status" 	=> "success",
	    		"message" 	=> "Total Updated: ".$callback['total_updated'].", Total Added: ".$callback['total_added']
	    	];

	    	curl_close($ch);
	    }else{
	    	return [
	    		"status" 	=> "error",
	    		"message" 	=> "Today's Prices are up to date!"
	    	];
	    }
    }

    /*
    |-----------------------------------------
    | UPDATE EQUITIES PRICES
    |-----------------------------------------
    */
    public function updateEquitiesPrices($market_prices){
    	// check if not empty
		$total_updated 	= 0;
		$total_added 	= 0;
		foreach ($market_prices as $el) {
			$security = Security::where("security", $el['SYMBOL'])->first();
            if($security !== null){

                # code...
                $casts_data                 = new Cast();
                $casts_data->board          = $security->board;
                $casts_data->security       = $security->security;
                $casts_data->ref_price      = $security->ref_price;
                $casts_data->open_price     = $security->open_price;
                $casts_data->close_price    = $security->close_price;
                $casts_data->previous_close = $security->previous_close;
                $casts_data->status         = 'casted';
                $casts_data->save();

                $update_security                 = Security::find($security->id);
                $update_security->ref_price      = $el['Value'];
                $update_security->open_price     = $el['Value'];
                $update_security->close_price    = $el['Value'];
                $update_security->previous_close = $security->close_price;
                $update_security->status         = 'open';
                if($update_security->update()){
                	// count updated
                    $total_updated++;
                }
            }else{

                $add_new_security                 = new Security();
                $add_new_security->board          = "EQTY";
                $add_new_security->security       = $el['SYMBOL'];
                $add_new_security->ref_price      = $el['Value'];
                $add_new_security->open_price     = $el['Value'];
                $add_new_security->high_price     = $el['Value'];
                $add_new_security->low_price      = $el['Value'];
                $add_new_security->close_price    = $el['Value'];
                $add_new_security->change_price   = 0;
                $add_new_security->daily_volume   = 0;
                $add_new_security->daily_value    = 0;
                $add_new_security->dvmv_today     = 0;
                $add_new_security->mvtn_trade     = 0;
                $add_new_security->no_of_trade    = 0;
                $add_new_security->previous_close = 0;
                $add_new_security->status         = 'open';
                if($add_new_security->save()){
                	// count added
                    $total_added++;
                }
            }
		}

        $data = [
            'total_updated' => $total_updated,
            'total_added'   => $total_added
        ];

    	return $data;
    }


    /*
    |-----------------------------------------
    | VERIFY ALREADY UPDATED
    |-----------------------------------------
    */
    public function verifyAlreadyUpdate(){

    	$current_date 	= Carbon::now();
    	$verify_date 	= $current_date->toDateString();

    	// body
    	$already_update = Security::orderBy('updated_at', 'DESC')->first();
    	if($already_update !== null){
    		$updated_dated = $already_update->updated_at->toDateString();
    		if($updated_dated === $verify_date){
    			return true;
    		}else{
    			return false;
    		}
    	}
    }
}
