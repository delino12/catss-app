<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;

class Account extends Model
{
    /*
    |-----------------------------------------
    | get the user that own this account
    |-----------------------------------------
    */
    public function users(){
        // relationship
        return $this->belongsTo(User::class);
    }

    /*
    |-----------------------------------------
    | lock account
    |-----------------------------------------
    */
    public function lock($user_id){
    	// body
    	$account = Account::where("user_id", $user_id)->first();
    	$block_account 					= Account::find($account->id);
    	$block_account->account_status 	= "lock";
    	if($block_account->update()){
    		$data = [
    			'status'	=> 'error',
    			'message' 	=> 'This account has been blocked!'
    		];
    	}else{
    		$data = [
    			'status'	=> 'error',
    			'message' 	=> 'Could not block user account, kindly verify user account is valid'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | unlock account
    |-----------------------------------------
    */
    public function unlock($user_id){
    	// body
    	$account = Account::where("user_id", $user_id)->first();
    	$block_account 					= Account::find($account->id);
    	$block_account->account_status 	= "open";
    	if($block_account->update()){
    		$data = [
    			'status'	=> 'success',
    			'message' 	=> 'This account has been unlocked!'
    		];
    	}else{
    		$data = [
    			'status'	=> 'error',
    			'message' 	=> 'Could not unblock user account, kindly verify user account is valid'
    		];
    	}

    	// return
    	return $data;
    }

    /*
    |-----------------------------------------
    | credit seller
    |-----------------------------------------
    */
    public function creditSeller($id, $expected_total){
        // body
        $update_account                     = Account::find($id);
        $update_account->account_balance    = $update_account->account_balance + $expected_total;
        $update_account->update();
    }

    /*
    |-----------------------------------------
    | debit buyer
    |-----------------------------------------
    */
    public function debitBuyer($id, $expected_total){
        // body
        $update_account                     = Account::find($id);
        $update_account->account_balance    = $update_account->account_balance - $expected_total;
        $update_account->update();
    }

    /*
    |-----------------------------------------
    | update bank account
    |-----------------------------------------
    */
    public function updateBankAccount($id, $payload){
        // body
        $account = Account::where("user_id", $id)->first();
        if($account !== null){
            $update                 = Account::find($account->id);
            $update->account_name   = $payload->account_name;
            $update->account_bank   = $payload->bank_name;
            $update->account_number = $payload->account_no;
            if($update->update()){
                $data = [
                    'status'    => 'success',
                    'message'   => 'Bank information updated'
                ];
            }else{
                $data = [
                    'status'    => 'error',
                    'message'   => 'Error updating bank details'
                ];
            }
        }else{
            $data = [
                'status'    => 'error',
                'message'   => 'Could not find bank details'
            ];
        }

        return $data;
    }

    /*
    |-----------------------------------------
    | get account details
    |-----------------------------------------
    */
    public function getAccountDetails($user_id){
        // body
        $account = Account::where("user_id", $user_id)->first();
        if($account !== null){
            $data = [
                'status'        => true,
                'message'       => 'Bank details retrieve!',
                'details'       => [
                    'bank_name'     => $account->account_bank,
                    'account_no'    => $account->account_number,
                    'account_name'  => $account->account_name
                ]
            ];
        }else{
            $data = [
                'status'        => false,
                'message'       => 'No bank information found!',
                'details'       => [
                    'bank_name'     => $account->account_bank,
                    'account_no'    => $account->account_number,
                    'account_name'  => $account->account_name
                ]
            ];
        }

        return $data;
    }
}
