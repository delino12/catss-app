<?php

namespace CATSS;

use Illuminate\Database\Eloquent\Model;
use CATSS\Events\TradeUpdate;
use CATSS\Order;
use CATSS\User;
use CATSS\Transaction;
use CATSS\Stock;
use CATSS\Commission;
use CATSS\Account;
use CATSS\DB;
use Auth;

class Nano extends Model
{
    /*
    |-----------------------------------------
    | EXECUTE SWEEP ON NEW ORDER
    |-----------------------------------------
    */
    public function run($payload){
    	// body
    }


    /*
    |-----------------------------------------
    | SETTLE ONE CLICK TRADE
    |-----------------------------------------
    */
    public function settleBuyTrade($request){
    	// body
    	// get the current logged in user
        $user_id    = Auth::user()->id;
        $email      = Auth::user()->email;

        // request sec id
        $pid        = $request->pid;
        $pair_name  = $request->pn;

        // get trade qty
        $trade_qty  = $request->qty;
        $trade_type = $request->trade_type; 

        // buy section
        $buy_price  = $request->buy_price;
        $buy_id     = $request->buy_id;

        // sell section
        $sell_price = $request->sell_price;
        $sell_id    = $request->sell_id;

        // scan trade price 
        $buy_price  = str_replace(',', '', $buy_price);
        $sell_price = str_replace(',', '', $sell_price);

    	// calculate consideration
        $amount = $trade_qty * $buy_price;

        // charge percentange on consideration
        $percentange_charge = 0.01725;
        $commission_charge  = ($percentange_charge * $amount) + 4;
        $expected_total     = $commission_charge + $amount;

        // seller expected amount
        $expected_total_seller = $amount - $commission_charge;


        $order 			= Order::where('id', $buy_id)->first();
        $seller_id 		= $order->user_id;

        // fetch account information 
        $account_details = Account::where('user_id', $user_id)->first();

        // fetch account information counter party
        $seller_account_details = Account::where('user_id', $seller_id)->first();

        # check if balance is enough
        if($account_details->account_balance < $expected_total){
            // response
            $data = [
                'status'  => 'error',
                'message' => 'Insuffient Funds, You can not buy '.$pair_name
            ];
        }else{
            // save commission charges
            $commission = new Commission();
            $commission->saveBuyerCommission($user_id, $percentange_charge, $trade_qty, $commission_charge);
            $commission->saveSellerCommission($seller_id, $percentange_charge, $trade_qty, $commission_charge);

            // update user account balance
            $account = new Account();
            $account->creditSeller($seller_account_details->id, $expected_total_seller);
            $account->debitBuyer($account_details->id, $expected_total);

            // update user account balance for seller
            $update_account                     = Account::find($seller_account_details->id);
            $update_account->account_balance    = $update_account->account_balance + $expected_total_seller;
            $update_account->update();

            // create transaction information
            $transaction = new Transaction();
            $transaction->saveBuyerTransaction($user_id, $pair_name, $buy_price, $trade_qty, $amount, $percentange_charge, $commission_charge);
            $transaction->saveSellerTransaction($seller_id, $pair_name, $buy_price, $trade_qty, $amount, $percentange_charge, $commission_charge);

            // first check if same stock already exits, if same stock exits update existing...
            $stocks = Stock::where([['name', $pair_name], ['user_id', $user_id]])->first();
            // if pair is new
            if($stocks == null){
                // no stock exits so W/A is new
                $f1 = $trade_qty * $buy_price;
                $fresh_wa = $f1 / $trade_qty;

                // else save new Stock
                $stocks            = new Stock();
                $stocks->user_id   = $user_id;
                $stocks->name      = $pair_name;
                $stocks->qty       = $trade_qty;
                $stocks->price     = $buy_price;
                $stocks->amount    = $trade_qty * $buy_price;
                $stocks->w_average = $fresh_wa;
                $stocks->save();
            }else{
                // now check if stock exist
                $existed_stock = Stock::find($stocks->id);

                // capture incoming stock qty*price 
                $f2 = $trade_qty * $buy_price;

                // get 
                $f1 = $existed_stock->qty * $existed_stock->w_average; 

                # w/average
                $f3 = $f1 + $f2; // compute net old qty * old price
                $f4 = $existed_stock->qty + $trade_qty; // compute net qty
                $f5 = $f3/$f4;

                $existed_stock->price       = $buy_price;
                $existed_stock->w_average   = $f5;
                $existed_stock->qty         = $f4;
                $existed_stock->amount      = $f3;
                $existed_stock->update();
            }

            // first check if same stock already exits, if same stock exits update existing...
            $stocks = Stock::where([['name', $pair_name], ['user_id', $seller_id]])->first();
            // if pair is new
            if($stocks == null){
                // no stock exits so W/A is new
                $f1 = $trade_qty * $buy_price;
                $fresh_wa = $f1 / $trade_qty;

                // else save new Stock
                $stocks            = new Stock();
                $stocks->user_id   = $user_id;
                $stocks->name      = $pair_name;
                $stocks->qty       = $trade_qty;
                $stocks->price     = $buy_price;
                $stocks->amount    = $trade_qty * $buy_price;
                $stocks->w_average = $fresh_wa;
                $stocks->save();
            }else{
                // now check if stock exist
                $existed_stock = Stock::find($stocks->id);

                // capture incoming stock qty*price 
                $f2 = $trade_qty * $buy_price;

                // get 
                $f1 = $existed_stock->qty * $existed_stock->w_average; 

                # w/average
                $f3 = $f1 + $f2; // compute net old qty * old price
                $f4 = $existed_stock->qty + $trade_qty; // compute net qty
                $f5 = $f3/$f4;

                $existed_stock->price       = $buy_price;
                $existed_stock->w_average   = $f5;
                $existed_stock->qty         = $f4;
                $existed_stock->amount      = $f3;
                $existed_stock->update();
            }

            if($trade_qty == $order->qty){
				// update order qty
	            $update_order = Order::find($order->id);
	            $update_order->delete();
            }else{
            	$update_order = Order::find($order->id);
	            $update_order->qty = $update_order->qty - $trade_qty;
	            $update_order->update();
            }

            // response
            $data = [
                'status'  => 'success',
                'message' => $pair_name.' Trade Successful!'
            ];

            // trade update
            \Event::fire(new TradeUpdate());
        }

        // return 
        return $data;
    }
}
