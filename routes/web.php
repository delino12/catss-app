<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Log all application errors
Route::get('/dev-logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
Route::get('/test/pusher', 'AndroidTestingController@pusher');

// load graphs
Route::get('/load/graph/set', 			'GraphChartController@fetchData');

// load daily price listing 
Route::get('/daily/price/list',  		'ExternalPagesController@priceList');
Route::post('/send-messages',     		'ExternalPagesController@sendUsMail');
Route::get('/stock/news',         		'ExternalPagesController@stockNews');
Route::get('/ranking-list/users',		'ExternalPagesController@rankListing');
Route::get('/forum',              		'ExternalPagesController@showForum');
Route::get('/load/winner',              'ExternalPagesController@winner');
Route::get('/privacy-policy', 			'ExternalPagesController@policy');
Route::get('/terms-and-conditions', 	'ExternalPagesController@terms');

// post forum messages
Route::post('/send/forum-post',      	'ForumPostController@createPost');
Route::post('/send/forum-comment',   	'ForumPostController@postComment');
Route::get('/load/forum-post',       	'ForumPostController@loadPosts');
Route::get('/forum/reply/post/{id}', 	'ForumPostController@comments');
Route::get('/load/post/title/{id}',  	'ForumPostController@loadCard');
Route::get('/load/comments/{id}',    	'ForumPostController@loadComments');



// start authentications for admin and clients
Auth::routes();
// a get post to show registration form
// Route::get('/create-account',  			'RegisterController@showSignup');
// Route::post('/create-account', 			'RegisterController@createAccount');

// Activate all users Account
Route::get('/activate-account',  		'AccountActivationController@activateByLink');
Route::post('/activate-account', 		'AccountActivationController@activateByCode');
Route::get('/resend/activation', 		'AccountActivationController@resendActivationCode');
// Accept invites btn & link
// Route::get('/accept-invites/{token}', 'AcceptInvitationsController@acceptFromLink');
Route::get('/accept-invites/{id}', 		'AcceptInvitationsController@acceptFromBtn');
Route::get('/reject-invites/{id}', 		'AcceptInvitationsController@reject');
Route::get('/check-invites/users', 		'JsonResponseController@invitationsStatus');


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/trade/live/{group_name}/{group_id}', 	'GroupTradeController@loadTrade');
Route::get('/group/trading/{id}', 					'GroupTradeController@loadGroupTrade');



/*
|------------------------------------------------------------------------------------------
| LOAD APPLICATION ON A RESFUL PACE using JSON
|------------------------------------------------------------------------------------------
|
*/
Route::get('/load/wallet/balance', 			'JsonResponseController@loadWallets');
Route::get('/load/group/info-screen/{id}', 	'JsonResponseController@loadMembersInfo');
Route::get('/load/trading-groups', 			'JsonResponseController@loadAcceptedGroups');
Route::get('/account-status', 				'JsonResponseController@activationStatus');
Route::get('/accountbalance', 				'JsonResponseController@accountBalance');
Route::get('/news/notifications', 			'JsonResponseController@newsUpdates');
Route::get('/transactions/notifications', 	'JsonResponseController@transactionsUpdates');
Route::get('/load/transaction/{id}', 		'JsonResponseController@viewTransaction');
Route::get('/stocks/notifications', 		'JsonResponseController@stocksUpdates');
Route::get('/load/market/index', 			'JsonResponseController@pairsIndexUpdates');
Route::get('/catss/trade', 					'JsonResponseController@equityPairs');
Route::get('/catss/trade/random', 			'JsonResponseController@equityPairsRandom');
Route::get('/open/ntrade/live', 			'JsonResponseController@nTrade');
Route::get('/market/equity/live', 			'JsonResponseController@loadEquityMarket');
Route::get('/load/users/stocks', 			'JsonResponseController@loadStockBal');
Route::get('/load/ranking', 				'JsonResponseController@loadRanking');
Route::get('/load/trade/stats', 			'JsonResponseController@loadTradingStat');
Route::get('/load/financial/statements', 	'JsonResponseController@profitRevaluation');
Route::get('/load/stock/qty', 				'JsonResponseController@countTotalStock');
Route::post('/request-trade', 				'JsonResponseController@trade');
Route::get('/load/trade-orders',		 	'JsonResponseController@loadOrders');
Route::get('/load/all/transaction',			'JsonResponseController@loadAllTransaction');
Route::get('/load/all/stock/brokers',		'JsonResponseController@loadStockBrokers');
Route::post('/hire/stock/broker',			'JsonResponseController@hireStockBroker');
Route::get('/fetch/brokers/clients',		'JsonResponseController@fetchBrokersClient');
Route::post('/initate/dashboard/switch',	'JsonResponseController@switchAccount');
Route::post('/user/save/client',			'JsonResponseController@addNewClient');
Route::post('/set/price/alert', 			'JsonResponseController@setPriceAlert');
Route::post('/set/watch/list',				'JsonResponseController@setWatchList');
Route::get('/load/watchlist', 				'JsonResponseController@loadWatchList');
Route::post('/remove/watchlist/item', 		'JsonResponseController@cancelWatchList');
Route::get('/load/security/all', 			'JsonResponseController@allSecurity');
Route::get('/load/price-alert', 			'JsonResponseController@loadPriceAlert');
Route::post('/cancel/alert', 				'JsonResponseController@cancelAlert');
Route::get('/load/user/news',				'JsonResponseController@fetchDashboardNews');
Route::get('/load/user/transactions',		'JsonResponseController@getUserTransactions');
Route::get('/list/all/banks',				'JsonResponseController@listAllBanks');
Route::post('/update/client/bank',			'JsonResponseController@updateBank');

// fetch equities prices
Route::get('/fetch/market/prices/equities', 'MarketDataController@loadEquities');

// bonds and tbills updates
Route::get('/fetch/market/prices/bonds',	'MarketDataController@loadBonds');
Route::get('/fetch/market/prices/tbills',	'MarketDataController@loadTbills');
Route::get('/fetch/market/one/tbills',		'MarketDataController@loadOneTbills');

// place order
Route::post('/place/new/tbill/order', 		'MarketDataController@placeOrderTbill');
Route::post('/place/new/bonds/order', 		'MarketDataController@placeOrderBond');
Route::post('/place/new/fx/order',			'MarketDataController@placeOrderFx');

Route::get('/load/fx/orders',				'MarketDataController@loadOrderFx');


// trade on bonds and tbills
Route::post('/process/trade/bonds',			'MarketDataController@tradeBonds');
Route::post('/process/trade/tbills',		'MarketDataController@tradeTbills');

// fetch traded security FI
Route::get('/load/user/stock/bonds',		'MarketDataController@loadUserBonds');
Route::get('/load/user/stock/tbills',		'MarketDataController@loadUserTbills');
Route::get('/load/all/stock/bonds',			'MarketDataController@loadAllBonds');
Route::get('/load/all/stock/tbills',		'MarketDataController@loadAllTbills');

Route::get('/load/summary/stock/bonds',		'MarketDataController@loadSummaryBonds');
Route::get('/load/summary/stock/tbills',	'MarketDataController@loadSummaryTbills');


// fetch tbills & bonds from sp
Route::get('/load/user/stock/bonds/sp',		'MarketDataController@loadTbillsBalance');
Route::get('/load/user/stock/tbills/sp',	'MarketDataController@loadBondsBalance');

// start payment processing
Route::get('/start/payment', 				'PaymentProcessController@startProcess');
Route::get('/load/payment', 				'PaymentProcessController@loadBalance');
Route::post('/log-payment/details', 		'PaymentProcessController@logPayment');


/*
* Version 1.0.2
* CATSS Matching System 
* Jobs & Order
* Events & pusher
* Auto trading 
*/
Route::get('/load/assets/{sec_name}', 			'OrderResponseController@loadAsset');
Route::get('/load/assets/history/{sec_name}', 	'OrderResponseController@loadAssetTradeHistory');
Route::get('/load/order/{sec_id}/{user_id}',	'OrderResponseController@loadOrders');
Route::get('/load/order/all', 					'OrderResponseController@loadOrdersAll');
Route::get('/load/buy/request/{sec_id}', 		'OrderResponseController@askOrders');
Route::get('/load/sell/request/{sec_id}', 		'OrderResponseController@offerOrders');
Route::get('/load/global/orders',				'OrderResponseController@loadGlobalOrders');

Route::post('/job/order-buy/{sec_id}/{user_id}','OrderResponseController@placeOrderBuy');
Route::post('/job/order-sell/{sec_id}/{user_id}','OrderResponseController@placeOrderSell');

Route::post('/trade/buy/{sec_id}/{user_id}',	'OrderResponseController@handleBuyTrade');
Route::post('/trade/sell/{sec_id}/{user_id}',	'OrderResponseController@handleSellTrade');

Route::post('/cancel/order', 					'OrderResponseController@cancelOrder');
Route::post('/value-assets/order', 				'OrderResponseController@valueAsset');
Route::post('/value-assets/by-percentage', 		'OrderResponseController@valueAssetPercent');
Route::post('/value-assets/by-amount',			'OrderResponseController@valueAssetAmount');
Route::post('/value-assets/by-quantity',		'OrderResponseController@valueAssetQuantity');

// when selling off market orders
Route::post('/value-assets/order/sell', 		'OrderResponseController@valueAssetToSell');
Route::post('/value-assets/by-percentage/sell', 'OrderResponseController@valueAssetPercentToSell');
Route::post('/value-assets/by-amount/sell',		'OrderResponseController@valueAssetAmountToSell');
Route::post('/value-assets/by-quantity/sell',	'OrderResponseController@valueAssetQuantityToSell');

// orders for FI instruments
Route::get('/load/all/tbills/orders',			'OrderResponseController@loadTbillOrders');

// place new orders
Route::post('/oneclick/place/order',			'OrderResponseController@placeOneclickOrder');


// API news
Route::get('/load/api-news', 					'NewsRoomController@loadApiNews');
Route::get('/load/api-news/bloomberg', 			'NewsRoomController@loadApiNewsBloomberg');

// view return and call back
Route::get('/', 							'ClientPageController@index');
Route::get('/dashboard', 					'ClientPageController@dashboard');
Route::get('/account', 						'ClientPageController@account');
Route::get('/daily/price/list', 			'ClientPageController@priceList');
Route::get('/transactions', 				'ClientPageController@transactions');
Route::get('/view/transaction/{id}', 		'ClientPageController@transDetails');
Route::get('/open/trading/bonds',			'ClientPageController@openBonds');
Route::get('/open/trading/tbills',			'ClientPageController@openTbills');
Route::get('/open/trading/tbills/u2u',		'ClientPageController@openTbillsU2U');
Route::get('/open/trading/fx/u2u',			'ClientPageController@openFxU2U');
// Route::get('/open/trading/pad', 'ClientPageController@tradingPad');openEquity
Route::get('/open/trading/pad', 			'ClientPageController@openEquity');
Route::get('/open/fast/trading', 			'ClientPageController@fastTrade');
Route::get('/equities/{name}/{id}', 		'ClientPageController@history');
// Route::get('/home', 						'ClientPageController@dashboard');
Route::get('/stocks', 						'ClientPageController@stocksBalance');
Route::get('/watch-list', 					'ClientPageController@watchList');
Route::get('/price-alert', 					'ClientPageController@priceAlert');
Route::get('/users/stocks', 				'ClientPageController@myStock');
Route::get('/realtime/stock', 				'ClientPageController@loadPersonalStock');
Route::get('/strategy', 					'ClientPageController@balanceSheet');
Route::get('/setting', 						'ClientPageController@setting');
Route::get('/ranking', 						'ClientPageController@rankLists');
Route::get('/user/clients',					'ClientPageController@myClients');
Route::get('/user/hire',					'ClientPageController@hireBroker');
Route::get('/transactions/history/{equity}','ClientPageController@tradeHistory');
// Route::get('/load/users/stocks/{pair}', 'ClientPageController@loadPairDetails');

// check step by step Guide for users
Route::get('/get/user/lesson', 				'GetStartedController@checkFirstTimeAccess');

Route::get('/startGroupTrade', 				'GroupTradeController@startDemo');
Route::get('/statements', 					'ClientPageController@accountStatments');
Route::get('/demoTrade', 					'DemoTradeController@demo');
Route::get('/login-account-reset', 			'Auth\RecoverAccountController@recoverAccount');

// chat Functions
Route::get('/load/chat/msg', 				'ChatController@loadChat');
Route::get('/load/chat/msg/catss', 			'ChatController@loadChatCatss');
Route::get('/admin/load-reply/{id}', 		'ChatController@loadReply');
Route::post('/send-chat-message', 			'ChatController@sendChat');



/*
|-----------------------------------------
| FxTrade Module routes
|-----------------------------------------
*/
Route::get('/load/fx/symbols',				'FxTradingController@loadAllFxSymbols');
Route::get('/load/fx/symbols/base',			'FxTradingController@loadSingleFxSymbols');
Route::get('/load/fx/symbols/full',			'FxTradingController@loadFxFull');


/*
|--------------------------------------------------------------------------
| Web Routes for POST PARAMS
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::post('/login-account', 				'Auth\LoginController@loginUser');
Route::post('/ntrade/get-pair-info', 		'ClientPageController@fetchPair');
Route::post('/process-trade-transaction', 	'TransactionsController@saveTransaction');
Route::post('/search-account', 				'Auth\RecoverAccountController@searchAccount');
Route::post('/create/trade_group', 			'GroupTradeController@createTradeGroup');
Route::post('/upload/user/profile', 		'ClientPageController@updateProfile');
Route::post('/upload/avatar', 				'ClientPageController@uploadAvatar');
Route::post('/change/password', 			'ClientPageController@changePassword' );
Route::post('/save-personal-stock', 		'ClientPageController@savePersonalStock');

// Logout and Login
Route::get('/login-account', 				'Auth\LoginController@showlogin')->name('login');
Route::post('/login/via/phone', 			'Auth\LoginController@loginViaPhone');
Route::post('/login/via/email', 			'Auth\LoginController@loginViaEmail');
Route::get('/user/logout', 					'Auth\LoginController@logoutUsers');

/*
|--------------------------------------------------------------------------
| ADMIN Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
// All Admin Route Here
Route::get('/admin/reward/winner/{id}/{p}/{n}', 'AdminController@winnerBoard'); // p for position
Route::post('/reward/winner',               'AdminController@rewardPrice');
Route::get('/admin/payment/quota', 			'AdminController@paymentLog');
Route::get('/admin/reset/{id}', 			'AdminController@resetWeek');
Route::get('/admin/reset/paid/{id}', 		'AdminController@resetWeekPaidUsers');
Route::post('/admin/set/{id}',        		'AdminController@updateStock');
Route::get('/admin/load/vault', 	  		'AdminController@loadVault');
Route::get('/admin/load-commission',  		'CommissionController@all_charges');
Route::get('admin/commission',        		'AdminController@commission');
Route::get('/admin/news',             		'AdminController@newsForm');
Route::post('/admin/news',            		'AdminController@postNews');
Route::post('/admin/update/news',     		'AdminController@updateNews');
Route::get('/edit-news/{id}',         		'AdminController@editNews');
Route::get('/load-news/{id}',         		'AdminController@loadNews');
Route::get('/load-all-news',          		'AdminController@loadAllNews');
Route::get('/delete-news/{id}',       		'AdminController@deleteNews');
Route::get('/admin/view-stock',  	  		'AdminController@allStock');
Route::get('/admin/touch',       	  		'AdminController@feedStock');
Route::get('/admin/open/trade',  	  		'AdminController@openTrade');
Route::get('/admin/close/trade', 	  		'AdminController@closeTrade');
Route::get('/admin',             	  		'AdminController@dashboard');
Route::get('/admin/dashboard',   	  		'AdminController@dashboard');
Route::get('/catss/portal',      	  		'AdminController@portal');
Route::get('/admin/charts',        	  		'AdminController@chartScreen');
Route::get('/admin/transactions',        	'AdminController@transactions');
Route::get('/admin/orders',        	  		'AdminController@orders');
Route::get('/admin/notifications', 	  		'AdminController@showNotifications');
Route::get('/admin/customer-care', 	  		'AdminController@helpDesk');
Route::get('/admin/create',        	  		'AdminController@createBrokerageFirm');
Route::get('/admin/create/broker',			'AdminController@createBroker');
Route::get('/admin/reward/',       	  		'AdminController@rewardUser');
Route::get('/admin/users/list',    	  		'AdminController@allUsers');
Route::get('/admin/ranking',       	  		'AdminController@tradeRanking');
Route::get('/admin/load/ranking',     		'AdminController@loadRanking');
Route::get('/admin/remove/{id}',      		'AdminController@removeUser');
Route::get('/admin/loadchat',         		'AdminController@loadChatAdmin');
Route::get('/admin/delete-message/{id}', 	'AdminController@deleteChatMsg');
Route::post('/admin/reply/send',      		'AdminController@replyChat');
Route::get('/load/nse/list',          		'AdminController@loadEquityList');
Route::get('/admin/new/clients',			'AdminController@newClient');
Route::get('/admin/view/brokers/{id}',		'AdminController@viewBrokerAgent');
// update security
Route::post('/security/toggle/{sid}', 		'AdminController@updateSec');
Route::get('/admin/add/stock',        		'AdminController@toggleStock');
Route::get('/admin-view/transaction/{id}',	'AdminController@showUserTransactions');


// admin json section
Route::get('/admin/load/orders-all', 		'AdminJsonResponseController@loadOrders')->name('load_orders');
Route::get('/admin/load/transaction-all',	'AdminJsonResponseController@loadTransactions')->name('load_transactions');
Route::get('/admin/list/stock_firms',		'AdminJsonResponseController@stockFirms');
Route::get('/admin/list/firm/names',		'AdminJsonResponseController@listFirms');
Route::get('/admin/list/added/firms',		'AdminJsonResponseController@listAddedFirms');
Route::post('/admin/save/broker',			'AdminJsonResponseController@saveBroker');
Route::post('/admin/save/brokerage/firm',	'AdminJsonResponseController@addBrokageFirm');
Route::post('/admin/save/client',			'AdminJsonResponseController@saveClient');
Route::get('/admin/get/brokers',			'AdminJsonResponseController@allBrokerAgent');
Route::get('/admin/all/firms',				'AdminJsonResponseController@loadAllFirms');
Route::get('/admin/all/brokerage/firms',	'AdminJsonResponseController@loadAllBrokerageFirms');
Route::get('/admin/all/clients',			'AdminJsonResponseController@allClients');
Route::post('/block/user/account',			'AdminJsonResponseController@blockUserAccount');
Route::post('/unblock/user/account',		'AdminJsonResponseController@unblockUserAccount');
Route::get('/admin/load/all/users',			'AdminJsonResponseController@listAllUsers');
Route::get('/admin-fetch/transaction/{id}',	'AdminJsonResponseController@fetchUserTransactions');
Route::post('/update/equities/prices',		'AdminJsonResponseController@updatePriceNse');
Route::post('/admin/reset/account',			'AdminJsonResponseController@resetAccount');

// Admin Auth
Route::get('/admin/logout',    				'Auth\AdminLoginController@logoutAdmin');
Route::get('/admin/login',     				'Auth\AdminLoginController@showLoginForm');
Route::post('/admin-login',    				'Auth\AdminLoginController@loginAdmin');
// Route::post('/admin/upload/equities', 'AdminController@uploadEquities');
Route::post('/admin/upload/equities', 		'AdminController@dropzoneStore');
Route::get('/admin/update/price', 			'AdminController@updatePrice');

Route::get('/load/activity/log', 			'ActivityLogController@loadLogs');

// orders and jobs section
Route::get('/admin/scan/orders/buy', 		'ProcessOrdersController@scanningBuyOrders');
Route::get('/admin/scan/orders/sell', 		'ProcessOrdersController@scanningSellOrders');

// clear config & caches files
Route::get('/dev-clear', function (){
	Artisan::call('config:clear');
	Artisan::call('view:clear');	
	Artisan::call('cache:clear');
	Artisan::call('route:clear');
	return redirect()->back();
});

// start maintainance
Route::get('/dev-down', function (){
	Artisan::call('down');
	return redirect()->back();
});

// stop maintainance
Route::get('/dev-up', function (){
	Artisan::call('up');
	return redirect()->back();
});