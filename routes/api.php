<?php

use Illuminate\Http\Request;

/*
|-----------------------------------------------------------------------------------------
| API Routes
|-----------------------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

/*
|-----------------------------------------------------------------------------------------
| Web Routes For API Calls
|-----------------------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('/catss/signup', 				'CatssApiController@registerUser'); 		// signup
Route::post('/catss/login', 				'CatssApiController@loginUser'); 			// login
Route::post('/catss/login/email', 			'CatssApiController@loginViaEmail'); 		// login
Route::get('/catss/live/{security}', 		'CatssApiController@liveMarket'); 			// get security market
Route::get('/catss-cavidel/live/equities', 	'CatssApiController@liveMarketCavidel');    // load random secutiry
Route::get('/catss/accounts', 				'CatssApiController@userAccount'); 			// load users account
Route::get('/catss/transactions/{id}', 		'CatssApiController@userTransactions'); 	// load recent transaction
Route::get('/catss/stocks/{id}', 			'CatssApiController@userStock'); 			// load users stock
Route::get('/catss/news', 					'CatssApiController@newsUpdates'); 			// load news updates 



/*
|--------------------------------------------------------------------------------------------
| Web API for new Android mobile
|--------------------------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/droid', 			            'DroidApiController@greet'); // Signup User
Route::post('/droid/signup/user', 			'DroidApiController@signup'); // Signup User
Route::post('/droid/login/user', 			'DroidApiController@login'); // Login User
Route::post('/droid/add/fingerprint',		'DroidApiController@addFingerPrint'); // Add finger print hash
Route::post('/droid/verify/fingerprint',	'DroidApiController@verifyFingerPrintHash'); // Verify finger print hash
Route::get('/droid/account/user', 			'DroidApiController@getAccountInformation'); // Get user
Route::get('/droid/transaction/user',		'DroidApiController@transactionLogs'); // Load user transactions
Route::get('/droid/stocks/user',			'DroidApiController@stocksBalance'); // Load user transactions
Route::get('/droid/load/ranking',			'DroidApiController@loadRankingList'); // Load Ranking list
Route::get('/droid/load/news',				'DroidApiController@loadNews'); // Load News updates
Route::get('/droid/load/securities',		'DroidApiController@loadEquities'); // Load Equities
Route::get('/droid/profile-info/user',		'DroidApiController@loadProfile'); // Load Profile
Route::post('/droid/update/profile',		'DroidApiController@updateProfile'); // Update Profile
Route::get('/droid/load/stockindex',		'DroidApiController@stockNews'); // Load Stock Index

Route::get('/droid/load/watchlist', 		'DroidApiController@loadWatchList'); // Load Watch List
Route::post('/droid/set/watchlist',			'DroidApiController@setWatchList'); // Set List
Route::post('/droid/cancel/watchlist',  	'DroidApiController@cancelWatchList'); // cancel watchlist

Route::post('/droid/set/price-alert',    	'DroidApiController@setPriceAlert'); // set price alert


/*
|------------------------------------------------------------------------------------------
| PUSH NOTIFICATION ANDROID 
|------------------------------------------------------------------------------------------
|
*/
Route::post('/droid/push/notifications', 	'DroidApiController@pushNotification');


/*
|------------------------------------------------------------------------------------------
| VERIFY AUTHENCTICATED
|------------------------------------------------------------------------------------------
|
*/
Route::post('/droid/verify/authenticated', 'DroidApiController@verifyAuthenticated');


/*
|------------------------------------------------------------------------------------------
| UPDATE USER SETTING
|------------------------------------------------------------------------------------------
|
*/
Route::get('/droid/load/profile',			'DroidApiController@loadSettings');
Route::post('/droid/update/setting', 		'DroidApiController@updateSetting');
Route::post('/droid/change/password', 		'DroidApiController@updateSecurity');
Route::post('/droid/contact/message',		'DroidApiController@contactUs');


/*
|------------------------------------------------------------------------------------------
| TRADE SECTION
|------------------------------------------------------------------------------------------
|
| Here is where all trad request are handle. These
| routes are loaded by the RouteServiceProvider
*/
Route::post('/droid/trade/request/buy', 	'DroidApiController@handleBuyTrade');
Route::post('/droid/trade/request/sell', 	'DroidApiController@handleSellTrade');
Route::post('/droid/place/order/buy', 		'DroidApiController@handleBuyOrderTrade');
Route::post('/droid/place/order/sell', 		'DroidApiController@handleSellOrderTrade');
Route::post('/droid/cancel/order',			'DroidApiController@cancelOrder');
Route::get('/droid/load/orders', 			'DroidApiController@loadSingleOrders');
Route::get('/droid/load/orders/all', 		'DroidApiController@loadAllOrders');


/*
|-----------------------------------------------------------------------------------------
| LOG USER PAYMENT
|-----------------------------------------------------------------------------------------
*/
Route::post('/droid/deposit/payment',		'DroidApiController@logPayment');
Route::post('/droid/withdraw',				'DroidApiController@requestWithdraw');
Route::get('/droid/fetch/withdraw',			'DroidApiController@fetchUserWithdraw');


/*
|-----------------------------------------
| ADD BANK INFORMATION
|-----------------------------------------
*/
Route::post('/droid/add/bank/details',		'DroidApiController@addBankDetails');
Route::get('/droid/fetch/all/bank',			'DroidApiController@fetchAllBanks');
Route::get('/droid/get/bank/details',		'DroidApiController@getUserBankInfo');


/*
|-----------------------------------------------------------------------------------------
| THIS ENDPOINT RUN TRADE ENGINE 
|-----------------------------------------------------------------------------------------
*/
Route::get('/run/nano/engine',				'NanoJsonResponseController@run');




/*
|--------------------------------------------------------------------------
| Backend Auto-Deployment Webhook
|--------------------------------------------------------------------------
*/
Route::post('/auto-deploy', function (){
	Artisan::call('git:deploy');
	// return 
	return response()->json(['status' => 'success']);
});