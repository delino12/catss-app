<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px; color: #000;padding: 0.8em;">
    <center>
    	<img src="{{ env("APP_URL") }}/img/logo.png" width="40%" alt="placeholder+image">
    	<br />
    	<h1 style="color:#03F;">Welcome to CATSS Equity Trading Platform </h1>
        <img src="{{ env("APP_URL") }}/img/e.jpg" height="50%" width="auto">
    	<p>
          Hello <b> {{ $info['name'] }} </b>, You have recieved &#8358;100,000.00 (Hundred Thousand Niara) credited into your CATSS trading account, This is a digital currency that allow you to trade on all kinds of Equities here on CATSS Platform, Master the one-click trade system. Trade fast, Trade Smart and Trade more.
          <br /> 
          We will be glad to Guide you, CATSS provide daily price list of all available Equities.
        </p>
        <br /><br />

        Click <a href="{{ env("APP_URL") }}">here</a> to login and start trading.

        <br /><br />

        <div style="border-radius: 4px;padding: 0.7em;color:#000;">
            <div class="well">
                For more Technical Support<hr />
                Contact: Ekpoto Liberty Bernard <br />
                Email: bernard.ekpoto@cavidel.com <br />
                Tell: +2348127160258
            </div>
        </div>
    </center>
</body>
</html>