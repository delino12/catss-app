<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px; color: #3d37d0;padding: 0.8em;">
    <div style="box-shadow: 1px 1px 2px 1px #ccc; padding:1em;">
    	<img src="{{ env("APP_URL") }}/img/logo.png" width="40%" alt="placeholder+image">
    	<br />
    	<h1>Welcome to CATSS Equity Trading Platform. </h1>
    	
    	<p style="color:#000;"><b>{{ $invites_info['host'] }}</b>, has sent you a request to join catss trading platform, earn &#8358;10,000.00 weekly. </p>
    	<p>Play the equities game, train your trading skill and unlock your trading potentials</p>
    	<a href="{{ $invites_info['link'] }}" style="font-weight:800;text-decoration:none;">Accept Invitation</a>

    	<h4>Master Trade </h4>
        <p style="color:#000;"> The advancement
            of technology and remote access which have been utilized in security trading.
            The emergence of new international financial payment currency such as crypto-currencies <br />
            Catss offers a Trading Platform providing unlimited guides and support plus weekly prizes in stocks, coins or cash.
        </p> 
        <p style="color:#000;">
            Trends of liberalization and the removal of restrictions used to be imposed on
            foreign ownership, and the movement towards regional integration of that stock exchanges,
            clearing and settlements organizations, and other financial institutions.
        </p>
        <br /><br />

        <center><img src="{{ env("APP_URL") }}/img/catss-screen-3.png" width="400" height="300" alt="placeholder+image"> <br /></center>
        <h4>Trade Tactics</h4>
        <p style="color:#000;">
            You need to trade in order to invest new cash (if you are still working), 
            extract cash (if you are retired), rebalance (if your portfolio gets top-heavy in one stock or industry)
            and, especially, to harvest losses.
        </p>
        <p style="color:#000;">
            Smart trading means getting in and out of positions without leaving a 
            needlessly large pile of Nairas on the table for the other guys. 
            The other guys involved in your trade are stockbrokers, 
            high-frequency trading programs, market makers and hedge funds.
        </p>
        <br /><br />

        <center><img src="{{ env("APP_URL") }}/img/catss-screen-2.png" width="200" height="150" alt="placeholder+image"> <br /></center>
        <h4>Invest Your Capital</h4>
        <p style="color:#000;">
            The globalization of the world stock markets is the most significant development that has
            occurred during the last decade.
        </p>

        <div style="border-radius: 4px;padding: 0.7em;color:#03F;">
            <div class="well">
                For more Technical Support<hr />
                Contact: Ekpoto Liberty Bernard <br />
                Email: bernard.ekpoto@cavidel.com <br />
                Tell: +2348127160258
            </div>
        </div>
    </div>
</body>
</html>