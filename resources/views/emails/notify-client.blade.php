<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
    <body style="font-family: corbel; font-size: 14px; color: #000;padding: 0.8em;">
        <div style="box-shadow: 1px 1px 4px 1px #ccc; padding: 1em;">
        	<img src="{{ env("APP_URL") }}/img/logo.png" width="40%" alt="placeholder+image">
        	<br />
        	<h1 style="color:#03F;">Welcome to CATSS Equity Trading Platform </h1>
        	<p>
              Hello <b> {{ $info['name'] }} </b>, Welcome to CATSS Online Trading.
              <br /> 
              Your account has been created successfully!, Below are your login credentials.

              <table style="padding: 1em;" cellpadding="2" cellspacing="3">
                  <tr>
                      <td>Email:</td>
                      <td>{{ $info['email'] }}</td>
                  </tr>

                  <tr>
                      <td>Password: </td>
                      <td>{{ $info['password'] }}</td>
                  </tr>
              </table>
            </p>
            <p>
                <br />
                Click <a href="{{ env("APP_URL") }}/login-account">here</a> to login and start trading.
                <br />
            </p>
            <div style="border-radius: 4px;padding: 0.7em;color:#000;">
                <div class="well">
                    For more Technical Support<hr />
                    Contact: Ekpoto Liberty Bernard <br />
                    Email: bernard.ekpoto@cavidel.com <br />
                    Tell: +2348127160258
                </div>
            </div>
        </center>
    </body>
</html>