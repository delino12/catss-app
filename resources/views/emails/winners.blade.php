<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px; color: #000;padding: 0.8em;">
    <center>
    	<img src="{{ env("APP_URL") }}/img/logo.png" width="40%" alt="placeholder+image">
    	<br />
    	<h1 style="color:#03F;">Congratulations !!!</h1>
    	<p>
            Hello <b> {{ $info['name'] }} </b>, 
            We are glad to announced you as {{ $info['position'] }} winner this week. Your Wallet account has been credited with your cash price.
            <br /> 
            <table class="table">
                <tr>
                    <th>Points</th>
                    <th>Position</th>
                </tr>
                <tr>
                    <td>{{ $info['amount'] }}</td>
                    <td>{{ $info['position'] }}</td>
                </tr>
            </table>
        </p>
        <br /><br />
        Click <a href="{{ env("APP_URL") }}">here</a> to join catss today for free.
        <br /><br />

        <div style="border-radius: 4px;padding: 0.7em;color:#000;">
            <div class="well">
                For more Technical Support<hr />
                Contact: Ekpoto Liberty Bernard <br />
                Email: bernard.ekpoto@cavidel.com <br />
                Tell: +2348127160258
            </div>
        </div>
    </center>
</body>
</html>