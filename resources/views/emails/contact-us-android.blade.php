<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px;">
    <div style="box-shadow: 1px 1px 2px 1px #ccc; padding:1em;margin: 20px;">
    	<br />
        <img src="{{ env("APP_URL") }}/img/logo.png" width="35%" alt="placeholder+image">
        <br />
    	<h3>We recieved your message. </h3>
    	
    	<p>Hello <b> {{ $data['name'] }} </b>, <br /><br />
            Thanks for contacting us, we will be glad to respond any questions you will like to ask about our serices
        </p>

        <div style="border-radius: 4px;padding: 0.7em;color:#03F;">
            Technical Expert: Ekpoto Liberty Bernard<br />
            Email: bernard.ekpoto@cavidel.com <br />
            Cavidel Limited.
        </div>
    </div>
</body>
</html>