<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px;">
    <div style="box-shadow: 1px 1px 2px 1px #ccc; padding:1em;margin: 20px;">
    	<br />
    	<img src="{{ env("APP_URL") }}/img/logo.png" width="35%" alt="placeholder+image">
    	<br />
    	<h3>Hello, You have a new contact mesage</h3>
    	
    	<p>Message from <b> {{ $data['name'] }} </b>, <br /><br />
            {{ $data['body'] }}
        </p>
    </div>
</body>
</html>