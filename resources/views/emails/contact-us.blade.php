<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px; color: #000;padding: 0.8em;">
    <div style="box-shadow: 1px 1px 2px 1px #ccc; padding:1em;">
    	<img src="{{ env("APP_URL") }}/img/logo.png" width="35%" alt="placeholder+image">
    	<br />
    	<h3>We recieved your message. </h3>
    	
    	<p>Hello <b> {{ $names }} </b>, Thanks for contacting us, we will be glad to respond any questions you will like to ask about our serices</p>
    	<p>Join us now, Play the equities game, train your trading skill and unlock your trading potentials</p>
    	<a href="{{ env("APP_URL") }}/create-account/" style="font-weight:800;text-decoration:none;">Signup Now</a>

    	<h4>Master Trade </h4>
        <p> The advancement
            of technology and remote access which have been utilized in security trading, the emergence of
            new international financial institutions offering financial services regardless of geographical
            jurisdictions. 
        </p> 
        <p>
            Trends of liberalization and the removal of restrictions used to be imposed on
            foreign ownership, and the movement towards regional integration of that stock exchanges,
            clearing and settlements organizations, and other financial institutions.
        </p>
        <br /><br />

        <center><img src="{{ env("APP_URL") }}/img/catss-screen-3.png" width="400" height="300" alt="placeholder+image"> <br /></center>
        <h4>Trade Tactics</h4>
        <p>
            You need to trade in order to invest new cash (if you are still working), 
            extract cash (if you are retired), rebalance (if your portfolio gets top-heavy in one stock or industry)
            and, especially, to harvest losses.
        </p>
        <p>
            Smart trading means getting in and out of positions without leaving a 
            needlessly large pile of Nairas on the table for the other guys. 
            The other guys involved in your trade are stockbrokers, 
            high-frequency trading programs, market makers and hedge funds.
        </p>
        <br /><br />


        <div style="border-radius: 4px;padding: 0.7em;color:#03F;">
            Technical Expert: Ekpoto Liberty Bernard<br />
            Email: bernard.ekpoto@cavidel.com <br />
            Cavidel Limited.
        </div>
    </div>
</body>
</html>