<!DOCTYPE html>
<html>
<head>
	<title></title>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootswatch/3.3.7/cerulean/bootstrap.min.css">
</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-md-offset-2 small">
            	<img src="{{ env("APP_URL") }}/img/logo.png" width="50%" alt="placeholder+image">
            	<br /><br /><br />

                <h1 class="lead">Hello <b> {{ $info['name'] }},  </b></h1><br /><br />

                <div class="panel panel-primary small">
                    <div class="panel-heading small"> <h1 class="lead" style="color:#FFF;">Your trade was successful !</h1> </div>
                    <div class="panel-body small">
                        <p class="lead">
                            {{ $info['message'] }} 
                        </p>
                        <hr />
                        <a href="{{ env("APP_URL") }}" class="btn btn-primary small">Login to continue trading</a> 
                    </div>
                </div>

                <hr /><br />
                <div class="well">
                    For more Technical Support<hr />
                    Contact: Ekpoto Liberty Bernard <br />
                    Email: bernard.ekpoto@cavidel.com <br />
                    Tell: +2348127160258
                </div>
            </div>
        </div>
    </div>
</body>
</html>