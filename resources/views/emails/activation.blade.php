<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body style="font-family: corbel; font-size: 13px; color: #000;padding: 0.8em;">
    <center>
    	<img src="{{ env("APP_URL") }}/img/logo.png" width="40%" alt="placeholder+image">
    	<br />
    	<h1 style="color:#03F;">Welcome to CATSS Equity Trading Platform </h1>

    	<p>
          Hello <b> {{ $info['name'] }} </b>, We recieved your registration, and we will like to verify your account email. <br /> 
          We will be glad to Guide you, please activate account by using the code below or click on the activation link.
        </p>
    	<p>Here is your activation code <h1 style="color:#666;font-size:60px;">{{ $info['code'] }}</h1> </p><br /><br />
    	<a href="{{ env("APP_URL") }}/activate-account/?id={{ $info['id'] }}&code={{ $info['code'] }}" style="font-weight:800;text-decoration:none;">Click Here to confim account</a>

        <div style="border-radius: 4px;padding: 0.7em;color:#03F;">
            <div class="well">
                For more Technical Support<hr />
                Contact: Ekpoto Liberty Bernard <br />
                Email: bernard.ekpoto@cavidel.com <br />
                Tell: +2348127160258
            </div>
        </div>
    </center>
</body>
</html>