@extends('layouts.admin-skin')

@section('title')
   Rewarrd Users
@endsection

@section('contents')
  <div class="container">
    <div class="row">
      <div class="col-md-6">
        @if(session('reset_msg'))
          <div class="alert alert-success">
            {{ session('reset_msg') }}
          </div>
        @endif
        <h3>Reward Winner</h3>
      </div>
    </div>
    <div class="row">
      <div class="col-md-10 small">
        <div class="card">
          <div class="card-header" data-background-color="orange">
            <h4 class="title"><i class="fa fa-trophy"></i> Weekly Winner Stats</h4>
            <p class="category">New Winner on {{ date("M Y ") }}</p>
          </div>
          <div class="card-content table-responsive">
            <table class="table table-hover reward-table">
              <thead class="text-warning">
                <th>S/N</th>
                <th><i class="fa fa-users"></i> Name</th>
                <th>&#8358; Profits</th>
                <th>Date</th>
                <th>Action</th>
              </thead>
              <tbody class="list-ranking"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 small">
        <div class="card">
          <div class="card-header" data-background-color="green">
            <h4 class="title"><i class="fa fa-users"></i> Registered | Users (PROMO)</h4>
            <p class="category">Subscription logs {{ date("M Y ") }}</p>
          </div>
          <div class="card-content table-responsive">
            <table class="table table-hover registered-table">
              <thead class="text-warning">
                <th>S/N</th>
                <th><i class="fa fa-users"></i> Name</th>
                <th>Amount (&#8358;)</th>
                <th>Trans</th>
                <th>Date</th>
              </thead>
              <tbody class="list-subscribers"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-10 small">
        <div class="card">
          <div class="card-header" data-background-color="orange">
            <h4 class="title"><i class="fa fa-money"></i> Payment Logs</h4>
            <p class="category"> {{ date("M Y ") }}</p>
          </div>
          <div class="card-content table-responsive">
            <table class="table table-hover payment-table">
              <thead class="text-warning">
                <th>S/N</th>
                <th><i class="fa fa-users"></i> Name</th>
                <th>Amount (&#8358;)</th>
                {{-- <th>Total (&#8358;)</th> --}}
                <th>Tran. ID (Last Updated)</th>
                <th>Quota</th>
                <th>Date</th>
                <th>Renew Subscription</th>
              </thead>
              <tbody class="list-payments"></tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
  <hr />

  {{-- scripts --}}
  <script type="text/javascript">
    // load ranking 
    $.get('/load/ranking', function(data) {
      // sort data 
      var data = data.ranking.sort(function (a, b){
          return b.profit - a.profit; 
      });

      var sn = 0;
      $.each(data, function(index, el) {
          // console.log(el);
          sn++;
          var name   = el.user;
          var profit = el.profit.toLocaleString();
          var date   = el.date;

          // reward first top 3
          if(sn > 3){
              var btn = '----';
            }else{
              var btn = '<a href="/admin/reward/winner/'+el.id+'/'+sn+'/'+profit+'">Reward</a>';
            }

          // show only users with actual figures
          if(el.profit > 0){
            $(".list-ranking").append(`
                <tr>
                    <td>`+sn+`</td>
                    <td>`+name+`</td>
                    <td>&#8358;`+profit+`</td>
                    <td>`+date+`</td>
                    <td>`+btn+`</td>
                </tr>
            `);
          }else{

            $(".list-ranking").append(`
                <tr>
                    <td>`+sn+`</td>
                    <td>`+name+`</td>
                    <td><span class="text-danger"> &#8358; `+profit+` </span></td>
                    <td>`+date+`</td>
                    <td>`+btn+`</td>
                </tr>
            `);
          }
      });

      $(".reward-table").dataTable();
    });

    // load vault balance
    $.get('/admin/load/vault', function (data){
      /* load url json response */
      $('.list-subscribers').html();
      var sn = 0;
      $.each(data, function(index, val) {
        /* iterate through array or object */
        console.log(val);
        sn++;
        $('.list-subscribers').append(`
          <tr>
            <td>`+sn+`</td>
            <td>`+val.name+`</td>
            <td>&#8358;`+val.amount+`</td>
            <td>`+val.type+`</td>
            <td>`+val.date+`</td>
          </tr>
        `);
      });

      $(".registered-table").dataTable();
    });

    // load payment logs
    $.get('/admin/payment/quota', function (data){
      /* load url json response */
      $('.list-payments').html();
      var sn = 0;
      $.each(data, function(index, val) {
        /* iterate through array or object */
        console.log(val);
        sn++;
        $('.list-payments').append(`
          <tr>
            <td>`+sn+`</td>
            <td>`+val.name+`</td>
            <td>&#8358;`+val.balance+`</td>
            <td>`+val.trans_id+`</td>
            <td>`+val.quota+` remaining</td>
            <td>`+val.date+`</td>
            <td><a href="/admin/reset/paid/`+val.user_id+`">Renew</a></td>
          </tr>
        `);
      });

      $(".payment-table").dataTable();
    });
  </script>
@endsection