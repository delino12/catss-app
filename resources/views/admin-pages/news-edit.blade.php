@extends('layouts.admin-skin')

@section('title')
  CATSS | Flash News
@endsection

@section('contents')
    <div class="container">
      <div class="row">
        <div class="col-md-6">
          <h3>News Update: </h3>
          <br />
            <div class="panel panel-info">
              <form method="post" onsubmit="return saveEdit()">
                <div class="panel-heading"><div class="success_msg"></div></div>
                <div class="panel-body">
                  <div class="form-group">
                    <label>Edit Title</label>
                    <input type="hidden" id="news_id" value="{{ $news->id }}">
                    <input type="title" id="title" class="form-control" value="{{ $news->title }}">
                  </div>

                  <div class="form-group">
                    <label>Body</label>
                    <textarea cols="10" rows="10" id="body" class="form-control">{{ $news->body }}</textarea>
                  </div>
                  <button class="btn btn-primary">Update News</button>
                  <br />
                  
                </div>
              </form>
            </div>
        </div>
        <div class="col-md-6">
          <div class="load-single-news"></div>
        </div>
      </div>
    </div>
    <hr /><br />
    <br />
    {{-- update news --}}
    <script type="text/javascript">

      function saveEdit(){
        // data html
        var id    = $('#news_id').val();
        var title = $('#title').val();
        var body  = $('#body').val();
        var token = '{{ csrf_token() }}';


        // data set
        var data = {
          _token:token,
          id:id,
          title:title,
          body:body
        }

        // ajax post
        $.ajax({
          url: '/admin/update/news',
          type: 'POST',
          dataType: 'json',
          data: data,
          success: function (data){
            // console.log(data);
            $('.success_msg').html(`
              <p class="text-success">`+data.message+`</p>
            `);
            refreshDiv();
          },
          error: function (data){
            alert('Error, fail to send update request');
          }
        });

        return false;
      }

      // load news
      $.get('/load-news/{{ $news->id }}', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $('.load-single-news').html(`
          <div class="panel panel-info">
            <div class="panel-heading">`+data.title+`</div>
            <div class="panel-body">
              <p>`+data.body+`</p>
              <span class="small">
               `+data.date+`
              </span>
              <hr />
              <a href="/delete-news/`+data.id+`"> <i class="fa fa-bitbucket"></i> Delete </a>
            </div>
          </div>
        `);
      });

      function refreshDiv(){
        // load news
        $.get('/load-news/{{ $news->id }}', function(data) {
          /*optional stuff to do after success */
          // console.log(data);
          $('.load-single-news').html(`
            <div class="panel panel-info">
              <div class="panel-heading">`+data.title+`</div>
              <div class="panel-body">
                <p>`+data.body+`</p>
                <span class="small">
                 `+data.date+`
                </span>
                <hr />
                <a href="/delete-news/`+data.id+`"> <i class="fa fa-bitbucket"></i> Delete </a>
              </div>
            </div>
          `);
        });
      }      
    </script>
@endsection