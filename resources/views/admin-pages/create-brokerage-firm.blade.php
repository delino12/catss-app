@extends('layouts.admin-skin')

@section('title')
  CATSS | Add New Stock Broker
@endsection

@section('contents')
  <style type="text/css">
    .typeahead.tt-input {
      width: 100%;
    }  
  </style>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>Create/Add Brokerage Firm</h3>
          <div class="card">
            <div class="card-header btn-info">
              Add a Stock Brokerage Firm

              <a href="javascript:void(0);" class="pull-right" style="color:#fff;" data-toggle="modal" data-target="#add-new-brokerage-firm">
                <i class="fa fa-plus"></i> Add
              </a>
            </div>
            <div class="card-body" style="padding: 1em;">
              <div class="row">
                <div class="col-md-12">
                  <table class="table" id="brokerage-table">
                    <thead>
                      <tr>
                        <th>Brokerage Firm</th>
                        <th>Total Broker</th>
                        <th>Total Client</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody class="load-brokerage-firms"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- Add a brokerage client -->
    <div class="modal fade" id="add-new-brokerage-firm" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Add a Brokerage Firm</h4>
          </div>
          <div class="modal-body">
            <div class="col-md-12">
              <form id="create-agent-form" method="post" onsubmit="return addBrokerageFirm()">
                <div class="form-group">
                  <label>Select a Brokerage firm</label>
                  <input type="text" class="form-control" id="broker_firm" placeholder="Eg. Afritrust, InvestmentOne..." required="">
                </div>
                <div class="form-group">
                  <button class="btn btn-info" id="add-new-agent">Add</button>
                </div>
              </form>
            </div>
          </div>
          <div class="modal-footer">
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
      // get all stock broking firm
      loadStockFirm();
      getAllFirms();

      function loadStockFirm() {
        $.get('{{ url('admin/list/firm/names') }}', function(data) {
          $('#broker_firm').typeahead({
            source: data
          });
        });
      }

      function addBrokerageFirm() {
        $("#add-new-agent").prop('disabled', true);
        $("#add-new-agent").html(`Creating account please wait!`);
        var token           = $("#token").val();
        var broker_firm     = $("#broker_firm").val();

        var params = {
          broker_firm: broker_firm,
          _token: token
        };

        $.post('{{ url('admin/save/brokerage/firm') }}', params, function(data, textStatus, xhr) {
          /*optional stuff to do after success */
          if(data.status == "success"){
            swal(
              "success",
              data.message,
              data.status
            );
            $("#create-agent-form")[0].reset();
            $("#add-new-agent").html(`Add`);
            getAllFirms();
          }else{
            swal(
              "oops",
              data.message,
              data.status
            );
          }

          $("#add-new-agent").prop('disabled', false);
        });

        // return 
        return false;
      }

      function getAllFirms() {
        $.get('{{ url('admin/all/brokerage/firms') }}', function(data) {
          /*optional stuff to do after success */
          $(".load-brokerage-firms").html("");
          $.each(data, function(index, val) {
            $(".load-brokerage-firms").append(`
              <tr>
                <td>${val.firm_name}</td>
                <td>${val.total_broker}</td>
                <td>${val.total_client}</td>
                <td>
                  <a href="{{ url('admin/view/brokers') }}/${val.id}">view</a>
                </td>
              </tr>
            `);
          });
        });
      }
    </script>
@endsection