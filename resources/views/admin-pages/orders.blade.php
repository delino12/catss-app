@extends('layouts.admin-skin')

@section('title')
  CATSS | All Orders
@endsection

@section('contents')
  <div class="container">
    <div class="row">
     <div class="col-md-10">
          <div class="card">
              <div class="card-header" data-background-color="purple">
                  <h4 class="title">All Orders history</h4>
                  <p class="category">Last updated {{ date("d M Y ") }}</p>
              </div>
              <div class="card-content table-responsive">
                  <table class="table table-hover" id="orders-table">
                      <thead class="text-warning">
                        <tr>
                          <th>S/N</th>
                          <th>Dealer</th>
                          <th>Security</th>
                          <th>price</th>
                          <th>Quantity</th>
                          <th>Amount&#8358; </th>
                          <th>Trade Type</th>
                          <th>Trade Option</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                      <tbody class="load-orders-charge"></tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  </div>

@endsection


@section('scripts')
  {{-- scripts --}}
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script type="text/javascript">
    loadAllOrders();
    

    // load all users orders
    function loadAllOrders() {
      $.get('{{url('admin/load/orders-all')}}', function(data) {
        $(".load-orders-charge").html("");
        let sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $(".load-orders-charge").append(`
            <tr>
              <td>${sn}</td>
              <td>${val.owner}</td>
              <td>${val.security}</td>
              <td>&#8358; ${val.price}</td>
              <td>${val.qty}</td>
              <td>&#8358; ${val.amount}</td>
              <td>${val.trade_type}</td>
              <td>${val.option}</td>
              <td>${val.last_seen}</td>
            </tr>
          `);
        });
      });
    }

    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
      encrypted: true,
      cluster: "eu"
    });

    // update trade screen
    var new_order_update = pusher.subscribe('new-order-update');
    new_order_update.bind('CATSS\\Events\\NewOrderUpdate', function(data) {
      // refresh trade data
      console.log(data);
      loadAllOrders();
    });

  </script>
@endsection