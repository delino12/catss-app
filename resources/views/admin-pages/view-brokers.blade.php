@extends('layouts.admin-skin')

@section('title')
  CATSS | Add New Stock Broker
@endsection

@section('contents')
  <style type="text/css">
    .typeahead.tt-input {
      width: 100%;
    }  
  </style>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>Brokers Info</h3>
          <div class="card">
            <div class="card-header btn-info">
              <i class="fa fa-user"></i> {{ $broker->name }} (Stock Broker)
            </div>
            <div class="card-body" style="padding: 1em;">
              <div class="row">
                <div class="col-md-12">
                  <h2 class="lead">Listed Assets.</h2>
                  <table class="table" id="stock-table">
                    <thead>
                      <tr>
                        <th>Asset</th>
                        <th>Unit Price</th>
                        <th>Total Volume</th>
                        <th>Weighted Average</th>
                        <th>Profit/Loss</th>
                      </tr>
                    </thead>
                    <tbody class="load-stock-balance"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
      // get all stock broking firm
      loadStockBalance();

      function loadStockBalance() {
        
      }
    </script>
@endsection