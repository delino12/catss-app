@extends('layouts.admin-skin')

@section('title')
  Upload Daily Equitie
@endsection

@section('contents')
  <div class="container">
    <div  class="row">
      <div class="col-md-4">
        <h3>Update Equities</h3>
        <div class="panel panel-primary">
          <div class="panel-heading">
            Click Update Button to Update Prices
          </div>
          <div class="panel-body">
            <button class="btn btn-primary" id="eqt-btn" onclick="updateEquities()">
              Update Equities
            </button>
            <div class="upload-status"></div>
          </div>
        </div>

        <br />

       {{--  <h3>Update Bonds</h3>
        <div class="panel panel-primary">
          <div class="panel-heading">
            Click Update Button to Update Prices
          </div>
          <div class="panel-body">
            <button class="btn btn-primary">
              Update Bonds
            </button>
          </div>
        </div>

        <br />
        <h3>Update TBILLS</h3>
        <div class="panel panel-primary">
          <div class="panel-heading">
            Click Update Button to Update Prices
          </div>
          <div class="panel-body">
            <button class="btn btn-primary">
              Update Bonds
            </button>
          </div>
        </div> --}}
      </div>

      <div class="col-md-7">
        <h3>Equities List</h3>
        <div class="panel panel-primary">
          <div class="panel-heading">
            Daily price list
          </div>
          <div class="panel-body">
            <form method="post" onsubmit="return generateReports()">
              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <label for="start_date">From</label>
                    <input type="date" placeholder="Select date" id="start_date" class="form-control" required="">
                  </div>
                  <div class="col-sm-4">
                    <label for="start_date">To</label>
                    <input type="date" placeholder="Select date" id="end_date" class="form-control" required="">
                  </div>
                </div>
              </div>

              <div class="form-group">
                <div class="row">
                  <div class="col-sm-4">
                    <button type="submit" id="generate-btn" class="btn btn-info">
                      Download Report
                    </button>
                  </div>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  {{-- create a smaple height --}}
  <div style="height: 200px;"></div>
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      // $.get('/admin/update/price', function(data) {
      //   /*optional stuff to do after success */
      //   console.log('new price updates sent !');
      //   console.log(data);
      // });

      // // body...
      // $.get('/admin/scan/orders/buy', function(data) {
      //   /*optional stuff to do after success */
      //   console.log('drone released looking for buy orders, scanning...');
      //   console.log(data);
      // });
      
      // $.get('/admin/scan/orders/sell', function(data) {
      //   /*optional stuff to do after success */
      //   console.log('drone released looking for sell orders, scanning...');
      //   console.log(data);
      // });
    });

    function updateEquities() {
      $("#eqt-btn").html('Updating....');
      var token = '{{ csrf_token() }}';
      
      $.post('{{ url("update/equities/prices") }}', {_token: token}, function(data, textStatus, xhr) {
        if(data.status == "success"){
          $("#eqt-btn").html('Update Equities');
          swal(
            "Ok",
            data.message,
            data.status
          );
        }else{
          $("#eqt-btn").html('Update Equities');
          swal(
            "Oops",
            data.message,
            data.status
          );
        }
      }).fail(err => {
        $("#eqt-btn").html('Update Equities');
          swal(
            "Oops",
            "Network not available, reload and try again!",
            "error"
          );
      });
        
    }

    $.get('/load/nse/list', function (data){
      // console.log(data);
      $(".nse-equities-list").html("");
      var sn = 0;
      $.each(data, function (index, value){
        sn++;

        $(".nse-equities-list").append(`
          <div style="padding:0.4em;">
            <span>`+value.date+` </span>
            <span class="pull-right">
              <a href="/equities/`+value.name+`"> <i class="fa fa-download"></i> Download  Equities  </a>
            </span>
          </div>
        `);
      });
    });

    // Pusher.logToConsole = true;
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
      encrypted: false
    });

    var channel = pusher.subscribe('new-stock-upload');
    channel.bind('CATSS\\Events\\NewStockUpload', function(data) {
      var id    = data.id;
      var name  = data.name;
      var date  = data.date;

      var container = $(".nse-equities-list");
      var markup = `
        <div>
          <span>`+data.date+` </span>
          <span class="pull-right">
            <a href="/equities/`+data.name+`"> <i class="fa fa-download"></i> Download  Equities  </a>
          </span>
        </div>
      `;
      container.prepend(markup);
    });
  </script>
@endsection