@extends('layouts.admin-skin')

@section('title')
  CATSS | Add New Stock Broker
@endsection

@section('contents')
  <style type="text/css">
    .typeahead.tt-input {
      width: 100%;
    }  
  </style>
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h3>Create/Add a Stock Broker</h3>
        <div class="card">
          <div class="card-header btn-info">
            Add Stock Broker
            <a href="javascript:void(0);" class="pull-right" style="color:#fff;" data-toggle="modal" data-target="#add-new-broker">
              <i class="fa fa-user"></i> Add
            </a>
          </div>
          <div class="card-body" style="padding: 1em;">
            <div class="row">
              <div class="col-md-12">
                <table class="table" id="brokerage-table">
                  <thead>
                    <tr>
                      <th>Brokerage Firm</th>
                      <th>Total Broker</th>
                      <th>Total Client</th>
                      <th>Option</th>
                    </tr>
                  </thead>
                  <tbody class="load-brokerage-firms"></tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!-- Add a brokerage client -->
  <div class="modal fade" id="add-new-broker" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <h4>Add a Brokerage Firm</h4>
        </div>
        <div class="modal-body">
          <div class="col-md-12">
            <form id="create-agent-form" method="post" onsubmit="return addStockBroker()">
              <div class="form-group">
                <label>Select a Brokerage firm</label>
                <select class="form-control" id="broker_firm" required="">
                  <option value=""> none </option>
                </select>
              </div>

              <div class="form-group">
                <label>Enter Agent's Name</label>
                <input type="text" class="form-control" id="broker_name" placeholder="First name, Last name" name="">
              </div>

              <div class="form-group">
                <label>Enter Agent's Email</label>
                <input type="text" class="form-control" id="broker_email" placeholder="someone@domain.com" name="">
              </div>

              <div class="form-group">
                <button class="btn btn-info" id="add-new-agent">Add New Agent</button>
              </div>
            </form>
          </div>
        </div>
        <div class="modal-footer">
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
      // get all stock broking firm
      loadStockFirm();
      getAllFirms();

      function loadStockFirm() {
        $.get('{{ url('admin/list/added/firms') }}', function(data) {
          $.each(data, function(index, val) {
            $('#broker_firm').append(`
              <option value="${val.id}">${val.firm_name}</option>
            `);
          });
        });
      }

      function addStockBroker() {
        $("#add-new-agent").prop('disabled', true);
        $("#add-new-agent").html(`Creating account please wait!`);
        var token           = $("#token").val();
        var broker_firm     = $("#broker_firm").val();
        var broker_name     = $("#broker_name").val();
        var broker_email    = $("#broker_email").val();

        var params = {
          broker_firm: broker_firm,
          broker_name: broker_name,
          broker_email: broker_email,
          _token: token
        };

        $.post('{{ url('admin/save/broker') }}', params, function(data, textStatus, xhr) {
          /*optional stuff to do after success */
          if(data.status == "success"){
            swal(
              "success",
              data.message,
              data.status
            );
            $("#create-agent-form")[0].reset();
            $("#add-new-agent").html(`Add`);
            getAllFirms();
          }else{
            swal(
              "oops",
              data.message,
              data.status
            );
          }

          $("#add-new-agent").prop('disabled', false);
        });

        // return 
        return false;
      }

      function getAllFirms() {
        $.get('{{ url('admin/all/brokerage/firms') }}', function(data) {
          /*optional stuff to do after success */
          $(".load-brokerage-firms").html("");
          $.each(data, function(index, val) {
            $(".load-brokerage-firms").append(`
              <tr>
                <td>${val.firm_name}</td>
                <td>${val.total_broker}</td>
                <td>${val.total_client}</td>
                <td>
                  <a href="{{ url('admin/view/brokers') }}/${val.id}">view</a>
                </td>
              </tr>
            `);
          });
        });
      }
    </script>
@endsection