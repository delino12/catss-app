@extends('layouts.admin-skin')

@section('title')
  CATSS | Flash News
@endsection

@section('contents')
    <div class="container">
        <div class="row">
          <div class="col-md-8">
            <h3>Daily news help clients start their weekly call plan for Trade tactics. </h3>

            @if(session('news_status'))
              <p class="text-success">
                {{ session('news_status') }}
              </p>
            @endif
            <form method="POST" action"/admin/news">
             {{ csrf_field() }}
             <div class="form-group">
              <label>Title</label>
              <textarea cols="10" rows="1" placeholder="news title" name="title" class="form-control" required=""></textarea>
             </div>

             <div class="form-group">
              <label>Body</label>
              <textarea cols="50" rows="10" placeholder="Type here..." name="body" class="form-control" required=""></textarea>
             </div>

             <div class="form-group">
               <button class="btn btn-primary">Post News</button>
             </div>
            </form>
          </div>
          <div class="col-md-4">
            <h3>News Update: </h3>
            <br />
            <div class="load-single-news"></div>
          </div>
        </div>
    </div>
    <hr /><br />
@endsection

@section("scripts")
    {{-- load news --}}
    <script type="text/javascript">
      $.get('/load-all-news', function(data) {
        // console.log(data);
        $('.load-single-news').html();

        $.each(data, function(index, val) {
           /* iterate through array or object */
           // console.log(val);
          $('.load-single-news').append(`
            <div class="panel panel-info">
              <div class="panel-heading">`+val.title+`</div>
              <div class="panel-body">
                <p>`+val.body+`</p>
                <span class="small">
                 `+val.date+`
                </span>
                <hr />
                <a href="/delete-news/`+val.id+`"> <i class="fa fa-bitbucket"></i> Delete </a>
                <a class="pull-right" href="/edit-news/`+val.id+`"> <i class="fa fa-edit"></i> Edit </a>
              </div>
            </div>
          `);
        });

      });
    </script>
@endsection