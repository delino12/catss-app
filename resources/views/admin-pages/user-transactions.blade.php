@extends('layouts.admin-skin')

@section('title')
  CATSS | All Transactions
@endsection

@section('contents')
  <div class="container">
    <div class="row">
     <div class="col-md-10">
        <div class="card">
            <div class="card-header" data-background-color="purple">
                <h4 class="title">All Transactions history</h4>
                <p class="category">Last updated {{ date("d M Y ") }}</p>
            </div>
            <div class="card-content table-responsive">
                <table class="table table-hover" id="transaction-table">
                    <thead class="text-warning">
                      <tr>
                        <th>S/N</th>
                        <th>Dealer</th>
                        <th>Security</th>
                        <th>price</th>
                        <th>Quantity</th>
                        <th>Amount&#8358; </th>
                        <th>Trade Type</th>
                        <th>Date</th>
                      </tr>
                    </thead>
                    <tbody class="load-transactions"></tbody>
                </table>
            </div>
        </div>
      </div>
    </div>
  </div>

  {{-- scripts --}}
  <script type="text/javascript">
    // $(document).ready(function(){
      loadAllOrders();
    // });

    // load all users orders
    function loadAllOrders() {
      $.get('{{url('admin-fetch/transaction')}}/{{ $user_id }}', function(data) {
        $(".load-transactions").html("");
        let sn = 0;
        $.each(data, function(index, val) {
          sn++;
          $(".load-transactions").append(`
            <tr>
              <td>${sn}</td>
              <td>${val.owner}</td>
              <td>${val.security}</td>
              <td>&#8358; ${val.price}</td>
              <td>${val.qty}</td>
              <td>&#8358; ${val.amount}</td>
              <td>${val.trade_type}</td>
              <td>${val.last_seen}</td>
            </tr>
          `);
          // console.log(val);
        });

        $("#transaction-table").dataTable();
      });
    }
  </script>
@endsection