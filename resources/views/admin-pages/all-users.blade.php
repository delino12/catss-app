@extends('layouts.admin-skin')

@section('title')
  CATSS | List Users
@endsection

@section('contents')
  <div class="row">
    <div class="col-md-12">
      @if(session('reset_msg'))
        <div class="alert alert-success">
          {{ session('reset_msg') }}
        </div>
      @endif
      <div class="card">
          <div class="card-header" data-background-color="orange">
              <h4 class="title">All CATSS Users</h4>
              <p class="category">Updates of users {{ date("M Y ") }}</p>
          </div>
         
          <div class="card-content table-responsive">
              <table class="table table-hover" id="users-table">
                  <thead class="text-warning">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Transactions</th>
                    <th>&#8358; Balance</th>
                    <th>Option</th>
                    <th>Action</th>
                    <th>Status</th>
                    <th>Date</th>
                  </thead>
                  <tbody class="load-all-users">
                    <tr>
                      <td>Loading...</td>
                    </tr>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
  <script type="text/javascript">

    // load modules
    loadAllUsers();


    function loadAllUsers() {
      $.get('{{ url('admin/load/all/users') }}', function(data) {
        $(".load-all-users").html("");
        $.each(data, function(index, val) {
         var lock_access;
          var rowShade;
          if(val.status == 'open'){
            rowShade = '';
            lock_access = `
              <a href="javascript:void(0);" onclick="blockUserAccount(${val.id})" class="dino-link">Disable</a>
            `;
          }else{
            rowShade = 'bgcolor="lightblue"';
            lock_access = `
              <a href="javascript:void(0);" onclick="unblockUserAccount(${val.id})" class="dino-link">Enable</a>
            `;
          }

          $(".load-all-users").append(`
            <tr ${rowShade}>
              <td>${val.name}</td>
              <td>${val.email}</td>
              <td><a href="/admin-view/transaction/${val.id}" class="dino-link"> view</a></td>
              <td>&#8358;${val.balance}</td>
              <td>
                ${lock_access}
              </td>
              <td><a href="javascript:void(0);" class="btn btn-danger btn-sm" onclick="resetAccount(${val.id})" class="dino-link"> Reset</a></td>
              <td>${val.status}</td>
              <td>${val.updated}</td>
            </tr>
          `);
        });
        $("#users-table").dataTable();
      });
    }

    function blockUserAccount(user_id) {
      swal({
        title: 'Are you sure?',
        text: "This user will be denied login access until account is unblock",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed'
      }).then((result) => {
        if (result.value) {
          var token = $("#token").val();
          var params = {
            _token: token,
            user_id: user_id
          };
        
          // hire agent
          $.post('{{ url('block/user/account') }}', params, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            if(data.status == "success"){
              swal(
                "success",
                data.message,
                data.status
              );
              loadAllUsers();
            }else{
              swal(
                "oops",
                data.message,
                data.status
              );
            }
          });
        }
      });
    }

    function unblockUserAccount(user_id) {
      swal({
        title: 'Are you sure?',
        text: "This user access will be restored!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed'
      }).then((result) => {
        if (result.value) {
          var token = $("#token").val();
          var params = {
            _token: token,
            user_id: user_id
          };
        
          // hire agent
          $.post('{{ url('unblock/user/account') }}', params, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            if(data.status == "success"){
              swal(
                "success",
                data.message,
                data.status
              );
              loadAllUsers();
            }else{
              swal(
                "oops",
                data.message,
                data.status
              );
            }
          });
        }
      });
    }

    function resetAccount(user_id) {
      var token = $("#token").val();
      var params = {
        _token: token,
        user_id: user_id
      };
      $.post('{{ url("admin/reset/account") }}', params, function(data) {
        if(data.status == "success"){
          window.location.reload();
        }
      });
    }
  </script>
@endsection