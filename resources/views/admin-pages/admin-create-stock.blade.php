@extends('layouts.admin-skin')

@section('title')
  CATSS Toggle Stock Price
@endsection

@section('contents')
  <br />
  @if(session('t_status'))
    <div class="row">
      <div class="col-md-6 col-md-offset-3">
        <p> {{ session('t_status') }} </p>
      </div>
    </div>
  @endif()
  
  <div class="col-md-6">
    <div class="panel panel-info panel-heading">
      <h3>toggle Securities price</h3>

      <table class="table">
        <thead>
          <tr>
            <th>Security</th>
            <th>Open Mkt Price</th>
            <th>Previous Close</th>
            <th>Option</th>
          </tr>
        </thead>
        <tbody>
          @foreach($securities as $security)
          <form method="post" action="/security/toggle/{{ $security->id }}">
            {{ csrf_field() }}
            <div class="form-group">
              <tr>
                <td>{{ $security->security }}</td>
                <td><input type="text" class="form-control" name="touch_price" value="{{ $security->close_price }}"></td>
                <td>{{ $security->previous_close }}</td>
                <td><button class="btn btn-info">Toggle Price</button> </td>
              </tr>
            </div>
          </form>
          @endforeach
          
        </tbody>
      </table>
    </div>
  </div>
@endsection