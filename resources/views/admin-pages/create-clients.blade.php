@extends('layouts.admin-skin')

@section('title')
  CATSS | Add New Stock Broker
@endsection

@section('contents')
  <style type="text/css">
    .typeahead.tt-input {
      width: 100%;
    }  
  </style>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <h3>Create new client</h3>
          <div class="card">
            <div class="card-header btn-info">Add New Client Account</div>
            <div class="card-body" style="padding: 1em;">
              <div class="row">
                <div class="col-md-4">
                  <form method="post" onsubmit="return addStockBroker()">
                    <div class="form-group">
                      <label>Select a Brokerage firm</label>
                      <input type="text" class="form-control" id="broker_firm" placeholder="Eg. Afritrust, InvestmentOne..." required="">
                    </div>

                    <div class="form-group">
                      <label>Client's Name</label>
                      <input type="text" class="form-control" id="broker_name" placeholder="First name, Last name" name="">
                    </div>

                    <div class="form-group">
                      <label>Client's Email</label>
                      <input type="text" class="form-control" id="broker_email" placeholder="someone@domain.com" name="">
                    </div>

                    <div class="form-group">
                      <label>Choose a default password</label>
                      <input type="password" class="form-control" id="broker_password" placeholder="*****" name="">
                    </div>

                    <div class="form-group">
                      <button class="btn btn-info" id="add-new-client">Create Client</button>
                    </div>
                  </form>
                </div>
                <div class="col-md-8">
                  <table class="table" id="brokerage-table">
                    <thead>
                      <tr>
                        <th>Brokerage Firm</th>
                        <th>Client Name</th>
                        <th>Client Email</th>
                        <th>Option</th>
                      </tr>
                    </thead>
                    <tbody class="load-firms-agent"></tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
      // get all stock broking firm
      loadStockFirm();
      getAllFirms();

      function loadStockFirm() {
        $.get('{{ url('admin/list/firm/names') }}', function(data) {
          $('#broker_firm').typeahead({
            source: data
          });
        });
      }

      function getAllFirms() {
        $.get('{{ url('admin/all/clients') }}', function(data) {
          /*optional stuff to do after success */
          $.each(data, function(index, val) {

            if(val.firm_name == null){
              val.firm_name = "No brokerage firm found!";
            }
            $(".load-firms-agent").append(`
              <tr>
                <td>${val.firm_name}</td>
                <td>${val.name}</td>
                <td>${val.email}</td>
                <td>
                  <a href="javascript:void(0);">view</a>
                </td>
              </tr>
            `);
          });

          $("#brokerage-table").dataTable();
        });
      }
    </script>
@endsection