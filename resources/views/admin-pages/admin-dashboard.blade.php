@extends('layouts.admin-skin')

@section('title')
  CATSS | Dashboard
@endsection

@section('contents')
  <div class="row">
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="orange">
                <i class="material-icons">content_copy</i>
            </div>
            <div class="card-content">
                <p class="category">Total Commission</p>
                <h3 class="title">
                  <span class="total-sum"></span>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons text-success">date_range</i>
                    <a href="/admin/commission">Equities Trade</a>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="green">
                <i class="material-icons">store</i>
            </div>
            <div class="card-content">
                <p class="category">Total Revenue</p>
                <h3 class="title">
                  <span class="total-sum"></span>
                </h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">date_range</i> Last 24 Hours
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="red">
                <i class="material-icons">info_outline</i>
            </div>
            <div class="card-content">
                <p class="category">Total No. of Trade </p>
                <i class="material-icons">swap_vert</i>
                <h3 class="title no_of_trade"></h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">local_offer</i> Tracked from CATSS Trade
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-3 col-md-6 col-sm-6">
        <div class="card card-stats">
            <div class="card-header" data-background-color="blue">
                <i class="fa fa-twitter"></i>
            </div>
            <div class="card-content">
                <p class="category">Followers</p>
                <i class="material-icons">person</i>
                <h3 class="title">+245</h3>
            </div>
            <div class="card-footer">
                <div class="stats">
                    <i class="material-icons">update</i> Just Updated
                </div>
            </div>
        </div>
    </div>
  </div>
  <div class="row">
    <div class="col-md-12">
      @if(session('reset_msg'))
        <div class="alert alert-success">
          {{ session('reset_msg') }}
        </div>
      @endif
      <div class="card">
          <div class="card-header" data-background-color="orange">
              <h4 class="title">All CATSS Users</h4>
              <p class="category">Updates of users {{ date("M Y ") }}</p>
          </div>
         
          <div class="card-content table-responsive">
              <table class="table table-hover" id="users-table">
                  <thead class="text-warning">
                    <th>Name</th>
                    <th>Email</th>
                    <th>Transactions</th>
                    <th>&#8358; Balance</th>
                    <th>Option</th>
                    <th>Status</th>
                    <th>Date</th>
                  </thead>
                  <tbody class="load-all-users">
                    <tr>
                      <td>Loading...</td>
                    </tr>
                  </tbody>
              </table>
          </div>
      </div>
    </div>
  </div>
  
  <script type="text/javascript">
    // refresh data 
    function refreshData(){
      $.get('/admin/load-commission', function(data) {
        /*optional stuff to do after success */
        $('.total-sum').html(`
          &#8358; `+data.total+`
        `);

        $('.no_of_trade').html(`
          `+data.count+`
        `);  
      });
    }

    $(document).ready(function(){
      refreshData();
    });

    // load modules
    loadAllUsers();
    
    function loadAllUsers() {
      $.get('{{ url('admin/load/all/users') }}', function(data) {
        $(".load-all-users").html("");
        $.each(data, function(index, val) {

          var lock_access;
          var rowShade;
          if(val.status == 'open'){
            rowShade = '';
            lock_access = `
              <a href="javascript:void(0);" onclick="blockUserAccount(${val.id})" class="dino-link">Lock Account</a>
            `;
          }else{
            rowShade = 'bgcolor="lightblue"';
            lock_access = `
              <a href="javascript:void(0);" onclick="unblockUserAccount(${val.id})" class="dino-link">Unlock Account</a>
            `;
          }

          $(".load-all-users").append(`
            <tr ${rowShade}>
              <td>${val.name}</td>
              <td>${val.email}</td>
              <td><a href="/admin-view/transaction/${val.id}" class="dino-link"> view</a></td>
              <td>&#8358;${val.balance}</td>
              <td>
                ${lock_access}
              </td>
              <td>${val.status}</td>
              <td>${val.updated}</td>
            </tr>
          `);
        });
        $("#users-table").dataTable();
      });
    }

    function blockUserAccount(user_id) {
      swal({
        title: 'Are you sure?',
        text: "This user will be denied login access until account is unblock",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed'
      }).then((result) => {
        if (result.value) {
          var token = $("#token").val();
          var params = {
            _token: token,
            user_id: user_id
          };
        
          // hire agent
          $.post('{{ url('block/user/account') }}', params, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            if(data.status == "success"){
              swal(
                "success",
                data.message,
                data.status
              );
              window.location.reload();
            }else{
              swal(
                "oops",
                data.message,
                data.status
              );
            }
          });
        }
      });
    }

    function unblockUserAccount(user_id) {
      swal({
        title: 'Are you sure?',
        text: "This user access will be restored!",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, Proceed'
      }).then((result) => {
        if (result.value) {
          var token = $("#token").val();
          var params = {
            _token: token,
            user_id: user_id
          };
        
          // hire agent
          $.post('{{ url('unblock/user/account') }}', params, function(data, textStatus, xhr) {
            /*optional stuff to do after success */
            if(data.status == "success"){
              swal(
                "success",
                data.message,
                data.status
              );
              loadAllUsers();
            }else{
              swal(
                "oops",
                data.message,
                data.status
              );
            }
          });
        }
      });
    }
  </script>
@endsection
