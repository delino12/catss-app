@extends('layouts.trade-room-skin')

@section('title')
    CATSS NSE Price List
@endsection

@section('contents')
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <section>
                  <!--for demo wrap-->
                  <h1>Equities Daily Prices List</h1>
                  <div class="tbl-header">
                    <table cellpadding="0" cellspacing="0" border="0">
                      <thead>
                        <tr>
                          <th>Security</th>
                          <th>(&#8358;) Ref  </th>
                          <th>(&#8358;) Open </th>
                          <th>(&#8358;) High </th>
                          <th>(&#8358;) Low  </th>
                          <th>(&#8358;) Close </th>
                          <th>(&#8358;) Previous Close</th>
                          <th><i class="fa fa-exclamation-triangle"></i> Price</th>
                          <th>Date</th>
                        </tr>
                      </thead>
                    </table>
                  </div>
                  <div class="tbl-content">
                    <table cellpadding="0" cellspacing="0" border="0">
                      <tbody class="equity-row small"></tbody>
                    </table>
                  </div>
                </section>
            </div>
            <div style="height: 100px;"></div>
        </div>
    </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    loadDailyPrices();
    
    // account balance state
    $.get("/accountbalance", function (data){
      $("#ac").text(data.account_balance);
      $(".account_balance").text(data.account_balance);
    });
    
    function loadDailyPrices(){
      $.get('/market/equity/live', function (data){
        $(".equity-row").html("");
        $.each(data, function (index, value){
          var stats;
          if(value.previous_close > value.close_price){
            stats = `<i class="fa fa-arrow-down text-danger"></i>`;
          }

          if(value.previous_close < value.close_price){
            stats = `<i class="fa fa-arrow-up text-success"></i>`;
          }

          if(value.previous_close == value.close_price){
            stats = `<i class="fa fa-stop text-info"></i>`;
          }

          $(".equity-row").append(`
            <tr>
              <td bgcolor="#000">`+value.security+`</td>
              <td>`+value.ref_price+`</td>
              <td>`+value.open_price+`</td>
              <td>`+value.high_price+`</td>
              <td>`+value.low_price+`</td>
              <td>`+stats+` `+value.close_price+`</td>
              <td>`+value.previous_close+`</td>
              <td>`+value.change_price+`</td>
              <td><span>`+value.date+`</span></td>
            </tr>
          `);
        });

        // $("#pairs_table").dataTable({
        //     fixedHeader: true,
        //     pageLength: 50
        // });
      });
    }

    // '.tbl-content' consumed little space for vertical scrollbar, scrollbar width depend on browser/os/platfrom. Here calculate the scollbar width .
    $(window).on("load resize ", function() {
      var scrollWidth = $('.tbl-content').width() - $('.tbl-content table').width();
      $('.tbl-header').css({'padding-right':scrollWidth});
    }).resize();
  </script>
@endsection