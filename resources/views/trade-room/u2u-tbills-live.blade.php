@extends('layouts.market-skin')

@section('title')
  CATSS Live | TBills
@endsection

@section('contents')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-2">
      <div class="c-card">
        {{-- simulation section --}}
        <br />
        <p class="text-center">My Transaction</p>
        {{-- <img src="/svg/catss-loading-transactions.svg" class="loading-transactions"> --}}
        <table class="table small">
          <thead>
            <tr>
              <th>Assets</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody class="load-transactions-users"></tbody>
        </table>
      </div>
    </div>

    <div class="col-md-7">
      <div class="c-card">
        <table class="table">
          <thead class="">
            <tr>
              <th>
                <select class="form-control" id="instrument" onchange="loadAssetDefault()"></select>
              </th>
              <th>
                <button class="dino-input" onclick="placeNewOrder()">Order</button>
              </th>
              <th>Bid</th>
              <th></th>
              <th></th>
              <th>Ask</th>
              <th></th>
              <th></th>
            </tr>
            <tr>
              <th><i class="fa fa-file"></i> Securities</th>
              <th class="text-success"><i class="fa fa-database"></i> vol</th>
              <th class="text-success">price(&#8358;)</th>
              <th></th>
              <th></th>
              <th class="text-danger">price(&#8358;)</th>
              <th class="text-danger"><i class="fa fa-database"></i> vol</th>
              <th></th>
            </tr>
          </thead>
          <tbody class="load-tbills-u2u"></tbody>
        </table>
      </div>
    </div>

    <div class="col-md-3">
      <div class="c-card">
        {{-- simulation section --}}
        <br />
        <p class="text-center">Last Updated Transaction</p>
        {{-- <img src="/svg/catss-loading-transactions.svg" class="loading-transactions"> --}}
        <table class="table small">
          <thead>
            <tr>
              <th>Assets</th>
              <th>Dis. (%)</th>
              <th>Qty</th>
            </tr>
          </thead>
          <tbody class="load-transactions"></tbody>
        </table>
      </div>
      <div class="c-card">
        <hr />
        <div class="load-headlines container-fluid"><img src="/svg/catss-loading-news.svg"></div>
      </div>
    </div>
  </div>

  {{-- modal popup section --}}
  <div class="display-modal">
    <div class="modal fade" id="pop_trade_form" role="dialog">
      <div class="modal-dialog modal-md">
        <div class="modal-content">
          <div class="modal-body">
            <form method="post" onsubmit="return false">
              <div class="col-sm-12"><span class="lead text-info sec-title-a1"></span></div>
              <input type="hidden" id="security" value="" />
              <div class="col-sm-6">
                <h1 class="lead text-success">Bid</h1>
                <div class="form-group">
                  <input type="number" min="0" max="9999" step=any maxlength="4" class="form-control" id="bid-price-a1" placeholder="price" required>
                </div>
                <div class="form-group">
                  <input type="number" min="0" max="1000000" step=any maxlength="18" class="form-control" id="bid-qty-a1" placeholder="qty">
                </div>
              </div>
              <div class="col-sm-6">
                <h1 class="lead text-danger">Ask</h1>
                <div class="form-group">
                  <input type="number" min="0" max="9999" step=any maxlength="4" class="form-control" id="ask-price-a1" placeholder="price" required>
                </div>
                <div class="form-group">
                  <input type="number" min="0" max="1000000" step=any maxlength="18" class="form-control" id="ask-qty-a1" placeholder="qty">
                </div>
              </div>
              <div class="col-sm-12">
                <div class="form-group">
                  <button class="btn btn-primary" id="place-order">
                    <span class="option">Place Order</span>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  // init document
  $(document).ready(function (){
    // loadMarketPrices();
    fetchNews();
    loadTbillsOrders();
    loadAssetsList();
    
    // loadTransactions();
    // loadTransactionsAll();
    // fetchSummaryTbills();
  });

  // fetch bloomberg
  function fetchNews() {
    // dino get data
    $.get('/load/api-news/bloomberg', function(data) {
      // console.log(data);
      let sn = 0;
      $(".load-headlines").html("");
      $.each(data.articles, function(index, val) {
        // console.log(val);
        sn++;
        $(".load-headlines").append(`
          <div class="row">
            <div class="col-xs-3">
              <img src="`+val.urlToImage+`" width="100%" height="auto">
            </div>
            <div class="col-xs-9">
              <span class="dino-link">`+val.title+`</span><br />
              <p>`+val.description+` <a class="c-title" target="_blank" href="`+val.url+`">read more</a></p>
            </div>
          </div>
          <br />
        `);
        if(sn == 10){
          return false;
        }
      });
    });
  }

  // post orders
  function placeOrderRequest(sec_id, req_type) {

    // init params
    let token = '{{ csrf_token() }}';
    let qty   = $("#qty_"+sec_id).val();
    let price = $("#price_"+sec_id).val();

    // init req option
    let data = {
      _token: token,
      secid: secid,
      qty: qty,
      price: price,
    };

    // check order req type
    if(req_type == 'ask' || req_type == 'bid'){
      data.ordertype = req_type;
    }

    console.log(data);

    // void form
    return false;
  }

  // load tbills orders
  function loadTbillsOrders() {
    $.get('/load/all/tbills/orders', function(data) {
      $(".load-tbills-u2u").html("");
      var sn = 0;
      $.each(data, function(index, val) {
        sn++;
        // console.log(val);
        $(".load-tbills-u2u").append(`
          <tr>
            <td>${val.security}</td>
            <td class="text-success">${numeral(val.bid_qty).format('0,0')}</td>
            <td class="text-success">
              <button title="bid" onclick="displayOrderForm(${sn}, ${val.best_ask}, ${val.best_bid}, ${val.bid_qty}, ${val.ask_qty}, '${val.security}')" class="dino-input">&#8358;${numeral(val.best_bid).format('0.00')}</button>
            </td>
            <td></td>
            <td></td>
            <td class="text-danger">
              <button title="ask" onclick="displayOrderForm(${sn}, ${val.best_ask}, ${val.best_bid}, ${val.bid_qty}, ${val.ask_qty}, '${val.security}')" class="dino-input">&#8358;${numeral(val.best_ask).format('0.00')}</button>
            </td>
            <td class="text-danger">${numeral(val.ask_qty).format('0,0')}</td>
            <td>
              <button class="ask-sec dino-input"><i class="fas fa-chart-area"></i> Depth</button>
            </td>
          </tr>
        `);
      });
    });
  }

  // display trade form
  function displayOrderForm(sn, best_ask, best_bid, bid_qty, ask_qty, security) {
    // body...
    $("#security").val(security);
    $("#bid-price-a1").val(best_bid);
    $("#ask-price-a1").val(best_ask);

    $("#bid-qty-a1").val(bid_qty);
    $("#ask-qty-a1").val(ask_qty);

    $(".sec-title-a1").html('NTBL'+security);
    $('#pop_trade_form').modal('show');
  }

  // place order
  function placeNewOrder() {
    // var token     = '';
    // var bidPrice  = $("#bid-price-a1").val();
    // var bidQty    = $("#bid-qty-a1").val();
    // var askPrice  = $("#ask-price-a1").val();
    // var askQty    = $("#ask-qty-a1").val();
    // var security  = $("#security").val();

    // // option
    // var params = {
    //   _token: token,
    //   security:security,
    //   bidPrice: bidPrice,
    //   bidQty: bidQty,
    //   askPrice: askPrice,
    //   askQty: askQty  
    // };

    // // console.log(params);

    // // place order
    // $.post('/place/new/tbill/order', params, function(data){
    //   console.log(data);
    // });

    return false;
  }

  $("#place-order").click(function (e){
    e.preventDefault(e);

    var token     = '{{ csrf_token() }}';
    var bidPrice  = $("#bid-price-a1").val();
    var bidQty    = $("#bid-qty-a1").val();
    var askPrice  = $("#ask-price-a1").val();
    var askQty    = $("#ask-qty-a1").val();
    var security  = $("#security").val();

    // option
    var params = {
      _token: token,
      security:security,
      bidPrice: bidPrice,
      bidQty: bidQty,
      askPrice: askPrice,
      askQty: askQty  
    };

    // place order
    $.post('/place/new/tbill/order', params, function(data){
      // console.log(data);
      if(data.status == 'success'){
        loadTbillsOrders();
        $("#pop_trade_form").modal('hide');
      }
    });
  });

  // place new order
  function placeNewOrder(){
    // body...
    var security = $("#instrument").val();

    $(".sec-title-a1").html(security);
    $("#pop_trade_form").modal('show');
  }

  // load security list
  function loadAssetsList(){
    $.get('/fetch/market/prices/tbills', function(data) {
      // console.log(data);
      $("#instrument").html("");
      $.each(data, function(index, val) {
        // console.log(val);
        $("#instrument").append(`
          <option value="${val.Description}">${val.Description}</option>
        `);
      });
    });
  }

  // on select updates
  function loadAssetDefault(){
    var security = $("#instrument").val();
    var params = {
      asset: security
    };

    $.get('/fetch/market/one/tbills', params, function (data){
      // console.log(data);
      // body...
      $("#security").val(data.Description);
      $("#bid-price-a1").val(data.ClosingMktPrice);
      $("#ask-price-a1").val(data.ClosingMktPrice);

      $("#bid-qty-a1").val();
      $("#ask-qty-a1").val();
    });
  }
</script>
@endsection
