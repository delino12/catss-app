@extends('layouts.order-skin')

@section('title')
  CATSS Live Feeds
@endsection

@section('contents')
  <div class="container-fluid order-section" style="padding: 2px;">
    <div class="row">
      <div class="col-md-12">
        <nav class="navbar navbar-inverse" style="background-color: rgba(110,110,100,0.70);left: 17%; position: fixed;z-index: 200;">
          <div class="container-fluid small">
            <div class="navbar-header">
              <a class="navbar-brand" href="#">
                <i clas=s="fa fa-user"></i> <span style="font-size: 12px;">{{ Auth::user()->name }}</span>
              </a>
            </div>
            <ul class="nav navbar-nav">
              <li class="active"><a href="#" style="color: #29ad14;">&#8358; <span class="account_balance"></span></a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="{{ URL::previous() }}"><span class="glyphicon glyphicon-arrow-left"></span> Back</a></li>
            </ul>
          </div>
        </nav>
      </div>
    </div>

    {{--  --}}
    <div class="row">
      {{-- order book section --}}
      <div class="col-md-2" style="padding-right: 0px;padding-left: 5px;">
        <div class="card" style="height: 780px;">
          <div class="card-body">
            <div class="chart-summary">
              <h1 class="lead small text-center" data-step="1" data-intro="Hi, You can skip or click next. This section show all available orders ">Oder Book</h1>
              <hr />
              <span data-step="2" data-intro="this section show all available Bid in the market">Offer</span>
              <table class="table small ">
                <thead>
                  <tr>
                    <td><span class="c-title">Price</span> (&#8358;)</td>
                    <td><span class="c-title">Volume</span></td>
                  </tr>
                </thead>
                <tbody class="load-offer-spread"></tbody>
              </table>
              <div class="text-center" style="padding: 0.6em;background-color: rgba(000,000,000,0.60);">
                <span data-step="4" data-intro="spread line for Bid/Ask">spread</span>
              </div>
              
              <span data-step="3" data-intro="this section show all available Ask in the market">Ask</span>
              <table class="table small">
                <thead>
                  <tr>
                    <td><span class="c-title">Price</span> (&#8358;)</td>
                    <td><span class="c-title">Volume</span></td>
                  </tr>
                </thead>
                <tbody class="load-ask-spread"></tbody>
              </table>
            </div>
          </div>
        </div>
      </div>

      {{-- trade dashboard section --}}
      <div class="col-md-6">
        <div class="card" style="height: auto;">
          <div class="card-body">
            <h1 class="lead small text-right">Trading Room (<span class="c-title">{{ $name }} <span class="last_seen_price"></span></span>)</h1>
            <hr />

            {{-- <img src="/img/stock-exchange-image.png" class="img img-responsive"> --}}
            {{-- <div id="candlestickChart" class="h-300"> --}}
            
            <br />
            <div id="exTab1"> 
              <ul  class="nav nav-pills">
                <li class="active">
                  <a  href="#1a" data-toggle="tab"><span class="c-title" data-step="5" data-intro="Trade at market limit">Limit</span></a>
                </li>
                <li><a href="#2a" data-toggle="tab"><span class="c-title">Market</span></a>
                </li>
                <li><a href="#3a" data-toggle="tab"><span class="c-title">Stop Limit <i class="fa fa-warning"></i></span>  </a>
                </li>
              </ul>

              <div class="tab-content clearfix">

                {{-- LIMIT ORDER --}}
                <div class="tab-pane active" id="1a">
                  <p class="small"><span class="text-info">Note:</span> Limit order is an order that sets the maximum or minimum at which you are willing to buy or sell a particular stock.</p>
                  <div class="row">
                    {{-- buy section --}}
                    <div class="col-sm-6">
                      <h1 class="lead small"><i class="fa fa-money"></i> Buy 
                        <span class="c-title">{{ $name }}</span> 
                        <span class="last_seen_price pull-right c-title"></span>
                      </h1>

                      <hr />
                      <form id="buy-form" method="post" onsubmit="return placeOrderBuy()">
                        <div class="success_msg"></div>
                        <div class="error_msg"></div>
                        <table class="c-table small">
                          <tr>
                            <td><span data-step="6" data-intro="Enter bid price">Price</span></td>
                            <td><input type="text" onblur="valueAsset()" id="b-price" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td><span data-step="7" data-intro="place trade limit-order using amount or account balance">Amount</span></td>
                            <td><input type="text" id="b-amount" onblur="valueByAmount()" class="c-btn" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td><span data-step="8" data-intro="place trade limit-order using percentage calculation">
                            <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentage('25')">25%</a>
                            <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentage('50')">50%</a>
                            <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentage('75')">75%</a>
                            <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentage('100')">100%</a></span>
                            </td>
                          </tr>
                          <tr>
                            <td><span data-step="9" data-intro="place trade limit-order using total unit of assets ">Total</span> <i class="fa fa-cubes"></i></td>
                            <td><input type="text" id="b-total" onblur="valueByQuantity()" class="c-btn" required="" placeholder="Qty: 1000"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button class="c-btn" data-step="9" data-intro="click Buy button to place orders ">
                                <img src="/img/live-loader.gif" class="loading" width="15" height="15">  Buy 
                                <span class="c-title">{{ $name }}</span>
                              </button>
                            </td>
                          </tr>
                        </table>
                      </form>
                    </div>

                    {{-- sell section --}}
                    <div class="col-sm-6">
                      <h1 class="lead small"><i class="fa fa-money"></i> Sell 
                        <span class="c-title">{{ $name }}</span> 
                        <span class="last_seen_price pull-right c-title"></span>
                      </h1>
                      <hr />
                      <div class="sell_error_msg"></div>
                      <form id="sell-form" method="post" onsubmit="return placeOrderSell()">
                        <table class="c-table small">
                          <tr>
                            <td>Price</td>
                            <td><input type="text" id="s-price" onblur="valueAssetToSell()" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td>Amount</td>
                            <td><input type="text" id="s-amount" onblur="valueByAmountToSell()" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('25')">25%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('50')">50%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('75')">75%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('100')">100%</a>
                            </td>
                          </tr>
                          <tr>
                            <td>Total <i class="fa fa-cubes"></i></td>
                            <td><input type="text" id="s-total" onblur="valueByQuantityToSell()" class="c-btn" required="" placeholder="Qty: 1000"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button class="c-btn">
                                <img src="/img/live-loader.gif" class="s-loading" width="15" height="15"> Sell <span class="c-title">{{ $name }}</span>
                              </button>
                            </td>
                          </tr>
                        </table>
                      </form>
                    </div>
                  </div>
                </div>

                {{-- MARKET LIMIT --}}
                <div class="tab-pane" id="2a">
                  <p class="small"><span class="text-info">Note:</span> This order will be executed using the best market price available.</p>
                  <div class="row">
                    {{-- buy section at MARKET --}}
                    <div class="col-sm-6">
                      <h1 class="lead small"><i class="fa fa-money"></i> Buy 
                        <span class="c-title">{{ $name }}</span> 
                        <span class="last_seen_price pull-right c-title"></span>
                      </h1>

                      <hr />
                      <form id="buy-form-market" method="post" onsubmit="return placeOrderBuyMkt()">
                        <div class="success_msg_buy_mkt"></div>
                        <div class="error_msg_buy_mkt"></div>
                        <table class="c-table small">
                          
                          <tr>
                            <td>Amount</td>
                            <td><input type="text" id="b-amount-mkt" onblur="valueByAmountMkt()" class="c-btn" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageMkt('25')">25%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageMkt('50')">50%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageMkt('75')">75%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageMkt('100')">100%</a>
                            </td>
                          </tr>
                          <tr>
                            <td>Total <i class="fa fa-cubes"></i></td>
                            <td><input type="text" id="b-total-mkt" onblur="valueByQuantityMkt()" class="c-btn" required="" placeholder="Qty: 1000"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button class="c-btn">
                                <img src="/img/live-loader.gif" style="display: none;" class="loading-mkt" width="15" height="15"> Buy <span class="c-title">{{ $name }}</span>
                              </button>
                            </td>
                          </tr>
                        </table>
                      </form>
                    </div>

                    {{-- sell section at MARKET --}}
                    <div class="col-sm-6">
                      <h1 class="lead small"><i class="fa fa-money"></i> Sell 
                        <span class="c-title">{{ $name }}</span> 
                        <span class="last_seen_price pull-right c-title"></span>
                      </h1>
                      <hr />
                      <div class="success_msg_sell_mkt"></div>
                      <div class="error_msg_sell_mkt"></div>
                      <form id="sell-form-market" method="post" onsubmit="return placeOrderSellMkt()">
                        <table class="c-table small">
                          <tr>
                            <td>Amount</td>
                            <td><input type="text" id="s-amount-mkt" onblur="valueByAmountToSellMkt()" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSellMkt('25')">25%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSellMkt('50')">50%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSellMkt('75')">75%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSellMkt('100')">100%</a>
                            </td>
                          </tr>
                          <tr>
                            <td>Total <i class="fa fa-cubes"></i></td>
                            <td><input type="text" id="s-total-mkt" onblur="valueByQuantityToSellMkt()" class="c-btn" required="" placeholder="Qty: 1000"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button class="c-btn">
                                <img src="/img/live-loader.gif" style="display: none;" class="s-loading-mkt" width="15" height="15"> Sell <span class="c-title">{{ $name }}</span>
                              </button>
                            </td>
                          </tr>
                        </table>
                      </form>
                    </div>
                  </div>
                </div>

                {{-- STOP LIMIT --}}
                <div class="tab-pane" id="3a">
                  <p class="small"><span class="text-info">Tips:</span> A stop order, also referred to as a stop-loss order, is an order to buy or sell a stock once the price of the stock reaches a specified price, known as the stop price. When the stop price is reached, a stop order becomes a market order. A buy stop order is entered at a stop price above the current market price.</p>
                  <div class="row">
                    {{-- buy section STOP LIMIT --}}
                    <div class="col-sm-6">
                      <h1 class="lead small"><i class="fa fa-money"></i> Buy 
                        <span class="c-title">{{ $name }}</span> 
                        <span class="last_seen_price pull-right c-title"></span>
                      </h1>

                      <hr />
                      <form id="buy-form-stop" method="post" onsubmit="return placeOrderBuyStp()">
                        <div class="error_msg_buy_stp"></div>
                        <div class="success_msg_buy_stp"></div>
             
                        <table class="c-table small">
                          <tr>
                            <td>Stop Limit</td>
                            <td><input type="text" onblur="valueAssetStp()" id="b-price-stp" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td>Amount</td>
                            <td><input type="text" id="b-amount-stp" onblur="valueByAmountStp()" class="c-btn" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageStp('25')">25%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageStp('50')">50%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageStp('75')">75%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageStp('100')">100%</a>
                            </td>
                          </tr>
                          <tr>
                            <td>Total <i class="fa fa-cubes"></i></td>
                            <td><input type="text" id="b-total-stp" onblur="valueByQuantityStp()" class="c-btn" required="" placeholder="Qty: 1000"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button class="c-btn">
                                <img src="/img/live-loader.gif" class="b-loading-stp" width="15" height="15"> Buy <span class="c-title">{{ $name }}</span>
                              </button>
                            </td>
                          </tr>
                        </table>
                      </form>
                    </div>

                    {{-- sell section STOP LIMIT --}}
                    <div class="col-sm-6">
                      <h1 class="lead small"><i class="fa fa-money"></i> Sell 
                        <span class="c-title">{{ $name }}</span> 
                        <span class="last_seen_price pull-right c-title"></span>
                      </h1>
                      <hr />
                      <div class="sell_error_msg"></div>
                      <form id="sell-form-stop" method="post" onsubmit="return placeOrderSell()">
                        <div class="error_msg_sell_stp"></div>
                        <div class="success_msg_sell_stp"></div>

                        <table class="c-table small">
                          <tr>
                            <td>Stop Limit</td>
                            <td><input type="text" id="s-price" onblur="valueAssetToSell()" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td>Amount</td>
                            <td><input type="text" id="s-amount" onblur="valueByAmountToSell()" class="c-btn" required="" placeholder="&#8358; 00.00"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('25')">25%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('50')">50%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('75')">75%</a>
                              <a class="c-percent" href="javascript:void(0)" onclick="valueByPercentageToSell('100')">100%</a>
                            </td>
                          </tr>
                          <tr>
                            <td>Total <i class="fa fa-cubes"></i></td>
                            <td><input type="text" id="s-total" onblur="valueByQuantityToSell()" class="c-btn" required="" placeholder="Qty: 1000"></td>
                          </tr>
                          <tr>
                            <td></td>
                            <td>
                              <button class="c-btn">
                                <img src="/img/live-loader.gif" class="s-loading-stp" width="15" height="15"> Sell <span class="c-title">{{ $name }}</span>
                              </button>
                            </td>
                          </tr>
                        </table>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>

            <br /><br />
            <span class="c-title" data-step="10" data-intro="This section shows order history">Orders</span>
            <table class="table small">
              <thead>
                <tr>
                  <td>security</td>
                  <td>price (&#8358;)</td>
                  <td><i class="fa fa-cubes"></i> qty</td>
                  <td>trade</td>
                  <td>last updated</td>
                  <td>action</td>
                </tr>
              </thead>
              <tbody class="load-orders"></tbody>
            </table>
          </div>
        </div>
      </div>

      {{-- Recent trade section --}}
      <div class="col-md-2">
        <div class="card" style="height: 780px;">
          <div class="card-body">
            <h1 class="lead small text-center" data-step="11" data-intro="This section all recent trade within 24/7hrs">Recent Trade (24/7hour)</h1>
            <hr />
            <table class="table small">
              <thead>
                <tr>
                  <td><span class="c-title">Time</span></td>
                  <td><span class="c-title">Price</span> (&#8358;)</td>
                  <td><span class="c-title">Volume</span></td>
                </tr>
              </thead>
              <tbody class="load-trade-history"></tbody>
            </table>
          </div>
        </div>
      </div>

      {{-- news section --}}
      <div class="col-md-2">
        <div class="card" style="height: 780px;">
          <div class="card-body">
            <h1 class="lead small text-center" data-step="12" data-intro="This section shows news headlines">Top Headlines</h1>
            <hr />
            <div class="load-headlines small"></div>
            <div class="load-headlines-two small" style="display: none;"></div>
          </div>
        </div>
      </div>
    </div>
  </div>

  {{-- pusher js Trade updates --}}
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script src="/js/factory.js"></script>
  <script type="text/javascript">
    // get compact 
    var secId    = '{{ $id }}';
    var sectName = '{{ $name }}';

    // Pusher 
    // Pusher.logToConsole = true; 
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
      encrypted: false,
      cluster: "eu"
    });

    // update trade screen
    var new_price_updates = pusher.subscribe('feed-equities-prices');
    new_price_updates.bind('App\\Events\\EquityUpdateChange', function(data) {
      // console.log('new price discovered !!');
      refreshPrices();
    });

    // update trade screen
    var new_order_placed = pusher.subscribe('new-order');
    new_order_placed.bind('App\\Events\\PlaceOrder', function(val) {
      // console.log('new order');
      // console.log(val);
      let user_id = '{{ Auth::user()->id }}';
      if(val.user_id == user_id){
        $(".load-orders").prepend(`
          <tr>
            <td>`+val.asset+`</td>
            <td>&#8358;`+val.price+`</td>
            <td>`+val.qty+`</td>
            <td>`+val.type+`</td>
            <td>`+val.date+`</td>
            <td><a class="c-title" href="javascript:void()" onclick="cancelOrder('`+val.id+`')">Cancel</a></td>
          </tr>
        `);

        // update offer 
        if(val.type == 'sell'){
          $(".load-offer-spread").prepend(`
            <tr>
              <td>&#8358;`+val.price+`</td>
              <td>`+val.qty+`</td>
            </tr>
          `);
        }

        // update ask
        if(val.type == 'buy'){
          $(".load-ask-spread").prepend(`
            <tr>
              <td>&#8358;`+val.price+`</td>
              <td>`+val.qty+`</td>
            </tr>
          `);
        }

      }else{

        // update offer 
        if(val.type == 'sell'){
          $(".load-offer-spread").prepend(`
            <tr>
              <td>&#8358;`+val.price+`</td>
              <td>`+val.qty+`</td>
            </tr>
          `);
        }

        // update ask
        if(val.type == 'buy'){
          $(".load-ask-spread").prepend(`
            <tr>
              <td>&#8358;`+val.price+`</td>
              <td>`+val.qty+`</td>
            </tr>
          `);
        }
      }
    });

    // update trade screen
    var cleared_order = pusher.subscribe('cleared-order');
    cleared_order.bind('App\\Events\\ClearOrder', function(data) {

      // console.log('data');
      if(data.status = 'success'){
        // alert(data.message);
      }

      // refresh assets
      refreshPrices();
      refreshOrders();
      refreshAccount();
    });

    // account balance state
    $.get("/accountbalance", function (data){
      $("#ac").text(data.account_balance);
      $(".account_balance").text(data.account_balance);
    });

    /*
    |--------------------------------------------------------------------------
    | Using the market function STOP LIMIT
    |--------------------------------------------------------------------------
    |
    | Assest Revaluation when buying
    |
    */

    // calculate asset worth
    function valueAssetStp(){
      $(".loading-stp").show();
      // alert('calculating your assets');
      let price = parseFloat($('#b-price-stp').val());

      // data to json
      let data = {
        _token: '{{ csrf_token() }}',
        user_id: '{{ Auth::user()->id }}',
        price: price
      };

      $.post('/value-assets/order', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-amount-stp").val(data.amount);
        $("#b-total-stp").val(data.total);
        $(".loading-stp").hide();
      });
    }

    // value using percentage asset 
    function valueByPercentageStp(x){
      $(".loading-mkt").show();
      // data args
      let sec_id      = '{{ $id }}';
      let user_id     = '{{ Auth::user()->id }}';
      let percent     = x;
      let token       = '{{ csrf_token() }}';
      let price       = $("#b-price-mkt").val();

      if(price == ""){
        alert("select price");
        return false;
      }

      // data to json
      let data = {
        _token:token,
        percent:percent,
        price:price,
        user_id:user_id,
        sec_id:sec_id
      }

      // post data
      $.post('/value-assets/by-percentage', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-price-mkt").val(data.price);
        $("#b-amount-mkt").val(data.amount);
        $("#b-total-mkt").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    // value using amount value
    function valueByAmountStp(x) {
      $(".loading-mkt").show();
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let amount  = $("#b-amount-mkt").val();
      let price   = $("#b-price-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'market';

      // json to data
      let data = {
        _token:token,
        amount:amount,
        sec_id:sec_id,
        price:price,
        type:type,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-amount', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        $("#b-price-mkt").val(data.price);
        $("#b-amount-mkt").val(data.amount);
        $("#b-total-mkt").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    // value using quantity
    function valueByQuantityStp(x) {
      $(".loading-mkt").show();
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let unit  = $("#b-total-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type = 'market';

      // json to data
      let data = {
        _token:token,
        unit:unit,
        type:type,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-quantity', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-price-mkt").val(data.price);
        $("#b-amount-mkt").val(data.amount);
        // $("#b-total").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    /*
    |--------------------------------------------------------------------------
    | Valuation of assets when selling at STOP LIMIT
    |--------------------------------------------------------------------------
    |
    | Assest Revaluation when selling
    |
    */

    // value using percentage asset 
    function valueByPercentageToSellStp(x){
      // data args
      let sec_id      = '{{ $id }}';
      let user_id     = '{{ Auth::user()->id }}';
      let percent     = x;
      let token       = '{{ csrf_token() }}';
      let price       = $("#s-price-mkt").val();

      // data to json
      let data = {
        _token:token,
        percent:percent,
        price:price,
        user_id:user_id,
        sec_id:sec_id
      }

      // post data
      $.post('/value-assets/by-percentage/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-amount-mkt").val(data.amount);
        $("#s-total-mkt").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using amount value
    function valueByAmountToSellStp(x) {
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let amount  = $("#s-amount-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'market';

      // json to data
      let data = {
        _token:token,
        amount:amount,
        sec_id:sec_id,
        user_id:user_id,
        type:type
      }

      // value assets by percent
      $.post('/value-assets/by-amount/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-price-mkt").val(data.price);
        $("#s-amount-mkt").val(data.amount);
        $("#s-total-mkt").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using quantity
    function valueByQuantityToSellStp(x) {
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let unit    = $("#s-total-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'market';

      // json to data
      let data = {
        _token:token,
        unit:unit,
        type:type,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-quantity/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-price-mkt").val(data.price);
        $("#s-amount-mkt").val(data.amount);
        // $("#b-total").val(data.total);
        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }


    /*
    |--------------------------------------------------------------------------
    | Using the market function MARKET
    |--------------------------------------------------------------------------
    |
    | Assest Revaluation when buying
    |
    */

    // calculate asset worth
    function valueAssetMkt(){
      $(".loading-mkt").show();
      // alert('calculating your assets');
      let price = parseFloat($('#b-price-mkt').val());

      // data to json
      let data = {
        _token: '{{ csrf_token() }}',
        user_id: '{{ Auth::user()->id }}',
        price: price
      };

      $.post('/value-assets/order', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-amount-mkt").val(data.amount);
        $("#b-total-mkt").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    // value using percentage asset 
    function valueByPercentageMkt(x){
      $(".loading-mkt").show();
      // data args
      let sec_id      = '{{ $id }}';
      let user_id     = '{{ Auth::user()->id }}';
      let percent     = x;
      let token       = '{{ csrf_token() }}';
      let price       = 0;

      // data to json
      let data = {
        _token:token,
        percent:percent,
        price:price,
        user_id:user_id,
        sec_id:sec_id
      }

      // post data
      $.post('/value-assets/by-percentage', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        console.log(data);
        // reset value
        $("#b-price-mkt").val(data.price);
        $("#b-amount-mkt").val(data.amount);
        $("#b-total-mkt").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    // value using amount value
    function valueByAmountMkt(x) {
      $(".loading-mkt").show();
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let amount  = $("#b-amount-mkt").val();
      let price   = $("#b-price-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'market';

      // json to data
      let data = {
        _token:token,
        amount:amount,
        sec_id:sec_id,
        price:price,
        type:type,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-amount', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        $("#b-price-mkt").val(data.price);
        $("#b-amount-mkt").val(data.amount);
        $("#b-total-mkt").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    // value using quantity
    function valueByQuantityMkt(x) {
      $(".loading-mkt").show();
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let unit  = $("#b-total-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type = 'market';

      // json to data
      let data = {
        _token:token,
        unit:unit,
        type:type,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-quantity', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-price-mkt").val(data.price);
        $("#b-amount-mkt").val(data.amount);
        // $("#b-total").val(data.total);
        $(".loading-mkt").hide();
      });
    }

    /*
    |--------------------------------------------------------------------------
    | Valuation of assets when selling at MARKET
    |--------------------------------------------------------------------------
    |
    | Assest Revaluation when selling
    |
    */

    // value using percentage asset 
    function valueByPercentageToSellMkt(x){
      // data args
      let sec_id      = '{{ $id }}';
      let user_id     = '{{ Auth::user()->id }}';
      let percent     = x;
      let token       = '{{ csrf_token() }}';
      let price       = $("#s-price-mkt").val();

      // data to json
      let data = {
        _token:token,
        percent:percent,
        price:price,
        user_id:user_id,
        sec_id:sec_id
      }

      // post data
      $.post('/value-assets/by-percentage/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-amount-mkt").val(data.amount);
        $("#s-total-mkt").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using amount value
    function valueByAmountToSellMkt(x) {
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let amount  = $("#s-amount-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'market';

      // json to data
      let data = {
        _token:token,
        amount:amount,
        sec_id:sec_id,
        user_id:user_id,
        type:type
      }

      // value assets by percent
      $.post('/value-assets/by-amount/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-price-mkt").val(data.price);
        $("#s-amount-mkt").val(data.amount);
        $("#s-total-mkt").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using quantity
    function valueByQuantityToSellMkt(x) {
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let unit    = $("#s-total-mkt").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'market';

      // json to data
      let data = {
        _token:token,
        unit:unit,
        type:type,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-quantity/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-price-mkt").val(data.price);
        $("#s-amount-mkt").val(data.amount);
        // $("#b-total").val(data.total);
        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    /*
    |--------------------------------------------------------------------------
    | Valuation of assets when purchasing 
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */

    // calculate asset worth
    function valueAsset(){
      $(".loading").show();
      // alert('calculating your assets');
      let price = parseFloat($('#b-price').val());
      let type = 'limit';

      // data to json
      let data = {
        _token: '{{ csrf_token() }}',
        user_id: '{{ Auth::user()->id }}',
        type: type,
        price: price
      };

      $.post('/value-assets/order', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-amount").val(data.amount);
        $("#b-total").val(data.total);
        $(".loading").hide();
      });
    }

    // value using percentage asset 
    function valueByPercentage(x){
      $(".loading").show();
      // data args
      let sec_id      = '{{ $id }}';
      let user_id     = '{{ Auth::user()->id }}';
      let percent     = x;
      let token       = '{{ csrf_token() }}';
      let price       = $("#b-price").val();
      let type        = 'limit';

      if(price == ""){
        alert("select price");
        return false;
      }

      // data to json
      let data = {
        _token:token,
        percent:percent,
        price:price,
        user_id:user_id,
        type: type,
        sec_id:sec_id
      }

      // post data
      $.post('/value-assets/by-percentage', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-amount").val(data.amount);
        $("#b-total").val(data.total);
        $(".loading").hide();
      });
    }

    // value using amount value
    function valueByAmount(x) {
      $(".loading").show();
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let amount  = $("#b-amount").val();
      let price   = $("#b-price").val();
      let token   = '{{ csrf_token() }}';
      let type    = 'limit';

      // json to data
      let data = {
        _token:token,
        amount:amount,
        price:price,
        type:type,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-amount', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        // $("#b-price").val(data.price);
        $("#b-amount").val(data.amount);
        $("#b-total").val(data.total);
        $(".loading").hide();
      });
    }

    // value using quantity
    function valueByQuantity(x) {
      $(".loading").show();
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let unit    = $("#b-total").val();
      let price   = $("#b-price").val();
      let type    = 'limit';
      let token   = '{{ csrf_token() }}';

      // json to data
      let data = {
        _token:token,
        unit:unit,
        price:price,
        type:type,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-quantity', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#b-price").val(data.price);
        $("#b-amount").val(data.amount);
        // $("#b-total").val(data.total);
        $(".loading").hide();
      });
    }



    /*
    |--------------------------------------------------------------------------
    | Valuation of assets when selling 
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */

    // calculate asset worth
    function valueAssetToSell(){
      // alert('calculating your assets');
      let price = parseFloat($('#s-price').val());

      // data to json
      let data = {
        _token: '{{ csrf_token() }}',
        user_id: '{{ Auth::user()->id }}',
        sec_id: '{{ $id }}',
        price: price
      };

      // get sell results
      $.post('/value-assets/order/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-amount").val(data.amount);
        $("#s-total").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using percentage asset 
    function valueByPercentageToSell(x){
      // data args
      let sec_id      = '{{ $id }}';
      let user_id     = '{{ Auth::user()->id }}';
      let percent     = x;
      let token       = '{{ csrf_token() }}';
      let price       = $("#s-price").val();

      if(price == ""){
        alert("input your price");
        return false;
      }

      // data to json
      let data = {
        _token:token,
        percent:percent,
        price:price,
        user_id:user_id,
        sec_id:sec_id
      }

      // post data
      $.post('/value-assets/by-percentage/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-amount").val(data.amount);
        $("#s-total").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using amount value
    function valueByAmountToSell(x) {
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let amount  = $("#s-amount").val();
      let token   = '{{ csrf_token() }}';

      // json to data
      let data = {
        _token:token,
        amount:amount,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-amount/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-price").val(data.price);
        $("#s-amount").val(data.amount);
        $("#s-total").val(data.total);

        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }

    // value using quantity
    function valueByQuantityToSell(x) {
      // body...
      let sec_id  = '{{ $id }}';
      let user_id = '{{ Auth::user()->id }}';
      let unit  = $("#s-total").val();
      let token   = '{{ csrf_token() }}';

      // json to data
      let data = {
        _token:token,
        unit:unit,
        sec_id:sec_id,
        user_id:user_id
      }

      // value assets by percent
      $.post('/value-assets/by-quantity/sell', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        // reset value
        $("#s-price").val(data.price);
        $("#s-amount").val(data.amount);
        // $("#b-total").val(data.total);
        if(data.status == 'error'){
          $(".sell_error_msg").html(`
            <p class="text-danger">`+data.message+` </p>
          `);
        }
      });
    }


    /*
    |--------------------------------------------------------------------------
    | Spread Bid and Offer 
    |--------------------------------------------------------------------------
    |
    | Debit Cash account and Credit Stock account
    |
    */

    // Ask price
    $.get('/load/buy/request/{{ $id }}', function(data) {
      /*optional stuff to do after success */
      $(".load-offer-spread").html("");
      $.each(data, function(index, val) {
        $(".load-ask-spread").append(`
          <tr>
            <td>&#8358;`+val.price+`</td>
            <td>`+val.qty+`</td>
          </tr>
        `);
      });
    });

    // Offer Price
    $.get('/load/sell/request/{{ $id }}', function(data) {
      /*optional stuff to do after success */
      $.each(data, function(index, val) {
        $(".load-offer-spread").append(`
          <tr>
            <td>&#8358;`+val.price+`</td>
            <td>`+val.qty+`</td>
          </tr>
        `);
      });
    });



    /*
    |--------------------------------------------------------------------------
    | Load orders 
    |--------------------------------------------------------------------------
    |
    | Load orders for Authenticated user
    |
    */

    // load orders
    $.get('/load/order/'+{{$id}}+'/'+{{Auth::user()->id}}, function(data) {
      /*optional stuff to do after success */
      $(".load-orders").html();
      let sn = 0;
      $.each(data.orders, function(index, val) {
        sn++;
        /* iterate through array or object */
        if(val.user_id == '{{ Auth::user()->id }}'){
          $(".load-orders").append(`
            <tr>
              <td>`+val.asset+`</td>
              <td>&#8358;`+val.price+`</td>
              <td>`+val.qty+`</td>
              <td>`+val.type+`</td>
              <td>`+val.date+`</td>
              <td><a class="c-title" href="javascript:void()" onclick="cancelOrder('`+val.id+`')">Cancel</a></td>
            </tr>
          `);
        }

        // limit record to 4
        if(sn > 3){
          return false;
        }
      });

      $(".load-orders").append(`
        <div class="col-sm-12 text-center">
           <a class="btn btn-link" href="/transactions"><i class="fa fa-arrow-down"></i> see more</a>
        </div>
      `);
    });

    // load orders all
    $.get('/load/order/all/', {sec_id:secId}, function(data) {
      /*optional stuff to do after success */
      console.log(data);
      let sn = 0;
      $(".load-spread-buy").html("");
      $.each(data, function(index, val) {
        sn++;
         /* iterate through array or object */
        $(".load-spread-buy").append(`
          <tr>
            <td>&#8358;`+val.price+`</td>
            <td>`+val.qty+`</td>
          </tr>
        `);
      });
    });

    // get current prices
    $.get('/load/assets/{{ $name }}', function(data) {
      /*optional stuff to do after success */
      // $('.load-recent-trade').html("");
      $('.load-recent-trade').html(`
        <tr>
          <td>`+data.date+`</td>
          <td>&#8358;`+data.new+`</td>
          <td>`+data.vol+`</td>
        </tr>
      `);

      $(".last_seen_price").html(`
        &#8358;`+data.new+`
      `);
    });

    // load asset trans history
    $.get('/load/assets/history/{{ $name }}', function(data) {
      /*optional stuff to do after success */
      $(".load-trade-history").html("");
      $.each(data, function(index, val) {
        // console.log(val);
        // $('.load-recent-trade').html("");
        $('.load-trade-history').append(`
          <tr>
            <td>`+val.date+`</td>
            <td>&#8358;`+val.price+`</td>
            <td>`+val.qty+`</td>
          </tr>
        `);
      });
    });

    // cancel order
    function cancelOrder(id) {
      // data to json
      let data = {
        _token: '{{ csrf_token() }}',
        id:id
      }

      // post cancel orders
      $.post('/cancel/order', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        if(data.status == 'success'){
          return refreshOrders();
        }
      });
    }


    /*
    |--------------------------------------------------------------------------
    | Place order at Limit
    |--------------------------------------------------------------------------
    |
    | Place order limit option buy/sell trade
    |
    */

    // place order buy
    function placeOrderBuy(argument) {
      $(".loading").show();
      // body...
      var token   = '{{ csrf_token() }}';
      var sec_id  = '{{ $id }}';
      var user_id = '{{ Auth::user()->id }}';
      var price   = parseFloat($("#b-price").val());
      var total   = $("#b-total").val();
      var option  = 'limit';
      var type    = 'buy';

      // data to json
      var data = {
        _token:token,
        price:price,
        total:total,
        type:type,
        option:option
      }

      $.ajax({
        url: '/job/order-buy/'+sec_id+'/'+user_id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          // alert('data sent !');
          if(data.status == 'success'){
            // alert(data.message);
            $(".success_msg").html(`
              <p class="text-success">
                `+data.message+`
              </p>
            `);
          }

          if(data.status == 'error'){
            // alert(data.message);
            $(".error_msg").html(`
              <p class="text-danger">
                `+data.message+`
              </p>
            `);
          }
          $("#buy-form")[0].reset();
          // console success response
          // console.log(data);
          $(".loading").hide();
        },
        error: function (data){
          // console error response
          // console.log(data);
          $(".loading").hide();
          alert('Error, Fail to sent order request..');
        }
      });
      return false;
    }

    // place order sell
    function placeOrderSell(argument) {
      $(".s-loading").show();
      // body...
      var token   = '{{ csrf_token() }}';
      var sec_id  = '{{ $id }}';
      var user_id = '{{ Auth::user()->id }}';
      var price   = parseFloat($("#s-price").val());
      var total   = $("#s-total").val();
      var option  = 'limit';
      var type    = 'sell';

      // data to json
      var data = {
        _token:token,
        price:price,
        total:total,
        type:type,
        option:option
      }

      // console.log(data);
      $.ajax({
        url: '/job/order-sell/'+sec_id+'/'+user_id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          console.log(data);
          // alert('data sent !');
          if(data.status == 'success'){
            // alert(data.message);
            $(".sell_success_msg").html(`
              <p class="text-success">
                `+data.message+`
              </p>
            `);
          }

          if(data.status == 'error'){
            alert(data.message);
            $(".error_msg").html(`
              <p class="text-danger">
                `+data.message+`
              </p>
            `);
          }
          $("#sell-form")[0].reset();
          // console success response
          // console.log(data);
          $(".s-loading").hide();
        },
        error: function (data){
          // console error response
          // console.log(data);
          $(".s-loading").hide();
          alert('Error, Fail to sent order request..');
        }
      });
    
      return false;
    }


    /*
    |--------------------------------------------------------------------------
    | Place order at Market
    |--------------------------------------------------------------------------
    |
    | Place a market request on buy/sell trade
    |
    */

    // place order buy
    function placeOrderBuyMkt(argument) {
      $(".loading-mkt").show();
      // body...
      var token   = '{{ csrf_token() }}';
      var sec_id  = '{{ $id }}';
      var user_id = '{{ Auth::user()->id }}';
      var price   = parseFloat($("#b-price-mkt").val());
      var total   = $("#b-total-mkt").val();
      var option  = 'limit';
      var type    = 'buy';

      // data to json
      var data = {
        _token:token,
        price:price,
        total:total,
        type:type,
        option:option
      }

      $.ajax({
        url: '/trade/buy/'+sec_id+'/'+user_id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          // alert('data sent !');
          if(data.status == 'success'){
            // alert(data.message);
            $(".success_msg_buy_mkt").html(`
              <p class="text-success">
                `+data.message+`
              </p>
            `);
          }

          if(data.status == 'error'){
            // alert(data.message);
            $(".error_msg_buy_mkt").html(`
              <p class="text-danger">
                `+data.message+`
              </p>
            `);
          }
          $("#buy-form-market")[0].reset();
          // console success response
          // console.log(data);
          $(".loading-mkt").hide();
        },
        error: function (data){
          // console error response
          // console.log(data);
          $(".loading-mkt").hide();
          alert('Error, Fail to send order request..');
        }
      });
    
      return false;
    }

    // place order sell
    function placeOrderSellMkt(argument) {
      $(".s-loading-mkt").show();
      // body...
      var token   = '{{ csrf_token() }}';
      var sec_id  = '{{ $id }}';
      var user_id = '{{ Auth::user()->id }}';
      var total   = $("#s-total-mkt").val();
      var option  = 'market';
      var type    = 'sell';

      // data to json
      var data = {
        _token:token,
        total:total,
        type:type,
        option:option
      }
      // console.log(data);
      // alert('selling at market');
      $.ajax({
        url: '/trade/sell/'+sec_id+'/'+user_id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          // alert('data sent !');
          if(data.status == 'success'){
            // alert(data.message);
            $(".success_msg_sell_mkt").html(`
              <p class="text-success">
                `+data.message+`
              </p>
            `);
          }

          if(data.status == 'error'){
            // alert(data.message);
            $(".error_msg_sell_mkt").html(`
              <p class="text-danger">
                `+data.message+`
              </p>
            `);
          }
          $("#sell-form-market")[0].reset();
          // console success response
          // console.log(data);
          $(".s-loading-mkt").hide();
        },
        error: function (data){
          // console error response
          // console.log(data);
          $(".s-loading-mkt").hide();
          alert('Error, Fail to sent order request..');
        }
      });
    
      return false;
    }


    /*
    |--------------------------------------------------------------------------
    | Place order at Stop Limit
    |--------------------------------------------------------------------------
    |
    | Place a stop limit on buy/sell trade
    |
    */

    // place order buy
    function placeOrderBuyStp(argument) {
      $(".loading-stp").show();
      // body...
      var token   = '{{ csrf_token() }}';
      var sec_id  = '{{ $id }}';
      var user_id = '{{ Auth::user()->id }}';
      var price   = parseFloat($("#b-price-stp").val());
      var total   = $("#b-total-stp").val();
      var option  = 'stop';
      var type    = 'buy';

      // data to json
      var data = {
        _token:token,
        price:price,
        total:total,
        type:type,
        option:option
      }

      $.ajax({
        url: '/job/order-buy/'+sec_id+'/'+user_id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          // alert('data sent !');
          if(data.status == 'success'){
            // alert(data.message);
            $(".success_msg_buy_stp").html(`
              <p class="text-success">
                `+data.message+`
              </p>
            `);
          }

          if(data.status == 'error'){
            // alert(data.message);
            $(".error_msg_buy_stp").html(`
              <p class="text-danger">
                `+data.message+`
              </p>
            `);
          }
          $("#buy-form-stop")[0].reset();
          // console success response
          // console.log(data);
          $(".loading-stp").hide();
        },
        error: function (data){
          // console error response
          // console.log(data);
          $(".loading-stp").hide();
          alert('Error, Fail to send order request..');
        }
      });
    
      return false;
    }

    // place order sell
    function placeOrderSellStp(argument) {
      $(".s-loading-stp").show();
      // body...
      var token   = '{{ csrf_token() }}';
      var sec_id  = '{{ $id }}';
      var user_id = '{{ Auth::user()->id }}';
      var total   = $("#s-total-stp").val();
      var option  = 'stop';
      var type    = 'sell';

      // data to json
      var data = {
        _token:token,
        total:total,
        type:type,
        option:option
      }
      // console.log(data);
      // alert('selling at market');
      $.ajax({
        url: '/job/order-sell/'+sec_id+'/'+user_id,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (data){
          // console.log(data);
          // alert('data sent !');
          if(data.status == 'success'){
            // alert(data.message);
            $(".success_msg_sell_stp").html(`
              <p class="text-success">
                `+data.message+`
              </p>
            `);
          }

          if(data.status == 'error'){
            // alert(data.message);
            $(".error_msg_sell_stp").html(`
              <p class="text-danger">
                `+data.message+`
              </p>
            `);
          }
          $("#sell-form-stop")[0].reset();
          // console success response
          // console.log(data);
          $(".s-loading-stp").hide();
        },
        error: function (data){
          // console error response
          // console.log(data);
          $(".s-loading-stp").hide();
          alert('Error, Fail to sent order request..');
        }
      });
    
      return false;
    }


    // refresh prices
    function refreshPrices() {
      // body...
      // get current prices
      $.get('/load/assets/{{ $name }}', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $('.load-recent-trade').html(`
          <tr>
            <td>`+data.date+`</td>
            <td>&#8358;`+data.new+`</td>
            <td>`+data.vol+`</td>
          </tr>
        `);

        $(".last_seen_price").html(`
          &#8358;`+data.new+`
        `);
      });
    }

    // refresh order
    function refreshOrders() {
      // load orders

      $(".load-orders").html("");
      $.get('/load/order/'+{{$id}}+'/'+{{Auth::user()->id}}, function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $(".load-orders").html();
        let sn = 0;
        $.each(data.orders, function(index, val) {
          sn++;
           /* iterate through array or object */
          $(".load-orders").append(`
            <tr>
              <td>`+val.asset+`</td>
              <td>&#8358;`+val.price+`</td>
              <td>`+val.qty+`</td>
              <td>`+val.type+`</td>
              <td>`+val.date+`</td>
              <td><a class="c-title" href="javascript:void()" onclick="cancelOrder('`+val.id+`')">Cancel</a></td>
            </tr>
          `);
        });
      });
    }

    // refresh account balance 
    function refreshAccount(argument) {
      // body...
      // account balance state
      $(".account_balance").text("");
      $.get("/accountbalance", function (data){
        // $("#ac").text(data.account_balance);
        $(".account_balance").text(data.account_balance);
      });
    }
  </script>

  {{-- API News --}}
  <script type="text/javascript">
    $.get('/load/api-news', function(data) {
      // console.log(data);
      $(".load-headlines").html("");
      let sn = 0;
      $.each(data.articles, function(index, val) {
        // console.log(val);
        sn++;
        $(".load-headlines").append(`
          <table cellpadding="2">
            <tr>
              <td><img src="`+val.urlToImage+`" width="50" height="50"><td>
              <td><h4 style="font-size:9px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
            </tr>
          </table> 
          <p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
        `);
        if(sn == 4){
          return false;
        }
      });
    });

    // dino get data
    $.get('/load/api-news/bloomberg', function(data) {
      // console.log(data);
      let sn = 0;
      $.each(data.articles, function(index, val) {
        // console.log(val);
        sn++;
        $(".load-headlines-two").append(`
          <table cellpadding="2">
            <tr>
              <td><img src="`+val.urlToImage+`" width="50" height="50"><td>
              <td><h4 style="font-size:9px;margin-left:5px;" class="news-title">`+val.title+`</h4></td>
            </tr>
          </table> 
          <p>`+val.description+` <a class="c-title pull-right" target="_blank" href="`+val.url+`">read more</a></p><br />
        `);
        if(sn == 4){
          return false;
        }
      });
    });

    // change news card
    window.setInterval(function (){
      $(".load-headlines").toggle({ direction: "left" }, 1500);
      $(".load-headlines-two").toggle({ direction: "right" }, 1500);
    }, 1000 * 15);
  </script>
@endsection
