@extends('layouts.trade-room-skin')

@section('title')
    CATSS Live Feeds
@endsection

@section('contents')
  <style type="text/css">
    .eq-calculator {
      height: auto;
      display: none;
    }
  </style>
  {{-- news updates sidebar --}}
  <div class="header navbar-fixed-top fixed-sidebar" style="box-shadow: 1px 1px 4px 1px #000;">
    {{-- calculator div --}}
    <div class="eq-calculator"></div>
    <h3 class="dino-link"><i class="fas fa-volume-on"></i> News Updates</h3>
    <hr />
    <div class="load-news" id="load-flash-news"></div>
    {{-- rules --}}
    <div class="rule-list" id="flash-rules" style="display:none;padding: 0.6em;">
      <ul>
        <li class="rule-li"><img src="/img/rules.gif"> Trading Rules</li>
        <li class="rule-li">1.  Each N1,000.00 subscription credits account with N10million Valid for four weeks.</li>
        <li class="rule-li">2.  Additional N1,000.00 subscribers extends validity by another 4 weeks.</li>
        <li class="rule-li">3.  Account is reset to N10million every Monday for a new trading week.</li>
        <li class="rule-li">4.  Valid subscription are eligible to win weekly prizes.</li>
        <li class="rule-li">5.  Number of winners are determine by the number of subscribers up to a minimum of (3) three.</li>
        <li class="rule-li">6.  All top winner get weekly prizes.</li>
        {{-- <li class="rule-li">7.  Cash prizes credited to winner’s Bank Account on Saturday.</li> --}}
        <li class="rule-li">7.  Every purchase and sale prices includes commission charge.</li>
        <li class="rule-li">8.  Equities Market prices are update daily.</li>
        <li class="rule-li">9.  Stock balances are revalued each time there is a change in market price.</li>
        <li class="rule-li">10. Price changes within the CATSS Market are randomnized.</li>
      </ul>
    </div>
  </div>

  {{-- load transaction updates sidebar --}}
  <div class="header navbar-fixed-top fixed-sidebar-2" style="box-shadow: 1px 1px 4px 1px #000;">
    <div id="trade_status"></div>
    <div class="load-transactions dino-link" data-step="1" data-intro="Tips: display last transaction" data-position='right'></div>
    <h3>
      <i class="fa fa-database"></i> <span class="total_qty"></span> <span data-step="2" data-intro="Tips: display stock balance" data-position='right'>Stocks</span>
    </h3>
    <hr />
    <div class="load-stocks dino-link">
      <div class="load-stock-balance"></div>
    </div>
  </div>

  {{-- load transaction push notification updates --}}
  <div class="header navbar-fixed-top fixed-sidebar-3" style="box-shadow: 1px 1px 4px 1px #000;">
    <h3 class="dino-link"><i class="fas fa-volume-on"></i> Recent Transactions</h3>
    <hr />
    <table class="dino-table">
      <thead>
        <tr>
          <td>Equity</td>
          <td>Price</td>
          <td>Volume</td>
          <td>Time</td>
        </tr>
      </thead>
      <tbody class="load-updated-trade"></tbody>
    </table>

    <div class="dino-table-cover">
      <h3 class="dino-link"><i class="fas fa-volume-on"></i> Recent Orders</h3>
      <hr />
      <table class="dino-table">
        <thead>
          <tr>
            <td>Equity</td>
            <td>Price</td>
            <td>Volume</td>
            <td>Date</td>
          </tr>
        </thead>
        <tbody class="load-orders"></tbody>
      </table>
    </div>
  </div>

  <div class="row small" style="margin-left:15.2%;margin-right:30%;font-size:12px;box-shadow: 1px 1px 4px 1px #000;">
    <div class="col-md-12" style="background-color: rgba(000,000,000,0.90);">
      <h3>
        <i class="fa fa-bar-chart"></i>
        Today's Equity Market <button class="rule btn btn-danger">Read Me (News/Rules)</button>
        <span class="pull-right">
          <span class="text-danger">Note:</span>  1.35% Commission fee is charged on transactions
        </span>
      </h3>

      {{-- trade table listing equities --}}
      <table class="table dino-link" id="equities_table">
        <thead>
          <th><span data-step="3" data-intro="Tips: securities list table" data-position='right'>Security</span></th>
          <th><i class="fa fa-align-center"></i> Place Order</th>
          <th><span data-step="4" data-intro="input quantity for trade" data-position='right'>Request Qty</span></th>
          <th>
            <span data-step="5" data-intro="use the iBuy button to purchase assets" data-position='right'>
              <i class="fa fa-money"></i> Bid
            </span>
          </th>
          <th>
            <span data-step="6" data-intro="use the iSell button to sell assets" data-position='left'>
              <i class="fa fa-money"></i> Offer 
            </span>
          </th>
        </thead>
        <tbody class="equities_data"></tbody>
      </table>
    </div>
  </div>

  <!-- Modal structure -->
  <div id="order-modal" class="modal"  tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color: rgba(000,000,000,0.70);">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="dino-link">Enter <span class="pair-order-name"></span> Order</span>
        </div>
        <div class="modal-body">
          <form method="post" onsubmit="return false">
            <input type="hidden" id="eqt_sec_id" name="">
            <div class="row">
              <div class="col-md-12">
                <label for="all-client-lists">Select client</label>
                <select class="form-control all-client-lists" id="order-client-lists" style="width: 100%;">
                  <option value=""> select client</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <h5>Bid</h5>
                <div class="form-group">
                  <input type="number" placeholder="Enter volume" id="eqt_bid_volume" class="form-control">
                </div>
                <div class="form-group">
                  <input type="number" step="any" min="0" placeholder="Enter price" id="eqt_bid_price" class="form-control">
                </div>
              </div>
              <div class="col-md-6">
                <h5>Ask</h5>
                <div class="form-group">
                  <input type="number" placeholder="Enter volume" id="eqt_ask_volume" class="form-control">
                </div>
                <div class="form-group">
                  <input type="number" step="any" min="0" placeholder="Enter price" id="eqt_ask_price" class="form-control">
                </div>
              </div>
              <div class="col-md-12">
                <div class="form-group">
                  <button class="dino-buy-sell-order-btn" onclick="placeBidAskOrder()">
                    <i class="fa fa-align-center"></i> Place Bid/Ask
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <!-- Modal structure -->
  <div id="oneclick-modal" class="modal"  tabindex="-1" role="dialog">
    <div class="modal-dialog">
      <div class="modal-content" style="background-color: rgba(000,000,000,0.70);">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <span class="dino-link"><span class="pair-oneclick-name"></span></span>
        </div>
        <div class="modal-body">
          <form method="post" onsubmit="return false">
            <input type="hidden" id="eqt_sec_id" name="">
            <input type="hidden" id="eq_trade_sn" name="">
            <input type="hidden" id="eq_trade_type" name="">
            <div class="row">
              <div class="col-md-12">
                <label for="all-client-lists">Select client</label>
                <select class="form-control all-client-lists" id="oneclick-client-lists" style="width: 100%;">
                  <option value="">--none--</option>
                </select>
              </div>
            </div>
            <br />
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <button class="dino-buy-sell-order-btn" onclick="executeTrade()">
                    <i class="fa fa-align-center"></i> Trade
                  </button>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

@endsection


@section('scripts')
  <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
  <script type="text/javascript">
    loadAllOrders();
    $('.rule').click(function (e){
      e.preventDefault();

      $('.rule-list').toggle();
      $('.load-news').toggle();
      $('.eq-calculator').toggle();
      $('#addClass').toggle();
    });

    var user_id = '{{ Auth::user()->id }}';
    var logged_email = '{{ Auth::user()->email }}';

    // Pusher 
    // Pusher.logToConsole = true;
    var pusher = new Pusher('{{env("PUSHER_APP_KEY")}}', {
      encrypted: true,
      cluster: "eu"
    });

    var transaction = pusher.subscribe('transaction-updates');
    transaction.bind('CATSS\\Events\\TransactionNotifications', function(data) {
      var value = data;
      var tradeType = value.stock_trade;
      if(user_id == value.user_id){
        if(tradeType == 'buy'){
          var word = 'Purchased';
        }
        if(tradeType == 'sell'){
          var word = 'Sold';
        }

        $(".load-transactions").html(
          word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
          '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
        );
      } 
    });

    var stocks = pusher.subscribe('stock-updates');
    stocks.bind('CATSS\\Events\\StockBalanceNotifications', function(data) {
      if(data.stocks[0].user_id == user_id){
        $(".load-stock-balance").html("");
        $.each(data.stocks, function (index, value){
          $(".load-stock-balance").append(`
              <tr>
                <td style="padding:0.5em;">`+value.name+`</td>
                <td style="padding:0.5em;">&#8358; `+value.price+`</td>
                <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
              </tr>
          `);
          $(".total_qty").html(value.total_qty); 
        });
      }
    });

    var account = pusher.subscribe('update-account');
    account.bind('CATSS\\Events\\AccountUpdate', function(data) {
      if(data.user_id == user_id){
        $("#ac").text(data.bal);
        $(".account_balance").text(data.bal);
      }
    });

    var trade_screen = pusher.subscribe('feed-equities-prices');
    trade_screen.bind('CATSS\\Events\\EquityUpdateChange', function(data) {
      refreshTradeData();
    });

    var trade_update = pusher.subscribe('trade-update-data');
    trade_update.bind('CATSS\\Events\\TradeUpdate', function(data) {
      refreshTradeData();
    });

    // update trade screen
    var new_order_update = pusher.subscribe('new-order-update');
    new_order_update.bind('CATSS\\Events\\NewOrderUpdate', function(data) {
      // refresh trade data
      refreshTradeData();
      var rowShade;
      if(data.type == "buy"){
        rowShade = ` class="order-row-buy" `;
      }else{
        rowShade = ` class="order-row-sell" `;
      }
      $(".load-orders").prepend(`
        <tr ${rowShade}>
          <td>${data.asset}</td>
          <td>${data.price}</td>
          <td>${data.qty}</td>
          <td>${data.date}</td>
        </tr>
      `);

      // sound notification
      var audio = document.getElementById("order-audio");
      audio.pause();
      audio.play();
    });

    $(".pairs_table").html(
      '<h3 class="ml2">Equities is loading......</h3>'
    );

    // check for updates
    function checkUpdates(){
      // show stocks balance 
      $.get('/stocks/notifications', function (data){
        $(".load-stock-balance").html("");
        $.each(data, function (index, value){
          $(".load-stock-balance").append(`
            <tr>
              <td style="padding:0.5em;">`+value.name+`</td>
              <td style="padding:0.5em;">&#8358; `+value.price+`</td>
              <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
            </tr>
          `);
          $(".total_qty").html(value.total_qty);
        });  
      });

      // account balance state
      $.get("/accountbalance", function (data){
        $("#ac").text(data.account_balance);
        $(".account_balance").text(data.account_balance);
      });
      
      // show transactions notifications
      $.get('/transactions/notifications', function (data){
        $.each(data, function (index, value){
          var tradeType = value.stock_trade;
          if(tradeType == 'buy'){
            var word = 'Purchased';
          }
          if(tradeType == 'sell'){
            var word = 'Sold';
          }

          $(".load-transactions").html(
            word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
            '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
          );
        });
      });
    };
    
    // show stocks balance 
    $.get('/stocks/notifications', function (data){
      $(".load-stock-balance").html("");
      $.each(data, function (index, value){
        $(".load-stock-balance").append(`
          <tr>
            <td style="padding:0.5em;">`+value.name+`</td>
            <td style="padding:0.5em;">&#8358; `+value.price+`</td>
            <td style="padding:0.5em;"><i class="fa fa-database"></i> `+value.qty+`</td>
          </tr>
        `);
        $(".total_qty").html(value.total_qty);
      });  
    });

    // account balance state
    $.get("/accountbalance", function (data){
      $("#ac").text(data.account_balance);
      $(".account_balance").text(data.account_balance);
    });

    // show news notifications
    $.get('/news/notifications', function (data){
      $.each(data, function (index, value){
        $(".load-news").append(`
          <h3>`+value.news_title+`</h3>
          <p>`+value.news_body+`</p>
          <span class="small">`+value.news_date+`</span>
        `);
      });
    });

    // show transactions notifications
    $.get('/transactions/notifications', function (data){
      $.each(data, function (index, value){
        var tradeType = value.stock_trade;
        if(tradeType == 'buy'){
          var word = 'Purchased';
        }
        if(tradeType == 'sell'){
          var word = 'Sold';
        }

        $(".load-transactions").html(
          word+'<br /> <b>'+value.stock_qty+'</b> of '+value.stock_name+' for <b>&#8358; '+value.stock_amount+'</b> at &#8358;'+value.stock_unit+
          '<span class="pull-right small">'+value.stock_date+'</span> <br /><br />'
        );
      });
    });

    // load equities market
    refreshTradeData();

    // get load trading functions
    function refreshTradeData(){
      $.get('/catss/trade/random', function (data){
        $("#catss-loading").hide();
        $(".equities_data").html("");
        var sn = 0;
        $.each(data, function (index, value){
          sn++;
          var pid    = value.id;
          var pn     = value.security; // equity name
          var sp     = value.close_price; // start price
          var cp     = value.open_price; // close price
          var pp     = value.previous_close; // close price
          var t_stat = value.status;
          var gap    = value.change_price;
          var vol    = value.daily_volume;
          var date   = value.date;

          var stats;
          if(value.previous_close > value.close_price){
            stats = `<td><i class="fa fa-arrow-down text-danger"></i> &#8358;`+value.close_price+`</td>`;
          }

          if(value.previous_close < value.close_price){
            stats = `<td><i class="fa fa-arrow-up text-success"></i> &#8358;`+value.close_price+`</td>`;
          }

          if(value.previous_close == value.close_price){
            stats = `<td><i class="fa fa-stop ticker-middle"></i> &#8358;`+value.close_price+`</td>`;
          }

          $(".equities_data").append(`
            <tr>
              <td valign="middle">
              <a href="/equities/`+pn+`/`+pid+`" class="dino-link">
                <button class="dino-pair-name">
                  <i class="fa fa-database"></i> `+pn+`
                </button>
              </a>
              </td>
              <td>
                <button class="dino-buy-sell-live" onclick="showOrderModal('${pn}', ${pid})">
                  <i class="fa fa-align-center"></i> Bid/Ask
                </button>
              </td>
              <td>
                <input type="number" id="trade_qty_`+sn+`" onkeyup="simulateEquities('`+pid+`', '`+pn+`', '`+value.buy_price+`', '`+value.sell_price+`')" class="dino-input-live" placeholder="1000" required="">
              </td>

              <td>
                <button class="dino-buy-live" onclick="showOneclickModal(${sn}, 'sell', '${pn}', ${pid})">
                  <i class="fa fa-chart-line"></i> Sell &#8358;`+value.buy_price+` <br />
                  <i class="fa fa-database"></i> Sell &#8358;`+value.buy_qty+`
                </button>
              </td>

              <td>
                <button class="dino-sell-live" onclick="showOneclickModal(${sn}, 'buy', '${pn}', ${pid})">
                 <i class="fa fa-chart-area"></i> Buy &#8358;`+value.sell_price+` <br />
                 <i class="fa fa-database"></i> Buy &#8358;`+value.sell_qty+`
                </button>
              </td>

              <input type="hidden" id="pair_id_${sn}" value="`+pid+`" />
              <input type="hidden" id="pair_name_${sn}" value="`+pn+`" />

              <input type="hidden" id="buy_id_${sn}" value="`+value.buy_id+`" />
              <input type="hidden" id="buy_user_${sn}" value="`+value.sell_user+`" />
              <input type="hidden" id="buy_price_${sn}" value="`+value.buy_price+`" />
              
              <input type="hidden" id="sell_id_${sn}" value="`+value.sell_id+`" />
              <input type="hidden" id="sell_user_${sn}" value="`+value.sell_user+`" />
              <input type="hidden" id="sell_price_${sn}" value="`+value.sell_price+`" />
            </tr>
          `);
        });

        $("#equities_table").dataTable();
      });
    }

    // execute trade 
    function executeTrade() {
      var trade_sn   = $("#eq_trade_sn").val();
      var trade_type = $("#eq_trade_type").val();
      console.log(trade_sn);
      console.log(trade_type);
      var token     = $("#token").val();
      var trader_id = $(".all-client-lists").val();
      var pair_id   = $("#pair_id_"+trade_sn).val();
      var pair_name = $("#pair_name_"+trade_sn).val();
      var buy_id    = $("#buy_id_"+trade_sn).val();
      var buy_user  = $("#buy_user_"+trade_sn).val();
      var buy_price = $("#buy_price_"+trade_sn).val();
      var sell_id   = $("#sell_id_"+trade_sn).val();
      var sell_user = $("#sell_user_"+trade_sn).val();
      var sell_price = $("#sell_price_"+trade_sn).val();
      var qty       = $("#trade_qty_"+trade_sn).val();

      var query = {
          _token:token,
          pid: pair_id,
          pn: pair_name,
          buy_id: buy_id,
          buy_user: buy_user,
          buy_price: buy_price,
          sell_id: sell_id,
          sell_user: sell_user,
          sell_price: sell_price,
          qty: qty,
          trade_type: trade_type,
          trader_id: trader_id
        }
        console.log(query)

      // post trade
      $.ajax({
        type: "POST",
        url: "/request-trade",
        data: query,
        cache: false,
        success: function(data){
          if(data.status == 'success'){
            iziToast.show({
              title: "Ok",
              message: data.message,
              position: 'bottomLeft'
            });
            checkUpdates();
          }else{
            iziToast.show({
              title: "oops",
              message: data.message,
              position: 'bottomLeft'
            });
          }
        }
      });
    }

    // This calculate the current Equities
    function simulateEquities(sec_id, sec_name, buy_price, sell_price) {
      // get item quantity
      var qty = $('#trade_qty_'+sec_id).val();

      // get calculated details
      var sell_consideration    = qty * sell_price;
      var buy_consideration    = qty * buy_price;
      const nse_fee         = (0.3/100) * buy_consideration;
      const stamp_fee       = (0.075/100) * buy_consideration;
      const commission_fee  = (1.35/100) * buy_consideration;

      // cscs fee only when selling equities
      const cscs_fee = (0.3/100) * buy_consideration;

      // all fee charged + cscs alert
      var estimated_bid_total = nse_fee + stamp_fee + commission_fee + 4.00 + buy_consideration;
      var estimated_ask_total = sell_consideration - (nse_fee + cscs_fee + stamp_fee + commission_fee + 4.00);

      // body...
      $(".eq-calculator").show();
      $(".eq-calculator").html(`
        <span class="dino-link">`+sec_name+`</span> <p class="pull-right">Mkt (&#8358;`+sell_price+`)</p>
        <table class="table">
          <tr>
            <td>Consideration</td>
            <td>
              <span class="dino-link text-success">Bid:`+buy_consideration.toLocaleString()+`</span> <br />
              <span class="dino-link text-danger">Ask:`+sell_consideration.toLocaleString()+`</span>
            </td>
          </tr>
          <tr>
            <td>Commission(1.35)</td>
            <td><span class="dino-link">`+commission_fee.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>CSCS Alert(&#8358;4.00)</td>
            <td>4.00</td>
          </tr>
          <tr>
            <td>CSCS Fee(0.3%)</td>
            <td><span class="dino-link">`+cscs_fee.toLocaleString()+` (- sell)</span></td>
          </tr>
          <tr>
            <td>NSE/SEC Fee(0.3%)</td>
            <td><span class="dino-link">`+nse_fee.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>Stamp Duties(0.075)</td>
            <td><span class="dino-link">`+stamp_fee.toLocaleString()+`</span></td>
          </tr>
          <tr>
            <td>
              Buy <br />
              <span class="text-info h5">&#8358;`+estimated_bid_total.toLocaleString()+`</span>
            </td>
            <td>
              <span class="dino-link">Sell </span><br />
              <span class="text-success h5">&#8358;`+estimated_ask_total.toLocaleString()+`</span>
            </td>
          </tr>
        </table>
      `);
    }

    // show order modal
    function showOrderModal(pair_name, pair_id) {
      $(".pair-order-name").html(pair_name);
      $("#eqt_sec_id").val(pair_id);
      $("#order-modal").modal('show');
    }

    // show order modal
    function showOneclickModal(trade_sn, trade_type, pair_name, pair_id) {
      $("#eq_trade_sn").val(trade_sn);
      $("#eq_trade_type").val(trade_type);
      if(trade_type == 'buy'){
        $(".pair-oneclick-name").html('Buy ' + pair_name);
      }else if(trade_type == 'sell'){
        $(".pair-oneclick-name").html('Sell ' + pair_name);
      }
      $("#eqt_sec_id").val(pair_id);
      $("#oneclick-modal").modal('show');
    }

    // place bid ask order
    function placeBidAskOrder() {
      var token       = $("#token").val();
      var trader_id   = $(".all-client-lists").val();
      var sec_id      = $("#eqt_sec_id").val();
      var bid_price   = $("#eqt_bid_price").val();
      var bid_volume  = $("#eqt_bid_volume").val();
      var ask_price   = $("#eqt_ask_price").val();
      var ask_volume  = $("#eqt_ask_volume").val();

      var params = {
        _token: token,
        sec_id: sec_id,
        trader_id: trader_id,
        bid_price: bid_price,
        bid_volume: bid_volume,
        ask_price: ask_price,
        ask_volume: ask_volume
      };

      // post data
      $.post('{{ url('oneclick/place/order') }}', params, function(data, textStatus, xhr) {
        if(data.status == "success"){
          iziToast.show({
            title: "Ok",
            message: data.message,
            position: 'bottomLeft'
          });
          $("#order-modal").modal('hide');
        }else{
          iziToast.show({
            title: "Ok",
            message: data.message,
            position: 'bottomLeft'
          });
        }
      });

      // void form
      return false;
    }

    // load all order
    function loadAllOrders() {
      $.get('{{ url("load/global/orders") }}', function(data) {
        // console log data
        $(".load-orders").html("")
        $.each(data, function(index, val) {
          var rowShade;
          if(val.type == "buy"){
            rowShade = ` class="order-row-buy" `;
          }else{
            rowShade = ` class="order-row-sell" `;
          }
          $(".load-orders").append(`
            <tr ${rowShade}>
              <td>${val.asset}</td>
              <td>${val.price}</td>
              <td>${val.qty}</td>
              <td>${val.date}</td>
            </tr>
          `);
        });
      });
    }

    // load brokers client
    fetchBrokersClient();

    function fetchBrokersClient() {
      $.get('{{ url('fetch/brokers/clients') }}', function(data) {
        $(".all-client-lists").html("");
        $.each(data, function(index, val) {
          $(".all-client-lists").append(`
            <option value="${val.id}">${val.client_name}</option>
          `);
        });
        $(".all-client-lists").select2();
      });
    }
  </script>
@endsection
