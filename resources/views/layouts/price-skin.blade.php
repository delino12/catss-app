
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS provide users a secure means to trade safe accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Cavidel">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon.png">
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">
        <link rel="stylesheet" type="text/css" href="/css/chat-css.css">
        <link rel="stylesheet" type="text/css" href="/css/trade-css.css">
        <link rel="stylesheet" type="text/css" href="{{asset("css/iziToast.css")}}">
        <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
        <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
        <link href="{{asset('css/select2.min.css')}}" rel="stylesheet" />
    </head>
  <body>
    <input type="hidden" id="token" value="{{ csrf_token() }}">
    <audio id="order-audio" hidden="">
      <source src="{{asset('audio/exquisite.ogg')}}" type="audio/ogg">
      <source src="{{asset('audio/exquisite.mp3')}}" type="audio/mpeg">
      Your browser does not support the audio element.
    </audio>
    <style type="text/css">
        table td, table tr, table th{
            background: transparent !important;
        }
    </style>
    <div id="ntrade-home">
        <nav class="navbar ntrade-nav navbar-fixed-top" role="navigation" style="border-radius: 0px;padding: 5px;">
            <div class="price-tickers">
                <marquee>
                    <span class="stock-index"></span>
                </marquee>
            </div>
        </nav>
        <br /><br /><br />
        @yield('contents')
    </div>

    <script src="/js/jquery.1.11.1.js"></script>
    <script src="/js/modernizr.custom.js"></script>
    <script src="/js/bootstrap.js"></script>
    <script src="/intro-assets/intro.js"></script>
    <script src="/js/custom.js"></script>
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="//use.fontawesome.com/releases/v5.0.8/js/solid.js"></script>
    <script src="//use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@8"></script>
    <script src="{{asset("js/iziToast.js")}}"></script>
    <script src="{{asset('js/select2.min.js')}}"></script>
    <script type="text/javascript">
        $.get('/load/market/index', function (data){
            $(".stock-index").html("");
            $.each(data, function (index, value){
                var op = value.close;
                var cp = value.open;
                var diff;
                if(op > cp){
                    op = ' &#8358; '+op+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> ';
                    cp = ' &#8358; '+cp+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                }else{
                    op = ' &#8358; '+op+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                    cp = ' &#8358; '+cp+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> '; 
                }

                $(".stock-index").append(`
                  <span class="index-news">
                     <span class="dino-link">`+value.pairs+` </span> `+op+`
                  </span>
                `);
            });
        });
    </script>
    @yield('scripts')
</body>
</html>