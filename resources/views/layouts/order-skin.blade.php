<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS provide users a secure means to trade safe accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Cavidel">
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">
        
        {{-- custom scripts --}}
        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="/js/jquery.1.11.1.js"></script>
        <script type="text/javascript" src="/js/custom.js"></script>

        <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
        {{-- scripts --}}
        <script type="text/javascript" src="/intro-assets/intro.js"></script>

        <!-- Start of Async Drift Code -->
        <script>
            !function() {
              var t;
              if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
              t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
              t.factory = function(e) {
                return function() {
                  var n;
                  return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                };
              }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
              }), t.load = function(t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
              });
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('xf9aiywc6shz');
        </script>
        <!-- End of Async Drift Code -->
    </head>
  <body>
    <div class="order-place">
        @yield('contents')
    </div>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript">
        $(document).ready(function(e) {
            // check if first time logging !
            var page   = 'order-trade';
            var userid = '{{ Auth::user()->id }}';

            let data = {
                page:page,
                userid:userid
            };

            $.get('/get/user/lesson', data, function(data) {
                console.log(data);
                if(data.status == 'unseen'){
                }
            });

            // body...
            // introJs().start();
        });
    </script>
</body>
</html>