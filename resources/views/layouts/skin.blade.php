<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS provide users a secure means to trade safe accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Cavidel, Cavidel.com">

        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" href="/img/apple-touch-icon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/apple-touch-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/apple-touch-icon-114x114.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/animate.css">

        <!-- Slider
        ================================================== -->
        <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
        <link href="/css/owl.theme.css" rel="stylesheet" media="screen">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">


        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        <!-- Start of Async Drift Code -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('xf9aiywc6shz');
</script>
<!-- End of Async Drift Code -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
  <body>
    <style type="text/css">
        .dino-input {
            padding: 0.7em;
            background-color: rgba(000,000,000,0.09);
            color: #FFF;
            border: 1px solid #999;
            border-radius: 3px;
            width:80%;
        }
        .dino-link {
            color: #ECB;
        }
    </style>

    <!--Simple nav bar-->
    <header class="header navbar-fixed-top">
        <nav class="navbar" role="navigation" style="background-color: #999; font-size: 12px;">
            <div class="container">
                <div class="menu-container js_nav-item">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container" >
                        <div class="top-nav" align="right">
                            <ul class="nav navbar-nav navbar-left">
                                <li><a href="/dashboard"><i class="fa fa-user"></i>  {{ Auth::user()->name }} </a></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right">
                                @if($account_info)
                                    @foreach($account_info as $info)
                                    <li><a href="javascript:void(0);">
                                        <span class="text-success">Account Bal: &#8358; {{ number_format($info->account_balance, 2) }}</span>
                                        </a>
                                    </li>
                                    @endforeach
                                @endif
                                <li><a href="/strategy"><i class="fa fa-money"></i> Trade Strategy </a></li>
                                <li><a href="/transactions"><i class="fa fa-bank"></i> Transactions </a></li>
                                <li><a href="/stocks"><i class="fa fa-area-chart"></i> Stocks </a></li>
                                <li><a href="/open/ntrade/live"><i class="fa fa-stack-exchange"></i>Market</a></li>
                                <li><a href="/setting"><i class="fa fa-cog"></i> Setting </a></li>
                                <li><a href="/user/logout"><i class="fa fa-sign-out"></i> Logout </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header> <br />
    <div id="top"></div>
	<!-- Contents Section -->
    <div class="container" style="font-size: 12px;">
        <div class="row">
           @yield('contents')
        </div>
    </div>

    <nav id="footer">
        <div class="container">
             <div class="pull-left">
                <p> {{ date('Y') }} © Cavidel Automated Trading Simulation System. All Rights Reserved.
                <a href="https://cavidel.com">Cavidel</a></p>
            </div>
            <div class="pull-right">
                <a href="#home" class="page-scroll">Back to Top <span class="fa fa-angle-up"></span></a>
            </div>
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="/js/jasny-bootstrap.min.js"></script>

    <script src="/js/owl.carousel.js"></script>
    <script src="/js/typed.js"></script>
    <script>
      $(function(){
          $("#head-title").typed({
            strings: ["Trade now Buy More, Sell More^1000", "24/7 available Stock Exchange^1000" ,"Web Trade in a secured Space^1000"],
            typeSpeed: 100,
            loop: true,
            startDelay: 100
          });
      });
    </script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="/js/main.js"></script>

</body>
</html>