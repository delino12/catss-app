<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
        <title>@yield('title')</title>
        <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <link href="/admin/assets/css/bootstrap.min.css" rel="stylesheet" />
        <link href="/admin/assets/css/material-dashboard.css?v=1.2.0" rel="stylesheet" />
        <link href="/admin/assets/css/demo.css" rel="stylesheet" />
        <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
        <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>        
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/datatables.min.css"/>
    </head>
    <body>
        <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
        <div class="wrapper">
            <div class="sidebar" data-color="blue" data-image="/img/f-bg.jpg">
                <div class="logo">
                    <a href="{{ url('admin/dashboard') }}" class="simple-text">
                        CATSS EQUITIES
                    </a>
                </div>
                <div class="sidebar-wrapper">
                    <ul class="nav">
                        <li class="active">
                            <a href="/admin/dashboard">
                                <i class="fa fa-server"></i>
                                <p>Dashboard</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/users/list">
                                <i class="fa fa-users"></i>
                                <p>All Users</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/ranking">
                                <i class="fa fa-flag"></i>
                                <p>Trade Ranking List</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/orders">
                                <i class="fa fa-hand-o-up"></i>
                                <p>Orders</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/transactions">
                                <i class="fa fa-line-chart"></i>
                                <p>Transactions</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/view-stock">
                                <i class="fa fa-copy"></i>
                                <p>Upload Equities</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/customer-care">
                                <i class="fa fa-eye"></i>
                                <p>View Activity Logs</p>
                            </a>
                        </li>
                        
                        <li>
                            <a href="/admin/news">
                                <i class="fa fa-pencil"></i>
                                <p>Post News</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/reward/">
                                <i class="fa fa-money"></i>
                                <p>Ranking Reward</p>
                            </a>
                        </li>

                        <li>
                            <a href="/admin/create">
                                <i class="fa fa-bank"></i>
                                <p>Brokerage Firm</p>
                            </a>
                        </li>

                        <li>
                            <a href="/admin/create/broker">
                                <i class="fa fa-user"></i>
                                <p>Add a Broker</p>
                            </a>
                        </li>
                        <li>
                            <a href="/admin/logout">
                                <i class="fa fa-sign-out"></i>
                                <p>Exit (Logout)</p>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="main-panel">
                <nav class="navbar navbar-transparent navbar-absolute">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle" data-toggle="collapse">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a class="navbar-brand" href="#"> Admin Control Panel </a>
                        </div>
                        <div class="collapse navbar-collapse">
                            <ul class="nav navbar-nav navbar-right">
                                
                                <li>
                                    <a href="/admin/create">
                                        <i class="material-icons">group</i>
                                        Add Broker (Agent)
                                    </a>
                                </li>
                                <li>
                                    <a href="/admin/notifications">
                                        <i class="material-icons">notifications</i>
                                    </a>
                                </li>
                            </ul>
                            
                        </div>
                    </div>
                </nav>
                <div class="content">
                    <div class="container-fluid">
                        @yield('contents')
                    </div>
                </div>
                <footer class="footer">
                    <div class="container-fluid">
                        <nav class="pull-left">
                            <ul>
                                <li>
                                    <a href="/">
                                        Home
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.cavidel.com">
                                        Company
                                    </a>
                                </li>
                                <li>
                                    <a href="http://www.cavidel.com">
                                        Portfolio
                                    </a>
                                </li>
                                <li>
                                    <a href="/forum">
                                        Blog
                                    </a>
                                </li>
                            </ul>
                        </nav>
                        <p class="copyright pull-right">
                            &copy;
                            <script>
                                document.write(new Date().getFullYear())
                            </script>
                            <a href="http://www.cavidel.com">Cavidel </a>, made with love for a better web trading service
                        </p>
                    </div>
                </footer>
            </div>
        </div>

        <!--   Core JS Files   -->
        <script src="/admin/assets/js/jquery.min.js" type="text/javascript"></script>
        <script src="//cdn.datatables.net/v/bs/jq-3.3.1/dt-1.10.18/datatables.min.js"></script>
        <script src="/js/dropzone.js"></script>
        <script src="//cdnjs.cloudflare.com/ajax/libs/dropzone/4.2.0/min/dropzone.min.js"></script>
        <script src="/admin/assets/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="/admin/assets/js/material.min.js" type="text/javascript"></script>
        <script src="/admin/assets/js/arrive.min.js"></script>
        <script src="/admin/assets/js/perfect-scrollbar.jquery.min.js"></script>
        <script src="/admin/assets/js/bootstrap-notify.js"></script>
        <script src="/admin/assets/js/material-dashboard.js?v=1.2.0"></script>
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.all.min.js"></script>
        <script src="/js/typeahead.js"></script>
        @yield('scripts')
    </body>
</html>