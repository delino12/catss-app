<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS is a secure platform for equities trading accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Cavidel">
        
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        
        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/animate.css">

        <!-- Slider
        ================================================== -->
        <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
        <link href="/css/owl.theme.css" rel="stylesheet" media="screen">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">


        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        <!-- Start of Async Drift Code -->
<script>
!function() {
  var t;
  if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
  t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
  t.factory = function(e) {
    return function() {
      var n;
      return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
    };
  }, t.methods.forEach(function(e) {
    t[e] = t.factory(e);
  }), t.load = function(t) {
    var e, n, o, i;
    e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
    o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
    n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
  });
}();
drift.SNIPPET_VERSION = '0.3.1';
drift.load('xf9aiywc6shz');
</script>
<!-- End of Async Drift Code -->

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
  <body>
    <style type="text/css">
        .dino-input {
            padding: 0.7em;
            background-color: rgba(000,000,000,0.09);
            color: #FFF;
            border: 1px solid #999;
            border-radius: 3px;
            width:80%;
        }
        .dino-link {
            color: #ECB;
        }

        .signup-div {
             color: #FFF;
            background-color: rgba(000,000,000,0.59);
            margin-top: -100px;
            padding: 1em;
            box-shadow: 1px 1px 4px 1px #666;
        }

        .login-div {
            color: #FFF;
            background-color: rgba(000,000,000,0.59);
            margin-top: -100px;
            padding: 1em;
            box-shadow: 1px 1px 4px 1px #666;
        }

        input {
            color: #FFF;
        }

        input::-webkit-input-placeholder,
        textarea::-webkit-input-placeholder {
          color: #CCC;
        }
        input:-moz-placeholder,
        textarea:-moz-placeholder {
          color: #CCC;
        }
        input::-moz-placeholder,
        textarea::-moz-placeholder {
          color: #CCC;
        }
        input:-ms-input-placeholder,
        textarea:-ms-input-placeholder {
          color: #CCC;
        }
    </style>
   
    <!--Simple nav bar-->
    <header class="header navbar-fixed-top">
        <nav class="navbar" role="navigation">
            <div class="container">
                <div class="menu-container js_nav-item">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="toggle-icon"></span>
                    </button>
                </div>

                <div class="collapse navbar-collapse nav-collapse">
                    <div class="menu-container" >
                        <div class="top-nav" align="right">
                            <ul class="nav navbar-nav navbar-right">
                                <li><a href="/" class="dino-link"><i class="fa fa-home"></i> Home </a></li>
                                <li><a href="#" class="dino-link"><i class="fa fa-money"></i> Trade Strategy </a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    </header>
	<!-- Contents Section -->
	<div id="home">
        <div class="overlay">
    	    <div class="container text-center">
    	        <div class="content">
    	           @yield('contents')
    	        </div>
    	    </div>
        </div>
	</div>


	<!-- Contact Section -->
    <div id="contact" style="background-color: rgba(000,000,000,0.90);">
        <div class="container">
            <div class="section-title text-center">
                <h2>Contact Us</h2>
                <hr>
            </div>
            <div class="space"></div>

            <div class="row">
                <div class="col-md-3">
                    <address>
                        <strong>Address</strong><br>
                        <br>
                        visit: <a href="http://cavidel.com"> cavidel.com</a> <br>
                        Block D1 Studio Apartment 2,<br>
                        1004 Housing Estate, Victoria Island Lagos.<br>

                        Phone: <br />
                        Mobile 1: +2348065252826 <br /> 
                        Mobile 2: +2348061382122

                         
                        <ul class="social">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                          </ul>
                    </address>
                </div>

                <div class="col-md-9">
                    <form autocomplete="off" method="POST" action="/send-messages">
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Your Name">
                                <input type="text" class="form-control" placeholder="Phone No.">
                            </div>
                            <div class="col-md-6">
                                <input type="text" class="form-control" placeholder="Email">
                                <input type="text" class="form-control" placeholder="Subject">
                            </div>
                        </div>
                        <textarea class="form-control" rows="4" placeholder="Message"></textarea>
                        <div class="text-right">
                            <a href="#" class="btn send-btn">Send</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <nav id="footer">
        <div class="container">
             <div class="pull-left">
                <p> {{ date('Y') }} © Cavidel Automated Trading Simulation System. All Rights Reserved. 
                <a href="https://cavidel.com">Cavidel</a></p>
            </div>
            <div class="pull-right"> 
                <a href="#home" class="page-scroll">Back to Top <span class="fa fa-angle-up"></span></a>
            </div>
        </div>
    </nav>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="/js/jasny-bootstrap.min.js"></script>

    <script src="/js/owl.carousel.js"></script>
    <script src="/js/typed.js"></script>
    <script>
      $(function(){
          $("#head-title").typed({
            strings: ["Trade now Buy More, Sell More^1000", "24/7 available Stock Exchange^1000" ,"Web Trade in a secured Space^1000"],
            typeSpeed: 100,
            loop: true,
            startDelay: 100
          });
      });
    </script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="/js/main.js"></script>

</body>
</html>