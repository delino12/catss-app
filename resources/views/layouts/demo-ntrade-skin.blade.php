<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title> @yield('title') </title>
        <meta name="description" content="CATSS provide users a secure means to trade safe accross the Financial Market">
        <meta name="keywords" content="CATSS stock exchange market.">
        <meta name="author" content="Dipo, cavidel.com">
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">

        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
        <link rel="apple-touch-icon" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon.png">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/animate.css">

        <!-- Slider
        ================================================== -->
        <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
        <link href="/css/owl.theme.css" rel="stylesheet" media="screen">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">


        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>

        <script type="text/javascript" src="/js/modernizr.custom.js"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
  <body>
    <style type="text/css">
        .dino-input {
            padding: 0.7em;
            background-color: rgba(000,000,000,0.09);
            color: #FFF;
            border: 1px solid #999;
            border-radius: 3px;
            width:80%;
        }
        .dino-link {
            color: #ECB;
        }

        .ntrade-nav {
            background-color: rgba(000,000,000,0.79);
        }

        .ntrade-nav ul li a {
            color: #FFF;
        }

        .ntrade-high {
            color:green;
        }

        .ntrade-medium {
            color:#ECB;
        }

        .ntrade-low {
            color:#F00;
        }

        .ntrade-feeds {
            padding: 0.7em;
        }

        .dino-transaction {

        }

        .stock {
            font-weight: bolder;
        }

        .stock-total {
            font-weight: bolder;
            font-size: 15px;
        }

        .dino-input-live {
          background-color: transparent;
          border:1px solid #CCE;
          width: 80px;
        }

        .dino-select-live {
          background-color: transparent;
          border:1px solid #CCE;
          width: 60px;
        }

        .dino-button-live {
          background-color: transparent;
          border:1px solid #CCE;
          width: 60px;
        }

        .ticker-up {
          color: green;
        }

        .ticker-middle {
          color: #CCE;
        }

        .ticker-down {
          color: #F00;
        }
    </style>

    <div id="ntrade-home">
        <!--Simple nav bar-->
        <header class="header navbar-fixed-top">
            <nav class="navbar ntrade-nav" role="navigation">
                <div class="container" style="font-size: 12px;">
                    <div class="menu-container js_nav-item">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>
                    </div>
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container" >
                            <div class="top-nav" align="right">
                                <ul class="nav navbar-nav navbar-left">

                                    @foreach($account_info as $info)
                                    <li><a href="javascript:void(0);">
                                        <span class="text-success">Account Bal: &#8358; {{ number_format($info->account_balance, 2) }} </span>
                                        </a>
                                    </li>
                                    @endforeach
                                </ul>
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="/strategy"><i class="fa fa-money"></i> Trade Strategy </a></li>
                                    <li><a href="/transactions"><i class="fa fa-bank"></i> Transactions </a></li>
                                    <li><a href="/stocks"><i class="fa fa-area-chart"></i> Stocks </a></li>
                                    <li><a href="/dashboard"><i class="fa fa-sign-out"></i> Exit Trade </a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </nav>
        </header>
        @if(session('trade_status'))
            <center>
                <div class="alert alert-success" style="width: 40%;">
                    <p>
                      {{ session('trade_status') }}
                      click <a href="/transactions/logs"> view transactions</a> to see details
                    </p>
                </div>
            </center>
        @endif



        <br />
        <div id="top"></div>
        <!-- Contents Section -->
        <div class="container" style="font-size: 12px;">
            <div class="row">
               @yield('contents')
            </div>
        </div>
        <div>

        </div>

        <header class="header navbar-fixed-bottom">
        <nav class="navbar" style="background-color: rgba(000,000,000,0.95); color:#FFFFFF;border-radius: 0px;" role="navigation">

            <div class="collapse navbar-collapse nav-collapse">
                <div class="menu-container" >
                    <div class="top-nav" align="left">
                        Stocks News:
                        <marquee direction="right" speed="slow">
                            <div class="slide-stock">
                                <span class="stock-index"></span>
                            </div>
                        </marquee> 
                    </div>
                </div>
            </div>
            
        </nav>
    </header>
    </div>

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="/js/jasny-bootstrap.min.js"></script>

    <script src="/js/owl.carousel.js"></script>
    <script src="/js/typed.js"></script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="/js/main.js"></script>
    <script type="text/javascript">

        $.get('/load/market/index', function (data){
                $(".stock-index").html("");
                $.each(data, function (index, value){
                    var op = value.close;
                    var cp = value.open;
                    var diff;
                    if(op > cp){
                        op = ' &#8358; '+op+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> ';
                        cp = ' &#8358; '+cp+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                    }else{
                        op = ' &#8358; '+op+'<span class="text-success"><i class="fa fa-angle-up"></i></span> ';
                        cp = ' &#8358; '+cp+'<span class="text-danger"><i class="fa fa-angle-down"></i></span> '; 
                    }

                    $(".stock-index").append(`
                      <span class="index-news">
                         <span class="dino-link">`+value.pairs+` </span> `+op+`
                      </span>
                    `);
                });
            });
        // refresh index
        // setInterval(showIndex, 1000 * 2);
    </script>

</body>
</html>