<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> @yield('title') </title>
    <meta name="description" content="CATSS trading platform">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="all,follow">
    <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">
    <!-- Bootstrap CSS-->
    <link rel="stylesheet" href="/catss-assets/css/bootstrap.min.css">
    <!-- Google fonts - Roboto -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,700">
    <!-- theme stylesheet-->
    <link rel="stylesheet" href="/catss-assets/css/style.default.css" id="theme-stylesheet">
    <!-- Custom stylesheet - for your changes-->
    <link rel="stylesheet" href="/catss-assets/css/custom.css">
    <!-- Font Awesome CDN-->
    <!-- you can replace it by local Font Awesome-->
    {{-- <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css"> --}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://js.pusher.com/4.1/pusher.min.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/solid.js"></script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.8/js/fontawesome.js"></script>

    {{-- intro js --}}
    <link rel="stylesheet" type="text/css" href="/intro-assets/introjs.css">
    <script type="text/javascript" src="/intro-assets/intro.js"></script>

    <link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
    <script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <!-- Start of Async Drift Code -->
    <script>
      !function() {
        var t;
        if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
        t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
        t.factory = function(e) {
          return function() {
            var n;
            return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
          };
        }, t.methods.forEach(function(e) {
          t[e] = t.factory(e);
        }), t.load = function(t) {
          var e, n, o, i;
          e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
          o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
          n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
        });
      }();
      drift.SNIPPET_VERSION = '0.3.1';
      drift.load('xf9aiywc6shz');
    </script>
    <!-- End of Async Drift Code -->
  </head>
  <body>
    <div id="exc-cur"></div>
    <input type="hidden" id="token" value="{{ csrf_token() }}" name="">
    <div class="page home-page">
      <!-- Main Navbar-->
      <header class="header">
        <nav class="navbar" style="background-color: #000;">
          <div class="container-fluid" >
            <div class="navbar-holder d-flex align-items-center justify-content-between">
              <!-- Navbar Header-->
              <div class="navbar-header">
                <!-- Navbar Brand --><a href="/" class="navbar-brand">
                <div class="brand-text brand-big hidden-lg-down"><span> CATSS </span></div>
                <div class="brand-text brand-small" style="color: #c4ca0e;"><strong>CATSS</strong></div></a>
                <!-- Toggle Button-->
                <a id="toggle-btn" href="#" class="menu-btn active"><span></span><span></span><span></span></a>
              </div>
              <!-- Navbar Menu -->
              <ul class="nav-menu list-unstyled d-flex flex-md-row align-items-md-center small">
                <!-- Search-->
                <li class="nav-item d-flex align-items-center"><a id="search" href="#"><i class="icon-search"></i></a></li>
                {{-- <li>
                  <a class="btn btn-link" href="javascript:void(0);" onclick="javascript:introJs().start();">
                    <i class="fa fa-sign-out"></i> Get started
                  </a>
                </li> --}}
                
                <li class="nav-item" data-step="4" data-intro="Enter trading room to start trading"> 
                  <a href="/open/trading/pad" class="nav-link logout"> <i class="fas fa-chart-line"></i> Start Trading Equities</a>
                </li>
                
                <li class="nav-item" data-step="5" data-intro="Click to show daily price list"> 
                  <a href="/daily/price/list" class="nav-link logout"> <i class="fas fa-chart-bar"></i> Daily Price List</a>
                </li>

                <li class="nav-item" data-step="7" data-intro="view all transactions details"> 
                  <a href="/transactions" class="nav-link logout"> <i class="fas fa-clone"></i> Transactions </a>
                </li>

                <li class="nav-item" data-step="8" data-intro="click to show all stock balance"> 
                  <a href="/stocks" class="nav-link logout"> <i class="fas fa-database"></i> Stock Balance </a>
                </li>

                @if(Auth::user()->account_type == "broker")
                <li class="nav-item"><a href="/user/clients" class="nav-link logout"><i class="fa fa-users"></i> My Clients</a></li>
                @endif
                    
                <li class="nav-item"><a href="/user/logout" class="nav-link logout"><i class="fa fa-sign-in-alt"></i> Logout</a></li>
              </ul>
            </div>
          </div>
        </nav>
      </header>
      <div class="page-content d-flex align-items-stretch">
        <!-- Side Navbar -->
        <nav class="side-navbar">
          <!-- Sidebar Header-->
          <div class="sidebar-header d-flex align-items-center">
            <div class="avatar">
              <div id="profileSet"></div>
            </div>
            <div class="title">
              <h1 class="h4" style="color:#FFF;">{{ ucfirst(Auth::user()->name) }} </h1>
              <p>{{ ucfirst(Auth::user()->account_type) }}</p>
            </div>
          </div>

          <!-- Sidebar Navidation Menus--><span class="heading">Main</span>
          <ul class="list-unstyled">
            <li data-step="1" data-intro="click here to navigate back to home" data-position='right'> 
              <a href="/"><i class="fas fa-home"></i> Home</a>
            </li>

            <li data-step="2" data-intro="click dashboard to view latest updates" data-position='right'> 
              <a href="/dashboard"><i class="fas fa-list-ul"></i> Dashboard</a>
            </li>

            <li data-step="3" data-intro="To Deposit/Withdraw funds">
              <a href="/account"><i class="fas fa-money-bill-alt"></i> Account</a>
            </li>

            <li data-step="13" data-intro="click to show all trading group">
              <a href="#trade_menu" aria-expanded="false" data-toggle="collapse"> 
                <i class="fas fa-chart-line"></i> Trading (one-click live) 
              </a>
              <ul id="trade_menu" class="collapse list-unstyled">
                <li data-step="4" data-intro="Enter trading room to start trading"> 
                  <a href="/open/trading/pad"> <i class="fas fa-chart-line"></i> Equities</a>
                </li>
                <li> 
                  <a href="/open/trading/bonds"> <i class="fas fa-chart-line"></i> Bonds</a>
                </li>
                <li> 
                  <a href="/open/trading/tbills"> <i class="fas fa-chart-line"></i> Tbills</a>
                </li>
              </ul>
            </li>

            <li data-step="13" data-intro="click to show all trading group">
              <a href="#trade_menu_2" aria-expanded="false" data-toggle="collapse"> 
                <i class="fas fa-chart-area"></i> Trading (bid/ask live) 
              </a>
              <ul id="trade_menu_2" class="collapse list-unstyled">
                <li> 
                  <a href="/open/trading/tbills/u2u"> <i class="fas fa-chart-area"></i> Tbills</a>
                </li>
                <li> 
                  <a href="/open/trading/fx/u2u"> <i class="fas fa-chart-area"></i> FX</a>
                </li>
              </ul>
            </li>

            <li data-step="9" data-intro="Track stocks using watch-list mode"> 
              <a href="/watch-list"> <i class="fas fa-eye"></i> Watch List </a>
            </li>
            
            <li data-step="10" data-intro="You can set a custom price alert, Get a price change notifications !"> 
                  <a href="/price-alert"> <i class="fas fa-bell"></i> Price Alert </a>
            </li>

            <li data-step="6" data-intro="Click here to see trade ranking list !"> 
              <a href="/ranking"> <i class="fas fa-trophy"></i> Ranking </a>
            </li>

            <li data-step="11" data-intro="This section show stock trade Financial Statement"> 
              <a href="/statements"> <i class="fa fa-file-alt"></i> Fin. Statement </a>
            </li>
          </ul>

          <span class="heading">Extras</span>
          
          <ul class="list-unstyled">
            <li data-step="12" data-intro="You can add personal asset to simulate real-time assets using catss Formulae">
              <a href="/users/stocks"> <i class="fas fa-cubes"></i> My Realtime Stocks </a>
            </li>
            
            <li data-step="13" data-intro="click to show all trading group">
              <a href="#dashvariants" aria-expanded="false" data-toggle="collapse"> <i class="fas fa-users">
              </i> Trading Group </a>
              <ul id="dashvariants" class="collapse list-unstyled">
                <li><a href="/startGroupTrade">Create Group</a></li>
                
                {{-- @if(!empty($groups))
                  @foreach($groups as $group)
                  <li>
                    <a href="/trade/live/{{ $group->name }}/{{ $group->id }}">
                      <i class="fas fa-bar-chart"></i> 
                      {{ $group->name }}
                    </a>
                  </li>
                  @endforeach
                @else
                  <li><a href="#">No group yet</a></li>
                @endif --}}
              </ul>
            </li>
            {{-- <li> <a href="#"><i class="fa fa-comments"></i>Membership Group</a></li> --}}
            <div class="group-lists"></div>
            <li data-step="18" data-intro="Enter forum disccussion">
              <a href="/forum"> <i class="fa fa-users"></i> Forum </a>
            </li>

            <li data-step="19" data-intro="Setup account setting">
              <a href="/setting"> <i class="fa fa-cog"></i> Setting </a>
            </li>
          </ul>
        </nav>
        <div class="content-inner">
          <!-- Page Header-->
          <header class="page-header">
            <div class="container-fluid">
              <div class="row">
              <div class="col-sm-3">
                <h2 class="no-margin-bottom" data-step="14" data-intro="to view account balance">
                  Bal &#8358; <span id="ac"></span>
                </h2>
              </div>
              <div class="col-sm-3"></div>
              <div class="col-sm-3"></div>
              <div class="col-sm-3">
                <h2 class="no-margin-bottom">
                  <span class="pull-right">
                    <span id="np" style="margin-right: 20px;"></span>
                    <span id="nl" style="margin-right: 20px;"></span>
                  </span>
                </h2>
              </div>
              </div>
              
            </div>
          </header>

          @yield('contents')

          <div style="height: 50px;"></div>
        </div>
      </div>
    </div>
    <!-- Javascript files-->
    <script src="/catss-assets/js/tether.min.js"></script>
    <script src="/catss-assets/js/bootstrap.min.js"></script>
    <script src="/catss-assets/js/jquery.cookie.js"> </script>
    <script src="/catss-assets/js/jquery.validate.min.js"></script>
    <script src="/catss-assets/js/front.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.28.11/dist/sweetalert2.all.min.js"></script>
    <script type="text/javascript">
      $.get("/accountbalance", function (data){
        // load account balance
        $("#ac").text(data.account_balance);
        // load profile image
        if(data.profile_image == null){
          $("#profileSet").html(`
            <img id="profile_img" src="/img/loading.gif" class="img-fluid rounded" alt="placeholder+image">
          `);
        }else{
          $("#profileSet").html(`
            <img id="profile_img" src="/uploads/`+data.profile_image+`" class="img-fluid rounded" alt="placeholder+image">
          `);
        }
      });

      // load trading...
      $.get('/load/trading-groups', function (data){
        // console.log(data);
        $('.group-lists').html("");
        $.each(data, function (index, value){
          $('.group-lists').append(`
            <li style="color:#FFF;"><a href="/group/trading/`+value.id+`"><i class="fa fa-fighter-jet"></i> <i class="fa fa-line-chart"></i>  `+value.group_name+` </a></li>
          `);
        });
      });
      
      // get financial statements too
      $.get('/load/financial/statements', function (data){
        // console.log(data);
        if(data.bal < 0){
          // trading profit
          $("#nl").html(`
            <p class="text-danger">Loss &#8358;  `+data.bal.toLocaleString()+`</p>
          `);
        }else{
          // realized profit & loss
          $("#np").html(`
            <p class="text-success">Profit:  &#8358;  `+data.bal.toLocaleString()+`</p>
          `);
        }
      });
    </script>
    @yield('scripts')
  </body>
</html>