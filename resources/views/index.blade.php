<!DOCTYPE html>
<html lang="en">
    <head>
        <!-- Basic Page Needs
        ================================================== -->
        <meta charset="utf-8">
        <!--[if IE]><meta http-equiv="x-ua-compatible" content="IE=9" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>CATSS Market</title>
        <meta name="description" content="CATSS provide users a secure world for all kinds of Equities trading, secure and safe accross the Financial Market, Also provide participation on real-time trading data with NSE stocks updates">
        <meta name="keywords" content="CATSS stock exchange market, Equities, T-bills, Commercial Paper, Bonds and Others Financial Assets.">
        <meta name="author" content="Cavidel">

        <!-- Facebook and Twitter integration -->
        <meta property="fb:129481477502486">
        <meta property="og:type" content="Business">
        <meta property="og:title" content="CATSS | Equtities Trading"/>
        <meta property="og:image" content="http://www.equities.catss.ng/img/catss-screen-3.png"/>
        <meta property="og:url" content="http://www.equities.catss.ng"/>
        <meta property="og:site_name" content="CATSS"/>
        <meta property="og:description" content="CATSS provide users a secure world for all kinds of Equities trading simulation, secure and safe accross the Financial Market, Also provide participation on real-time trading data with NSE stocks updates"/>
        
        <!-- Favicons
        ================================================== -->
        <link rel="shortcut icon" href="/img/favicon.png" type="image/x-icon">

        <!-- Bootstrap -->
        <link rel="stylesheet" type="text/css"  href="/css/bootstrap.css">
        <link rel="stylesheet" type="text/css" href="/font-awesome-4.2.0/css/font-awesome.css">
        <link rel="stylesheet" type="text/css" href="/css/jasny-bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="/css/animate.css">

        <!-- Slider
        ================================================== -->
        <link href="/css/owl.carousel.css" rel="stylesheet" media="screen">
        <link href="/css/owl.theme.css" rel="stylesheet" media="screen">

        <!-- Stylesheet
        ================================================== -->
        <link rel="stylesheet" type="text/css"  href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="/css/responsive.css">
        <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
        <link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/animejs/2.0.2/anime.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/vide/0.5.1/jquery.vide.js"></script>
        {{-- <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.5.6/angular.min.js"></script> --}}
        <!-- Anuglar Lib -->
        <script src="https://ajax.googleapis.com/ajax/libs/angularjs/1.6.4/angular.min.js"></script>
        <!-- Start of Async Drift Code -->
        <script>
            !function() {
              var t;
              if (t = window.driftt = window.drift = window.driftt || [], !t.init) return t.invoked ? void (window.console && console.error && console.error("Drift snippet included twice.")) : (t.invoked = !0, 
              t.methods = [ "identify", "config", "track", "reset", "debug", "show", "ping", "page", "hide", "off", "on" ], 
              t.factory = function(e) {
                return function() {
                  var n;
                  return n = Array.prototype.slice.call(arguments), n.unshift(e), t.push(n), t;
                };
              }, t.methods.forEach(function(e) {
                t[e] = t.factory(e);
              }), t.load = function(t) {
                var e, n, o, i;
                e = 3e5, i = Math.ceil(new Date() / e) * e, o = document.createElement("script"), 
                o.type = "text/javascript", o.async = !0, o.crossorigin = "anonymous", o.src = "https://js.driftt.com/include/" + i + "/" + t + ".js", 
                n = document.getElementsByTagName("script")[0], n.parentNode.insertBefore(o, n);
              });
            }();
            drift.SNIPPET_VERSION = '0.3.1';
            drift.load('xf9aiywc6shz');
        </script>
        <!-- End of Async Drift Code -->

        {{-- facebook --}}
        <script>
            window.fbAsyncInit = function() {
                FB.init({
                  appId            : '129481477502486',
                  autoLogAppEvents : true,
                  xfbml            : true,
                  version          : 'v2.12'
                });
            };

            (function(d, s, id){
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id)) {return;}
                js = d.createElement(s); js.id = id;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
    </head>

  <body>

    <!-- Off Canvas Navigation
    ================================================== -->
    <div class="navmenu navmenu-default navmenu-fixed-left offcanvas"> <!--- Off Canvas Side Menu -->
        <div class="close" data-toggle="offcanvas" data-target=".navmenu">
            <span class="fa fa-close"></span>
        </div>
        <div class="add-margin"></div>
        <ul class="nav navmenu-nav"> <!--- Menu -->
            <li><a href="#home" class="page-scroll">CATSS</a></li>
            @if(auth::user())
                <li><a href="/dashboard"><i class="fa fa-dashboard"></i> Dashboard </a></li>
                <li><a href="/transactions"><i class="fa fa-bank"></i> Transactions </a></li>
                <li><a href="/stocks"><i class="fa fa-area-chart"></i> Stocks </a></li>
                <li><a href="/setting"><i class="fa fa-cog"></i> Setting </a></li>
                <li><a href="/daily/price/list" class="page-scroll"><i class="fa fa-database"></i> Market Price</a></li>
                <li><a href="#about-us" class="page-scroll"><i class="fa fa-users"></i> About Us</a></li>
                <li><a href="#contact" class="page-scroll"><i class="fa fa-envelope-o"></i> Contact Us</a></li>
                <li><a href="/forum" class="page-scroll"><i class="fa fa-users"></i> Forum</a></li>
                <li><a href="/user/logout"><i class="fa fa-sign-out"></i> Logout </a></li>
            @else
                <li><a href="#about-us" class="page-scroll"><i class="fa fa-users"></i> About</a></li>
                <li><a href="#contact" class="page-scroll"><i class="fa fa-envelope-o"></i> Contact Us</a></li>
            @endif 
        </ul><!--- End Menu -->
    </div> <!--- End Off Canvas Side Menu -->

    <div class="activation-div"></div>

    <!-- Home Section -->
    <div id="ntrade-welcome" data-vide-bg="/bg-video/video-bg.mp4">
        <div class="ntrade-overlay">
            <div class="container text-center">
                <!-- Navigation -->
                <nav id="menu" data-toggle="offcanvas" data-target=".navmenu">
                    <span class="fa fa-bars"></span>
                </nav>

                <div class="content">
                    <div class="top-right links">
                        @if(auth::user())
                            <a href="javascript:void(0)" id="shareBtn" class="btn read-more-btn"> <i class="fa fa-facebook"></i> 
                                Share 
                            </a>
                            <a href="/start/payment/" class="btn read-more-btn"> <i class="fa fa-money"></i> 
                            Instant Deposit
                            </a>
                            <a href="/daily/price/list" class="btn read-more-btn"> <i class="fa fa-area-chart"></i> 
                            Enter Equiities Market
                            </a>
                            <a href="/dashboard" class="btn read-more-btn"> <i class="fa fa-dashboard"></i> 
                            Dashboard
                            </a>

                            <script type="text/javascript">
                                $.get('/account-status', function(data){
                                    console.log(data);
                                    if(data.status == 'error'){
                                        $(".activation-div").html(`
                                            <div class="alert alert-danger">`+data.message+`</div>    
                                        `);
                                    }
                                });
                            </script>
                        @else
                            <a href="javascript:void(0)" id="shareBtn" class="btn read-more-btn"> <i class="fa fa-facebook"></i> 
                                Share 
                            </a>

                            <a href="/daily/price/list" class="btn read-more-btn"><i class="fa fa-money"></i> Daily Price List</a>
                            <a href="/login-account" class="btn read-more-btn">Login</a>
                        @endif
                    </div>

                    <h4>We've got the special power, Buy more, Sell more</h4>
                    <hr>
                    <div class="header-text btn">
                        <h1><span id="head-title"></span></h1>
                    </div>
                </div>

                <a href="#meet-us" class="down-btn page-scroll">
                    <span class="fa fa-angle-down"></span>
                </a>
                <br /><br /><br />
            </div>
        </div>
    </div>

    {{-- trigger share button --}}
    <script>
        document.getElementById('shareBtn').onclick = function() {
          FB.ui({
            method: 'share',
            display: 'popup',
            href: 'http://equities.catss.ng',
          }, function(response){});
        }
        </script>

    <!-- Services Section -->
    <div id="services">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <div class="section-title">
                        <h2 id="meet-us">Meet Us, What we do</h2>
                        <hr>
                    </div>
                    <p>We trade all kinds of Equities</p>
                    <a href="#equities" class="down-btn page-scroll">
                        <span class="fa fa-angle-down"></span>
                    </a>
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-md-5">
                    <p class="ml14">Take the lead, top the Weekly trade ranking on CATSS</p>
                    <p>Earn More, Trade More and Trade Fast</p>
                    <p class="ml14">
                      <span class="text-wrapper">
                        <span class="letters small">Discover Your Trading<br /> Skills</span>
                        <span class="line"></span>
                        <br />
                      </span>
                    </p>
                </div>
                <div class="col-md-7">
                    <img src="/img/catss-screen-3.png" alt="placeholder+image" class="img-responsive">
                </div>
            </div>
            <br />
            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-md-6">
                    <img src="/img/catss-screen-1.jpg" alt="placeholder+image" class="img-responsive">
                </div>
                <div class="col-md-6">
                    <br />
                    <h3 class="ml5">
                      <span class="text-wrapper">
                        <span class="letters letters-left"><i class="fa fa-lightbulb-o"></i> Trade Smart</span>
                        <hr />
                        <br />
                        <span class="letters letters-right"><i class="fa fa-bolt"></i> Trade Fast</span>
                        <hr />
                        <br />
                        <span class="letters letters-right"><i class="fa fa-line-chart"></i> Trade More</span>
                      </span>
                    </h3>
                    
                </div>
            </div>

            <div class="space"></div>
            <br />
            <br />
            <br />
            <br />
            <div class="row">
                <div class="col-md-6">
                    <br />
                    <p>We trade on Equities</p>
                    <p>Signup now and get 50% Bonus for free</p>
                    <p>Login and start trading today</p>

                    <p class="ml1">
                      <span class="text-wrapper">
                        <span class="line line1"></span>
                        <span class="letters">Join us, Every Day @ CATSS Live</span>
                        <span class="line line2"></span>
                      </span>
                    </p>
                </div>
                <div class="col-md-6">
                    <img src="/img/catss-screen-2.png" alt="placeholder+image" class="img-responsive">
                </div>
            </div>
        </div>
    </div>

    <!-- Contact Section -->
    <div id="contact" style="background-color: rgba(000,000,000,0.90);">
        <div class="container">
            <div class="section-title text-center">
                <h2>Contact Us</h2>
                <hr>
            </div>
            <div class="space"></div>

            <div class="row">
                <div class="col-md-3">
                    <address>
                        <strong>Address</strong><br>
                        <br>
                        visit: <a href="http://cavidel.com"> cavidel.com</a> <br>
                        Block D1 Studio Apartment 2,<br>
                        1004 Housing Estate, Victoria Island Lagos.<br>

                        Phone: <br />
                        Mobile 1: +2348065252826 <br /> 
                        Mobile 2: +2348061382122

                         
                        <ul class="social">
                            <li><a href="#"><span class="fa fa-facebook"></span></a></li>
                            <li><a href="#"><span class="fa fa-google-plus"></span></a></li>
                            <li><a href="#"><span class="fa fa-twitter"></span></a></li>
                            <li><a href="#"><span class="fa fa-dribbble"></span></a></li>
                          </ul>
                    </address>
                </div>
                <div class="col-md-9">
                    
                    <p class="text-success"><span id="status"></span></p>
                    <form method="post" onsubmit="return sendContactMsg()" id="contact-form">
                      {{ csrf_field() }}
                        <div class="row">
                            <div class="col-md-6">
                                <input type="text" id="name" class="form-control" placeholder="Your Name" required="" style="color:#FFF;">
                                <input type="text" id="phone" pattern="[0-9]*" class="form-control" placeholder="Mobile No." required="" style="color:#FFF;">
                            </div>
                            <div class="col-md-6">
                                <input type="text" id="email" class="form-control" placeholder="Email" required="" style="color:#FFF;">
                                <input type="text" id="subject" class="form-control" placeholder="Subject" required="" style="color:#FFF;">
                            </div>
                        </div>
                        <textarea class="form-control" id="message" rows="4" placeholder="Message" required="" style="color:#FFF;"></textarea>
                        <div class="text-right">
                            <button class="btn read-more-btn col-md-12" style="color: #000;">
                                Send
                            </button>
                            <br />
                            <div class="success_msg"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <header class="header navbar-fixed-bottom">
        <nav style="background-color: rgba(000,000,000,0.95);color:#FFF;padding: 0.5em;" role="navigation">
            <div class="collapse navbar-collapse nav-collapse">
                <div class="menu-container" >
                    <div class="top-nav" align="left">
                        Stocks News:
                        <marquee direction="right" speed="slow">
                            <div class="slide-stock warning">
                                <span class="stock-index"></span>
                            </div>
                        </marquee> 
                    </div>
                </div>
            </div>
        </nav>
    </header>

    <nav id="footer">
        <div class="container">
             <div class="pull-left">
                <p> {{ date("Y") }} © Cavidel. All Rights Reserved</p>
            </div>
            <div class="pull-right"> 
                <a href="#ntrade-home" class="page-scroll">Back to Top <span class="fa fa-angle-up"></span></a>
            </div>
        </div>
    </nav>

    <!-- External Scripts -->
    <script>
        $(function() {
            var on = false;
            window.setInterval(function() {
                on = !on;
                if (on) {
                    $('.invalid').addClass('invalid-blink')
                } else {
                    $('.invalid-blink').removeClass('invalid-blink')
                }
            }, 1000);
        });
        window.setTimeout(function (){
            modal.style.display = "block";
            $("#close-modal").click(function (){
                modal.style.display = "none";
            });
        }, 1000 * 5);

        // Get the modal
        var modal = document.getElementById('myModal');
        window.onclick = function(event) {
            if (event.target == modal) {
                modal.style.display = "none";
            }
        }

        function sendContactMsg(){
            $(".send-loader").show();
            $(".send").hide();

            var token   = $("input[name=_token]").val();
            var names   = $("#name").val();
            var phone   = $("#phone").val();
            var email   = $("#email").val();
            var message = $("#message").val();
            var subject = $("#subject").val();

            var data = {
                _token:token,
                names:names,
                phone:phone,
                email:email,
                subject:subject,
                message:message
            };

            $.ajax({
                type: "post",
                url: "/send-messages",
                dataType: 'json',
                data: data,
                success: function (data){
                    // $("#status").html(data);
                    console.log(data);
                    $('.success_msg').html(`
                        <div class="alert alert-success">
                            <span class="text-success">`+data.message+`</span>
                        </div>
                    `);
                    $("#contact-form")[0].reset();
                },
                error: function (data){
                    alert("Fail to send message, try again !");
                }
            });

            return false;
        }
    </script>
    <script type="text/javascript" src="/js/scripts.js"></script>
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/modernizr.custom.js"></script>
    <script type="text/javascript" src="/js/jquery.1.11.1.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script type="text/javascript" src="/js/bootstrap.js"></script>
    <script type="text/javascript" src="/js/SmoothScroll.js"></script>
    <script type="text/javascript" src="/js/jasny-bootstrap.min.js"></script>

    <script src="/js/owl.carousel.js"></script>
    <script src="/js/typed.js"></script>
    <script>
      $(function(){
          $("#head-title").typed({
            strings: [
                "Trade now Buy More, Sell More^1000", 
                "24/7 available Stock Exchange^1000", 
                "Web Trade in a secured Space^1000",
                "Join CATSS, Discover your potentials",
                "Learn how to trade with CATSS", 
                "We Secure your Equities and Capital"
            ],
            typeSpeed: 100,
            loop: true,
            startDelay: 100
          });
      });
    </script>

    <!-- Javascripts
    ================================================== -->
    <script type="text/javascript" src="/js/main.js"></script>

  </body>
</html>