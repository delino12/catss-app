@extends('layouts.catss-skin')

@section('title')
    Dashboard
@endsection

@section('contents')
  <br />
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-4">
        <div class="activate-form"></div>
      </div>
      <div class="col-md-8">
        <div class="activation-div"></div>
        <div class="invites-box"></div>
      </div>
    </div>
  </div>

  <!-- Updates Section                                                -->
  <section class="updates no-padding-top">
    <div class="container-fluid">
      <div class="row">
        <!-- Recent Updates-->
        <div class="col-lg-4">
          <div class="recent-updates card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4" data-step="15" data-intro="get latest news updates">News Updates</h3>
            </div>
            <div class="card-body no-padding load-news-update">Loading..</div>
          </div>
        </div>
        <!-- Daily Feeds -->
        <div class="col-lg-4">
          <div class="daily-feeds card"> 
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4" data-step="16" data-intro="follow forum discussion updates">Forum Discussion </h3>
            </div>
            <div class="card-body no-padding">
              <!-- Item-->
              <div class="list-topics"></div>
            </div>
          </div>
        </div>

        <!-- Recent Activities -->
        <div class="col-lg-4">
          <div class="recent-activities card">
            <div class="card-close">
              <div class="dropdown">
                <button type="button" id="closeCard" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="dropdown-toggle"><i class="fa fa-ellipsis-v"></i></button>
                <div aria-labelledby="closeCard" class="dropdown-menu has-shadow"><a href="#" class="dropdown-item remove"> <i class="fa fa-times"></i>Close</a><a href="#" class="dropdown-item edit"> <i class="fa fa-gear"></i>Edit</a></div>
              </div>
            </div>
            <div class="card-header">
              <h3 class="h4" data-step="17" data-intro="show transactions activity logs">Recent Trading Activities</h3>
            </div>
            <div class="card-body no-padding load-user-transactions">
              Loading...
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection


@section('scripts')
  <script type="text/javascript">
    $.get('/load/forum-post', function (data){
      // console.log(data);
      $(".list-topics").html("");
      $(".top-recent").html("");
      var sn = 0;
      $.each(data, function (index, value){
        sn++;
        // console.log(value);
        if(value.avatar == null){
          var avatar = '<img src="http://www.iconninja.com/files/373/611/612/person-user-profile-male-man-avatar-account-icon.svg" alt="person" class="img-fluid rounded-square">';
        }else{
          var avatar = '<img src="/uploads/'+value.avatar+'" alt="person" class="img-fluid rounded-square">';
        }

        // console.log(data);
        $(".list-topics").append(`
          <div class="item">
            <div class="feed d-flex justify-content-between">
              <div class="feed-body d-flex justify-content-between">
                <a href="#" class="feed-profile">
                  `+avatar+`
                </a>
                <div class="content">
                  <a class="link" href="/forum/reply/post/`+value.id+`">
                  <h5> `+value.name+`</h5><span class="small">Posted a new discussion in Forum</span> 
                  <br /><br /> <span style="color:#000;">`+value.title+`</span> 
                  <div class="full-date"><small>`+value.date+`</small></div>
                  </a>
                </div>
              </div>
              <div class="date text-right"><small>`+value.date+`</small></div>
            </div>
          </div>
        `);

        $(".top-recent").append(`
          <div style="box-shadow:1px 1px 2px 1px #CCC;padding:1em;">
            `+value.title+`
          </div>
          <br />
        `);
        if(sn > 3){
          return false;
        }
      });
    });
    
    $.get('/account-status', function(data){
        // console.log(data);
        if(data.status == 'error'){
          $(".activation-div").html(`
              <div class="alert alert-danger">`+data.message+`</div> 
          `);

          $(".activate-form").html(`
            <form method="post" action="/activate-account">
              <p>Enter Confirmation code</p>
              <div class="form-group">
                {{ csrf_field() }}
                <input type="text" placeholder="00-000" class="form-control col-md-5" required="" name="code">
              </div>
              <div class="form-group">
                <button class="btn btn-warning">Verify Account</button>
              </div>
            </form>
          `);
        }
    });

    $.get('/check-invites/users', function (data){
      $('.invites-box').html('');
      $.each(data, function (index, value){
        $('.invites-box').append(`
          <div class="alert alert-info small">
           You have 1 new Group Trading Invitation from <b> `+value.host+`</b> about `+value.date+`
           <br /><br />
           <a href="/accept-invites/`+value.id+`"><button class="btn btn-success">Accept</button></a>
           <a href="/reject-invites/`+value.id+`"><button class="btn btn-danger">Reject</button></a> 
          </div> 
        `);
      });
    });

    $(document).ready(function(e) {
      // check if first time logging !
      var page   = 'dashboard';
      var userid = '{{ Auth::user()->id }}';

      let data = {
        page:page,
        userid:userid
      };

      $.get('/get/user/lesson', data, function(data) {
        // console.log(data);
        if(data.status == 'unseen'){
          
        }
      });

      // body...
      // introJs().start();
    });

    // load dashboard news
    $.get('{{ url('load/user/news') }}', function(data) {
      $(".load-news-update").html("");
      $.each(data, function(index, val) {
        $(".load-news-update").append(`
          <div class="item d-flex justify-content-between">
            <div class="info d-flex">
              <div class="icon"><i class="icon-rss-feed"></i></div>
              <div class="title">
                <h5>${val.title}</h5>
                <p>${val.body}</p>
              </div>
            </div>
            <div class="date text-right"><span class="small">
             ${val.updated_at}
            </span></div>
          </div>
        `);
      });
    });

    // load previous transactions
    $.get('{{ url('load/user/transactions') }}', function(data) {
      $(".load-user-transactions").html("");
      $.each(data, function(index, val) {
        var trade_type;
        if(val.stock_trade == "buy"){
          trade_type = "Purchase";
        }else{
          trade_type = "Sold";
        }

        $(".load-user-transactions").append(`
          <div class="item">
            <div class="row">
              <div class="col-4 date-holder text-right">
                <div class="icon"><i class="icon-clock"></i></div>
                <div class="date"> ${val.created_at}</div>
              </div>
              <div class="col-8 content">
                <h5>${val.stock_name}</h5>
                <p>
                  ${trade_type} ${val.stock_qty}
                  of  <b>${val.stock_name}</b>, at unit price of 
                  <b>${val.stock_unit} </b>
                  for <b>&#8358;${val.stock_amount}</b> <br />
                  <a href="/transactions">view</a>
                </p>
              </div>
            </div>
          </div>
        `);
      });
    });
  </script>
@endsection
