@extends('layouts.catss-skin')

@section('title')
  CATSS Transactions Statement
@endsection

@section('contents')
  <br /><br />
<div class="container-fluid">
  <div class="row">
    <div class="col-md-8">
      <div class="panel panel-info">
        <div class="panel-heading">
            
        </div>
        <div class="panel-body">
          <div class="card">
            <table class="table">
              <thead>
                <tr>
                  <td><i class="fa fa-bank"></i> Transaction Details: <span class="sec_name text-success"></span>
            <b><span class="pull-right"><i class="fa fa-lock text-success"></i> ID: {{ Auth::user()->account_id }} </span></b></td>
                </tr>
              </thead>
              <tbody class="load-details"></tbody>
            </table>
          </div>
        </div>
      </div>  
    </div>
  </div>
</div>

{{-- load details --}}
<script type="text/javascript">
  $.get('/load/transaction/{{ $id }}', function(data) {
    /*optional stuff to do after success */
    console.log(data);

    $('.load-details').html(`
      <tr>
        <td>Security</td>
        <td>`+data.stock_name+`</td>
      </tr>
      
      <tr>
        <td>Type of Transaction</td>
        <td>`+data.stock_trade+`</td>
      </tr>
      
      <tr>
        <td> Price</td>
        <td>&#8358; `+data.stock_unit+`</td>
      </tr>

      <tr>
        <td> Unit</td>
        <td><i class="fa fa-database"></i> `+data.stock_qty+`</td>
      </tr>

      <tr>
        <td> Amount</td>
        <td>&#8358; `+data.stock_amount+`</td>
      </tr>

      <tr>
        <td></td>
        <td></td>
      </tr>

      <tr>
        <td>Commission Charge at</td>
        <td> % `+data.com_percent+`</td>
      </tr>   

      <tr>
        <td>Fee (Charges) </td>
        <td>&#8358; `+data.com_amount+`</td>
      </tr>

      <tr>
        <td>Tax </td>
        <td>&#8358; 0.00</td>
      </tr>

      <tr>
        <td>Total</td>
        <td>&#8358; `+data.net_total+`</td>
      </tr>
    `);

    $('.sec_name').html(`
      `+data.stock_name+`
    `);
  });
</script>
@endsection
