@extends('layouts.catss-skin')

@section('title')
  CATSS Online Wallets
@endsection

@section('contents')
<br /><br /><br />
{{-- paystack integration --}}
<script src="https://js.paystack.co/v1/inline.js"></script>

<div class="container-fluid"> 
  <div class="row">
      <div class="col-md-3"></div>
      <div class="col-md-6">
        <h3 class="title text-center">Get &#8358; 100,000,000.00 Capital,  start trading on Equities with &#8358; 20,000.00 Instant Deposit.</h3>
        <br /><br />
        <div id="paystackEmbedContainer"></div>
        <div class="success_msg"></div>
      </div>
      <div class="col-md-3"></div>
  </div>

  {{-- wallet service --}}
</div>
<br /><br />
{{-- init paystack --}}
<script>
  PaystackPop.setup({
    // key: 'pk_test_ec6da628ba73e57a21663ec7ed569a4fc56645b8',
    key: 'pk_live_35fc2795afc2ad31cf850bdd9efeac5693ead4db',
    email: '{{ Auth::user()->email }}',
    amount: 20000 * 100,
    container: 'paystackEmbedContainer',
    callback: function(response){
      console.log(response);
      // alert('successfully subscribed. transaction ref is ' + response.reference);
      let refId = response.reference;

      // log transactions 
      logPayment(refId);
    },
  });

  // load account balance
  $.get("/accountbalance", function (data){
    // load account balance
    $(".trading-balance").append(`
      &#8358; `+data.account_balance+`
    `);
  });

  // start loggin payments
  function logPayment(refId) {
    // set data
    var token  = '{{ csrf_token() }}';
    var email  = '{{ Auth::user()->email }}';
    var refId  = refId;
    
    // data to json
    var data = {
      _token:token,
      email:email,
      refId:refId
    }

    // data to ajax
    $.ajax({
      type: 'POST',
      url: '/log-payment/details',
      dataType: 'json',
      data: data,
      success: function (data) {
        // console.log(data);
        if(data.status == 'success'){
          $('.success_msg').append(`
            <p class="text-success">`+data.message+`</p>
          `);
        }
        setTimeout(function (){
          window.location.href = '/account';
        }, 1000 * 5);
        // alert('Successful, check console data');
      },
      error: function (data) {
        // console.log(data);
        alert('Error, fail to send request !');
      }
    });
  }
</script>
@endsection
