@extends('layouts.catss-cover-skin')

@section('title')
    Join CATSS
@endsection

@section('contents')
<div class="page login-page">
  <div class="container d-flex align-items-center" >
    <div class="form-holder has-shadow">
      <div class="row">
        <!-- Logo & Information Panel-->
        <div class="col-lg-6">
          <div class="info d-flex align-items-center">
            <div class="content">
              <div class="logo">
                <h1>Signup with CATSS</h1>
              </div>
              <p>Trade Smart, Trade More</p> 
              <p> Join CATSS Now and earn up to N10,000.00 Weekly, Play Smart, Work Smart</p>
            </div>
          </div>
        </div>
        <!-- Form Panel    -->
        <div class="col-lg-6 bg-white">
          <div class="form d-flex align-items-center">
            <div class="content">
              <hr />
              @if (session('status'))
                  <p class="text-success small">{{ session('status') }}</p>
              @endif
              @if (session('login-status'))
                  <p class="text-danger small">{{ session('login-status') }}</p>
              @endif
              @if ($errors->any())
                  @foreach ($errors->all() as $error)
                      <p>{{ $error }}</p>
                  @endforeach
              @endif
              <form id="login-form" method="post" action="/create-account">
                {{ csrf_field() }}
                <div class="form-group">
                  <div class="form-group">
                    <input type="checkbox" name="account_type" class="input-checkbox">
                    <label for="name" class="label-material">Are you a stock broker agent?</label>
                  </div>

                  <select id="brokers_firm" name="brokers_firm" class="input-material form-control">
                    @if(count($brokers_list) > 0 )
                      <option value=""> -- none -- </option>
                      @foreach($brokers_list as $firm)
                        <option value="{{ $firm['firm_name'] }}"> {{ $firm['firm_name'] }} </option>
                      @endforeach
                    @else
                      <option value=""> -- none -- </option>
                    @endif
                  </select>
                  <label for="name" class="label-material">Select a brokerage firm</label>
                </div>

                <div class="form-group">
                  <input id="name" type="text" name="name" required="" class="input-material">
                  <label for="name" class="label-material">Name</label>
                </div>

                <div class="form-group">
                  <input id="login-username" type="email" name="email" required="" class="input-material">
                  <label for="login-username" class="label-material">Email</label>
                </div>

                <div class="form-group">
                  <input id="login-password" type="password" name="password" required="" class="input-material">
                  <label for="login-password" class="label-material">Password</label>
                </div>

                <div class="form-group">
                  <input type="checkbox" name="terms" required="">
                  <label class="label-material"><a href="/terms-and-conditions">Terms & Condition</a></label>
                </div>

                <button class="btn btn-primary">Create Account</button>
                <br />
                <div class="fb-login">
                 
                </div>
                <hr />
              </form>
              
              <small>I'm not new to CATSS? </small>
              <a href="/login-account" class="signup">sigin</a> <br /><br />
              <a class="signup"> <i class="fa fa-facebook"></i> 
                 signup with Facebook 
                <fb:login-button 
                  scope="public_profile,email"
                  onlogin="checkLoginState();">
                </fb:login-button></a> or 
              <a href="https://google.com" class="signup"> <i class="fa fa-google"></i> signup with Google</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="copyrights text-center">
    <p>Design by <a href="https://cavidel.com" class="external">Cavidel</a></p>
  </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
<script type="text/javascript">
  // In your Javascript (external .js resource or <script> tag)
  $(document).ready(function() {
      $('#brokers_firm').select2();
  });

  // get login status
  FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
      console.log(response);
  });


  function checkLoginState() {
    FB.getLoginStatus(function(response) {
      statusChangeCallback(response);
    });
  }

  function statusChangeCallback(response) {
    // body...
    if(response.status === 'connected'){
      // setElements(true);
      console.log(response);
      console.log('login');
    }else{
      // setElements(false);
      console.log('not logged in !');
    }
  }
</script>

@endsection