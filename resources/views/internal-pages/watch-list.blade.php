@extends('layouts.catss-skin')

@section('title')
  CATSS Watch List
@endsection

@section('contents')
<br /><br />
<div class="container-fluid">
  <div class="row">
    <div class="col-md-5 small">
      <br />
      <div class="panel panel-info card card-body">
        <div class="panel-heading">
            <i class="fa fa-eye"></i> Watch List
        </div>
        <br />
        <div class="panel-body">
          <div class="panel panel-primary">
            <div class="panel-heading">
                <i class="fa fa-database"></i> What security would you like to watch ?
            </div>
            <div class="panel-body">
              <form id="alert-form" method="POST" onsubmit="return addToWatchList()">
                
                <div class="form-group">
                  <label>Security</label>
                  <select class="form-control" id="security"></select>
                </div>

                <div class="form-group">
                  <button class="btn btn-primary"><i class="fas fa-bell"></i> Add to Watch List </button>
                </div>
              </form>
              <div class="success_msg"></div>
              <div class="error_msg"></div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-7 small" >
      <br />
      <div class="panel panel-primary card card-body">
        <div class="panel-heading">
          <i class="fa fa-database"></i> My Watch List.
        </div>
        <div class="panel-body">
          <table class="table">
            <thead>
              <tr>
                <th>S/N</th>
                <th>Asset</th>
                <th>Last traded price</th>
                <th>Total no. Trade</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody class="load-watch-list"></tbody>
          </table>
        </div>
      </div>
    </div>  
  </div>
</div>
<br /><br />
{{-- scripts --}}
<script type="text/javascript">
  // load modules
  loadWatchList();

  // add watchlist 
  function addToWatchList() {
    // body...
    var token     = '{{ csrf_token() }}';
    var userid    = '{{ Auth::user()->id }}';
    var security  = $('#security').val();
    var price     = $('#pri').val();

    // data to object
    var data = {
      _token:token,
      security:security,
      userid:userid,
      price:price
    };

    // post
    $.post('/set/watch/list', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      // console.log(data);
      if(data.status == 'success'){
        $('.load-watch-list').html('');
        loadWatchList();
        // alert(data.message);
        $('.success_msg').html(`
          <span class="text-success">`+data.message+`</span>
        `);
      }

      if(data.status == 'error'){
        // refreshDivs();
        // alert(data.message);
        $('.error_msg').html(`
          <span class="text-danger">`+data.message+`</span>
        `);
      }
    });

    // void form
    return false;
  }

  // get securities 
  $.get('/load/security/all', function(data) {
    /*optional stuff to do after success */
    // console.log(data);
    $("#security").html();
    $.each(data, function(index, val) {
      // console.log(val);
      $("#security").append(`
        <option value="`+val.security+`"> `+val.security+`  (&#8358; `+val.price+`)</option>
      `);
    });
  });

  // remove from watchlist
  function removeItem(itemid) {
    // body...
    var token = '{{ csrf_token() }}';

    var data = {
      _token:token,
      itemid:itemid
    };

    // post 
    $.post('/remove/watchlist/item', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      if(data.status == 'success'){
        loadWatchList();
      }
    });

    return false;
  }

  // refresh data 
  function loadWatchList() {
    // body...
    $.get('/load/watchlist', function(data) {
      /*optional stuff to do after success */
      // console.log(data);
      $('.load-watch-list').html("");
      var sn = 0;
      $.each(data, function(index, val) {
        sn++;
        $('.load-watch-list').append(`
          <tr>
            <td>`+sn+`</td>
            <td>`+val.security+`</td>
            <td>`+val.price+`</td>
            <td>`+val.trade+`</td>
            <td><a href="javascript:void(0);" onclick="removeItem('`+val.id+`')"><i class="fa fa-trash"></i> </a></td>
          </tr>
        `);
      });
    });
  }
</script>
@endsection
