@extends('layouts.catss-skin')

@section('title')
  CATSS Price Alert
@endsection

@section('contents')
<br />
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4">
      <div class="panel panel-primary card card-body">
        <div class="panel-heading">
            <i class="fa fa-money"></i> Price Alert 
        </div>
        <div class="panel-body">
          <div class="success_msg"></div>
          <div class="error_msg"></div>
          <form id="alert-form" method="POST" onsubmit="return setPriceAlert()">
            <div class="form-group">
              <label>Security</label>
              <select class="form-control" id="security"></select>
            </div>

            <div class="form-group">
              <input type="text" id="price" placeholder="00.00" maxlength="5" class="form-control">
            </div>

            <div class="form-group">
              <button class="btn btn-primary"><i class="fas fa-bell"></i> Set Alert</button>
            </div>
          </form>
        </div>
      </div>
    </div>
    
    <div class="col-md-5">
      {{-- price alert list --}}
      <div class="panel panel-primary card card-body">
        <div class="panel-heading">
            <i class="fa fa-money"></i> Alert List
        </div>
        <div class="panel-body">
          <table class="table small">
            <thead>
              <tr>
                {{-- <td>s/n</td> --}}
                <td>security</td>
                <td>price (&#8358;)</td>
                <td>option</td>
              </tr>
            </thead>
            <tbody class="load-price-alert"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="panel panel-primary card card-body">
        <div class="panel-heading">
            <i class="fa fa-money"></i> Queue Orders 
        </div>
        <div class="panel-body">
          <table class="table small">
            <thead>
              <tr>
                <td>S/N</td>
                <td>Security</td>
                <td>Price (&#8358;)</td>
                <td>Trade</td>
                <td>Option</td>
                <td>Time</td>
                <td>Action</td>
              </tr>
            </thead>
            <tbody class="load-orders"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>
<br /><br />

{{-- pusher js Trade updates --}}
<script src="https://js.pusher.com/4.1/pusher.min.js"></script>
{{-- scripts --}}
<script type="text/javascript">
  // load orders
  let user_id = '{{ Auth::user()->id }}';
  $.get('/load/trade-orders', function(data) {
    /*optional stuff to do after success */
    // console.log(data);
    $(".load-orders").html();
    var sn = 0;
    $.each(data, function(index, val) {
      /* iterate through array or object */
      if(val.user_id == user_id){
        sn++;
        $(".load-orders").append(`
          <tr>
            <td>`+sn+`</td>
            <td>`+val.asset+`</td>
            <td>`+val.price+`</td>
            <td>`+val.type+`</td>
            <td>`+val.option+`</td>
            <td>`+val.date+`</td>
            <td><a class="c-title" href="javascript:void()" onclick="cancelOrder('`+val.id+`')">Cancel</a></td>
          </tr>
        `);
      }
    });
  });

  $.get('/load/price-alert', function(data) {
    /*optional stuff to do after success */

    $('#load-price-alert').html();
    $.each(data, function(index, val) {
      // console.log(val);
      $('.load-price-alert').append(`
        <tr>
          <td>`+val.security+`</td>
          <td>`+val.price+`</td>
          <td><a href="javascript:void(0);" onclick="cancelAlert('`+val.id+`')">cancel</a></td>
        </tr>
      `);
    });
  });

  // cancel order
  function cancelOrder(id) {
    // data to json
    let data = {
      _token: '{{ csrf_token() }}',
      id:id
    }

    // post cancel orders
    $.post('/cancel/order', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      // console.log(data);
      if(data.status == 'success'){
        return refreshOrders();
      }
    });

    function refreshOrders(){
      // load orders
      $(".load-orders").html("");
      $.get('/load/trade-orders', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $(".load-orders").html();
        var sn = 0;
        $.each(data, function(index, val) {
          /* iterate through array or object */
          sn++;
          $(".load-orders").append(`
            <tr>
              <td>`+sn+`</td>
              <td>`+val.asset+`</td>
              <td>`+val.price+`</td>
              <td>`+val.type+`</td>
              <td>`+val.option+`</td>
              <td>`+val.date+`</td>
              <td><a class="c-title" href="javascript:void()" onclick="cancelOrder('`+val.id+`')">Cancel</a></td>
            </tr>
          `);
        });
      });
    }
  }

  // cancel alert 
  function cancelAlert(id) {
    // body...
    // data to json
    let data = {
      _token: '{{ csrf_token() }}',
      id:id
    }

    // post cancel orders
    $.post('/cancel/alert', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      // console.log(data);
      if(data.status == 'success'){
        return refreshAlerts();
      }
    });
  }

  // refresh alert div
  function refreshAlerts() {
    $.get('/load/price-alert', function(data) {
      /*optional stuff to do after success */
      $('#load-price-alert').html('');
      $.each(data, function(index, val) {
        // console.log(val);
        $('.load-price-alert').append(`
          <tr>
            <td>`+val.security+`</td>
            <td>`+val.price+`</td>
            <td><a href="javascript:void(0);" onclick="cancelAlert('`+val.id+`')">cancel</a></td>
          </tr>
        `);
      });
    });
  }

  // add watchlist 
  function setPriceAlert() {
    // body...
    let token     = '{{ csrf_token() }}';
    let security  = $('#security').val();
    let price     = $('#price').val();

    // data to object
    let data = {
      _token:token,
      security:security,
      price:price
    };

    // post
    $.post('/set/price/alert', data, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      console.log(data);
      if(data.status == 'success'){
        // alert(data.message);
        $('.success_msg').html(`
          <span class="text-success">`+data.message+`</span>
        `);
        setTimeout(function (){
          $('.success_msg').hide('fast');
        }, 3000);
      }

      if(data.status == 'error'){
        // alert(data.message);
        $('.error_msg').html(`
          <span class="text-danger">`+data.message+`</span>
        `);
        setTimeout(function (){
          $('.error_msg').hide('fast');
        }, 3000);
      }
    });

    // void form
    return false;
  }

  // get securities 
  $.get('/load/security/all', function(data) {
    /*optional stuff to do after success */
    // console.log(data);
    $("#security").html();
    $.each(data, function(index, val) {
      // console.log(val);
      $("#security").append(`
        <option value="`+val.security+`"> `+val.security+`  (&#8358; `+val.price+`)</option>
      `);
    });
  });

  // Pusher.logToConsole = true; 
  var pusher = new Pusher('0495dd188ce5c0c6c192', {
    encrypted: true
  });

  // update trade screen
  var new_price_updates = pusher.subscribe('set-price-alert');
  new_price_updates.bind('App\\Events\\PriceAlert', function(val) {
    // console.log('new price discovered !!');
    if(val.user_id == user_id){
      $('.load-price-alert').prepend(`
        <tr class="text-success">
          <td>`+val.security+`</td>
          <td>`+val.price+`</td>
          <td><a href="javascript:void()" onclick="cancelAlert('`+val.id+`')">cancel</a></td>
        </tr>
      `);
    }
  });
</script>
@endsection
