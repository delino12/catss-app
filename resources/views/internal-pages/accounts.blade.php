@extends('layouts.catss-skin')


@section('title')
  CATSS Financial Statement
@endsection

@section('contents')
  <br /><br />
<div class="container-fluid">
  <div class="row">
    <!-- Statistics -->
    <div class="statistics col-md-3" style="font-size: 14px;">
      <div class="statistic d-flex align-items-center bg-white has-shadow">
        <div class="text">
          <strong>
            <span style="color:green;">
              <span class="total_profit"></span>
            </span>      
          </strong>
          <br><small>Net (Profit/Loss)</small></div>
      </div>
    </div>
    <!-- Line Chart-->
    <div class="col-md-9 small card card-body">
      <div class="line-chart bg-white d-flex align-items-center justify-content-center has-shadow">
        <table class="table small">
          <thead>
            <tr>
              <th>Equity</th>
              <th>W/A</th>
              <th>Sell price</th>
              <th>Qty sold</th>

              <th>Gap</th>
              <th>Balance(P/L)</th>
              <th>Status</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            @if(!empty($trading_statements))
              @foreach($trading_statements as $statements)
              <tr style="color:#000;">
                <td>{{ $statements->equity }}</td>
                <td>&#8358;{{ number_format($statements->w_average, 2) }}</td>
                <td>&#8358;{{ number_format($statements->last_sold_price, 2) }}</td>
                <td>{{ number_format($statements->last_sold_qty) }}</td>

                <td>{{ number_format($statements->gap, 2) }}</td>

                @if($statements['status'] == 'profit')
                  <td style="color:#0f0;">&#8358; {{ number_format($statements->balance, 2) }}</td>
                  <td><span class="text-success"> <i class="fa fa-arrow-up"></i> {{ $statements['status'] }}</span></td>
                  <td>{{ $statements->created_at->diffForHumans() }}</td>

                @elseif($statements->status == 'loss')
                  <td style="color:#f00;">&#8358; {{ number_format($statements->balance, 2) }}</td>
                  <td><span class="text-danger"> <i class="fa fa-arrow-down"></i> {{ $statements['status'] }}</span></td>
                  <td>{{ $statements->created_at->diffForHumans() }}</td>

                @elseif($statements->status == 'break-even')
                  <td style="color:#000;">&#8358; {{ number_format($statements->balance, 2) }}</td>
                  <td> Break Even </td>
                  <td>{{ $statements->created_at->diffForHumans() }}</td>
                @endif
              </tr>
              @endforeach
              <tr bgcolor="#000">
                <td></td>
                <td>Summary(Profit/loss)</td>
                <td></td>
                <td><span class="profit"></span></td>
                <td><span class="loss"></td>
                <td><span class="total_profit"></td>
                <td></td>
                <td></td>
              </tr>
            @endif     
        </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<br />
<br />

{{-- Load financial statement revaluation --}}
<script type="text/javascript">
  $.get('/load/financial/statements', function(data) {
    /*optional stuff to do after success */
    console.log(data);
    $(".total_profit").html(`
      <strong>
        <span class="text-success"><i class="fa fa-money"></i> &#8358; `+data.net.toLocaleString()+`</span>
      </strong>
    `);
    $(".profit").html(`
      <strong>
        <span class="text-success">&#8358; `+data.profit.toLocaleString()+`</span>
        <span class="text-danger"> (&#8358;`+data.loss.toLocaleString()+`)</span>
      </strong>
    `);
    $(".loss").html(`
      
    `);

  });
</script>
@endsection
