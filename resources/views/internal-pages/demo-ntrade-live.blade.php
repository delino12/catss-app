@extends('layouts.demo-ntrade-skin')

@section('title')
    CATSS Live Feeds
@endsection

@section('contents')
  <style type="text/css">
    .flash_demo {
      background-color:rgba(255,255,255, 0.8);
      position: absolute;
      top: 80px;
      right: 10px;
      z-index: 10;
      height: 260px;
      width: 250px;
      margin-right: 20px;
      box-shadow: 1px 1px 3px 1px #FFF;
    }
  </style>
  @if(session::has('flash_msg'))
    <div class="alert alert-info flash_demo" id="flash_demo">
      <span class="pull-right"><a href="#" id="closeFlash">close</a></span>
      <br />
      <p class="text-info">

       <b>{{ session::get('flash_msg') }} Group version</b>
        <br /><br />
        <ul class="list-inline">
          <li><br />Learn the Stock Exchange Market movement. </li>
          <li><br />Click on the group link for Instruction.  </li>
          <li><br />Each Stage completion will unlock the next stage. </li>
        </ul>
      </p>
    </div>
  @endif

  <script type="text/javascript">
    var removeFlash = function (){
      $("#flash_demo").hide();
    };
    setTimeout(removeFlash, 8000);
  </script>

    <div class="row">
      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Buy)
        <p><i class="fa fa-angle-up ticker-up"></i> increase in Equity Buy Price</p>
        <p><i class="fa fa-angle-down ticker-down"></i> decrease in Equity Buy Price</p>
      </div>

      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Sell)
        <p><i class="fa fa-arrow-up ticker-up"></i> increase in Equity Sell Price</p>
        <p><i class="fa fa-arrow-down ticker-down"></i> decrease in Equity Sell Price</p>
      </div>

      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Equillibrum)
        <p><i class="fa fa-stop ticker-middle"></i> at in Equal Point</p>
        <p><i class="fa fa-stop ticker-down"></i> Danger indication</p>
      </div>

      <div class="col-md-3" style="background-color: rgba(000,000,000,0.90);">
        <br />
        Trade Ticker (Others)
        <p><i class="fa fa-stop ticker-up"></i> at Profit Point</p>
        <p><i class="fa fa-stop" style="color: teal;"></i> Stock Status</p>
      </div>
    </div>

    <br />

    <div class="row">
      <div class="col-md-12" style="background-color: rgba(000,000,000,0.90);">
        <center>
          <!-- When Buying -->
          @if(session('out_of_stock'))
            <br />
            <p class="text-warning"> {{ session('out_of_stock') }} </p>
          @endif
          @if(session('low_stock_bal'))
            <br />
            <p class="text-danger"> {{ session('low_stock_bal') }} </p>
          @endif
          @if(session('out_of_funds'))
            <br />
            <p class="text-warning"> {{ session('out_of_funds') }} </p>
          @endif
          @if(session('trade_success'))
            <br />
            <p class="text-success"> {{ session('trade_success') }} </p>
          @endif


          <!-- When Selling -->
          @if(session('no_stock'))
            <br />
            <p class="text-warning"> {{ session('no_stock') }} </p>
          @endif
          @if(session('sell_stock_low'))
            <br />
            <p class="text-danger"> {{ session('sell_stock_low') }} </p>
          @endif
           @if(session('sold_status'))
            <br />
            <p class="text-success"> {{ session('sold_status') }} </p>
          @endif
        </b>
        </center>

      	<h3><i class="fa fa-bar-chart"></i>  Stock Market</h3>
      	<span class="pull-right">
        <span class="ntrade-feeds"><i class="fa fa-user"></i> {{ Auth::user()->account_id }} </span>
        <span class="ntrade-feeds"><i class="fa fa-lock"></i> TDN: 98</span></span>
      	<table class="table">
      		<tr>
      			<th>Equity</th>
      			<th>(&#8358;) Buy</th>
      			<th>(&#8358;) Sell</th>
      			<th><i class="fa fa-clock-o"></i> Start Time</th>
      			<th><i class="fa fa-clock-o"></i> Close Time</th>
            <th>Stock Qty</th>
      			<th>Status</th>
      			<th>Traffic</th>
            <th>Request Qty</th>
            <th>Option</th>
            <th>Action</th>
      			<th>Date</th>
      		</tr>

    			@foreach($pairs as $pair)
            <form method="post" method="post" action="/request-trade/{{$pair->id}}/{{$pair->equity}}">
              {{ csrf_field() }}
              <input type="hidden" name="trade_price" value="{{ $pair->start_price }}">
              <input type="hidden" name="sell_price" value="{{ $pair->close_price }}">
    				  <tr class="dino-link">
      					<td>{{ $pair->equity }}</td>
                @if( $pair->start_price > $pair->close_price)
                  <td bgcolor="red"><i class="fa fa-angle-up ticker-up"></i> {{ number_format($pair->start_price, 2) }}</td>
                  <td><i class="fa fa-arrow-down ticker-down"></i> {{ number_format($pair->close_price, 2) }}</td>
                @elseif($pair->close_price == $pair->start_price)
                  <td>{{ number_format($pair->start_price, 2) }}</td>
                  <td>{{ number_format($pair->close_price, 2) }}</td>
                @elseif($pair->close_price > $pair->start_price)
                  <td><i class="fa fa-angle-down ticker-down"></i>{{ number_format($pair->start_price, 2) }}</td>
                  <td><i class="fa fa-arrow-up ticker-up"></i>{{ number_format($pair->close_price, 2) }}</td>
                @endif

      					<td>{{ $pair->start_time }}</td>
      					<td>{{ $pair->close_time }}</td>
                <td>{{ number_format($pair->stock_qty) }}</td>
      					<td bgcolor="teal">{{ $pair->status }}</td>
      					<td>{{ $pair->traffic }}</td>
                <td><input type="number" name="trade_qty" class="dino-input-live" placeholder="100000" required=""></td>
                <td>
                  <select name="trade_type" class="dino-select-live">
                    <option value="buy">buy</option>
                    <option value="sell">sell</option>
                  </select>
                </td>
                <td><button class="dino-button-live">Trade</button></td>
      					<td><span class="small">{{ date('D M ', $pair->timing) }}</span></td>
      				</tr>
            </form>
    			@endforeach
      	</table>
      </div>
    </div>
    <div style="height: 200px;">

    </div>

    <script type="text/javascript">
      var refreshTrade = function feedTradeData(){
        window.location.reload();
      }
      window.setInterval(refreshTrade, 1000 * 60);

    </script>
@endsection
