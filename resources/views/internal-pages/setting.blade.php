@extends('layouts.catss-skin')

@section('title')
  CATSS Account Setting
@endsection

@section('contents')
	<style type="text/css">
		.push {
			width: 200px;
			position: absolute;
			top: 140px;
			right: 20px;
			z-index: 20;
		}
	</style>

	@if(session('success'))
		<div class="alert alert-success push">
			<p class="text-success">{{ session('success') }}</p>
		</div>
	@endif

	@if(session('warning'))
		<div class="alert alert-warning push">
			<p class="text-warning">{{ session('warning') }}</p>
		</div>
	@endif

	<br />
	<div class="container-fluid">
	  	<div class="row">
	  		<div class="col-md-6">
		  		<div class="panel panel-info card card-body">
		  			<div class="panel-heading">Profile Details</div>
	  				<div class="panel-body">
	  					<hr />
				  		@if(!empty($basic_information))
							<table class="table">
					  			<tbody>
					  				<tr>
					  					<td><i class="fa fa-user"></i> Name</td>
					  					<td><b>{{ $basic_information->name }}</b></td>
					  				</tr>
					  				<tr>
					  					<td><i class="fa fa-envelope"></i> Email</td>
					  					<td><b>{{ Auth::user()->email }}</b></td>
					  				</tr>
					  				<tr>
					  					<td><i class="fa fa-phone"></i> Phone</td>
					  					<td><b>{{ $basic_information->phone }}</b></td>
					  				</tr>
					  				<tr>
					  					<td><i class="fa fa-location-arrow"></i> Address</td>
					  					<td><b>{{ $basic_information->address }}</b></td>
					  				</tr>
					  				<tr>
					  					<td><i class="fa fa-flag"></i> State</td>
					  					<td><b>{{ $basic_information->state }}</b></td>
					  				</tr>
					  				<tr>
					  					<td><i class="fa fa-pencil"></i> Zip</td>
					  					<td><b>{{ $basic_information->zip_code }}</b></td>
					  				</tr>
					  			</tbody>
					  		</table>
					  	@else
					  		<table class="table">
					  			<tbody>
					  				<tr>
					  					<td>Name</td>
					  					<td>{{ Auth::user()->name }}</td>
					  				</tr>
					  				<tr>
					  					<td>Email</td>
					  					<td>{{ Auth::user()->email }}</td>
					  				</tr>
					  				<tr>
					  					<td>Phone</td>
					  					<td>{{ $basic_information->phone }}</td>
					  				</tr>
					  				<tr>
					  					<td>Address</td>
					  					<td>{{ $basic_information->address }}</td>
					  				</tr>
					  				<tr>
					  					<td>State</td>
					  					<td>{{ $basic_information->state }}</td>
					  				</tr>
					  				<tr>
					  					<td>Zip</td>
					  					<td>{{ $basic_information->zip_code }}</td>
					  				</tr>
					  			</tbody>
					  		</table>
				  		@endif
				  	</div>
				</div>
			</div>

	  		<div class="col-md-6">
		  		<div class="panel panel-info card card-body">
		  			<div class="panel-heading">Update Basic</div>
					<div class="panel-body">
						<hr />
				  		<form action="/upload/user/profile" method="post">
				  			{{ csrf_field() }}
							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Name</label>
									</div>

									<?php
										$names = Auth::user()->name;
										$split_names = explode(" ", $names);
									?>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="first_name" placeholder="First name" value="{{ $split_names[1] }}">
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control" name="last_name" placeholder="Last name" value="{{ $split_names[0] }}">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Sex</label>
									</div>
									<div class="col-sm-3">
										<select class="form-control" name="gender">
											<option value="male">Male</option>
											<option value="female">Female</option>
										</select>
									</div>
								</div>
							</div>
							<hr />
							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Address</label>
									</div>
									<div class="col-sm-10">
										<textarea name="address" class="form-control" cols="30" rows="2" placeholder="Eg. 16a, address str.." required=""></textarea>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>State</label>
									</div>
									<div class="col-sm-6">
										<select class="form-control" name="state">
											<option value="" selected="selected">- Select -</option>
											<option value="Abuja FCT">Abuja FCT</option>
											<option value="Abia">Abia</option>
											<option value="Adamawa">Adamawa</option>
											<option value="Akwa Ibom">Akwa Ibom</option>
											<option value="Anambra">Anambra</option>
											<option value="Bauchi">Bauchi</option>
											<option value="Bayelsa">Bayelsa</option>
											<option value="Benue">Benue</option>
											<option value="Borno">Borno</option>
											<option value="Cross River">Cross River</option>
											<option value="Delta">Delta</option>
											<option value="Ebonyi">Ebonyi</option>
											<option value="Edo">Edo</option>
											<option value="Ekiti">Ekiti</option>
											<option value="Enugu">Enugu</option>
											<option value="Gombe">Gombe</option>
											<option value="Imo">Imo</option>
											<option value="Jigawa">Jigawa</option>
											<option value="Kaduna">Kaduna</option>
											<option value="Kano">Kano</option>
											<option value="Katsina">Katsina</option>
											<option value="Kebbi">Kebbi</option>
											<option value="Kogi">Kogi</option>
											<option value="Kwara">Kwara</option>
											<option value="Lagos">Lagos</option>
											<option value="Nassarawa">Nassarawa</option>
											<option value="Niger">Niger</option>
											<option value="Ogun">Ogun</option>
											<option value="Ondo">Ondo</option>
											<option value="Osun">Osun</option>
											<option value="Oyo">Oyo</option>
											<option value="Plateau">Plateau</option>
											<option value="Rivers">Rivers</option>
											<option value="Sokoto">Sokoto</option>
											<option value="Taraba">Taraba</option>
											<option value="Yobe">Yobe</option>
											<option value="Zamfara">Zamfara</option>
											<option value="Outside Nigeria">Outside Nigeria</option>
							            </select>
									</div>
									<div class="col-sm-4">
										<input type="text" pattern="[0-9]*" name="zipcode" class="form-control" placeholder="Zip Code">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">
										<label>Phone</label>
									</div>
									<div class="col-sm-6">
										<input type="text" pattern="[0-9]*" name="phone" maxlength="11" placeholder="Mobile no" class="form-control">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-2">

									</div>
									<div class="col-sm-4">
										<button class="btn btn-info">Update Profile</button>
									</div>
									<div class="col-sm-4">

									</div>
								</div>
							</div>
				  		</form>
				  	</div>
				</div>
			</div>
	  	</div>

	  	<div class="row">
	  		<div class="col-md-6">
		  		<div class="panel panel-info card card-body">
		  			<div class="panel-heading">Bank Details</div>
	  				<div class="panel-body">
	  					<table class="table">
	  						<tr>
	  							<td>Bank Name</td>
	  							<td>{{ $account_info->account_bank }}</td>
	  						</tr>
	  						<tr>
	  							<td>Account Name.</td>
	  							<td>{{ $account_info->account_name}}</td>
	  						</tr>
	  						<tr>
	  							<td>Account Number</td>
	  							<td>{{ $account_info->account_number }}</td>
	  						</tr>
	  					</table>
				  	</div>
				</div>
			</div>

	  		<div class="col-md-6">
		  		<div class="panel panel-info card card-body">
		  			<div class="panel-heading">Enter Bank Details</div>
					<div class="panel-body">
						<hr />
				  		<form onsubmit="return updateBankInformation()" method="post">
				  			{{ csrf_field() }}
							<div class="form-group">
								<div class="row">
									<div class="col-sm-5">
										<label>Enter Account Number</label>
									</div>
									<div class="col-sm-5">
										<input type="number" maxlength="11" id="account_number" class="form-control" name="account_number" placeholder="Enter Account Number">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-5">
										<label>Enter Account Name</label>
									</div>
									<div class="col-sm-5">
										<input type="text" class="form-control" id="account_name" name="account_name" placeholder="Enter Account Name">
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-5">
										<label>Select Bank</label>
									</div>
									<div class="col-sm-5">
										<select class="form-control" id="bank_name" name="bank_name">
											<option value=""></option>
										</select>
									</div>
								</div>
							</div>

							<div class="form-group">
								<div class="row">
									<div class="col-sm-5">

									</div>
									<div class="col-sm-5">
										<button class="btn btn-info">Update Account Details</button>
									</div>
								</div>
							</div>
				  		</form>
				  	</div>
				</div>
			</div>
	  	</div>

		<div class="row">
	  		<div class="col-md-6">
	  			<div class="panel panel-info card card-body">
		  			<div class="panel-heading">Change your password</div>
	  				<div class="panel-body">
	  					<hr />
				  		<form action="/change/password" method="post" onsubmit="return passCheck()">
				  			{{ csrf_field() }}
				  			<div class="form-group col-md-6">
				  				<label for="new_password"><i class="fa fa-lock"></i> New Password</label>
				  				<input type="password" name="password" id="first" class="form-control" placeholder="type new password" />
				  			</div>

				  			<div class="form-group col-md-6">
				  				<label for="confirm_new_password"><i class="fa fa-lock"></i> Confirm New Password</label>
				  				<input type="password" id="second" class="form-control" placeholder="confirm new password" />
				  			</div>

				  			<div class="form-group col-md-6">
				  				<button class="btn btn-info">Update Password</button>
				  				@if(session('password_updates'))
				  					<br />
				  					<p class="text-success">{{ session('password_updates') }}</p>
				  					<script type="text/javascript">
				  						var logoutUser = function (){
				  							window.location.href = "/user/logout";
				  						};
				  						setTimeout(logoutUser, 7000);
				  					</script>
				  				@endif
				  			</div>
				  		</form>
				  	</div>
				</div>
	  		</div>

	  		<div class="col-md-6">
	  			<div class="panel panel-info card card-body">
		  			<div class="panel-heading">Change profile image</div>
	  				<div class="panel-body">
	  					<hr />
				  		<form method="post" action="/upload/avatar" enctype="multipart/form-data">
				  			{{ csrf_field() }}
				  			<div class="form-group col-md-6">
				  				<label for="file"><i class="fa fa-lock"></i> </label>
				  				<input type="file" name="file" class="form-control" required=""/>
				  			</div>

				  			<div class="form-group col-md-6">
				  				<button class="btn btn-info">Change Profile Picture</button>
				  			</div>
				  			<div class="form-group col-md-6">
				  				@if(session('success_upload'))
				  					<p class="text-success">{{ session('success_upload') }}</p>
				  				@endif
				  			</div>
				  		</form>
				  	</div>
				</div>
		  	</div>
	  	</div>
	  </div>
	</div>
@endsection

@section('scripts')
	<script type="text/javascript">
		fetchAllBanks();
		// compare password
		function passCheck(){
			var first = $("#first").val();
			var second = $("#second").val();
			if(first !== second)
			{
				alert("password did not match");
				return false;
			}
		}

		// fetch all banks
		function fetchAllBanks() {
			$.get('{{ url("list/all/banks") }}', function(data) {
				$("#bank_name").html("");
				$.each(data.data, function(index, val) {
					$("#bank_name").append(`
						<option value="${val.name}">${val.name}</option>
					`);
				});
			});
		}

		// update bank
		function updateBankInformation() {
			var token 			= $("#token").val();
			var bank_name 		= $("#bank_name").val();
			var account_no 		= $("#account_number").val();
			var account_name 	= $("#account_name").val();

			var params = {
				_token: token,
				bank_name: bank_name,
				account_no: account_no,
				account_name: account_name
			}

			$.post('{{ url("update/client/bank") }}', params, function(data, textStatus, xhr) {
				// console log data
				console.log(data);
				if(data.status == "success"){
					window.location.reload();
				}
			});

			// return
			return false;
		}
	</script>
@endsection
