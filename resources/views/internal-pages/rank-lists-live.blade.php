@extends('layouts.catss-skin')

@section('title')
    CATSS Live Feeds
@endsection

@section('contents')
  <div class="container">
    <div class="row">
      <div class="col-md-12" style="padding: 1rem;">

        <div class="card">
          <table class="table index-pairs">
            <thead>
              <tr>
                <th>S/N</th>
                <th><i class="fa fa-users"></i> Name</th>
                <th>Profit / loss (&#8358;)</th>
                <th>Date</th>
              </tr>
            </thead>
            <tbody class="list-ranking"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection

@section('scripts')
  <script type="text/javascript">
    // load ranking 
    $.get('/load/ranking', function(data) {
        /*optional stuff to do after success */

        // sort data 
        var data = data.ranking.sort(function (a, b){
            return b.profit - a.profit; 
        });

        var sn = 0;
        $.each(data, function(index, el) {
             // console.log(el);
            sn++;
            var name   = el.user;
            var profit = el.profit.toLocaleString();
            var date   = el.date;

            // show only users with actual figures
            if(el.profit > 0){
              $(".list-ranking").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+name+`</td>
                      <td>&#8358;`+profit+`</td>
                      <td>`+date+`</td>
                  </tr>
              `);
            }else{
              $(".list-ranking").append(`
                  <tr>
                      <td>`+sn+`</td>
                      <td>`+name+`</td>
                      <td><span class="text-danger"> &#8358; `+profit+` </span></td>
                      <td>`+date+`</td>
                  </tr>
              `);
            }
        });
    });

    $("#closeFlash").click(function (){
      $("#flash_demo").hide();
    });

    $(function() {
      var on = false;
      window.setInterval(function() {
          on = !on;
          if (on) {
              $('.invalid').addClass('invalid-blink')
          } else {
              $('.invalid-blink').removeClass('invalid-blink')
          }
      }, 1000);
    });

    window.setTimeout(function (){
      modal.style.display = "block";
      $("#close-modal").click(function (){
          modal.style.display = "none";
      });
    }, 1000 * 5);

    // Get the modal
    var modal = document.getElementById('myModal');
    window.onclick = function(event) {
      if (event.target == modal) {
          modal.style.display = "none";
      }
    }

    // account balance state
    $.get("/accountbalance", function (data){
      $("#ac").text(data.account_balance);
      $(".account_balance").text(data.account_balance);
    });

    // show news notifications
    $.get('/news/notifications', function (data){
      $(".load-news").html("");
      $.each(data, function (index, value){
        $(".load-news").append(
          '\
          <h3>'+value.news_title+'</h3>\
          <p>'+value.news_body+'</p>\
          <span class="small">'+value.news_date+'</span>\
          '
        );
      });
    });
  </script>
@endsection