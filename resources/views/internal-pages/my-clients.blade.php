@extends('layouts.catss-skin')

@section('title')
  CATSS | Clients
@endsection

@section('contents')
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <br />
      <div class="card">
        <div class="card-header">
          <p>
            <i class="fa fa-users"></i> All <b>{{ ucfirst(Auth::user()->brokerage_firm) }}</b> Clients
          </p>
          <a href="javascript:void(0);" class="btn btn-primary btn-sm pull-right" onclick="addClientModal()">
            Add New Client
          </a>
        </div>
        <div class="card-body">
          <table class="table" id="list-clients" data-pagination="true">
            <thead>
              <tr>
                <td>SN</td>
                <td>Name</td>
                <td>Email</td>
                <td>Portfolio Worth</td>
                <td>Brokerage Firm</td>
                <td>Start Trading</td>
              </tr>
            </thead>
            <tbody class="load-my-clients"></tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<!-- Modal -->
<div class="modal fade" id="add-client-modal" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Add new client</h4>
      </div>
      <div class="modal-body">
        <div class="card">
          <div class="card-header btn-default">Add New Client Account</div>
          <div class="card-body" style="padding: 1em;">
            <div class="row">
              <div class="col-md-12">
                <form method="post" onsubmit="return addStockBroker()">
                  <div class="form-group">
                    <input type="hidden" class="form-control" id="broker_firm" value="{{ Auth::user()->brokerage_firm }}">
                  </div>

                  <div class="form-group">
                    <label>Client's Name</label>
                    <input type="text" class="form-control" id="broker_name" placeholder="First name, Last name" name="">
                  </div>

                  <div class="form-group">
                    <label>Client's Email</label>
                    <input type="text" class="form-control" id="broker_email" placeholder="someone@domain.com" name="">
                  </div>

                  <div class="form-group" hidden="">
                    <label>Choose a default password</label>
                    <input type="password" class="form-control" id="broker_password" placeholder="*****" 
                    value="{{ rand(000,999).rand(000,999).rand(000,999) }}">
                  </div>

                  <div class="form-group">
                    <button class="btn btn-primary" id="add-new-client">Create Client</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>


{{-- scripts --}}
<script type="text/javascript">
  // load modules
  fetchBrokersClient();

  function fetchBrokersClient() {
    $.get('{{ url('fetch/brokers/clients') }}', function(data) {
      $(".load-my-clients").html("");
      var sn = 0;
      $.each(data, function(index, val) {
        sn++;
        $(".load-my-clients").append(`
          <tr>
            <td>${sn}</td>
            <td>${val.client_name}</td>
            <td>${val.client_email}</td>
            <td>&#8358;${val.client_balance}</td>
            <td>{{ ucfirst(Auth::user()->brokerage_firm) }}</td>
            <td>
              <a href="javascript:void(0);" onclick="switchDashboard('${val.client_email}')" class="btn btn-link">
                <i class="fa fa-toggle-on"></i>
                Switch
              </a>
            </td>
          </tr>
        `);
      });
      $("#list-clients").DataTable();
    });
  }

  // initiate trade switch
  function switchDashboard(client_email) {
    var token = $("#token").val();
    var params = {
      _token: token,
      client_email: client_email
    };

    $.post('{{ url('initate/dashboard/switch') }}', params, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      if(data.status == "success"){
        window.location.href = "{{ url('dashboard') }}";
      }else{
        swal(
          "success",
          data.message,
          data.status
        );
      }
    });
  }

  function addStockBroker() {
    $("#add-new-client").html(`Creating account please wait!`);
    var token           = $("#token").val();
    var broker_firm     = '{{ Auth::user()->brokerage_id }}';
    var broker_name     = $("#broker_name").val();
    var broker_email    = $("#broker_email").val();
    var broker_password = $("#broker_password").val();

    var params = {
      broker_firm: broker_firm,
      broker_name: broker_name,
      broker_email: broker_email,
      broker_password: broker_password,
      _token: token
    };

    $.post('{{ url('user/save/client') }}', params, function(data, textStatus, xhr) {
      /*optional stuff to do after success */
      if(data.status == "success"){
        swal(
          "success",
          data.message,
          data.status
        );
        window.location.href = "/user/clients";
      }else{
        swal(
          "oops",
          data.message,
          data.status
        );
      }

      $("#add-new-client").html(`Create Client`);
    });

    // return 
    return false;
  }

  function addClientModal() {
    $("#add-client-modal").modal('show');      
  }  
</script>
@endsection
