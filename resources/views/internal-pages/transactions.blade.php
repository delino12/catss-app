@extends('layouts.catss-skin')

@section('title')
  CATSS Transactions Statement
@endsection

@section('contents')
  <br /><br />
<div class="container-fluid">
  <div class="row">
    <div class="col-md-6">
      <div class="panel panel-info card card-body">
        <div class="panel-heading">
          <i class="fa fa-bank"></i> Recent Transactions Logs
        </div>
        <div class="panel-body">
          <table class="table small" id="trans_table" width="100%">
            <thead>
              <tr>
                <th>Trade</th>
                <th>Stock</th>
                <th>Price(&#8358;)</th>
                <th>Qty</th>
                <th>Amount(&#8358;)</th>
                <th>Option</th>
              </tr>
            </thead>
            <tbody class="load-transactions"></tbody>
          </table>
        </div>
      </div>  
    </div>
    
    <div class="col-md-6">
      <div class="panel panel-info card card-body">
        <div class="panel-heading">
          <i class="fa fa-bank"></i> Orders History
        </div>
        <div class="panel-body">
          <table class="table small" width="100%" id="oders_table">
            <thead>
              <tr>
                <td>S/N</td>
                <td>Security</td>
                <td>Price (&#8358;)</td>
                <td>Trade</td>
                <td>Option</td>
                <td></td>
              </tr>
            </thead>
            <tbody class="load-orders"></tbody>
          </table>
        </div>
      </div>  
    </div>
  </div>
</div>



@endsection


@section('scripts')
  {{-- scripts --}}
  <script type="text/javascript">
    // load orders
    loadOrders();

    // cancel order
    function cancelOrder(id) {
      // data to json
      let data = {
        _token: '{{ csrf_token() }}',
        id:id
      }

      // post cancel orders
      $.post('/cancel/order', data, function(data, textStatus, xhr) {
        /*optional stuff to do after success */
        // console.log(data);
        if(data.status == 'success'){
          loadOrders();
        }
      });

      
    }
    
    function loadOrders(){
      var user_id = '{{ Auth::user()->id }}';
      $.get('/load/trade-orders', function(data) {
        /*optional stuff to do after success */
        // console.log(data);
        $(".load-orders").html("");
        var sn = 0;
        $.each(data, function(index, val) {
          /* iterate through array or object */
          if(val.user_id == user_id){
            sn++;
            $(".load-orders").append(`
              <tr>
                <td>`+sn+`</td>
                <td>`+val.asset+`</td>
                <td>`+val.price+`</td>
                <td>`+val.type+`</td>
                <td>`+val.option+`</td>
                <td><a class="c-title" href="javascript:void()" onclick="cancelOrder('`+val.id+`')">
                <i class="fa fa-trash"></i>
                </a></td>
              </tr>
            `);
          }
        });
        $("#oders_table").dataTable();
      });
    }
    // load all user transactions
    $.get('/load/all/transaction', function(data) {
      // console.log(data);
      $(".load-transactions").html("");
      $.each(data, function(index, val) {
        $(".load-transactions").append(`
          <tr>
            <td><span class="text-info">`+val.stock_trade+`</span></td>
            <td>`+val.stock_name+`</td>
            <td>&#8358;`+val.stock_unit+`</td>
            <td>`+val.stock_qty+`</td>
            <td>&#8358;`+val.stock_amount+`</td>
            <td><a href="/view/transaction/`+val.id+`">view</a></td>
          </tr>
        `);
      });
      $("#trans_table").dataTable();
    });
  </script>
@endsection
