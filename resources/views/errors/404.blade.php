@extends('layouts.catss-cover-skin')

@section('title')
    Error
@endsection

@section('contents')
<div>
  <div class="page login-page">
    <div class="container d-flex align-items-center">
      <div class="form-holder has-shadow">
        <div class="row">
          <!-- Logo & Information Panel-->
          <div class="col-lg-6">
            <div class="info d-flex align-items-center">
              <div class="content">
                <div class="logo">
                  <h1>oops - We can't find the page you are looking for !</h1>
                </div>
                <p>Check page url or try again...</p> 
                <p></p>
              </div>
            </div>
          </div>
          <!-- Form Panel    -->
          <div class="col-lg-6 bg-white">
            <div class="form d-flex align-items-center">
              <div class="content">
                <h1>404 - Page not found !</h1>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div >
      <div class="copyrights text-center" style="padding: 0.5em;background-color: rgba(000,000,000,0.90);">
          <p>Design by <a href="https://cavidel.com" class="external">Cavidel</a></p>
        </div>
    </div>
  </div>
</div>
@endsection