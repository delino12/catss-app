@extends('layouts.price-skin')

@section('title')
    CATSS NSE Price List
@endsection

@section('contents')
    <div class="row" style="margin: 3px;">
      <div class="col-md-12" style="background-color: rgba(000,000,000,0.90);">
      	<h3>
          <i class="fa fa-bar-chart"></i> Today's Equities Market 
          <span class="pull-right"> {{ Date("D M 'Y") }}</span>
        </h3>
      	
      	<table class="table dino-link" id="pairs_table">
          <thead class="small" style="background-color: #000;">
            <tr>
              <th>Security</th>
              <th>(&#8358;) Ref </th>
              <th>(&#8358;) Open </th>
              <th>(&#8358;) High </th>
              <th>(&#8358;) Low </th>
              <th>(&#8358;) Close </th>
              <th>(&#8358;) Previous Close</th>
              <th><i class="fa fa-exclamation-triangle"></i> Price</th>
              <th>Daily Volume</th>
              <th>Daily Value</th>
              <th>DVMV Trade</th>
              <th>MVTN Trade</th>
              <th><i class="fa fa-money"></i> No. of Trades</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody class="equity-row small"></tbody>
      	</table>
      </div>
    </div>
    <div style="height: 200px;"></div>
    <script type="text/javascript">
      $.get('/market/equity/live', function (data){
        // console.log(data);
        $(".equity-row").html("");
        // iterate
        $.each(data, function (index, value){
          var stats;
          if(value.previous_close > value.close_price){
            stats = `<i class="fa fa-arrow-down text-danger"></i>`;
          }

          if(value.previous_close < value.close_price){
            stats = `<i class="fa fa-arrow-up text-success"></i>`;
          }

          if(value.previous_close == value.close_price){
            stats = `<i class="fa fa-stop text-info"></i>`;
          }

          $(".equity-row").append(`
            <tr>
              <td bgcolor="#000">`+value.security+`</td>
              <td>`+value.ref_price+`</td>
              <td>`+value.open_price+`</td>
              <td>`+value.high_price+`</td>
              <td>`+value.low_price+`</td>
              <td>`+stats+` `+value.close_price+`</td>
              <td>`+value.previous_close+`</td>
              <td>`+value.change_price+`</td>
              <td>`+value.daily_volume+`</td>
              <td>`+value.daily_value+`</td>
              <td>`+value.dvmv_today+`</td>
              <td>`+value.mvtn_trade+`</td>
              <td>`+value.no_of_trade+`</td>
              <td><span>`+value.date+`</span></td>
            </tr>
          `);
        });
      });
    </script>
@endsection