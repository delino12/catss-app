<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePairsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pairs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('equity');
            $table->decimal('start_price', 13, 4);
            $table->decimal('close_price', 13, 4);
            $table->decimal('old_start_price', 13, 4)->nullable();
            $table->decimal('old_close_price', 13, 4)->nullable();
            $table->string('status');
            $table->string('traffic');
            $table->float('stock_qty');
            $table->string('start_time');
            $table->string('close_time');
            $table->string('timing');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pairs');
    }
}