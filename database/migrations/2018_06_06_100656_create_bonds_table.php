<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBondsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bonds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('ref_id');
            $table->string('security');
            $table->integer('TransactionTypeID');
            $table->decimal('price', 18, 4);
            $table->integer('qty');
            $table->decimal('yield', 18, 4);
            $table->decimal('amount', 18, 4);
            $table->date('maturity');
            $table->date('tradeDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bonds');
    }
}
