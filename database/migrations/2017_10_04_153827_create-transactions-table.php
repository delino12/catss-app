<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('stock_name');
            $table->decimal('stock_unit', 13, 4);
            $table->integer('stock_qty');
            $table->string('stock_trade');
            $table->decimal('stock_amount', 13, 4);
            $table->string('stock_timing');
            $table->float('com_percent')->nullable();
            $table->decimal('com_amount', 15, 4)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
