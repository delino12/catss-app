<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSecuritiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('securities', function (Blueprint $table) {
            $table->increments('id');
            $table->string('board');
            $table->string('security');
            $table->decimal('ref_price', 15, 4)->nullable();
            $table->decimal('open_price', 15, 4)->nullable();
            $table->decimal('high_price', 15, 4)->nullable();
            $table->decimal('low_price', 15, 4)->nullable();
            $table->decimal('close_price', 15, 4)->nullable();
            $table->decimal('change_price', 15, 4)->nullable();
            $table->decimal('daily_volume', 15, 4)->nullable();
            $table->decimal('daily_value', 15, 4)->nullable();
            $table->decimal('dvmv_today', 15, 4)->nullable();
            $table->decimal('mvtn_trade', 15, 4)->nullable();
            $table->decimal('previous_close', 15, 4)->nullable();
            $table->decimal('no_of_trade', 15, 4)->nullable();
            $table->decimal('high_band', 15, 4)->nullable();
            $table->decimal('low_band', 15, 4)->nullable();
            $table->decimal('new_band', 15, 4)->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('securities');
    }
}