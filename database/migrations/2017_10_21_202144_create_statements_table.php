<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('statements', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->string('equity')->nullable();
            $table->decimal('w_average')->nullable();
            $table->decimal('last_buy_price', 13, 4)->nullable();
            $table->decimal('last_sold_price', 13, 4)->nullable();
            $table->float('last_sold_qty')->nullable();
            $table->decimal('amount', 13, 4)->nullable();
            $table->decimal('gap', 13, 4)->nullable();
            $table->decimal('balance', 13, 4)->nullable();
            $table->string('status')->nullable();
            $table->string('comments')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('statements');
    }
}
