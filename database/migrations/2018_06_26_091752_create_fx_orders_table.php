<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFxOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fx_orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_ref');
            $table->string('currency');
            $table->integer('bid_qty');
            $table->integer('ask_qty');
            $table->decimal('bid', 18, 4);
            $table->decimal('ask', 18, 4);
            $table->date('orderDate');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fx_orders');
    }
}
